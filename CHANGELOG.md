## [1.14.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.14.0...v1.14.1) (2024-03-18)

### Bug Fixes

- **Table:** 修复样式问题 ([5abcf03](http://home.siaikin.website:20017/nbicc/common-components/commit/5abcf0392f643f1d1d1e8080d012430b4c2cec53))

# [1.14.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0...v1.14.0) (2023-12-18)

### Bug Fixes

- **NTable:** 拖动条宽度计算错误 ([9465d8c](http://home.siaikin.website:20017/nbicc/common-components/commit/9465d8cb80356ef88f05ec5744f0d3a1a65265ac))
- **NTable:** 修复单元格弹出框错位, 修复 class, style 属性重复设置 ([c1575a1](http://home.siaikin.website:20017/nbicc/common-components/commit/c1575a1aaa19553137508961872521cfba3f3911))

### Features

- **NTable:** 增加甘特图表格 ([4cea00f](http://home.siaikin.website:20017/nbicc/common-components/commit/4cea00fd5ab5a43854d3fdef45b6e130fb4973c5))

# [1.14.0-beta.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0...v1.14.0-beta.1) (2023-12-12)

### Bug Fixes

- **NTable:** 拖动条宽度计算错误 ([9465d8c](http://home.siaikin.website:20017/nbicc/common-components/commit/9465d8cb80356ef88f05ec5744f0d3a1a65265ac))
- **NTable:** 修复单元格弹出框错位, 修复 class, style 属性重复设置 ([c1575a1](http://home.siaikin.website:20017/nbicc/common-components/commit/c1575a1aaa19553137508961872521cfba3f3911))

### Features

- **NTable:** 增加甘特图表格 ([4cea00f](http://home.siaikin.website:20017/nbicc/common-components/commit/4cea00fd5ab5a43854d3fdef45b6e130fb4973c5))

# [1.14.0-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0...v1.14.0-alpha.1) (2023-12-10)

### Bug Fixes

- **NTable:** 修复单元格弹出框错位, 修复 class, style 属性重复设置 ([c1575a1](http://home.siaikin.website:20017/nbicc/common-components/commit/c1575a1aaa19553137508961872521cfba3f3911))

### Features

- **NTable:** 增加甘特图表格 ([4cea00f](http://home.siaikin.website:20017/nbicc/common-components/commit/4cea00fd5ab5a43854d3fdef45b6e130fb4973c5))

# [1.13.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.12.1...v1.13.0) (2023-11-28)

### Bug Fixes

- **NDrawer,NModal:** 关闭 Drawer/Modal 时未销毁组件实例 ([635ecfe](http://home.siaikin.website:20017/nbicc/common-components/commit/635ecfed7df7dcb6415c1f7b0b04d9ff85175fb7))
- **NDrawer,NModal:** default slot 内容不放在内部的 div 中以避免影响样式 ([624e78c](http://home.siaikin.website:20017/nbicc/common-components/commit/624e78cb7dff78d539cafd9596a1c76a37efb918))
- **NPrint:** 系统缩放时长度计算错误 ([6f51e2a](http://home.siaikin.website:20017/nbicc/common-components/commit/6f51e2ae162b18db6f4a06415db1510487ef6d6e))
- **NTable:** 编辑模式下, Tooltip 错误提示应当挂载在 Table 容器下 ([1481510](http://home.siaikin.website:20017/nbicc/common-components/commit/1481510a2a7cd8c3cbad8fb7af909d9b1f6c327b))
- **NTable:** 丢失了 childrenColumnName 属性 ([0f1ff76](http://home.siaikin.website:20017/nbicc/common-components/commit/0f1ff769334aa45be19815b181f2bb002c2c0c9d))
- **NTable:** 兼容 rowSelection 中只传递了 selectedRowKeys 的情况 ([ec7c71a](http://home.siaikin.website:20017/nbicc/common-components/commit/ec7c71ab35c1df0bc87372c731fdcbae746ab3f8))

### Features

- **NDrawer,NModal:** 增加 initialData 用于加载异步数据 ([f81e827](http://home.siaikin.website:20017/nbicc/common-components/commit/f81e8279dbfd0ddbe023e5c7a1f3be66794236d3))
- **NDrawer:** 优化 Drawer 使用方式 ([bf6ecdb](http://home.siaikin.website:20017/nbicc/common-components/commit/bf6ecdb639b9f5806fb3784a282680818fafbe26))
- **NModal:** 优化 Modal 使用方式 ([b3c41bf](http://home.siaikin.website:20017/nbicc/common-components/commit/b3c41bf80566cf75ce65eb16f4e9dc55b8cee70d))
- **NTable:** 编辑表格增加 setEditedRowField 支持以编程方式修改表格数据 ([4a8e548](http://home.siaikin.website:20017/nbicc/common-components/commit/4a8e5487be16232fe4c6ae18d58a1a74b0388df9))
- **NTable:** 编辑表格支持 blur 方式触发的校验 ([b3bbb72](http://home.siaikin.website:20017/nbicc/common-components/commit/b3bbb727381675eb92a7957cb0ece464da650c38))

# [1.13.0-alpha.6](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0-alpha.5...v1.13.0-alpha.6) (2023-11-17)

### Bug Fixes

- **NTable:** 丢失了 childrenColumnName 属性 ([9bcfdff](http://home.siaikin.website:20017/nbicc/common-components/commit/9bcfdffac53d680a582d53f27d8defeb447a5483))

# [1.13.0-alpha.5](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0-alpha.4...v1.13.0-alpha.5) (2023-11-16)

### Bug Fixes

- **NTable:** 兼容 rowSelection 中只传递了 selectedRowKeys 的情况 ([807ff70](http://home.siaikin.website:20017/nbicc/common-components/commit/807ff70e6c364c053499de7ac4b09d6f95e9ca09))

# [1.13.0-alpha.4](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0-alpha.3...v1.13.0-alpha.4) (2023-11-15)

### Bug Fixes

- **NDrawer,NModal:** 关闭 Drawer/Modal 时未销毁组件实例 ([dec61e1](http://home.siaikin.website:20017/nbicc/common-components/commit/dec61e1735a69f36913c2e7a3fe8f61bf22cd7b9))

# [1.13.0-alpha.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0-alpha.2...v1.13.0-alpha.3) (2023-11-14)

### Bug Fixes

- **NTable:** 编辑模式下, Tooltip 错误提示应当挂载在 Table 容器下 ([b4d08d5](http://home.siaikin.website:20017/nbicc/common-components/commit/b4d08d538e502e52718488e9d043c81b41209105))

### Features

- **NDrawer,NModal:** 增加 initialData 用于加载异步数据 ([0b9a82f](http://home.siaikin.website:20017/nbicc/common-components/commit/0b9a82f690f0b14c2130259a53416270dd1c2dcf))

# [1.13.0-alpha.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.13.0-alpha.1...v1.13.0-alpha.2) (2023-11-13)

### Bug Fixes

- **NPrint:** 系统缩放时长度计算错误 ([344d68e](http://home.siaikin.website:20017/nbicc/common-components/commit/344d68e23308171bd26fac9907269991a1c48948))

# [1.13.0-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.12.0...v1.13.0-alpha.1) (2023-11-13)

### Bug Fixes

- **NDrawer,NModal:** default slot 内容不放在内部的 div 中以避免影响样式 ([b7b8363](http://home.siaikin.website:20017/nbicc/common-components/commit/b7b8363f0c1f36fa73f688863a3bb90004933878))
- **NSelect:** 可在外部设置 show-search, filter-option 属性 ([25a8126](http://home.siaikin.website:20017/nbicc/common-components/commit/25a81263bd96786f1d112b327c2e7363b5ca7777))

### Features

- **NDrawer:** 优化 Drawer 使用方式 ([d9d0323](http://home.siaikin.website:20017/nbicc/common-components/commit/d9d03235b2186a416a93b0e0c02ccb4a84893def))
- **NModal:** 优化 Modal 使用方式 ([92863cf](http://home.siaikin.website:20017/nbicc/common-components/commit/92863cf66d873f03ab485115d920f887db42c89b))
- **NTable:** 编辑表格增加 setEditedRowField 支持以编程方式修改表格数据 ([bbfdf44](http://home.siaikin.website:20017/nbicc/common-components/commit/bbfdf44c8de43e81fd92b11359a28b80a14eec85))

# [1.12.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.11.3...v1.12.0) (2023-10-31)

### Features

- **NSpecProvider:** 全局配置表单输入长度限制 30 ([69ef463](http://home.siaikin.website:20017/nbicc/common-components/commit/69ef463f54c5c40eb2c5ce96b59b211b747fd6a2))

## [1.11.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.11.2...v1.11.3) (2023-10-27)

### Bug Fixes

- **NTable:** 禁用自适应高度时应用样式 ([466bc7d](http://home.siaikin.website:20017/nbicc/common-components/commit/466bc7d578aeeb3c65c1c3fbd49ed3e347a175cd))

## [1.11.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.11.1...v1.11.2) (2023-10-26)

### Bug Fixes

- **NTable:** 禁用自适应高度时应用样式 ([3922e74](http://home.siaikin.website:20017/nbicc/common-components/commit/3922e74297522f8b132cda87c954edb8e587a6de))

## [1.11.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.11.0...v1.11.1) (2023-10-23)

### Bug Fixes

- **NSpace:** size 属性支持 Number 类型 ([0e4f523](http://home.siaikin.website:20017/nbicc/common-components/commit/0e4f523c74c6787d8648a80649b3a9fe0c9c1e73))

# [1.11.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.10.1...v1.11.0) (2023-10-23)

### Features

- **NTable:** rowKey 参数类型对其 antdv, 支持传入函数 ([28061de](http://home.siaikin.website:20017/nbicc/common-components/commit/28061de31c38288ac1e2685e101612af2edaf14a))

## [1.10.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.10.0...v1.10.1) (2023-10-18)

### Bug Fixes

- **NSelect:** NAutoLoadSelect 打开下拉框时占位选项闪现 ([f7cc693](http://home.siaikin.website:20017/nbicc/common-components/commit/f7cc693c6d4ad9a42d0d1908df09b8ad03192122))
- **NSelect:** NSelect 输入过滤时可能同时存在两个下拉框 ([6af2933](http://home.siaikin.website:20017/nbicc/common-components/commit/6af29335a0924351dcb0bf66351703a73002afd0))

# [1.10.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.5...v1.10.0) (2023-10-13)

### Features

- **NPageLayout:** 支持配置 NForm 的 colon 属性 ([c2b4aee](http://home.siaikin.website:20017/nbicc/common-components/commit/c2b4aeedf4e7f537438f8f68fe7e4a15ed89c98a))
- **NSpace:** 新增 NSpace 组件, 默认 size 为 6px. ([dc9a1c1](http://home.siaikin.website:20017/nbicc/common-components/commit/dc9a1c16e12f785645fe07dc5d58a737ab55a867))

## [1.9.5](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.4...v1.9.5) (2023-10-11)

### Bug Fixes

- **NTable:** NTable 和 NEasyTable 都将识别列属性: visible ([1f57ee2](http://home.siaikin.website:20017/nbicc/common-components/commit/1f57ee23153c9c84f12277c00a23a9935930644e))

## [1.9.4](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.3...v1.9.4) (2023-10-08)

### Bug Fixes

- **NPrint:** 重复获取物理单位转换系数导致响应死循环 ([fe5bb4e](http://home.siaikin.website:20017/nbicc/common-components/commit/fe5bb4e7029df5440d350230bc8808fbcbf2b3c2))

## [1.9.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.2...v1.9.3) (2023-09-23)

### Bug Fixes

- **NForm:** 当 NEasyForm 不可见时, 使用 dom 计算行高度错误 ([c949b20](http://home.siaikin.website:20017/nbicc/common-components/commit/c949b20bbd7b546b94a3e834d9c189da5fe48214))
- **NForm:** 优化 NEasyForm 计算行数逻辑, 支持不等宽表单项 ([7b5895b](http://home.siaikin.website:20017/nbicc/common-components/commit/7b5895bea606b55e147f4e9c0f20253c4934f0dd))

## [1.9.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.1...v1.9.2) (2023-09-15)

### Bug Fixes

- **NTable:** bodyCell 被忽略 ([2677968](http://home.siaikin.website:20017/nbicc/common-components/commit/2677968fc0f6d98c8b8092a542a3aad2dbaceada))

## [1.9.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.0...v1.9.1) (2023-09-15)

### Bug Fixes

- **NTable:** 修复可伸缩列宽无效的问题 ([6ad4762](http://home.siaikin.website:20017/nbicc/common-components/commit/6ad47625b3c2554af2fa464b832f772d36bbcad0))

## [1.9.1-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.9.0...v1.9.1-alpha.1) (2023-09-15)

### Bug Fixes

- **NTable:** 修复可伸缩列宽无效的问题 ([6ad4762](http://home.siaikin.website:20017/nbicc/common-components/commit/6ad47625b3c2554af2fa464b832f772d36bbcad0))

# [1.9.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.2...v1.9.0) (2023-09-13)

### Features

- **NTable:** NTable 可使用 Tooltip 来显示被省略的内容 ([8b6b267](http://home.siaikin.website:20017/nbicc/common-components/commit/8b6b267ac756f50dbc26d190f9cec8dda6c2786b))

## [1.8.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.1...v1.8.2) (2023-09-11)

### Bug Fixes

- **NPrint:** 打印内容变化后重新计算高度和缩放 ([3c05a61](http://home.siaikin.website:20017/nbicc/common-components/commit/3c05a61cd49f7ccdd7fbd668c3082334b568019d))
- **NPrint:** 容器宽高变化后重新计算高度和缩放 ([92d763d](http://home.siaikin.website:20017/nbicc/common-components/commit/92d763d4d04160c9a1e26d00f5ce1233b48f9708))

## [1.8.2-beta.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.2-beta.1...v1.8.2-beta.2) (2023-09-08)

### Bug Fixes

- **NPrint:** 容器宽高变化后重新计算高度和缩放 ([92d763d](http://home.siaikin.website:20017/nbicc/common-components/commit/92d763d4d04160c9a1e26d00f5ce1233b48f9708))

## [1.8.2-beta.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.1...v1.8.2-beta.1) (2023-09-08)

### Bug Fixes

- **NPrint:** 打印内容变化后重新计算高度和缩放 ([3c05a61](http://home.siaikin.website:20017/nbicc/common-components/commit/3c05a61cd49f7ccdd7fbd668c3082334b568019d))

## [1.8.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.0...v1.8.1) (2023-09-06)

### Bug Fixes

- **release:** netlify 构建不再忽略最新的 changelog ([05a1d81](http://home.siaikin.website:20017/nbicc/common-components/commit/05a1d8132d8d1d1ca630c36ac5a8ff992659ab75))

## [1.8.1-alpha.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.1-alpha.1...v1.8.1-alpha.2) (2023-09-06)

### Bug Fixes

- **release:** @semantic-release/git assets 默认值测试 ([5ca3d45](http://home.siaikin.website:20017/nbicc/common-components/commit/5ca3d45e5244eb4eb70b53b5b66b09210a3a28a6))

## [1.8.1-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.0...v1.8.1-alpha.1) (2023-09-06)

### Bug Fixes

- netlify 构建不再忽略最新的 changelog ([6222170](http://home.siaikin.website:20017/nbicc/common-components/commit/6222170395c74cde386f6333358552485e4533b6))
- **release:** @semantic-release/git assets 不支持 boolean ([d82233e](http://home.siaikin.website:20017/nbicc/common-components/commit/d82233eea3b293b183b7f9a843bdbac5d2a0c7ab))

## [1.8.1-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.8.0...v1.8.1-alpha.1) (2023-09-06)

### Bug Fixes

- netlify 构建不再忽略最新的 changelog ([6222170](http://home.siaikin.website:20017/nbicc/common-components/commit/6222170395c74cde386f6333358552485e4533b6))

# [1.8.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.7.0...v1.8.0) (2023-09-01)

### Bug Fixes

- **NForm:** 使用 emit 触发事件 ([a365f35](http://home.siaikin.website:20017/nbicc/common-components/commit/a365f35c28844ff231cbfe19a530a3e7b6ff9f6f))
- **NToolbar:** 当 primary 区域宽度不足时不会自动增长 ([2f77f15](http://home.siaikin.website:20017/nbicc/common-components/commit/2f77f15739f86887adce8d3fa30d1fbb2ea4e4c4))

### Features

- **NForm:** 暴露内部的 formData 代理对象 ([a078024](http://home.siaikin.website:20017/nbicc/common-components/commit/a0780246669490ac40cf0cbf1b918ac6b53756a6))
- **NForm:** 通过函数调用暴露内部的 formData 代理对象 ([a498b28](http://home.siaikin.website:20017/nbicc/common-components/commit/a498b283be34f5b89dbd06810a8a4f24b66de26d))
- **NForm:** 增加 syncFormData 函数用于主动同步表单数据 ([53ae53d](http://home.siaikin.website:20017/nbicc/common-components/commit/53ae53dcd98b887d3e586af23784aaec3c8537fa))
- **NForm:** 增加内置校验器 ([5eb0b68](http://home.siaikin.website:20017/nbicc/common-components/commit/5eb0b68da380f482cdb1da574b2ca621fe50e06f))
- **NInputNumber:** 增加 precisionRange 提供动态精度功能 ([9209281](http://home.siaikin.website:20017/nbicc/common-components/commit/920928142a3253be1cad421d5a329e4193f1cfd5))
- **NSpecProvider:** 合并外部和默认的 form 配置 ([42cbf17](http://home.siaikin.website:20017/nbicc/common-components/commit/42cbf17557909f453ae68397402e93aa3cbbfa93))
- **NSpecProvider:** 支持配置 NForm 的 modifiers 属性 ([4230287](http://home.siaikin.website:20017/nbicc/common-components/commit/4230287bd17a1f998fb670b20f383c9fed2da709))

# [1.7.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.2...v1.7.0) (2023-08-21)

### Bug Fixes

- **NForm:** NEasyForm 导出 search, reset 以及默认存在的方法 ([4a4e36e](http://home.siaikin.website:20017/nbicc/common-components/commit/4a4e36e0cb07db77dc85210aec2c71eaaf6db625))
- **NPrint:** 打印时强制设置打印页面 html/body 标签上的样式, 以避免意外的样式导致打印渲染异常 ([11efc47](http://home.siaikin.website:20017/nbicc/common-components/commit/11efc47fe38b1191bec9257ddfee0d76815e417d))
- **NPrint:** 更新 vue-to-print 以修复样式丢失的问题 ([2a3b5be](http://home.siaikin.website:20017/nbicc/common-components/commit/2a3b5be70fa6977ce014fba32ff6b7c1aca9165f))
- **NPrint:** 兼容 firefox , 使用 block 布局提到 flex 布局 ([5b6e650](http://home.siaikin.website:20017/nbicc/common-components/commit/5b6e6500f6adbfd1fa7f0102607fb399a9cd7e2e))
- **NPrint:** 兼容错误的 margin/padding 长度类型 ([e422804](http://home.siaikin.website:20017/nbicc/common-components/commit/e4228045b92bba7d1d7837d27bfa71ccc0907a38))
- **NPrint:** CLIP 模式下不需要 LODOP 自动检测并使用 ADD_PRINT_TABLE ([f2ae1c8](http://home.siaikin.website:20017/nbicc/common-components/commit/f2ae1c886d59bb0782db7331d88080d5ac3ceaba))
- **NTable:** 优化可编辑行的渲染效果, 不可见时隐藏弹出框 ([2fadbe8](http://home.siaikin.website:20017/nbicc/common-components/commit/2fadbe80416856391cf25fe0448aa43afa5a7612))

### Features

- **NForm:** 表单项布局参数可通过规范配置, 增加响应式表单示例页面 ([0a5fc51](http://home.siaikin.website:20017/nbicc/common-components/commit/0a5fc51aa4e48c85b7b18df8d087dce9f5918465))
- **NForm:** 增加 setField 方法用于编程式设置表单项的值 ([d18fe11](http://home.siaikin.website:20017/nbicc/common-components/commit/d18fe11053733204b6658587b62b6bac547975fa))
- **NPrint:** 增加 close 事件 ([1c4bdc0](http://home.siaikin.website:20017/nbicc/common-components/commit/1c4bdc0446ebd104b09000536ab8abf556f73a04))
- **NPrint:** 增加 LODOP 特定打印模式 TABLE, 以实现表格表头/表尾重复功能 ([b15dad7](http://home.siaikin.website:20017/nbicc/common-components/commit/b15dad74c37d9678afb566af49dc6058d7031eb3))

# [1.7.0-beta.4](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.7.0-beta.3...v1.7.0-beta.4) (2023-08-17)

### Bug Fixes

- **NPrint:** 更新 vue-to-print 以修复样式丢失的问题 ([2a3b5be](http://home.siaikin.website:20017/nbicc/common-components/commit/2a3b5be70fa6977ce014fba32ff6b7c1aca9165f))

# [1.7.0-beta.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.7.0-beta.2...v1.7.0-beta.3) (2023-08-17)

### Bug Fixes

- **NPrint:** 打印时强制设置打印页面 html/body 标签上的样式, 以避免意外的样式导致打印渲染异常 ([11efc47](http://home.siaikin.website:20017/nbicc/common-components/commit/11efc47fe38b1191bec9257ddfee0d76815e417d))
- **NPrint:** 兼容 firefox , 使用 block 布局提到 flex 布局 ([5b6e650](http://home.siaikin.website:20017/nbicc/common-components/commit/5b6e6500f6adbfd1fa7f0102607fb399a9cd7e2e))
- **NPrint:** 兼容错误的 margin/padding 长度类型 ([e422804](http://home.siaikin.website:20017/nbicc/common-components/commit/e4228045b92bba7d1d7837d27bfa71ccc0907a38))
- **NPrint:** CLIP 模式下不需要 LODOP 自动检测并使用 ADD_PRINT_TABLE ([f2ae1c8](http://home.siaikin.website:20017/nbicc/common-components/commit/f2ae1c886d59bb0782db7331d88080d5ac3ceaba))

### Features

- **NForm:** 表单项布局参数可通过规范配置, 增加响应式表单示例页面 ([0a5fc51](http://home.siaikin.website:20017/nbicc/common-components/commit/0a5fc51aa4e48c85b7b18df8d087dce9f5918465))
- **NPrint:** 增加 LODOP 特定打印模式 TABLE, 以实现表格表头/表尾重复功能 ([b15dad7](http://home.siaikin.website:20017/nbicc/common-components/commit/b15dad74c37d9678afb566af49dc6058d7031eb3))

# [1.7.0-beta.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.7.0-beta.1...v1.7.0-beta.2) (2023-08-11)

### Features

- **NPrint:** 增加 close 事件 ([1c4bdc0](http://home.siaikin.website:20017/nbicc/common-components/commit/1c4bdc0446ebd104b09000536ab8abf556f73a04))

# [1.7.0-beta.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.3-beta.2...v1.7.0-beta.1) (2023-08-10)

### Features

- **NForm:** 增加 setField 方法用于编程式设置表单项的值 ([d18fe11](http://home.siaikin.website:20017/nbicc/common-components/commit/d18fe11053733204b6658587b62b6bac547975fa))

## [1.6.3-beta.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.3-beta.1...v1.6.3-beta.2) (2023-08-10)

### Bug Fixes

- **NForm:** NEasyForm 导出 search, reset 以及默认存在的方法 ([4a4e36e](http://home.siaikin.website:20017/nbicc/common-components/commit/4a4e36e0cb07db77dc85210aec2c71eaaf6db625))

## [1.6.3-beta.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.2...v1.6.3-beta.1) (2023-08-10)

### Bug Fixes

- **NTable:** 优化可编辑行的渲染效果, 不可见时隐藏弹出框 ([2fadbe8](http://home.siaikin.website:20017/nbicc/common-components/commit/2fadbe80416856391cf25fe0448aa43afa5a7612))

## [1.6.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.1...v1.6.2) (2023-08-10)

### Bug Fixes

- **NTable:** 通过 api 控制编辑行时, 自动取消当前编辑行(如果有的话) ([a26db49](http://home.siaikin.website:20017/nbicc/common-components/commit/a26db497d71aca80b92f87ecade8a4c801dc5f41))

## [1.6.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.6.0...v1.6.1) (2023-08-03)

### Bug Fixes

- **NForm:** 增加表单项标签的前缀/后缀 ([43b482e](http://home.siaikin.website:20017/nbicc/common-components/commit/43b482e1c6b4a36f73fc475f12ac5a787120d6ad))

# [1.6.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.3...v1.6.0) (2023-08-03)

### Bug Fixes

- **FormHelper:** 深度合并导致 FormItemControl 的默认参数被修改 ([a757807](http://home.siaikin.website:20017/nbicc/common-components/commit/a757807877eabbe5a14ba8bd0b65a96d438fe99d))

### Features

- **NTable:** 编辑状态下的 textarea 组件将会渲染在弹出窗中 ([19191af](http://home.siaikin.website:20017/nbicc/common-components/commit/19191afd308ac3a336c4428af905e11ca4039100))

## [1.5.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.2...v1.5.3) (2023-08-03)

### Bug Fixes

- **NTable:** 优化选择行时的忽略逻辑. ([c307988](http://home.siaikin.website:20017/nbicc/common-components/commit/c30798854aabb2e8ccce47cb3e57169675900f99))

## [1.5.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.1...v1.5.2) (2023-08-02)

### Bug Fixes

- **NForm:** valueMapTo 修改外部数据导致死循环. ([35a9355](http://home.siaikin.website:20017/nbicc/common-components/commit/35a935531e4e98d5ec44088c45e98dfc420bba2d))
- **NTable:** 拖拽时最后一列自适应宽度 ([c323bde](http://home.siaikin.website:20017/nbicc/common-components/commit/c323bde5544fd8e9643bb71aa04cc11e7e6b2e08))

## [1.5.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0...v1.5.1) (2023-07-28)

### Bug Fixes

- **NForm:** valueMapTo 存在重复的无效调用. ([bc3a632](http://home.siaikin.website:20017/nbicc/common-components/commit/bc3a6321ac0b02b30adefa89f80615ebdf6dd759))
- **NTable:** TableHelper 加载错误时未重置 loading 属性. ([ae00054](http://home.siaikin.website:20017/nbicc/common-components/commit/ae00054ef41a26fed8efe54d4209c6dee864232b))

# [1.5.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.4.2...v1.5.0) (2023-07-26)

### Bug Fixes

- 导出类型 ([4ae7353](http://home.siaikin.website:20017/nbicc/common-components/commit/4ae7353d859feeba4cd90c906876c53146f68061))
- 更新 vue-to-print 解决使用了错误的文件格式的问题. ([e0ae639](http://home.siaikin.website:20017/nbicc/common-components/commit/e0ae6395490351a6cfb5180d74ebb1130120ee33))
- 兼容 Volar 类型提示 ([869df4b](http://home.siaikin.website:20017/nbicc/common-components/commit/869df4b11ce8e78dd4bd417767887d89ba6f9cd4))
- **NPrint:** 使用 LODOP 时增加 overflow, white-space css 属性. ([293f066](http://home.siaikin.website:20017/nbicc/common-components/commit/293f066ca9682c834ba4a5aae72f8750b1a3a796))
- **NPrint:** 修复 LODOP 初始化时传入空字符串导致 LODOP 报错的问题. ([be3a632](http://home.siaikin.website:20017/nbicc/common-components/commit/be3a6326eb4b2b0d57fecd2143cbc402594868b6))
- **NPrint:** 修复包裹元素的宽高会被缩放影响导致不能完整显示内容的问题. ([220941a](http://home.siaikin.website:20017/nbicc/common-components/commit/220941a8c27f27607f36b3b812ee99e547be147a))
- NToolbar 命名错误 ([4e6f8cd](http://home.siaikin.website:20017/nbicc/common-components/commit/4e6f8cdb1e22de6b69962a5c1cfdab8f73deeaeb))
- Revert "fix: 兼容 Volar 类型提示" ([74c9fcb](http://home.siaikin.website:20017/nbicc/common-components/commit/74c9fcb55cbfea7903ff7b534aaa0736e2fd0fa8))

### Features

- 使用 vue-tsc 生成 d.ts 以兼容 Volar. ([606e9a4](http://home.siaikin.website:20017/nbicc/common-components/commit/606e9a4e311fbff9ebaba362127c1ec039fde8aa))
- **NPrint:** 打印库切换到 vue-to-print 以支持其他浏览器. ([e7ccd99](http://home.siaikin.website:20017/nbicc/common-components/commit/e7ccd99d44465cd7f7b0c97ceafe138418e1e959))
- **NPrint:** 增加参数支持指定 C-LODOP 服务地址. ([5616d71](http://home.siaikin.website:20017/nbicc/common-components/commit/5616d714a8e54aeb79c5be4567b17fa5bcda59a4))
- **NPrint:** LODOP 兼容实现 ([bc3fb3b](http://home.siaikin.website:20017/nbicc/common-components/commit/bc3fb3b5d7921213f569324fa29a565942703092))

# [1.5.0-alpha.10](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.9...v1.5.0-alpha.10) (2023-07-26)

### Bug Fixes

- 导出类型 ([b188a90](http://home.siaikin.website:20017/nbicc/common-components/commit/b188a90bb022844c6c51d0440bae12e0b20f40a7))

# [1.5.0-alpha.9](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.8...v1.5.0-alpha.9) (2023-07-26)

### Bug Fixes

- Revert "fix: 兼容 Volar 类型提示" ([9ff0fe0](http://home.siaikin.website:20017/nbicc/common-components/commit/9ff0fe013d6ac71db9a0d53476addd6766e80aaf))

# [1.5.0-alpha.8](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.7...v1.5.0-alpha.8) (2023-07-26)

### Bug Fixes

- 兼容 Volar 类型提示 ([cc2c2f2](http://home.siaikin.website:20017/nbicc/common-components/commit/cc2c2f20bc9742fe2264cbef2baa072e46b179c7))

# [1.5.0-alpha.7](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.6...v1.5.0-alpha.7) (2023-07-24)

### Bug Fixes

- NToolbar 命名错误 ([a94094f](http://home.siaikin.website:20017/nbicc/common-components/commit/a94094fd6dc3f9b056d9c10144f5863dcb9d7420))

# [1.5.0-alpha.6](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.5...v1.5.0-alpha.6) (2023-07-24)

### Bug Fixes

- 更新 vue-to-print 解决使用了错误的文件格式的问题. ([bb5b9ec](http://home.siaikin.website:20017/nbicc/common-components/commit/bb5b9ec51ed4751dd567a653ec37a672b0643260))

### Features

- 使用 vue-tsc 生成 d.ts 以兼容 Volar. ([b27df32](http://home.siaikin.website:20017/nbicc/common-components/commit/b27df32c83b84234d9fb935a3728e0aa2516ae31))

# [1.5.0-alpha.5](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.4...v1.5.0-alpha.5) (2023-07-20)

### Features

- **NPrint:** 打印库切换到 vue-to-print 以支持其他浏览器. ([71100c6](http://home.siaikin.website:20017/nbicc/common-components/commit/71100c6136371669f90da17b64aeeea5d8a1caab))

# [1.5.0-alpha.4](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.3...v1.5.0-alpha.4) (2023-07-19)

### Bug Fixes

- **NPrint:** 修复包裹元素的宽高会被缩放影响导致不能完整显示内容的问题. ([88c4416](http://home.siaikin.website:20017/nbicc/common-components/commit/88c44161dabcd43cce98bc4551d9f073f4908f88))

### Features

- **NPrint:** 增加参数支持指定 C-LODOP 服务地址. ([b75c52f](http://home.siaikin.website:20017/nbicc/common-components/commit/b75c52f82f224e89bbf4ef4ce690a40d8952bcf4))

# [1.5.0-alpha.3](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.2...v1.5.0-alpha.3) (2023-07-18)

### Bug Fixes

- **NPrint:** 修复 LODOP 初始化时传入空字符串导致 LODOP 报错的问题. ([87aaf16](http://home.siaikin.website:20017/nbicc/common-components/commit/87aaf166c84559834d20a220242b3d442e593145))

# [1.5.0-alpha.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.5.0-alpha.1...v1.5.0-alpha.2) (2023-07-17)

### Bug Fixes

- **NPrint:** 使用 LODOP 时增加 overflow, white-space css 属性. ([9fad470](http://home.siaikin.website:20017/nbicc/common-components/commit/9fad47023738c25b1883f61698af66d5f3ca939f))

# [1.5.0-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.4.0...v1.5.0-alpha.1) (2023-07-14)

### Bug Fixes

- **NTable:** 修复会同时存在多个字段排序的问题. ([e4180f0](http://home.siaikin.website:20017/nbicc/common-components/commit/e4180f0a4bcae62866bd2e733b30aaa104ed1bf6))

### Features

- **NPrint:** LODOP 兼容实现 ([21f19e9](http://home.siaikin.website:20017/nbicc/common-components/commit/21f19e9d2b6c24af8a798bd25010a370ac9e6062))

# [1.4.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.3.0...v1.4.0) (2023-07-13)

### Bug Fixes

- **NPrint:** 修复 waitAllImageLoaded 函数在所有图像都已经加载完成的情况下再次调用, 返回的 Promise 将永远不会 resolve. ([f046237](http://home.siaikin.website:20017/nbicc/common-components/commit/f046237a58b0d6a82a8700312e956a0e72d02f8e))

### Features

- 修改样式相关文件结构, 增加 n-text-overflow-ellipsis 工具类 ([11e12a0](http://home.siaikin.website:20017/nbicc/common-components/commit/11e12a04d5dab680e0a9dca91964b0db6f035b04))
- 增加 NPrint 组件 ([ae7280c](http://home.siaikin.website:20017/nbicc/common-components/commit/ae7280c1d6c92bafba6115bf77c05bd835ac6381))
- 增加 NPrint 组件 ([50890fa](http://home.siaikin.website:20017/nbicc/common-components/commit/50890faf98064929b55446e5ec30e8edc36f5d86))
- **NPrint:** 导出类型定义 ([28a64a2](http://home.siaikin.website:20017/nbicc/common-components/commit/28a64a2d8fd7dd9d12010a5a984bb2e85561a3a4))
- **NPrint:** 增加 compact 模式实现紧凑打印. ([8a9b302](http://home.siaikin.website:20017/nbicc/common-components/commit/8a9b302d29809a905f1aa363410c74d3d33767ac))

# [1.3.0-alpha.2](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.3.0-alpha.1...v1.3.0-alpha.2) (2023-07-12)

### Bug Fixes

- **NPrint:** 修复 waitAllImageLoaded 函数在所有图像都已经加载完成的情况下再次调用, 返回的 Promise 将永远不会 resolve. ([3a3a43c](http://home.siaikin.website:20017/nbicc/common-components/commit/3a3a43c05fef28538d3d933bcf2a74b42294eee2))

### Features

- **NPrint:** 导出类型定义 ([be20e62](http://home.siaikin.website:20017/nbicc/common-components/commit/be20e6286fb56426f26e0a521db5e3b5dd2bf8e3))
- **NPrint:** 增加 compact 模式实现紧凑打印. ([fc857e8](http://home.siaikin.website:20017/nbicc/common-components/commit/fc857e8042305f9a0c9efa690f3987a95ce40520))

# [1.3.0-alpha.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.2.1...v1.3.0-alpha.1) (2023-07-12)

### Bug Fixes

- scmSkip 放在单独的 stage, 以确保没有 "skip ci" 标记的提交不会被意外中止. ([c6e850d](http://home.siaikin.website:20017/nbicc/common-components/commit/c6e850df313faf2aa215ce3a3f33cfb0450933e4))

### Features

- 修改样式相关文件结构, 增加 n-text-overflow-ellipsis 工具类 ([81c7e82](http://home.siaikin.website:20017/nbicc/common-components/commit/81c7e82aed53c5dace9d9e5bfc17a22881088a45))
- 增加 NPrint 组件 ([3c2315a](http://home.siaikin.website:20017/nbicc/common-components/commit/3c2315a9f24053a2141ff37fff5aa01b8764f4cd))

## [1.2.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.2.0...v1.2.1) (2023-07-10)

### Bug Fixes

- **NSelect:** fieldNames 中自定义了 `value` 的字段名称时, optionMap 依旧使用 value 属性作为 key. ([3ce5e10](http://home.siaikin.website:20017/nbicc/common-components/commit/3ce5e10f5f92eb537362ef33e3430516c29bc24d))

# [1.2.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.1.0...v1.2.0) (2023-07-03)

### Features

- 文档优化 ([48b2ad1](http://home.siaikin.website:20017/nbicc/common-components/commit/48b2ad15e9c6710af9b06745b04c1b98084bce74))

# [1.1.0](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.0.1...v1.1.0) (2023-06-30)

### Features

- 使用 terser 优化构建体积 ([055c766](http://home.siaikin.website:20017/nbicc/common-components/commit/055c7663ff0421894a0e3762c3308f5f93352d35))

# [1.1.0-beta.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.0.1...v1.1.0-beta.1) (2023-06-30)

### Features

- 使用 terser 优化构建体积 ([055c766](http://home.siaikin.website:20017/nbicc/common-components/commit/055c7663ff0421894a0e3762c3308f5f93352d35))

## [1.0.1](http://home.siaikin.website:20017/nbicc/common-components/compare/v1.0.0...v1.0.1) (2023-06-30)

### Bug Fixes

- **NTable,TableHelper:** editable 为 false 时实际上还在运行可编辑的逻辑. ([#6](http://home.siaikin.website:20017/nbicc/common-components/issues/6)) ([d9cd9e7](http://home.siaikin.website:20017/nbicc/common-components/commit/d9cd9e76018d3b1ad6ff996b922cd0489ba69ebc))

# 1.0.0 (2023-06-28)

### Bug Fixes

- 修复 dayjs 因不支持 esm 导致的构建失败. ([f37152f](http://home.siaikin.website:20017/nbicc/common-components/commit/f37152f0a38eada92c4529eef919a22cb77cc2ed))
- 修复 NButton 组件类型为 secondary 时, 样式错误的问题 ([ccf0ea2](http://home.siaikin.website:20017/nbicc/common-components/commit/ccf0ea21bd158be441ef7100b18f1c8b3bd4e64b))
- 修复 Nform 组件 data 属性未同步的问题. ([d7b2c15](http://home.siaikin.website:20017/nbicc/common-components/commit/d7b2c15f9d11480f32c5d7dd6f091a02bb7c0914))
- 修复 ssr 兼容性, 修复文档构建问题 ([fcc4f93](http://home.siaikin.website:20017/nbicc/common-components/commit/fcc4f93abbb27f4c600d7a550fb2c81ed6bea229))
- 修复列切换固定方式时, 未修改相关列的固定方式导致显示异常的问题. ([c035aa5](http://home.siaikin.website:20017/nbicc/common-components/commit/c035aa5a7df319783713799ac767f1dc41d05c9d))
- 修复引用外部依赖导致编译异常的问题 ([3e9eeff](http://home.siaikin.website:20017/nbicc/common-components/commit/3e9eeff751a99f48cf47d4b9d88b4a4ed0ec7b58))
- **FormHelper:** 修复默认的校验规则被修改的问题 ([d3dc568](http://home.siaikin.website:20017/nbicc/common-components/commit/d3dc5683675b9e3ea9ec8ae1c29a70ba7bda9c47))
- **NForm:** 当 data 从外部修改且属性值为空时, 会忽略掉该属性. ([85c6213](http://home.siaikin.website:20017/nbicc/common-components/commit/85c6213b94d323e917df9525c8a062c7a145620c))
- **NForm:** 兼容 data 会存在未在 fields 中声明的属性的情况. ([787f1b3](http://home.siaikin.website:20017/nbicc/common-components/commit/787f1b3fe5c14343adcb4f1835d0679cf5c52230))
- **NForm:** 同步 NButton 的修改 ([9bd3f64](http://home.siaikin.website:20017/nbicc/common-components/commit/9bd3f640314c1917a0901b8eb67d1fb4461eec7d))
- **NForm:** 未同步 data 中没有在 fields 中定义的属性. ([5add787](http://home.siaikin.website:20017/nbicc/common-components/commit/5add787c7d5bc1a81bd72bd3868a50587bbbb72d))
- **NForm:** 校验信息模板中的应该使用 label 占位. ([8ff17dc](http://home.siaikin.website:20017/nbicc/common-components/commit/8ff17dca195d1ade623d82aad93436e6116c5a86))
- **NForm:** 修复 NEasyForm 在未点击搜索按钮时同步了表单数据的问题. ([05d2f2e](http://home.siaikin.website:20017/nbicc/common-components/commit/05d2f2e95f7d313117328cd50fc2e55838c51941))
- **NForm:** 修复表单项初始值被绑定的 data 属性覆盖的问题. ([a5fa7c7](http://home.siaikin.website:20017/nbicc/common-components/commit/a5fa7c7e544ec0335e81bf28a9e7ab384fd17026))
- **NForm:** 修复空值判断错误的问题 ([0c14933](http://home.siaikin.website:20017/nbicc/common-components/commit/0c14933848db2a9d124b3a7e903c8d252e4a0e63))
- **NForm:** 修复无法从外部设置 NEasyForm 组件的 fieldLayout 属性问题. ([4973fa1](http://home.siaikin.website:20017/nbicc/common-components/commit/4973fa19b777e30919b8279c69a23eb368a401e9))
- **NForm:** 修复值变化事件监听在值变化后才添加导致的 data 同步问题 ([79da6d5](http://home.siaikin.website:20017/nbicc/common-components/commit/79da6d57070de400a106afc334c6be5883c221e2))
- **NForm:** fields 属性修改时会重置值为空且存在 initialValue 的表单项. ([54f5970](http://home.siaikin.website:20017/nbicc/common-components/commit/54f59708d0f18e2ee8dc1fec44db80cba0292f6c))
- npmmirror 同步参数错误 ([4003dd1](http://home.siaikin.website:20017/nbicc/common-components/commit/4003dd15d064770a3b72dc39ba5e80a6b850da51))
- **NSpecProvider,NModalWrapper,NDrawerWrapper:** NModalWrapper/NDrawerWrapper 嵌套导致通过 provide 注入的函数无法同时使用. ([64d9b0b](http://home.siaikin.website:20017/nbicc/common-components/commit/64d9b0b50c1cc90d21273df12f38dd71b3ef9fed))
- **NTable,NToolbar:** 修复设置窗口显示异常, 序号列无法设置 fixed 问题 ([8b06aaa](http://home.siaikin.website:20017/nbicc/common-components/commit/8b06aaa46d68cc8894dcd31b8acc7113cdc63f11))
- **NTable:** 编辑表格时丢失 size 属性 ([d7914e0](http://home.siaikin.website:20017/nbicc/common-components/commit/d7914e0381a4ec1f41d388ebaef07a64c773b0ca))
- **NTable:** 表格列拖拽排序时比较函数错误. ([8c3e84e](http://home.siaikin.website:20017/nbicc/common-components/commit/8c3e84e3be6e36da8226743441f6b251c05629ee))
- **NTable:** 当显示序号列且存在子节点时, 不显示子节点的序号. ([bfe74d7](http://home.siaikin.website:20017/nbicc/common-components/commit/bfe74d77ca0a5093440e0497af66dee5735c5898))
- **NTable:** 设置表格元素高度默认值. ([dba2f81](http://home.siaikin.website:20017/nbicc/common-components/commit/dba2f81d0691b4c99a123f4a51c99048e679f16b))
- **NTable:** 修复 selectedRows 初始状态始终为空数组的问题. ([5effedf](http://home.siaikin.website:20017/nbicc/common-components/commit/5effedff452d622d8584be17ddbbedc5be5cda01))
- **NTable:** 序号列设置默认宽度 80 px ([c9c2187](http://home.siaikin.website:20017/nbicc/common-components/commit/c9c21872c809e46ec51cc219944179ac9ccdaecc))
- **NTable:** 选择行时直接判断对象引用是否相等不可靠, 通过获取 key 值进行判断. ([41d2c5e](http://home.siaikin.website:20017/nbicc/common-components/commit/41d2c5e372d180b98b101c0d521c89d54aa31ec2))
- **NToolbar:** 设置了左/右固定的表格列不可设置为隐藏. ([e0f6e10](http://home.siaikin.website:20017/nbicc/common-components/commit/e0f6e103f0e8ffb360981dd2c39f5e438d118bef))
- **TableHelper:** 当前页数为 1 且总页数小于等于 1 时认为当前为最后一页. ([9cead83](http://home.siaikin.website:20017/nbicc/common-components/commit/9cead83a841416687519bb6f95370d7489ed2a83))
- **TableHelper:** 简化最后一页处理逻辑, 仅当当前页码小于总页码时请求最后一页. ([76640bf](http://home.siaikin.website:20017/nbicc/common-components/commit/76640bfc997020c5b726afd4faddbe066b7a9bb4))
- **TableHelper:** debounce 内使用箭头函数导致 this 丢失. debounce.flush 方法使用错误. ([f2dfdc7](http://home.siaikin.website:20017/nbicc/common-components/commit/f2dfdc737bbd3b88ed0a1cb7f7abb3912101d269))

### Features

- 导出 NPageLayout 组件 ([ede8e14](http://home.siaikin.website:20017/nbicc/common-components/commit/ede8e14721b96323f381781ae5093938af14fd95))
- 发布版本后自动同步到 npmmirror ([3c246d0](http://home.siaikin.website:20017/nbicc/common-components/commit/3c246d0c2a4bc1b6e22f3d4e5368cf268f29031d))
- 添加单元测试相关配置, 并创建部分组件的单元测试文件. ([f71d370](http://home.siaikin.website:20017/nbicc/common-components/commit/f71d3704ae11b65ea6078e54b83c2ab95975126a))
- 为 volar 添加全局组件类型 ([449a001](http://home.siaikin.website:20017/nbicc/common-components/commit/449a001d3c20b01313a111a5dcc8f441091086e7))
- 优化 NForm/NEasyForm 组件的功能分配. 部分特性迁移至 NForm. ([9fbae1c](http://home.siaikin.website:20017/nbicc/common-components/commit/9fbae1cb176f64eaa47f01131897eb855156bca4))
- 在文档中显示测试覆盖率. ([8fa9bcb](http://home.siaikin.website:20017/nbicc/common-components/commit/8fa9bcb8baadb2bd274d7d082508243876e2f5bb))
- 增加 changelog 生成脚本 ([315c440](http://home.siaikin.website:20017/nbicc/common-components/commit/315c44051008d9bc3448c6c1225ef8abe55245a2))
- 增加 n-form, n-easy-form 组件, n-button 增加 secondary 类型 ([e9b5924](http://home.siaikin.website:20017/nbicc/common-components/commit/e9b5924b395a0ee52af2087eda2356abf13e3e16))
- 增加 n-toolbar 组件, 提取组件类型定义到 \*-type.ts 文件中. ([bab4c38](http://home.siaikin.website:20017/nbicc/common-components/commit/bab4c3801adf0a363d437e4c1ded28292fe0342a))
- 增加 NButton, NLoadingButton 组件 ([3226760](http://home.siaikin.website:20017/nbicc/common-components/commit/32267605e4ffa22552294d1ad63fe6224f125ebe))
- 增加 NInputNumber 组件 ([14f2f79](http://home.siaikin.website:20017/nbicc/common-components/commit/14f2f79d0d71e31cb3f73b93af6182e66edf7603))
- 增加 NPageLayout 组件. 优化 NEasyForm 的使用方式. ([6290a44](http://home.siaikin.website:20017/nbicc/common-components/commit/6290a447371d927fba68989a4a74b27c2b68e3f8))
- 增加 NSelect, NAutoLoadSelect 组件 ([2d1a4e1](http://home.siaikin.website:20017/nbicc/common-components/commit/2d1a4e1e076912f320f9bf66836fb225cefacc1e))
- 增加 NSpecProvider 组件, 提供对规范的适配能力. ([4139b76](http://home.siaikin.website:20017/nbicc/common-components/commit/4139b7603924aa42b421204aa33373cf543e0cef))
- 增加 NUpload 组件 ([2af2ee5](http://home.siaikin.website:20017/nbicc/common-components/commit/2af2ee5e84a28d3d4beffd6952f5a92cb16c7d4a))
- 增加日期时间范围选择器, 支持自定义表单项值映射. ([8cf3c4a](http://home.siaikin.website:20017/nbicc/common-components/commit/8cf3c4aa9392ae59b24ec4b1b4374fe9b2a53c68))
- 增加状态存储功能, 增加对应的文档. ([55672b4](http://home.siaikin.website:20017/nbicc/common-components/commit/55672b472c16bb3e7fc2558a22a3b72c3c9b6ddd))
- 支持通过 Vue.use 注册全局组件 ([46ba266](http://home.siaikin.website:20017/nbicc/common-components/commit/46ba266679dbda56ceb178c583d23865bffc4f53))
- n-table 在数据为空时, 占位元素占满剩余空间 ([aa5a0df](http://home.siaikin.website:20017/nbicc/common-components/commit/aa5a0df6d33f862054e19153c7f71aa0fc00d670))
- **NButton:** 弃用 secondary 按钮类型, 增加 secondary 按钮属性用以标记次级按钮. ([00d11b9](http://home.siaikin.website:20017/nbicc/common-components/commit/00d11b95cabb22cda10597ac0208424f163b636a))
- **NButton:** NButton 组件增加 waring 参数用于显示警告按钮. ([4bf8911](http://home.siaikin.website:20017/nbicc/common-components/commit/4bf8911c509c13a0a6af42beee596372bd3994da))
- **NDrawerWrapper:** 增加 NDrawerWrapper 组件. ([b630a8e](http://home.siaikin.website:20017/nbicc/common-components/commit/b630a8ebcc5acd951e6573b6a37a7198a89ce25a))
- NEasyForm 组件支持添加字段校验规则. ([3e42b20](http://home.siaikin.website:20017/nbicc/common-components/commit/3e42b20b7f5b2d04c5b16a5d88c238305331c995))
- NForm 组件 fields 字段支持响应式. ([620becc](http://home.siaikin.website:20017/nbicc/common-components/commit/620becc8942f5bcd03dadbf2f5baed04a790c544))
- NForm 组件的表单项支持 antdv Grid 的布局方式. 文档增加自定义表单项布局的示例. ([ad65137](http://home.siaikin.website:20017/nbicc/common-components/commit/ad651379b0713fab9c88dcf5d58ad1399ac63e91))
- NForm 组件的表单项支持自定义表单项状态. 文档增加自定义表单项状态的示例. 增加表单项控件类型. ([18cbf40](http://home.siaikin.website:20017/nbicc/common-components/commit/18cbf40f17e730158b022100ad47fbcddf43a258))
- NForm 组件增加 NAutoLoadSelect 组件的支持 ([8f2b245](http://home.siaikin.website:20017/nbicc/common-components/commit/8f2b245a1b67bf16a25704e98504e0d64f7506ce))
- NForm 组件支持传递 FormItem 属性. 文档增加自定义 FormItem 布局的示例. ([390f029](http://home.siaikin.website:20017/nbicc/common-components/commit/390f029be4702a7787e1ff5fd34fcfc44c71a4a5))
- **NForm,NSelect:** NForm 组件 fields 支持响应式. NSelect 组件 label 支持响应式. ([34756ec](http://home.siaikin.website:20017/nbicc/common-components/commit/34756ecd24278b97f8d6bb946fbf8b0768901813))
- **NForm,NSpecProvider:** 增加 placeholder 规范配置. ([51b6912](http://home.siaikin.website:20017/nbicc/common-components/commit/51b69126fc8fc9f8e1e5b4ef42cbda08b60749d5))
- **NForm:** 表单项配置增加 events 属性用于配置控件事件. ([f330035](http://home.siaikin.website:20017/nbicc/common-components/commit/f33003567aa78436bb151f83041dd1cd30cd26b3))
- **NForm:** 即使表单项小于两行, 展开按钮也会占用宽度, 以分割表单和按钮组. ([1f419df](http://home.siaikin.website:20017/nbicc/common-components/commit/1f419df4fc0ec6428f7fc8b64a97fe24db35b9c8))
- **NForm:** 可以通过 NForm 组件 fields 属性添加的 v-model 修饰符属性. ([33bef99](http://home.siaikin.website:20017/nbicc/common-components/commit/33bef9970b229d31ebc9cccf80ddf04b0cff478a))
- **NForm:** 区分 NForm, NEasyForm 的全局配置. ([fe0d163](http://home.siaikin.website:20017/nbicc/common-components/commit/fe0d16319b93bace78bb29982d251618251f4c11))
- **NForm:** NEasyForm 组件的搜索表单支持配置未展开时显示的表单项行数. ([8d4f426](http://home.siaikin.website:20017/nbicc/common-components/commit/8d4f426878435df19d9f5b77d35ac6be9b2a63c3))
- **NForm:** NEasyForm 组件增加 beforeSearch 属性且支持通过 NSpecProvider 全局配置. ([1afff67](http://home.siaikin.website:20017/nbicc/common-components/commit/1afff6729e6489fe4269e68e650423786c53d109))
- **NForm:** NEasyForm 组件增加 change 事件获取实时的表单数据. ([86cf0e2](http://home.siaikin.website:20017/nbicc/common-components/commit/86cf0e2db96ea105a22aec6aa8742144623e4163))
- **NModalWrapper:** 增加 NModalWrapper 组件. ([c0c5882](http://home.siaikin.website:20017/nbicc/common-components/commit/c0c5882347bda6c2547c15c2e464b507f242293d))
- NPageLayout 组件默认插槽会和其他插槽同时存在. ([1420db4](http://home.siaikin.website:20017/nbicc/common-components/commit/1420db428f9d253113eee9a7fdce1c0c3a0e3037))
- **NSelect:** 增加 placeholderOptions 并移除 label 选项. ([1bba062](http://home.siaikin.website:20017/nbicc/common-components/commit/1bba062820c607d25da27e7364e1c01b6820614a))
- **NSpecProvider:** wrapModal, wrapDrawer 默认开启. ([bfff967](http://home.siaikin.website:20017/nbicc/common-components/commit/bfff967383382c6d2bae50418b6bd7c739d1e092))
- NTable 组件默认使用 small 大小. ([addd6a7](http://home.siaikin.website:20017/nbicc/common-components/commit/addd6a717cd54704d1469a9d2c56b47b9b829126))
- NTable 组件增加 disabledAdaptiveHeight 属性用于关闭自适应高度. ([978619e](http://home.siaikin.website:20017/nbicc/common-components/commit/978619e0424bdb15cb0d86033a6815926c9135dd))
- NTable 组件支持可伸缩列. 增加本地存储支持. ([39b848c](http://home.siaikin.website:20017/nbicc/common-components/commit/39b848c994f4d5bde8dc6fb229d473cdc6dd917a))
- **NTable:** 修改分页组件显示的文本. ([5f061a1](http://home.siaikin.website:20017/nbicc/common-components/commit/5f061a1861f0830646c2b056c0a3736aa7f1eff0))
- **NTable:** 优化自动控制编辑行的行为. 点击页面上除表格行之外的任何地方, 会保存当前编辑的行. ([98b2efe](http://home.siaikin.website:20017/nbicc/common-components/commit/98b2efe4e016977a347d5bf54fdd3aac7f4cd2c5))
- **NTable:** 增加可编辑表格功能. ([ee6a771](http://home.siaikin.website:20017/nbicc/common-components/commit/ee6a7711dbcdabdd515bac8a19c9940cb21333bf))
- **NTable:** 增加序号列选项 ([b820c01](http://home.siaikin.website:20017/nbicc/common-components/commit/b820c01836fc4d515d232c01e29902078cb802e8))
- **NTable:** NTable 租价支持斑马纹 ([23ca893](http://home.siaikin.website:20017/nbicc/common-components/commit/23ca8938c461760002cf08eeccce81ebf0bcaa51))
- NToolbar 组件列设置窗口增加列排序功能. ([03befed](http://home.siaikin.website:20017/nbicc/common-components/commit/03befedfed0882e1e9ad120586bc36cf098093c0))
- NToolbar 组件筛选更改为设置按钮, 支持表格列隐藏/左右固定. 优化 useAntDesignVueTableHelper 的类型定义. ([de54a9d](http://home.siaikin.website:20017/nbicc/common-components/commit/de54a9dcb33c5f4d0a4a7e8dbe2eed32b319e546))
- **TableHelper:** 提高 TableHelper 的易用性, 增加切换页数的接口. ([2daef0b](http://home.siaikin.website:20017/nbicc/common-components/commit/2daef0b9306448583532ed227f730e2503878ead))
- vue3 下需要定义 @vue/runtime-core ([adfd828](http://home.siaikin.website:20017/nbicc/common-components/commit/adfd828143b6394061c33790ce5e9139ffe1ce79))

### Performance Improvements

- 移除默认的省略策略, 使用列属性中的 ellipsis 作为替代 ([e6c1ede](http://home.siaikin.website:20017/nbicc/common-components/commit/e6c1ede6f94af214f85e618b81d1e75a63af9a89))
- 优化 resize 回调调用频率 ([70f5b11](http://home.siaikin.website:20017/nbicc/common-components/commit/70f5b11fcd27fb8156fc82c2701c89f631d53106))

## [0.0.1-41](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-40...v0.0.1-41) (2023-06-28)

### Features

- **NButton:** 弃用 secondary 按钮类型, 增加 secondary 按钮属性用以标记次级按钮. ([00d11b9](http://home.siaikin.website:20017/nbicc/common-components/commits/00d11b95cabb22cda10597ac0208424f163b636a))

## [0.0.1-40](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-39...v0.0.1-40) (2023-06-27)

## [0.0.1-39](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-38...v0.0.1-39) (2023-06-27)

## [0.0.1-38](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-37...v0.0.1-38) (2023-06-27)

### Features

- **NButton:** NButton 组件增加 waring 参数用于显示警告按钮. ([4bf8911](http://home.siaikin.website:20017/nbicc/common-components/commits/4bf8911c509c13a0a6af42beee596372bd3994da))

## [0.0.1-37](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-36...v0.0.1-37) (2023-06-26)

## [0.0.1-36](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-35...v0.0.1-36) (2023-06-26)

### Bug Fixes

- **NTable:** 序号列设置默认宽度 80 px ([c9c2187](http://home.siaikin.website:20017/nbicc/common-components/commits/c9c21872c809e46ec51cc219944179ac9ccdaecc))

## [0.0.1-35](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-32...v0.0.1-35) (2023-06-25)

### Bug Fixes

- npmmirror 同步参数错误 ([4003dd1](http://home.siaikin.website:20017/nbicc/common-components/commits/4003dd15d064770a3b72dc39ba5e80a6b850da51))
- **NTable:** 当显示序号列且存在子节点时, 不显示子节点的序号. ([bfe74d7](http://home.siaikin.website:20017/nbicc/common-components/commits/bfe74d77ca0a5093440e0497af66dee5735c5898))

### Features

- **NForm:** NEasyForm 组件增加 change 事件获取实时的表单数据. ([86cf0e2](http://home.siaikin.website:20017/nbicc/common-components/commits/86cf0e2db96ea105a22aec6aa8742144623e4163))

## [0.0.1-32](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-31...v0.0.1-32) (2023-06-21)

### Features

- 发布版本后自动同步到 npmmirror ([3c246d0](http://home.siaikin.website:20017/nbicc/common-components/commits/3c246d0c2a4bc1b6e22f3d4e5368cf268f29031d))

## [0.0.1-31](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-30...v0.0.1-31) (2023-06-21)

### Features

- 添加单元测试相关配置, 并创建部分组件的单元测试文件. ([f71d370](http://home.siaikin.website:20017/nbicc/common-components/commits/f71d3704ae11b65ea6078e54b83c2ab95975126a))
- 在文档中显示测试覆盖率. ([8fa9bcb](http://home.siaikin.website:20017/nbicc/common-components/commits/8fa9bcb8baadb2bd274d7d082508243876e2f5bb))
- **NForm:** NEasyForm 组件的搜索表单支持配置未展开时显示的表单项行数. ([8d4f426](http://home.siaikin.website:20017/nbicc/common-components/commits/8d4f426878435df19d9f5b77d35ac6be9b2a63c3))

## [0.0.1-30](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-29...v0.0.1-30) (2023-06-19)

### Bug Fixes

- **NForm:** 修复 NEasyForm 在未点击搜索按钮时同步了表单数据的问题. ([05d2f2e](http://home.siaikin.website:20017/nbicc/common-components/commits/05d2f2e95f7d313117328cd50fc2e55838c51941))
- **NTable:** 修复 selectedRows 初始状态始终为空数组的问题. ([5effedf](http://home.siaikin.website:20017/nbicc/common-components/commits/5effedff452d622d8584be17ddbbedc5be5cda01))

### Features

- **NForm:** 表单项配置增加 events 属性用于配置控件事件. ([f330035](http://home.siaikin.website:20017/nbicc/common-components/commits/f33003567aa78436bb151f83041dd1cd30cd26b3))
- **NForm:** NEasyForm 组件增加 beforeSearch 属性且支持通过 NSpecProvider 全局配置. ([1afff67](http://home.siaikin.website:20017/nbicc/common-components/commits/1afff6729e6489fe4269e68e650423786c53d109))
- vue3 下需要定义 @vue/runtime-core ([adfd828](http://home.siaikin.website:20017/nbicc/common-components/commits/adfd828143b6394061c33790ce5e9139ffe1ce79))

## [0.0.1-29](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-28...v0.0.1-29) (2023-06-15)

### Bug Fixes

- **NTable:** 编辑表格时丢失 size 属性 ([d7914e0](http://home.siaikin.website:20017/nbicc/common-components/commits/d7914e0381a4ec1f41d388ebaef07a64c773b0ca))
- **NTable:** 表格列拖拽排序时比较函数错误. ([8c3e84e](http://home.siaikin.website:20017/nbicc/common-components/commits/8c3e84e3be6e36da8226743441f6b251c05629ee))
- **NToolbar:** 设置了左/右固定的表格列不可设置为隐藏. ([e0f6e10](http://home.siaikin.website:20017/nbicc/common-components/commits/e0f6e103f0e8ffb360981dd2c39f5e438d118bef))
- **TableHelper:** 简化最后一页处理逻辑, 仅当当前页码小于总页码时请求最后一页. ([76640bf](http://home.siaikin.website:20017/nbicc/common-components/commits/76640bfc997020c5b726afd4faddbe066b7a9bb4))

### Features

- **NTable:** 优化自动控制编辑行的行为. 点击页面上除表格行之外的任何地方, 会保存当前编辑的行. ([98b2efe](http://home.siaikin.website:20017/nbicc/common-components/commits/98b2efe4e016977a347d5bf54fdd3aac7f4cd2c5))

## [0.0.1-28](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-27...v0.0.1-28) (2023-06-14)

## [0.0.1-27](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-26...v0.0.1-27) (2023-06-14)

### Bug Fixes

- 修复引用外部依赖导致编译异常的问题 ([3e9eeff](http://home.siaikin.website:20017/nbicc/common-components/commits/3e9eeff751a99f48cf47d4b9d88b4a4ed0ec7b58))
- **NTable:** 选择行时直接判断对象引用是否相等不可靠, 通过获取 key 值进行判断. ([41d2c5e](http://home.siaikin.website:20017/nbicc/common-components/commits/41d2c5e372d180b98b101c0d521c89d54aa31ec2))

## [0.0.1-26](http://home.siaikin.website:20017/nbicc/common-components/compare/0.0.1-25...v0.0.1-26) (2023-06-14)

### Bug Fixes

- **TableHelper:** 当前页数为 1 且总页数小于等于 1 时认为当前为最后一页. ([9cead83](http://home.siaikin.website:20017/nbicc/common-components/commits/9cead83a841416687519bb6f95370d7489ed2a83))

### Features

- **NForm:** 区分 NForm, NEasyForm 的全局配置. ([fe0d163](http://home.siaikin.website:20017/nbicc/common-components/commits/fe0d16319b93bace78bb29982d251618251f4c11))
- **NTable:** 修改分页组件显示的文本. ([5f061a1](http://home.siaikin.website:20017/nbicc/common-components/commits/5f061a1861f0830646c2b056c0a3736aa7f1eff0))
- **NTable:** 增加可编辑表格功能. ([ee6a771](http://home.siaikin.website:20017/nbicc/common-components/commits/ee6a7711dbcdabdd515bac8a19c9940cb21333bf))

## [0.0.1-25](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-24...0.0.1-25) (2023-06-14)

### Bug Fixes

- **NForm:** 修复空值判断错误的问题 ([0c14933](http://home.siaikin.website:20017/nbicc/common-components/commits/0c14933848db2a9d124b3a7e903c8d252e4a0e63))

## [0.0.1-24](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-23...v0.0.1-24) (2023-06-09)

### Bug Fixes

- **NForm:** 修复值变化事件监听在值变化后才添加导致的 data 同步问题 ([79da6d5](http://home.siaikin.website:20017/nbicc/common-components/commits/79da6d57070de400a106afc334c6be5883c221e2))

## [0.0.1-23](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-22...v0.0.1-23) (2023-06-08)

### Bug Fixes

- **NForm:** fields 属性修改时会重置值为空且存在 initialValue 的表单项. ([54f5970](http://home.siaikin.website:20017/nbicc/common-components/commits/54f59708d0f18e2ee8dc1fec44db80cba0292f6c))

## [0.0.1-22](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-21...v0.0.1-22) (2023-06-08)

### Bug Fixes

- **NForm:** 当 data 从外部修改且属性值为空时, 会忽略掉该属性. ([85c6213](http://home.siaikin.website:20017/nbicc/common-components/commits/85c6213b94d323e917df9525c8a062c7a145620c))

## [0.0.1-21](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-20...v0.0.1-21) (2023-06-08)

### Bug Fixes

- **NForm:** 修复表单项初始值被绑定的 data 属性覆盖的问题. ([a5fa7c7](http://home.siaikin.website:20017/nbicc/common-components/commits/a5fa7c7e544ec0335e81bf28a9e7ab384fd17026))

### Features

- **NTable:** NTable 租价支持斑马纹 ([23ca893](http://home.siaikin.website:20017/nbicc/common-components/commits/23ca8938c461760002cf08eeccce81ebf0bcaa51))

## [0.0.1-20](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-19...v0.0.1-20) (2023-06-07)

### Features

- 增加 changelog 生成脚本 ([315c440](http://home.siaikin.website:20017/nbicc/common-components/commits/315c44051008d9bc3448c6c1225ef8abe55245a2))
- 增加 NInputNumber 组件 ([14f2f79](http://home.siaikin.website:20017/nbicc/common-components/commits/14f2f79d0d71e31cb3f73b93af6182e66edf7603))
- 增加 NUpload 组件 ([2af2ee5](http://home.siaikin.website:20017/nbicc/common-components/commits/2af2ee5e84a28d3d4beffd6952f5a92cb16c7d4a))

## [0.0.1-19](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-18...v0.0.1-19) (2023-06-02)

### Bug Fixes

- **NTable,NToolbar:** 修复设置窗口显示异常, 序号列无法设置 fixed 问题 ([8b06aaa](http://home.siaikin.website:20017/nbicc/common-components/commits/8b06aaa46d68cc8894dcd31b8acc7113cdc63f11))

## [0.0.1-18](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-17...v0.0.1-18) (2023-06-02)

### Features

- **NTable:** 增加序号列选项 ([b820c01](http://home.siaikin.website:20017/nbicc/common-components/commits/b820c01836fc4d515d232c01e29902078cb802e8))

## [0.0.1-17](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-16...v0.0.1-17) (2023-05-31)

### Bug Fixes

- **NTable:** 设置表格元素高度默认值. ([dba2f81](http://home.siaikin.website:20017/nbicc/common-components/commits/dba2f81d0691b4c99a123f4a51c99048e679f16b))

## [0.0.1-16](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-15...v0.0.1-16) (2023-05-31)

### Features

- **NForm,NSpecProvider:** 增加 placeholder 规范配置. ([51b6912](http://home.siaikin.website:20017/nbicc/common-components/commits/51b69126fc8fc9f8e1e5b4ef42cbda08b60749d5))

## [0.0.1-15](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-14...v0.0.1-15) (2023-05-31)

### Bug Fixes

- **NForm:** 校验信息模板中的应该使用 label 占位. ([8ff17dc](http://home.siaikin.website:20017/nbicc/common-components/commits/8ff17dca195d1ade623d82aad93436e6116c5a86))

## [0.0.1-14](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-13...v0.0.1-14) (2023-05-31)

### Features

- **NForm:** 可以通过 NForm 组件 fields 属性添加的 v-model 修饰符属性. ([33bef99](http://home.siaikin.website:20017/nbicc/common-components/commits/33bef9970b229d31ebc9cccf80ddf04b0cff478a))

## [0.0.1-13](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-12...v0.0.1-13) (2023-05-29)

### Bug Fixes

- **NForm:** 未同步 data 中没有在 fields 中定义的属性. ([5add787](http://home.siaikin.website:20017/nbicc/common-components/commits/5add787c7d5bc1a81bd72bd3868a50587bbbb72d))

### Features

- **NSelect:** 增加 placeholderOptions 并移除 label 选项. ([1bba062](http://home.siaikin.website:20017/nbicc/common-components/commits/1bba062820c607d25da27e7364e1c01b6820614a))

## [0.0.1-12](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-11...v0.0.1-12) (2023-05-28)

### Bug Fixes

- **NSpecProvider,NModalWrapper,NDrawerWrapper:** NModalWrapper/NDrawerWrapper 嵌套导致通过 provide 注入的函数无法同时使用. ([64d9b0b](http://home.siaikin.website:20017/nbicc/common-components/commits/64d9b0b50c1cc90d21273df12f38dd71b3ef9fed))

## [0.0.1-11](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-10...v0.0.1-11) (2023-05-28)

### Features

- **NForm,NSelect:** NForm 组件 fields 支持响应式. NSelect 组件 label 支持响应式. ([34756ec](http://home.siaikin.website:20017/nbicc/common-components/commits/34756ecd24278b97f8d6bb946fbf8b0768901813))
- **NSpecProvider:** wrapModal, wrapDrawer 默认开启. ([bfff967](http://home.siaikin.website:20017/nbicc/common-components/commits/bfff967383382c6d2bae50418b6bd7c739d1e092))
- **TableHelper:** 提高 TableHelper 的易用性, 增加切换页数的接口. ([2daef0b](http://home.siaikin.website:20017/nbicc/common-components/commits/2daef0b9306448583532ed227f730e2503878ead))

## [0.0.1-10](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-9...v0.0.1-10) (2023-05-26)

### Bug Fixes

- **NForm:** 修复无法从外部设置 NEasyForm 组件的 fieldLayout 属性问题. ([4973fa1](http://home.siaikin.website:20017/nbicc/common-components/commits/4973fa19b777e30919b8279c69a23eb368a401e9))

### Features

- **NDrawerWrapper:** 增加 NDrawerWrapper 组件. ([b630a8e](http://home.siaikin.website:20017/nbicc/common-components/commits/b630a8ebcc5acd951e6573b6a37a7198a89ce25a))

## [0.0.1-9](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-8...v0.0.1-9) (2023-05-26)

### Features

- **NModalWrapper:** 增加 NModalWrapper 组件. ([c0c5882](http://home.siaikin.website:20017/nbicc/common-components/commits/c0c5882347bda6c2547c15c2e464b507f242293d))

## [0.0.1-8](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-7...v0.0.1-8) (2023-05-26)

### Bug Fixes

- **FormHelper:** 修复默认的校验规则被修改的问题 ([d3dc568](http://home.siaikin.website:20017/nbicc/common-components/commits/d3dc5683675b9e3ea9ec8ae1c29a70ba7bda9c47))
- **NForm:** 兼容 data 会存在未在 fields 中声明的属性的情况. ([787f1b3](http://home.siaikin.website:20017/nbicc/common-components/commits/787f1b3fe5c14343adcb4f1835d0679cf5c52230))

### Features

- **NForm:** 即使表单项小于两行, 展开按钮也会占用宽度, 以分割表单和按钮组. ([1f419df](http://home.siaikin.website:20017/nbicc/common-components/commits/1f419df4fc0ec6428f7fc8b64a97fe24db35b9c8))

## [0.0.1-7](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-6...v0.0.1-7) (2023-05-25)

## [0.0.1-6](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-5...v0.0.1-6) (2023-05-25)

### Bug Fixes

- 修复 Nform 组件 data 属性未同步的问题. ([d7b2c15](http://home.siaikin.website:20017/nbicc/common-components/commits/d7b2c15f9d11480f32c5d7dd6f091a02bb7c0914))

### Features

- 为 volar 添加全局组件类型 ([449a001](http://home.siaikin.website:20017/nbicc/common-components/commits/449a001d3c20b01313a111a5dcc8f441091086e7))
- 增加 NSpecProvider 组件, 提供对规范的适配能力. ([4139b76](http://home.siaikin.website:20017/nbicc/common-components/commits/4139b7603924aa42b421204aa33373cf543e0cef))
- 增加日期时间范围选择器, 支持自定义表单项值映射. ([8cf3c4a](http://home.siaikin.website:20017/nbicc/common-components/commits/8cf3c4aa9392ae59b24ec4b1b4374fe9b2a53c68))
- NPageLayout 组件默认插槽会和其他插槽同时存在. ([1420db4](http://home.siaikin.website:20017/nbicc/common-components/commits/1420db428f9d253113eee9a7fdce1c0c3a0e3037))
- NToolbar 组件列设置窗口增加列排序功能. ([03befed](http://home.siaikin.website:20017/nbicc/common-components/commits/03befedfed0882e1e9ad120586bc36cf098093c0))

## [0.0.1-5](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-4...v0.0.1-5) (2023-05-22)

### Bug Fixes

- 修复列切换固定方式时, 未修改相关列的固定方式导致显示异常的问题. ([c035aa5](http://home.siaikin.website:20017/nbicc/common-components/commits/c035aa5a7df319783713799ac767f1dc41d05c9d))

### Features

- 增加状态存储功能, 增加对应的文档. ([55672b4](http://home.siaikin.website:20017/nbicc/common-components/commits/55672b472c16bb3e7fc2558a22a3b72c3c9b6ddd))
- NTable 组件增加 disabledAdaptiveHeight 属性用于关闭自适应高度. ([978619e](http://home.siaikin.website:20017/nbicc/common-components/commits/978619e0424bdb15cb0d86033a6815926c9135dd))

## [0.0.1-4](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-3...v0.0.1-4) (2023-05-19)

### Bug Fixes

- 修复 NButton 组件类型为 secondary 时, 样式错误的问题 ([ccf0ea2](http://home.siaikin.website:20017/nbicc/common-components/commits/ccf0ea21bd158be441ef7100b18f1c8b3bd4e64b))

### Features

- 支持通过 Vue.use 注册全局组件 ([46ba266](http://home.siaikin.website:20017/nbicc/common-components/commits/46ba266679dbda56ceb178c583d23865bffc4f53))
- NForm 组件 fields 字段支持响应式. ([620becc](http://home.siaikin.website:20017/nbicc/common-components/commits/620becc8942f5bcd03dadbf2f5baed04a790c544))
- NTable 组件默认使用 small 大小. ([addd6a7](http://home.siaikin.website:20017/nbicc/common-components/commits/addd6a717cd54704d1469a9d2c56b47b9b829126))
- NTable 组件支持可伸缩列. 增加本地存储支持. ([39b848c](http://home.siaikin.website:20017/nbicc/common-components/commits/39b848c994f4d5bde8dc6fb229d473cdc6dd917a))
- NToolbar 组件筛选更改为设置按钮, 支持表格列隐藏/左右固定. 优化 useAntDesignVueTableHelper 的类型定义. ([de54a9d](http://home.siaikin.website:20017/nbicc/common-components/commits/de54a9dcb33c5f4d0a4a7e8dbe2eed32b319e546))

## [0.0.1-3](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-2...v0.0.1-3) (2023-05-10)

### Bug Fixes

- 修复 dayjs 因不支持 esm 导致的构建失败. ([f37152f](http://home.siaikin.website:20017/nbicc/common-components/commits/f37152f0a38eada92c4529eef919a22cb77cc2ed))

### Features

- NForm 组件的表单项支持 antdv Grid 的布局方式. 文档增加自定义表单项布局的示例. ([ad65137](http://home.siaikin.website:20017/nbicc/common-components/commits/ad651379b0713fab9c88dcf5d58ad1399ac63e91))
- NForm 组件的表单项支持自定义表单项状态. 文档增加自定义表单项状态的示例. 增加表单项控件类型. ([18cbf40](http://home.siaikin.website:20017/nbicc/common-components/commits/18cbf40f17e730158b022100ad47fbcddf43a258))
- NForm 组件支持传递 FormItem 属性. 文档增加自定义 FormItem 布局的示例. ([390f029](http://home.siaikin.website:20017/nbicc/common-components/commits/390f029be4702a7787e1ff5fd34fcfc44c71a4a5))

## [0.0.1-2](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-1...v0.0.1-2) (2023-05-06)

### Features

- 导出 NPageLayout 组件 ([ede8e14](http://home.siaikin.website:20017/nbicc/common-components/commits/ede8e14721b96323f381781ae5093938af14fd95))

## [0.0.1-1](http://home.siaikin.website:20017/nbicc/common-components/compare/v0.0.1-0...v0.0.1-1) (2023-05-05)

### Features

- NForm 组件增加 NAutoLoadSelect 组件的支持 ([8f2b245](http://home.siaikin.website:20017/nbicc/common-components/commits/8f2b245a1b67bf16a25704e98504e0d64f7506ce))

## [0.0.1-0](http://home.siaikin.website:20017/nbicc/common-components/compare/70f5b11fcd27fb8156fc82c2701c89f631d53106...v0.0.1-0) (2023-05-05)

### Bug Fixes

- 修复 ssr 兼容性, 修复文档构建问题 ([fcc4f93](http://home.siaikin.website:20017/nbicc/common-components/commits/fcc4f93abbb27f4c600d7a550fb2c81ed6bea229))

### Features

- 优化 NForm/NEasyForm 组件的功能分配. 部分特性迁移至 NForm. ([9fbae1c](http://home.siaikin.website:20017/nbicc/common-components/commits/9fbae1cb176f64eaa47f01131897eb855156bca4))
- 增加 n-form, n-easy-form 组件, n-button 增加 secondary 类型 ([e9b5924](http://home.siaikin.website:20017/nbicc/common-components/commits/e9b5924b395a0ee52af2087eda2356abf13e3e16))
- 增加 n-toolbar 组件, 提取组件类型定义到 \*-type.ts 文件中. ([bab4c38](http://home.siaikin.website:20017/nbicc/common-components/commits/bab4c3801adf0a363d437e4c1ded28292fe0342a))
- 增加 NButton, NLoadingButton 组件 ([3226760](http://home.siaikin.website:20017/nbicc/common-components/commits/32267605e4ffa22552294d1ad63fe6224f125ebe))
- 增加 NPageLayout 组件. 优化 NEasyForm 的使用方式. ([6290a44](http://home.siaikin.website:20017/nbicc/common-components/commits/6290a447371d927fba68989a4a74b27c2b68e3f8))
- 增加 NSelect, NAutoLoadSelect 组件 ([2d1a4e1](http://home.siaikin.website:20017/nbicc/common-components/commits/2d1a4e1e076912f320f9bf66836fb225cefacc1e))
- n-table 在数据为空时, 占位元素占满剩余空间 ([aa5a0df](http://home.siaikin.website:20017/nbicc/common-components/commits/aa5a0df6d33f862054e19153c7f71aa0fc00d670))
- NEasyForm 组件支持添加字段校验规则. ([3e42b20](http://home.siaikin.website:20017/nbicc/common-components/commits/3e42b20b7f5b2d04c5b16a5d88c238305331c995))

### Performance Improvements

- 移除默认的省略策略, 使用列属性中的 ellipsis 作为替代 ([e6c1ede](http://home.siaikin.website:20017/nbicc/common-components/commits/e6c1ede6f94af214f85e618b81d1e75a63af9a89))
- 优化 resize 回调调用频率 ([70f5b11](http://home.siaikin.website:20017/nbicc/common-components/commits/70f5b11fcd27fb8156fc82c2701c89f631d53106))
