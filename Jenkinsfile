pipeline {
  agent any

  stages {
    stage('Checkout') {
      steps {
        // 跳过具有 [skip ci] 标记的提交
        // https://plugins.jenkins.io/scmskip/#plugin-content-enable-on-pipeline
        scmSkip(deleteBuild: false, skipPattern:'.*(\\[skip ci\\]|\\[skip jenkins\\]).*')
      }
    }

    // 仅在文档更新时触发
    stage('Build Docs') {
      when {
        // 禁用文档构建
        expression { false }
        // 正则匹配规则来自 https://gist.github.com/marcojahn/482410b728c31b221b70ea6d2c433f0c , 增加了中文兼容.
        changelog '(docs){1}(\\([\\w\\-\\.]+\\))?(!)?: ([\\s\\S]*)'
      }

      steps {
        nodejs(cacheLocationStrategy: workspace(), configId: '4cc76bc5-85bc-43ce-9028-89af947e7792', nodeJSInstallationName: 'NodeJS 16') {
          bat "npm ci"
          bat 'npm run docs:build'
        }
        tar file: 'docs.tar.gz', dir: 'docs/.vitepress/dist', archive: true, compress: true, overwrite: true
      }
    }

    // 由 semantic-release 管理是否发布版本
    stage('Publish') {
      steps {
        withCredentials([usernameColonPassword(credentialsId: 'b2cddbd6-2b47-4290-b75f-5f4d66be7ba1', variable: 'GH_TOKEN')]) {
          withCredentials([string(credentialsId: '9914b560-94ca-432a-a89e-f336670a2666', variable: 'NPM_TOKEN')]) {
            nodejs(cacheLocationStrategy: executor(), configId: '4cc76bc5-85bc-43ce-9028-89af947e7792',, nodeJSInstallationName: 'NodeJS 18') {
              bat "npm ci"
              bat "npx semantic-release"

              // 记录测试覆盖率文件
              recordCoverage qualityGates: [[metric: 'LINE', threshold: 100.0]], tools: [[parser: 'COBERTURA', pattern: 'coverage/cobertura-coverage.xml']]
            }
          }
        }
      }
    }

    // 清理工作空间
    stage('Clean') {
      steps {
        cleanWs()
      }
    }
  }
}
