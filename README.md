<p align="center">

![npm (scoped)](https://img.shields.io/npm/v/%40nbicc/common-components) ![npm](https://img.shields.io/npm/dm/%40nbicc/common-components) [![Netlify Status](https://api.netlify.com/api/v1/badges/9ca296d4-624e-4e4d-a7c2-b1ff02d0ae27/deploy-status)](https://app.netlify.com/sites/nbicc-common-components/deploys)

</p>

## 安装

组件库依赖 `ant-design-vue`, `vue`, `@ant-design/icons-vue` 和 `lodash-es`. 请确保这些依赖已经安装.

::: tip
如果你使用 npm 版本 >= 8, npm 将自动安装这些依赖.
:::

```sh
npm install @nbicc/common-components
```

## 使用

```javascript
import { createApp } from "vue";
import Antd from "ant-design-vue";
import App from "./App";
import "ant-design-vue/dist/antd.css";

import CommonComponents from "@nbicc/common-components";
import "@nbicc/common-components/dist/style.css";

const app = createApp(App);

app.use(Antd).use(CommonComponents).mount("#app");
```

与 Ant Design Vue 一样, 需要引入样式文件. 一般来说会在 `main.js` 中引入.

## 链接

- [组件文档](https://nbicc-common-components.netlify.app/)
- [Ant Design Vue](https://www.antdv.com/)
- [Vite](https://vitejs.dev/)
- [VitePress](https://vitepress.dev/)

## 开发指南

组件库使用 [Vite](https://vitejs.dev/) 构建, 文档使用 [VitePress](https://vitepress.dev/) 构建.

### 必要的开发环境

- nodejs >= 16
  - 16 以下的版本不支持自动安装对等依赖
  - vuejs/language-tools 存在[已知的问题](https://github.com/vuejs/language-tools/issues/3124), 需要 16 以上的版本才能正常工作.

### 安装依赖

```sh
npm install
```

### 启动开发服务器

使用 vitepress 的文档服务器作为开发服务器, 会自动监听文件变化并重新编译. 文档位于 `docs` 目录下.

```sh
npm run docs:dev
```

### 修改代码...

随便改点什么...

### ~~运行单元测试 ([Vitest](https://vitest.dev/))~~

不需要, 因为没有单元测试.

```sh
npm run test:unit
```

### 提交代码

提交前请阅读关于 Commit Message 的规范 [Conventional Commits](https://www.conventionalcommits.org/zh-hans/v1.0.0/).

Commit Message 需要遵循 [Angular Commit Message Guidelines](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines).

**提交前会运行 [ESLint](https://eslint.org/) 和 [Prettier](https://prettier.io/), 检查未通过时会中断提交.**

## 推荐的 IDE 设置

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).
