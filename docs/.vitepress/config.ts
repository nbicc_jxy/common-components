import { defineConfig } from "vitepress";
import { fileURLToPath, URL } from "node:url";
import { whyframe } from "@whyframe/core";
import { whyframeVue } from "@whyframe/vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import markdownItFootnote from "markdown-it-footnote";

import { version } from "../../package.json";

// https://vitepress.dev/reference/site-config
// @ts-ignore
export default defineConfig({
  title: "Common Components",
  description: "公共组件演示",
  lang: "zh-CN",
  appearance: false,
  themeConfig: {
    outline: [1, 3],
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: "Home", link: "/" },
      {
        text: `v${version}`,
        items: [{ text: "Changelog", link: "/changelog.md" }]
      }
    ],

    aside: true,
    sidebar: [
      {
        text: "Components",
        items: [
          {
            text: "Page Layout",
            link: "/page-layout/index.md"
          },
          {
            text: "Table",
            link: "/table/",
            items: [
              { text: "通过点击行进行选择", link: "/table/select-by-row-click.md" },
              { text: "自定义行选择框", link: "/table/custom-row-selection.md" },
              { text: "可伸缩列宽", link: "/table/resizable-column.md" },
              { text: "序号列", link: "/table/number-column.md" },
              {
                text: "可编辑表格",
                link: "/table/editable-table/index.md",
                items: [
                  {
                    text: "编辑行",
                    link: "/table/editable-table/use-editable-table.md#编辑行"
                  },
                  {
                    text: "手动控制编辑行",
                    link: "/table/editable-table/use-editable-table.md#手动控制编辑行"
                  },
                  {
                    text: "添加校验规则",
                    link: "/table/editable-table/use-editable-table.md#添加校验规则"
                  },
                  {
                    text: "使用 slot 自定义控件",
                    link: "/table/editable-table/use-editable-table.md#使用-slot-自定义控件"
                  }
                ]
              },
              {
                text: "甘特图表格",
                link: "/table/gantt-table/index.md",
                items: [
                  {
                    text: "基本用法",
                    link: "/table/gantt-table/use-gantt-table.md"
                  }
                ]
              },
              { text: "使用 NEasyTable", link: "/table/use-easy-table.md" },
              { text: "使用 TableHelper", link: "/table/use-table-helper.md" }
            ]
          },
          {
            text: "Form",
            link: "/form/",
            items: [
              {
                text: "使用 NForm",
                link: "/form/use-form.md",
                items: [
                  { text: "添加校验规则", link: "/form/n-form/use-rules.md" },
                  { text: "NForm 设置禁用状态", link: "/form/n-form/disabled.md" },
                  { text: "NForm 设置只读状态", link: "/form/n-form/read-only.md" },
                  { text: "使用 slot 自定义控件", link: "/form/n-form/use-slot.md" },
                  { text: "给表单项标签添加前缀/后缀", link: "/form/n-form/prefix-and-suffix.md" },
                  { text: "内置校验工具", link: "/form/n-form/use-build-in-rules.md" }
                ]
              },
              {
                text: "使用 NEasyForm",
                link: "/form/use-easy-form/"
              }
            ]
          },
          {
            text: "Select",
            link: "/select/",
            items: [
              { text: "设置占位选项", link: "/select/custom-label.md" },
              { text: "使用下拉加载选择器", link: "/select/use-auto-load-select.md" },
              {
                text: "在 NForm 中使用下拉加载选择器",
                link: "/select/use-auto-load-select-in-form.md"
              }
            ]
          },
          {
            text: "Button",
            link: "/button/",
            items: [
              { text: "使用 NButton", link: "/button/use-button.md" },
              { text: "自动 loading", link: "/button/use-auto-loading.md" }
            ]
          },
          {
            text: "Toolbar",
            link: "/toolbar/",
            items: [{ text: "使用工具栏", link: "/toolbar/use-toolbar.md" }]
          },
          {
            text: "Upload",
            link: "/upload/",
            items: [
              { text: "Upload Button", link: "/upload/upload-button.md" },
              { text: "Upload PictureCard", link: "/upload/upload-picture-card.md" }
            ]
          },
          {
            text: "Input Number",
            link: "/input-number/",
            items: []
          },
          {
            text: "Print",
            link: "/print/",
            items: [
              { text: "标签打印(裁剪模式)", link: "/print/use-tag-print.md" },
              { text: "文档打印", link: "/print/use-compose-mode.md" },
              {
                text: "LODOP 集成",
                link: "/print/lodop/",
                items: [
                  { text: "标签打印(裁剪模式) LODOP", link: "/print/lodop/use-tag-print-lodop.md" },
                  {
                    text: "文档打印 LODOP",
                    link: "/print/lodop/use-document-print-lodop.md"
                  }
                ]
              }
            ]
          },
          {
            text: "Modal",
            link: "/modal/",
            items: [{ text: "使用 NModal", link: "/modal/use-modal.md" }]
          },
          {
            text: "Drawer",
            link: "/drawer/",
            items: [{ text: "使用 NDrawer", link: "/drawer/use-drawer.md" }]
          }
        ]
      },
      {
        text: "状态存储",
        link: "/storage/index.md"
      },
      {
        text: "API",
        link: "/types/modules.md"
      }
    ],

    // socialLinks: [{ icon: "github", link: "/https://github.com/vuejs/vitepress" }]

    search: {
      provider: "local"
    },
    editLink: {
      pattern: "http://192.168.233.181:8929/open_project/common-components/-/tree/master/docs/:path"
    }
  },
  lastUpdated: true,
  markdown: {
    config: (md) => {
      md.use(markdownItFootnote);
    }
  },
  vite: {
    plugins: [
      vueJsx(),
      whyframe({ defaultSrc: "/frames/default" }),
      whyframeVue({ include: /\.(?:vue|md)$/ })
    ],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("../../src", import.meta.url)),
        "@nbicc/common-components": fileURLToPath(new URL("../../src/main", import.meta.url))
      }
    },
    css: {
      preprocessorOptions: {
        less: {
          javascriptEnabled: true
        }
      }
    },
    build: {
      target: "es2015"
    }
  }
});
