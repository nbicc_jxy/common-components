import Theme from "vitepress/theme";
import DynamicLayout from "../components/dynamic-layout.vue";

import "./styles.less";

export default {
  ...Theme,
  // use our custom layout component that we'll create next
  Layout: DynamicLayout
};
