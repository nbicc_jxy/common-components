# 按钮组件

## NButton

::: tip
NButton 组件是对 ant-design-vue Button 组件的二次封装, 除了支持 ant-design-vue Button 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

[NButton 组件示例](use-button.md)

###### Props

| 参数      | 说明                                                                                                      | 类型                                              | 可选值 | 默认值    | 必填 |
| :-------- | :-------------------------------------------------------------------------------------------------------- | :------------------------------------------------ | :----- | :-------- | ---- |
| ...       | ant-design-vue Button 组件的所有参数                                                                      | ...                                               | ...    | ...       | ...  |
| delay     | 点击事件延时时间, 用于防止重复点击                                                                        | `number`, 单位: 毫秒                              | -      | 250       | 否   |
| type      | ~~按钮类型, 在 Button 组件的基础上增加了 `secondary` 类型.~~ secondary 类型弃用, 请使用 `secondary` 属性. | `primary, secondary, default, dashed, link, text` | -      | `default` | 否   |
| secondary | 是否为次要按钮, 会根据按钮类型自动设置背景色                                                              | `boolean`                                         | -      | `false`   | 否   |
| warning   | 是否为警告按钮, 会根据按钮类型自动设置背景色                                                              | `boolean`                                         | -      | `false`   | 否   |

## NLoadingButton

NLoadingButton 组件会根据 `loadingClick` 的返回值自动为按钮添加 loading 状态.

[NLoadingButton 组件示例](use-auto-loading.md)

###### Props

| 参数 | 说明                   | 类型 | 可选值 | 默认值 | 必填 |
| :--- | :--------------------- | :--- | :----- | :----- | ---- |
| ...  | NButton 组件的所有参数 | ...  | ...    | ...    | ...  |

###### Events

| 事件名称     | 说明                                             | 回调参数                                  |
| :----------- | :----------------------------------------------- | :---------------------------------------- |
| ...          | ant-design-vue Select 组件的所有事件             | ...                                       |
| loadingClick | 点击事件回调函数. 是否显示 loading 取决于返回值. | `(event: MouseEvent) => Promise<unknown>` |
