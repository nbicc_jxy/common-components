<script setup>
import { reactive } from "vue";
import { NLoadingButton } from "@/components/n-button";
import { requestPosts } from "../mock";

async function handleLoadingClick() {
  return await requestPosts(1, 1, 1000);
}
</script>

# 自动 loading

<iframe data-why class="demo-container">
  <div>
    <NLoadingButton @loadingClick="handleLoadingClick">Loading</NLoadingButton>
  </div>
</iframe>

::: code-group
```javascript
import { reactive } from "vue";
import { NLoadingButton } from "@nbicc/common-components";
import { requestPosts } from "../mock";

async function handleLoadingClick() {
  return await requestPosts(1, 1, 1000);
}
```

```vue
<div>
  <NLoadingButton @loadingClick="handleLoadingClick">Loading</NLoadingButton>
</div>
```
:::
