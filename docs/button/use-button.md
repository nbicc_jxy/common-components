# 使用 n-button

## secondary 按钮

<iframe data-why class="demo-container">
  <a-space style="padding: 8px">
    <n-button type="primary" secondary>Primary</n-button>
    <n-button secondary>Default</n-button>
    <n-button type="dashed" secondary>Default</n-button>
    <n-button type="text" secondary>Text</n-button>
    <n-button type="link" secondary>Link</n-button>
    <n-button disabled secondary>Disabled</n-button>
  </a-space>
  <h6>Loading</h6>
  <a-space style="padding: 8px">
    <n-button type="primary" secondary loading>Primary</n-button>
    <n-button secondary loading>Default</n-button>
    <n-button type="dashed" secondary loading>Default</n-button>
    <n-button type="text" secondary loading>Text</n-button>
    <n-button type="link" secondary loading>Link</n-button>
    <n-button disabled secondary loading>Disabled</n-button>
  </a-space>
  <h6>Ghost</h6>
  <a-space style="background: rgb(190, 200, 200); padding: 8px;">
    <n-button type="primary" secondary ghost>Primary</n-button>
    <n-button secondary ghost>Default</n-button>
    <n-button type="dashed" secondary ghost>Default</n-button>
    <n-button type="text" secondary ghost>Text</n-button>
    <n-button type="link" secondary ghost>Link</n-button>
    <n-button disabled secondary ghost>Disabled</n-button>
  </a-space>
</iframe>

::: details 点击查看代码

```html
<a-space style="padding: 8px">
  <n-button type="primary" secondary>Primary</n-button>
  <n-button secondary>Default</n-button>
  <n-button type="dashed" secondary>Default</n-button>
  <n-button type="text" secondary>Text</n-button>
  <n-button type="link" secondary>Link</n-button>
  <n-button disabled secondary>Disabled</n-button>
</a-space>
<h6>Loading</h6>
<a-space style="padding: 8px">
  <n-button type="primary" secondary loading>Primary</n-button>
  <n-button secondary loading>Default</n-button>
  <n-button type="dashed" secondary loading>Default</n-button>
  <n-button type="text" secondary loading>Text</n-button>
  <n-button type="link" secondary loading>Link</n-button>
  <n-button disabled secondary loading>Disabled</n-button>
</a-space>
<h6>Ghost</h6>
<a-space style="background: rgb(190, 200, 200); padding: 8px;">
  <n-button type="primary" secondary ghost>Primary</n-button>
  <n-button secondary ghost>Default</n-button>
  <n-button type="dashed" secondary ghost>Default</n-button>
  <n-button type="text" secondary ghost>Text</n-button>
  <n-button type="link" secondary ghost>Link</n-button>
  <n-button disabled secondary ghost>Disabled</n-button>
</a-space>
```

:::

## warning 按钮

<iframe data-why class="demo-container">
  <a-space style="padding: 8px">
    <n-button type="primary" warning>Primary</n-button>
    <n-button warning>Default</n-button>
    <n-button type="dashed" warning>Default</n-button>
    <n-button type="text" warning>Text</n-button>
    <n-button type="link" warning>Link</n-button>
    <n-button disabled warning>Disabled</n-button>
  </a-space>
  <h6>Loading</h6>
  <a-space style="padding: 8px">
    <n-button type="primary" warning loading>Primary</n-button>
    <n-button warning loading>Default</n-button>
    <n-button type="dashed" warning loading>Default</n-button>
    <n-button type="text" warning loading>Text</n-button>
    <n-button type="link" warning loading>Link</n-button>
    <n-button disabled warning loading>Disabled</n-button>
  </a-space>
  <h6>Ghost</h6>
  <a-space style="background: rgb(190, 200, 200); padding: 8px;">
    <n-button type="primary" warning ghost>Primary</n-button>
    <n-button warning ghost>Default</n-button>
    <n-button type="dashed" warning ghost>Default</n-button>
    <n-button type="text" warning ghost>Text</n-button>
    <n-button type="link" warning ghost>Link</n-button>
    <n-button disabled warning ghost>Disabled</n-button>
  </a-space>
</iframe>

::: details 点击查看代码

```html
<a-space style="padding: 8px">
  <n-button type="primary" warning>Primary</n-button>
  <n-button warning>Default</n-button>
  <n-button type="dashed" warning>Default</n-button>
  <n-button type="text" warning>Text</n-button>
  <n-button type="link" warning>Link</n-button>
  <n-button disabled warning>Disabled</n-button>
</a-space>
<h6>Loading</h6>
<a-space style="padding: 8px">
  <n-button type="primary" warning loading>Primary</n-button>
  <n-button warning loading>Default</n-button>
  <n-button type="dashed" warning loading>Default</n-button>
  <n-button type="text" warning loading>Text</n-button>
  <n-button type="link" warning loading>Link</n-button>
  <n-button disabled warning loading>Disabled</n-button>
</a-space>
<h6>Ghost</h6>
<a-space style="background: rgb(190, 200, 200); padding: 8px;">
  <n-button type="primary" warning ghost>Primary</n-button>
  <n-button warning ghost>Default</n-button>
  <n-button type="dashed" warning ghost>Default</n-button>
  <n-button type="text" warning ghost>Text</n-button>
  <n-button type="link" warning ghost>Link</n-button>
  <n-button disabled warning ghost>Disabled</n-button>
</a-space>
```

:::
