# 抽屉组件

###### 用法

## NDrawer

::: tip
NTable 组件是对 ant-design-vue Modal 组件的二次封装, 除了支持 ant-design-vue Modal 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

### Props

#### beforeOpen

打开抽屉前的回调, 返回 `false` 时会阻止抽屉打开.

- 类型: `() => Promise<boolean | void> | boolean | void`

#### beforeClose

关闭抽屉前的回调, 返回 `false` 时会阻止抽屉关闭.

- 类型: `() => Promise<boolean | void> | boolean | void`
