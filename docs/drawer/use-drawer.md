# 使用 NDrawer

<script setup>
import Sample from "./use-drawer-example.vue";
</script>

<iframe data-why class="demo-container" style="height: 768px;">
  <n-spec-provider>
    <Sample />
  </n-spec-provider>
</iframe>

::: code-group

<<< @/drawer/use-drawer-example.vue#script{javascript} [script setup]
<<< @/drawer/use-drawer-example.vue#template{html} [template]
