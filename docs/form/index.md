# 表单组件

## NForm

[NForm 组件示例](use-form.md)

::: tip
NForm 组件是对 ant-design-vue Form 组件的二次封装, 除了支持 ant-design-vue Form 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

NForm 组件会根据 `fields` 属性自动生成对应的表单项, 并跟 `data` 属性进行双向绑定.

### Props

| 参数        | 说明                                                 | 类型                                                        | 可选值 | 默认值                                 | 必填 |
| :---------- | :--------------------------------------------------- | :---------------------------------------------------------- | :----- | :------------------------------------- | ---- |
| ...         | ant-design-vue Form 组件的所有参数                   | ...                                                         | ...    | ...                                    | ...  |
| fields      | 表单项的集合, 用于描述表单的结构.                    | `Array<FormModelFieldArgs>`                                 | -      | []                                     | 否   |
| fieldLayout | 表单项的布局选项. [示例](use-form.md#控制表单项布局) | [NFormFieldLayout](/types/interfaces/NFormFieldLayout.html) | -      | `{ span: 24 }`                         | 否   |
| fieldStatus | 表单项的状态选项. [示例](use-form.md#设置表单项状态) | [NFormFieldStatus](/types/interfaces/NFormFieldStatus.html) | -      | `{ disabled: false, readOnly: false }` | 否   |
| data        | 表单数据对象, 可以用 v-model:data 双向绑定.          | {}                                                          | -      | -                                      | 否   |

### Methods

除以下方法外, 还继承了 [ant-design-vue Form](https://3x.antdv.com/components/form-cn/#方法) 组件的所有方法.

#### getFormData

获取表单数据代理对象, 可用于在外部修改表单数据.

### Slots

| 插槽名称         | 说明                               | 类型                                                      |
| :--------------- | :--------------------------------- | :-------------------------------------------------------- |
| $\{name\}        | 自定义名称为 `name` 的整个表单项   | `v-slot:[name]="{ field, formData, status }"`             |
| $\{name\}Label   | 自定义名称为 `name` 的表单项的标签 | `v-slot:[name + "Label"]="{ field, formData, status }"`   |
| $\{name\}Control | 自定义名称为 `name` 的表单项的控件 | `v-slot:[name + "Control"]="{ field, formData, status }"` |

[slot 示例](n-form/use-slot.md)

#### setField

设置表单项的值, 用于编程式设置表单项的值.

- 参数: `(name: string, value: any) => void`
- 示例: `nFormRef.value.setField("input", "不是输入框不是输入框不是输入框")`

## NEasyForm

[NEasyForm 组件示例](use-easy-form/index.md)

NEasyForm 基于 NForm 组件封装, 继承了 NForm 组件的所有参数/事件/方法/插槽, 并增加了展开动作以及重置/搜索按钮.

::: info
不同于 NForm 组件, NEasyForm 组件的 `data` 参数在表单字段的值变化时不会立即更新而是在 搜索按钮点击后触发.
:::

### Props

继承 NForm 组件的所有参数, 除此之外还有以下参数.

#### data

表单数据对象, 可以用 v-model:data 双向绑定.

::: tip
与 NEasyForm 组件的 `data` 参数仅在**初始化, 搜索/重置按钮点击后**触发.

如果需要在表单字段的值变化时立即获取到最新的值, 可以使用 [change](#Events) 事件.
:::

- 类型: `{}`

#### beforeSearch <Badge type="tip" text="spec configurable" />

search 事件触发前的钩子, 返回 false 或 Promise.reject 时将阻止 search 事件的触发.

- 类型: `(data) => boolean | Promise<boolean>`
- 默认值: [easyForm.beforeSearch](../spec-provider/#easyForm-beforeSearch)

#### defaultExpandRowCount <Badge type="tip" text="spec configurable" />

未展开时显示的行数, 默认值为 2. 可以通过对象的方式设置不同屏幕尺寸

- 类型: `number | { xs: number, sm: number, md: number, lg: number, xl: number, xxl: number }`
- 默认值: [easyForm.defaultExpandRowCount](../spec-provider/#easyForm-defaultExpandRowCount)

### Methods

除以下方法外, 还继承了 [ant-design-vue Form](https://3x.antdv.com/components/form-cn/#方法) 组件和 [NForm](#nform-nform) 组件的所有方法.

#### search

触发搜索按钮点击事件, 效果等同于点击搜索按钮. 用于编程式触发搜索按钮点击事件.

#### reset

触发重置按钮点击事件, 效果等同于点击重置按钮. 用于编程式触发重置按钮点击事件.

#### syncFormData

默认情况下, NEasyForm 组件的 `data` 参数在表单字段的值变化时不会立即更新而是在 **搜索/重置** 后更新.

调用该方法可以立即更新 `data` 参数.

### Events {#Events}

| 事件名称 | 说明                                                                              | 回调参数             |
| :------- | :-------------------------------------------------------------------------------- | :------------------- |
| ...      | ant-design-vue Form 组件的所有事件                                                | ...                  |
| search   | 搜索按钮点击事件                                                                  | `() => void`         |
| reset    | 重置按钮点击事件                                                                  | `() => void`         |
| change   | 表单数据变化事件, 入参中的 data 参数是只读的. 可以通过该事件获取到最新的表单数据. | `(data: {}) => void` |

##### 类型说明

###### FormModelFieldArgs {#FormModelFieldArgs}

详情见 [FormModelFieldArgs](/types/modules.html#formmodelfieldargs)
