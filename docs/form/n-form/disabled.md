# 设置表单项的禁用状态

以下示例设置了除 `开关(switch)` 以外的所有表单项为禁用状态, `switch` 表单项为可编辑状态.

<script setup>
import { ref, watch } from "vue";
import { NForm, useAntDesignVueFormHelper } from "@/components/n-form";
import { FormItemControlType } from "@/components/form-helper";

const data = ref({});
const fields = ref([
  { name: "input", label: "输入框", type: FormItemControlType.INPUT },
  { name: "input-number", label: "数字输入框", type: FormItemControlType.INPUT_NUMBER },
  [
    { name: "select", label: "下拉框", type: FormItemControlType.SELECT },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }],
  { name: "date-picker", label: "日期选择器", type: FormItemControlType.DATE_PICKER },
  { name: "time-picker", label: "时间选择器", type: FormItemControlType.TIME_PICKER },
  { name: "switch", label: "开关", type: FormItemControlType.SWITCH },
  [
    { name: "radio", label: "单选框", type: FormItemControlType.RADIO_GROUP },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }],
  [
    { name: "checkbox", label: "多选框", type: FormItemControlType.CHECKBOX_GROUP },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }
  ],
  { name: "textarea", label: "文本域", type: FormItemControlType.TEXT_AREA },
  { name: "slider", label: "滑块", type: FormItemControlType.SLIDER },
  { name: "tree-select", label: "树选择器", type: FormItemControlType.TREE_SELECT },
  { name: "cascader", label: "级联选择器", type: FormItemControlType.CASCADER }]);
const fieldStatus = ref({ disabled: true, fields: { switch: { disabled: false } } });
watch(data, () => console.log(data.value));
</script>

<iframe data-why class="demo-container" style="height: 720px">
  <NForm v-model:data="data" :fields="fields" :field-status="fieldStatus"></NForm>
</iframe>

::: code-group

```vue
<script setup>
import { ref, watch } from "vue";
import { NForm, useAntDesignVueFormHelper } from "@/components/n-form";
import { FormItemControlType } from "@/components/form-helper";

const data = ref({});
const fieldStatus = ref({
  // [!code focus:9]
  disabled: true,
  fields: {
    // 单独设置 switch 可编辑
    switch: {
      disabled: false
    }
  }
});
const fields = ref([
  { name: "input", label: "输入框", type: FormItemControlType.INPUT },
  { name: "input-number", label: "数字输入框", type: FormItemControlType.INPUT_NUMBER },
  [
    { name: "select", label: "下拉框", type: FormItemControlType.SELECT },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }
  ],
  { name: "date-picker", label: "日期选择器", type: FormItemControlType.DATE_PICKER },
  { name: "time-picker", label: "时间选择器", type: FormItemControlType.TIME_PICKER },
  { name: "switch", label: "开关", type: FormItemControlType.SWITCH },
  [
    { name: "radio", label: "单选框", type: FormItemControlType.RADIO_GROUP },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }
  ],
  [
    { name: "checkbox", label: "多选框", type: FormItemControlType.CHECKBOX_GROUP },
    {
      options: [
        { label: "option1", value: "option1" },
        { label: "option2", value: "option2" },
        { label: "option3", value: "option3" }
      ]
    }
  ],
  { name: "textarea", label: "文本域", type: FormItemControlType.TEXT_AREA },
  { name: "slider", label: "滑块", type: FormItemControlType.SLIDER },
  { name: "tree-select", label: "树选择器", type: FormItemControlType.TREE_SELECT },
  { name: "cascader", label: "级联选择器", type: FormItemControlType.CASCADER }
]);
watch(data, () => console.log(data.value));
</script>

<template>
  <NForm v-model:data="data" :fields="fields" :field-status="fieldStatus"></NForm>
</template>
```
