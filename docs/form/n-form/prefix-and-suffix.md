# 给表单项标签添加前缀/后缀

`NForm` 组件支持给表单项标签添加前缀/后缀, 通过 `labelPrefix` 和 `labelSuffix` 属性(或名为 `${fieldName}labelPrefix`/`${fieldName}labelSuffix` 的插槽)来设置.

## 示例

::: tip
`labelPrefix` 和 `labelSuffix` 属性可以是字符串或者 `VNode` 对象.
:::

<script setup>
import PrefixAndSuffix from "./prefix-and-suffix.vue";
</script>

## 编辑行

<iframe data-why class="demo-container" style="height: 512px">
<PrefixAndSuffix />
</iframe>

::: code-group

<<< @/form/n-form/prefix-and-suffix.vue#script{javascript} [script setup]
<<< @/form/n-form/prefix-and-suffix.vue#template{html} [template]

:::
