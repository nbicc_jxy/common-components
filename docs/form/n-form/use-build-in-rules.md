# 内置校验工具

## 比较大小

### isLessThan(name, message)

应用了 `isLessThan` 的表单项的值必须小于名为 `name` 表单项的值.

- **支持的表单项类型**:
  - 数字:
    - FormItemControlType.INPUT_NUMBER
  - 日期:
    - FormItemControlType.DATE_PICKER
    - FormItemControlType.DATE_RANGE_PICKER
    - FormItemControlType.TIME_PICKER
    - FormItemControlType.TIME_RANGE_PICKER
- **参数**:
  - `name`: 要比较的表单项的 `name` 属性值
  - `message`: 错误信息
- **示例**:
  ```javascript
  const fields = [
    {
      name: "startTime",
      type: FormItemControlType.DATE_PICKER,
      label: "开始时间",
      rules: [
        { validator: FormValidateRules.isLessThanOrEqual("endTime", "开始时间不可晚于结束时间") }
      ]
    },
    {
      name: "endTime",
      type: FormItemControlType.DATE_PICKER,
      label: "结束时间"
    }
  ];
  ```

### isLessThanOrEqual(name, message)

应用了 `isLessThanOrEqual` 的表单项的值必须小于**等于**名为 `name` 表单项的值.

- **支持的表单项类型**: 同 `isLessThan`
- **参数**: 同 `isLessThan`

### isGreaterThan(name, message)

应用了 `isGreaterThan` 的表单项的值必须大于名为 `name` 表单项的值.

- **支持的表单项类型**: 同 `isLessThan`
- **参数**: 同 `isLessThan`

### isGreaterThanOrEqual(name, message)

应用了 `isGreaterThanOrEqual` 的表单项的值必须大于**等于**名为 `name` 表单项的值.

- **支持的表单项类型**: 同 `isLessThan`
- **参数**: 同 `isLessThan`

## 字符校验

### allowChars(buildIn, message)

应用了 `allowChars` 的表单项的值只能包含指定的字符集.

- **支持的表单项类型**: 同 `allowCharsOrCustomize`
- **参数**:
  - `buildIn`: 内置的字符集, 可选值: 同 `allowCharsOrCustomize`
  - `message`: 错误信息
- **示例**:
  ```javascript
  const fields = [
    {
      name: "username",
      type: FormItemControlType.INPUT,
      label: "用户名",
      rules: [
        {
          validator: FormValidateRules.allowChars(
            ["zh", "en", "number"],
            "用户名只能包含中文字符, 英文字符和数字"
          )
        }
      ]
    }
  ];
  ```

### allowCharsOrCustomize(buildIn, custom, message)

应用了 `allowCharsOrCustomize` 的表单项的值只能包含指定的字符集或自定义字符集中的字符.

- **支持的表单项类型**:
  - 文本:
    - FormItemControlType.INPUT
    - FormItemControlType.TEXT_AREA
- **参数**:
  - `buildIn`: 内置的字符集, 可选值:
    - `zh`: 中文字符
    - `en`: 英文字符
    - `number`: 数字
  - `custom`: 额外的自定义字符集
  - `message`: 错误信息
- **示例**:
  ```javascript
  const fields = [
    {
      name: "username",
      type: FormItemControlType.INPUT,
      label: "用户名",
      rules: [
        {
          validator: FormValidateRules.allowCharsOrCustomize(
            ["zh", "en", "number"],
            "-",
            "用户名只能包含中文字符, 英文字符，数字和 '-'"
          )
        }
      ]
    }
  ];
  ```
