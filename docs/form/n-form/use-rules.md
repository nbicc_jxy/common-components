# 添加表单校验

表单校验规则与 `ant-design-vue` 的表单校验规则一致, 具体请参考 [Form 表单校验](https://www.antdv.com/components/form#Validation-Rules).
区别在于需要为每个表单字段单独设置校验规则.

<script setup>
import { ref, watch } from "vue";
import { FormItemControlType } from "@nbicc/common-components";

const data = ref({});
watch(data, () => console.log(data.value));
const fields = ref([
  {
    name: "input",
    label: "输入框",
    type: FormItemControlType.INPUT,
    rules: [{ required: true }] },
]);
</script>

<iframe data-why class="demo-container">
  <NForm v-model:data="data" :fields="fields"></NForm>
</iframe>

::: code-group

```vue {12}
<script>
import { ref, watch } from "vue";
import { NForm, FormItemControlType } from "@nbicc/common-components";

const data = ref({});
watch(data.value, () => console.log(data.value), { deep: true });
const fields = ref([
  {
    name: "input",
    label: "输入框",
    type: FormItemControlType.INPUT,
    rules: [{ required: true }]
  }
]);
</script>

<template>
  <NForm v-model:data="data" :fields="fields"></NForm>
</template>
```

:::
