# 自定义表单项值映射

通过在表单项配置对象中添加 `valueMapTo` 属性, 可以自定义表单项值映射.
`valueMapTo` 属性的值可以是一个字符串, 也可以是一个函数.

- 当 `valueMapTo` 的值是一个字符串时, 表单项的值将会以该字符串作为键值存储在表单数据对象中.
- 当 `valueMapTo` 的值是一个函数时, 函数的第一个参数是表单项的值, 第二个参数是表单数据, 第三个参数是表单项的配置, 可以在函数中自定义存储逻辑.
  ```javascript
  // 将表单项控件的值映射到 data 中的 field1 字段
  valueMapTo: (value, rawData) => {
    rawData["field1"] = value;
  };
  ```

## 示例

以下示例展示了如何自定义表单项值映射. 在输入框中输入内容, 可以在控制台中看到表单数据对象中的 `inputByString` 和 `inputByFunction` 字段的值.

::: tip
在 `valueMapTo` 函数中, 需要手动将表单项的值存储到表单数据对象中, 否则表单数据对象中不会有该字段.

示例中 `inputByString` 就没有在 `valueMapTo` 函数中手动存储, 所以对应输入框的值为空.
:::

<script setup>
import { ref, watch } from "vue";
import dayjs from "dayjs";
import { FormItemControlType } from "@nbicc/common-components";

const data = ref({});
const fields = ref([
  {
    name: "input",
    label: "使用字符串映射",
    type: FormItemControlType.INPUT,
    valueMapTo: "inputByString"
  },
  {
    name: "dateRangePicker",
    label: "使用函数映射",
    type: FormItemControlType.DATE_RANGE_PICKER,
    initialValue: [dayjs("1998-03-31"), dayjs("1998-03-31")],
    valueMapTo: (value, rawData) => {
      rawData.dateRangePicker = value;
      rawData.startDate = value[0];
      rawData.endDate = value[1];
    }
  },
]);
watch(data, () => console.log(data.value));
</script>

<iframe data-why class="demo-container" style="height: 512px">
  <NForm v-model:data="data" :fields="fields"></NForm>
</iframe>

::: code-group

```vue
<script setup>
import { ref, watch } from "vue";
import dayjs from "dayjs";
import { FormItemControlType } from "@nbicc/common-components";

const data = ref({});
const fields = ref([
  {
    name: "input",
    label: "使用字符串映射",
    type: FormItemControlType.INPUT,
    valueMapTo: "inputByString" // [!code ++]
  },
  {
    name: "dateRangePicker",
    label: "使用函数映射",
    type: FormItemControlType.DATE_RANGE_PICKER,
    initialValue: [dayjs(), dayjs()],
    valueMapTo: (value, rawData) => {
      // [!code ++:7]
      // 手动将表单项的值存储到表单数据对象中
      rawData.dateRangePicker = value;
      rawData.startDate = value[0];
      rawData.endDate = value[1];
    }
  }
]);
watch(data, () => console.log(data.value));
</script>

<template>
  <NForm v-model:data="data" :fields="fields"></NForm>
</template>
```
