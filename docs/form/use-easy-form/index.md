# NEasyForm

## 使用 NEasyForm

<script setup>
import UseEasyFormDemo from "./UseEasyFormDemo.vue";
</script>

<iframe data-why class="demo-container" style="height: 512px">
<UseEasyFormDemo />
</iframe>

::: details 点击查看代码
::: code-group

<<< @/form/use-easy-form/UseEasyFormDemo.vue#script{javascript} [script setup]
<<< @/form/use-easy-form/UseEasyFormDemo.vue#template{html} [template]

:::

## 响应式

> [响应式示例](responsive.md)
