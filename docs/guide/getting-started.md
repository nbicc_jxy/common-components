# common-components

基于 ant-design-vue 封装的公共组件库. 包含封装了公共业务逻辑的组件如 [NEasyForm](https://nbicc-common-components.netlify.app/form/),
[NEasyTable](https://nbicc-common-components.netlify.app/table/use-easy-table.html)等, 以及一些常用的组件.

<script setup>
import coverageSummary from "../../coverage/coverage-summary.json";

const { statements, branches, functions, lines } = coverageSummary.total;

const avgPercent = (statements.pct + branches.pct + functions.pct + lines.pct) / 4;
const allCovered = statements.covered + branches.covered + functions.covered + lines.covered;
const allTotal = statements.total + branches.total + functions.total + lines.total;

</script>

::: details 平均覆盖率 {{ avgPercent.toFixed(2) }}% ({{ allCovered }}/{{ allTotal }})

| Statements                                                                         | Branches                                                                     | Functions                                                                       | Lines                                                               |
| ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- | ------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
| {{ statements.pct.toFixed(2) }}% ({{ statements.covered }}/{{ statements.total }}) | {{ branches.pct.toFixed(2) }}% ({{ branches.covered }}/{{ branches.total }}) | {{ functions.pct.toFixed(2) }}% ({{ functions.covered }}/{{ functions.total }}) | {{ lines.pct.toFixed(2) }}% ({{ lines.covered }}/{{ lines.total }}) |

:::

<!--@include: ../../README.md-->
