---
layout: home

hero:
  name: "Common Components"
  text: ""
  tagline: ""
  actions:
    - theme: brand
      text: Page Layout 页面布局
      link: /page-layout/
    - theme: brand
      text: Table 表格
      link: /table/
    - theme: brand
      text: Form 表单
      link: /form/
    - theme: brand
      text: Select 选择器
      link: /select/
    - theme: brand
      text: Button 按钮
      link: /button/
    - theme: brand
      text: Toolbar 工具栏
      link: /toolbar/
    - theme: brand
      text: Upload 上传
      link: /upload/
    - theme: brand
      text: Print 打印
      link: /print/
    - theme: brand
      text: Modal 模态框
      link: /modal/
    - theme: brand
      text: Drawer 抽屉
      link: /drawer/
    - theme: alt
      text: SpecProvider 全局规范配置
      link: /spec-provider/
    - theme: alt
      text: API
      link: /types/modules.md
    - theme: alt
      text: 状态存储
      link: /storage/index.md

features:
  - icon: 🛠
    title: Get Started
    details: 快速上手 Common Components
    link: /guide/getting-started.md
  - icon: ⁉
    title: FAQ
    details: 常见问题, 请查看这里.
    link: /faq/index.md
---
