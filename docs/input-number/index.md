# 数字输入组件

## NInputNumber

::: tip
NInputNumber 同 antdv InputNumber 组件的二次封装, 同 antdv InputNumber 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

###### Props

| 参数                                    | 说明                                                                      | 类型             | 可选值 | 默认值 | 必填 |
| :-------------------------------------- | :------------------------------------------------------------------------ | :--------------- | :----- | :----- | ---- |
| ... 同 antdv InputNumber 组件的所有参数 | ...                                                                       | ...              | ...    | ...    |
| round                                   | 设置了精度时, 是否四舍五入                                                | boolean          | -      | true   | ×    |
| precisionRange                          | 动态精度, 用于设置精度的最大值和最小值. 当 `precision` 存在时, 该参数无效 | [number, number] | -      | -      | ×    |

### 示例

<script setup>
import { ref } from "vue";

const value = ref(1.99999);
const round = ref(true);
</script>

<iframe data-why class="demo-container">
  <div>
    <a-form-item label="四舍五入">
    <a-switch v-model:checked="round"></a-switch>
    </a-form-item>
    <n-input-number v-model:value="value" :precision-range="[2, 6]" :round="round"/>
  </div>
</iframe>

::: code-group

```javascript [script setup]
import { ref } from "vue";

const value = ref(1.99999);
const round = ref(true);
```

```html [template]
<div>
  <a-form-item label="四舍五入">
    <a-switch v-model:checked="round"></a-switch>
  </a-form-item>
  <n-input-number v-model:value="value" :precision-range="[2, 6]" :round="round" />
</div>
```

:::
