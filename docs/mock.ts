import { faker } from "@faker-js/faker";

export async function requestPosts(page: number, size: number, delay: number = 1000) {
  await wait(delay);

  const pages = faker.datatype.number({ min: 1, max: page + 10 });

  return {
    total: pages * size,
    data: Array.from({ length: size }, (_, index) => ({
      id: index + (page - 1) * size,
      name: faker.lorem.words(3),
      author: faker.name.fullName(),
      title: faker.lorem.sentence(),
      description: faker.lorem.sentence(),
      level: faker.datatype.number({ min: 1, max: 5 }),
      createdAt: faker.date.past().toISOString(),
      updatedAt: faker.date.future().toISOString()
    }))
  };
}

export function requestPost() {
  return {
    name: faker.lorem.words(3),
    author: faker.name.fullName(),
    title: faker.lorem.sentence(),
    description: faker.lorem.sentence(),
    level: faker.datatype.number({ min: 1, max: 5 }),
    createdAt: faker.date.past().toISOString(),
    updatedAt: faker.date.recent().toISOString()
  };
}

async function wait(timeout: number = 1000) {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}
