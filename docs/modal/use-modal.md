# 使用 NModal

<script setup>
import Sample from "./use-modal-example.vue";
</script>

<iframe data-why class="demo-container" style="height: 768px;">
  <n-spec-provider>
    <Sample />
  </n-spec-provider>
</iframe>

::: code-group

<<< @/modal/use-modal-example.vue#script{javascript} [script setup]
<<< @/modal/use-modal-example.vue#template{html} [template]
