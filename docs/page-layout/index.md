# 使用 NPageLayout

通过 NPageLayout 可以快速构建出一个典型的页面布局, 该组件可以通过 slot 自定义布局, 也提供了默认的布局.

### Slots

| 插槽名称 | 说明                                            | 类型             |
| :------- | :---------------------------------------------- | :--------------- |
| default  | 默认插槽.                                       | `v-slot:default` |
| header   | 顶部区域.                                       | `v-slot:header`  |
| content  | 内容部分, 如果可用高度不足会优先保证其高度占比. | `v-slot:content` |

> 不建议使用默认插槽, 使用具名插槽可以提供默认的布局优化

### 示例

示例中使用了如下组件, 详细使用说明请看对应的组件文档.

- **NEasyForm**: 上方的搜索表单使用的组件, 该组件可以根据配置创建带有展开收起功能的搜索表单.
  - [使用 NEasyForm](../form/use-easy-form/index.md)
- **NToolbar**: 表格上方的工具栏使用的组件, 带有对表格列的配置功能. 也可以自定义工具栏的内容.
  - [NToolbar](../toolbar/index.md)
- **NEasyTable**: 表格组件, 该组件可以根据配置创建带有分页, 排序, 筛选, 操作列等功能的表格.
  - [使用 NEasyTable](../table/use-easy-table.md)

<script setup>
import Sample from "./page-layout-example.vue";
</script>

<iframe data-why class="demo-container" style="height: 768px;">
<Sample />
</iframe>

::: code-group

<<< @/page-layout/page-layout-example.vue#script{javascript} [script setup]
<<< @/page-layout/page-layout-example.vue#template{html} [template]
