# 打印组件

使用前请仔细阅读 **➡ [Web 打印指南](./web-print-guide/index.md) ⬅**.

## NPrint

NPrint 组件可以在打印前预览打印内容, 并且可以自定义打印时的纸张大小, 边距, 分页模式等. 可以通过导出的 `print` 方法或 toolbar slot 传入的 `print` 函数来触发打印.

NPrint 组件中的打印内容需要放在 [NPrintPage](#nprintpage) 组件中. 一个 NPrint 组件可以通过包含多个 NPrintPage 组件打印多页.

###### 用法

- [标签打印(裁剪模式)](./use-tag-print.md)
- [文档打印](./use-compose-mode.md)

### Props

#### title

打印时的文档标题

- 类型: `string`

#### size

打印时的纸张大小. 支持预设的纸张大小, 以及自定义纸张大小.

- 类型: `"A5" | "A4" | "A3" | "B5" | "B4" | "JIS-B5" | "JIS-B4" | "letter" | "legal" | "ledger" | string | [string, string]`

  当传入的值不属于预设的纸张大小时, 会被当做自定义纸张大小处理. 自定义纸张大小可以通过一个字符串或由两个字符串组成的数组来指定宽高.

- 默认值: `A4`
- 示例:
  ```vue
  <!-- 自定义纸张大小 -->
  <n-print size="100mm" />
  <!-- 等价于 -->
  <n-print :size="['100mm', '100mm']" />
  ```

#### orientation

打印时的纸张方向.

- 类型: `"landscape" | "portrait"`
- 默认值: `portrait`

::: tip
纸张方向说明:

- portrait: 矩形平面以短边为底边的方向, 即高度大于等于宽度.
- landscape: 矩形平面以长边为底边的方向, 即宽度大于高度.

即当方向设置为 `landscape` 时, **始终以较长的边为底边**而无论矩形的垂直边较长还是水平边较长. `portrait` 同理.
:::

#### margin

打印时与纸张边缘的边距. 可用于避开打印时的非打印区域.

- 类型: `string | { top?: string; bottom?: string; left?: string; right?: string }`
- 默认值: `0`
- 示例:
  ```vue
  <!-- 设置所有边距 -->
  <n-print margin="10mm" />
  <!-- 设置上下边距 -->
  <n-print :margin="{ top: '10mm', bottom: '10mm' }" />
  <!-- 设置左右边距 -->
  <n-print :margin="{ left: '10mm', right: '10mm' }" />
  <!-- 设置上下左右边距 -->
  <n-print :margin="{ top: '10mm', bottom: '10mm', left: '10mm', right: '10mm' }" />
  ```

#### pagingMode

打印时的分页模式.

- 类型: `"auto" | "clip" | "compact"`
  - **`auto`**: 适用于打印报表, 表格等内容的情况. 高度会随内容自动增加.
  - **`clip`**: 适用于打印票据, 标签等内容. 固定高度并裁剪内容.
  - **`compact`**: 节省纸张. 类似 `auto` 模式, 但当一页纸能够完整渲染多个 [NPrintPage](#nprintpage) 时, 会尽量将多个内容渲染在同一页纸上, 以起到节省纸张的目的.
- 默认值: `auto`

### Slots

#### default

打印内容

- 类型: `() => VNode[]`
- 示例:
  ```vue
  <n-print>
    <n-print-page>
      <div>打印内容</div>
    </n-print-page>
  </n-print>
  ```

#### toolbar

打印预览工具栏. 可以通过 slot 传入的 `print` 函数来触发打印.

> 该插槽会在打印内容的头尾各渲染一次.

- 类型: `() => VNode[]`
- 示例:
  ```vue
  <n-print>
    <template #toolbar="{ print }">
      <n-button onClick="print">打印</n-button>
    </template>
  </n-print>
  ```

### Exported Methods

#### print

触发打印

- 类型: `() => void`
- 示例:
  ```vue
  <n-print ref="printRef" />
  <n-button onClick="printRef.print">打印</n-button>
  ```

## NPrintPage

NPrintPage 组件表示逻辑上的**一页**. 在实际打印时, 组件的内容可能会被渲染在多页纸上.

根据 [NPrint](#nprint) 组件的配置, NPrintPage 组件会自动计算出打印时的纸张大小, 边距, 分页模式等信息, 并根据这些信息来渲染打印内容.

> NPrintPage 需要作为 NPrint 的子组件使用. NPrint 组件内可以包含多个 NPrintPage 组件.

使用示例请查看 [用法](#用法) 部分.
