# LODOP 集成

## NPrintLodop

NPrintLodop 组件是 [NPrint](../index.md#nprint) 的一个 LODOP 兼容版本. 用法与 NPrint 一致. 但存在如下差异:

缺点:

- NPrintLodop 在 紧凑模式下, 不支持重复表头/表尾.
- NPrintLodop 不支持 macOS. 因为 LODOP 没有 macOS 版本.

优点:

- 在分页模式下, LODOP 可通过 LODOP 自定义的 HTML 属性实现分页小计, 分页合计等功能. 具体示例见 [LODOP 官网 - 如何打印表格的分页小计或合计](https://www.lodop.net/demolist/PrintSample31.html).

### Props

NPrintLodop 组件继承了 NPrint 的所有 props, slots 和 methods. 除此之外, NPrintLodop 新增了如下 props:

#### pagingMode

分页模式. 用于指定 LODOP 的分页模式. 除了 NPrint 的分页模式, LODOP 还支持 `table` 模式.

- 类型: `"auto" | "clip" | "compact" | "table"`
  - **`auto`**: 适用于打印报表, 表格等内容的情况. 高度会随内容自动增加.
  - **`clip`**: 适用于打印票据, 标签等内容. 固定高度并裁剪内容.
  - **`compact`**: 节省纸张. 类似 `auto` 模式, 但当一页纸能够完整渲染多个 [NPrintPage](#nprintpage) 时, 会尽量将多个内容渲染在同一页纸上, 以起到节省纸张的目的.
  - **`table`**: 适用于打印报表, 表格等内容的情况. 高度会随内容自动增加. 在打印时表格中的表头/表尾会在每页重复.
- 默认值: `auto`

#### serviceConfig

LODOP 服务配置. 用于指定 LODOP 服务的配置.

::: tip
serviceConfig 不支持动态修改. 请在初始化时设置.
:::

- 类型: [NPrintLodopServiceConfig](/types/modules.html#nprintclodopserviceconfig)
- 默认值: `{ hostname: "localhost", port: 8000, standbyPort: 18000 }`

### Events

NPrintLodop 组件继承了 NPrint 的所有 events. 除此之外, NPrintLodop 新增了如下 events:

| 事件名称 | 说明                                                                                          | 回调参数                |
| :------- | :-------------------------------------------------------------------------------------------- | :---------------------- |
| close    | LODOP 预览窗口关闭时触发. count 为 LODOP 预览窗口关闭前打印的次数, 为 0 时表示关闭窗口未打印. | (count: number) => void |

## 用法

- [标签打印(裁剪模式) LODOP](use-tag-print-lodop.md)
- [文档打印 LODOP](use-document-print-lodop.md)

## FAQ

### 如何在 LODOP 中实现分页小计/合计?

LODOP 可通过 LODOP 自定义的 HTML 属性实现分页小计, 分页合计等功能. 具体示例见 [LODOP 官网 - 如何打印表格的分页小计或合计](https://www.lodop.net/demolist/PrintSample31.html).

### 如何在 LODOP 中实现分页表头/表尾的重复?

在 LODOP 中除了将需要重复的表头/表尾放在 `thead`/`tfoot` 标签中以外,
还需要设置 [pagingMode](#pagingmode) 为 `table` 即可. 示例可参考 [文档打印 LODOP](use-document-print-lodop.md)(将分页模式设置为表格).

### 字间距为什么变大了?

::: tip
Chrome/Firefox/IE 的默认的字体大小为 16 px.
:::

![font-size-issue.jpg](font-size-issue.jpg)

选择的字体与字体大小会影响字间距. 例如, 选择 `宋体` 字体时, 字间距会变大. 选择 `微软雅黑` 字体时, 字间距会变小. 可以通过设置字体大小来调整字间距.
