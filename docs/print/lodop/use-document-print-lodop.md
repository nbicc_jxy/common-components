## 文档打印

<script setup>
import UseDocumentPrintByLodopDemo from "./UseDocumentPrintByLodopDemo.vue";

</script>

<iframe data-why style="height: 768px">
<UseDocumentPrintByLodopDemo />
</iframe>

::: details 点击查看代码

::: code-group
<<< @/print/lodop/UseDocumentPrintByLodopDemo.vue#script{javascript} [script setup]
<<< @/print/lodop/UseDocumentPrintByLodopDemo.vue#template{html} [template]
:::
