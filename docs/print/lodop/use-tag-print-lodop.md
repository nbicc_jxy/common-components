## 标签打印

<script setup>
import UsePrintDemo from "./UsePrintByLodopDemo.vue";
</script>

<iframe data-why style="height: 768px">
<UsePrintDemo />
</iframe>

::: details 点击查看代码

::: code-group
<<< @/print/lodop/UsePrintByLodopDemo.vue#script{javascript} [script setup]
<<< @/print/lodop/UsePrintByLodopDemo.vue#template{html} [template]
:::
