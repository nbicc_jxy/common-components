## 文档打印

::: tip
紧凑模式下, 可以用 [padding](./index.md#padding) 来控制打印在同一页的两个内容之间的距离.

可以在下方示例中调整**内边距**查看效果.
:::

<script setup>
import UseComposeModeDemo from "./UseComposeModeDemo.vue";
import { onMounted, ref } from "vue"; 

const href = ref("");

onMounted(() => {
  href.value = window.location.href
});
</script>

<iframe data-why :data-href="href" style="height: 768px">
<UseComposeModeDemo />
</iframe>

::: details 点击查看代码

::: code-group
<<< @/print/UseComposeModeDemo.vue#script{javascript} [script setup]
<<< @/print/UseComposeModeDemo.vue#template{html} [template]
:::
