## 标签打印

<script setup>
import UsePrintDemo from "./UsePrintDemo.vue";
</script>

<iframe data-why :data-href="href" style="height: 768px">
<UsePrintDemo />
</iframe>

::: details 点击查看代码

::: code-group
<<< @/print/UsePrintDemo.vue#script{javascript} [script setup]
<<< @/print/UsePrintDemo.vue#template{html} [template]
:::
