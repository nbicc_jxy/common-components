# Web 打印指南

### 开发前准备

- [确定打印纸张大小](#确定打印纸张大小). 如 A4, A5 等标准纸张. 或者自定义的纸张大小. **长度单位使用毫米, 厘米, 英寸等物理单位.**
- [确定打印方向](#确定打印方向). 纵向(`landscape`)或横向(`portrait`).
- [确定打印内容区域](#确定打印内容区域). 有时候需要考虑[边距或非打印区域](https://support.brother.com/g/b/faqend.aspx?c=cn&lang=zh&prod=hl4040cn_all&faqid=faq00000098_012).
  > 如下图所示, 有孔打印纸左右两侧的区域是不能打印的. 开发时可以设置左右边距避开这些区域.
  > ![有孔打印纸](./perforated_printing_paper.png)
- ~~准备一台支持在第一步确定好的纸张大小的打印机用于测试.~~ [虚拟打印机](#虚拟打印机) 使用虚拟打印机通过自定义页面大小进行测试. 下方列出了一些支持自定义纸张大小的虚拟打印机软件.
  - **windows**
    1. [clawPDF](https://www.coolutils.com/zh/ClawPDF): [自定义纸张大小](<https://github.com/clawsoftware/clawPDF/wiki/(Custom)-Paper-Sizes>).
    2. [pdf24](https://tools.pdf24.org/en/pdf-printer): 设置方式同 clawPDF.
  - **mac**: ...

如果用 NPrint 组件还需要确定分页模式.

- [确定分页模式](#确定分页模式).
  - **自动分页模式**: 适用于打印报表, 表格等内容的情况. 高度会随内容自动增加. 在打印预览时表格中的表头/表尾会根据配置决定是否在每页重复.
  - **裁剪模式**: 适用于打印票据, 标签等内容. 固定高度并裁剪溢出内容.

### 开发注意事项

下方列出了一些开发时需要注意的事项. 遵循这些规则可以避免一些常见的问题. 例如页面内预览跟浏览器预览弹框不一致, 打印内容不完整等.

#### 使用物理长度单位

由于打印动作是与物理打印机进行交互的, 所以打印内容在开发时尽量使用 **物理长度单位(例如 `mm` , `cm`, `inch`, `pt` 等)** 以避免在开发时不同屏幕分辨率带来的影响.

对于边框等细线条即使是 `1 mm` 也会很粗, 这时可以使用 `pt` 作为单位. 1 pt 约等于 0.35 mm.

单位换算: `1 inch = 25.4 mm = 2.54 cm = 72 pt`

#### 使用足够**清晰**的图片

打印时图片的清晰度不仅取决于图片的分辨率, 还取决于图片的 DPI[^4]. 一般来说, 在打印时图片需要最少 **300 DPI[^1][^2]** 才能达到较好的效果.
或将低 DPI 的图片缩小以提高单位面积的像素. 如 96 DPI 的图片缩小 32%(`(96 / x) = 300`) 既可以达到 300 DPI 的效果. **但这要求有足够大的原图**.

> DPI 实际上与图像的分辨率并无关联[^3]. 但是在打印时, 图像的 DPI 会影响打印时的清晰度.

作为替代另一种方式是使用 SVG 格式的图片[^5], SVG 格式的图片可以无限放大而不会失真. 适用于动态生成的二维码/条码场景.

[//]: # "* **条码示例**: [动态生成条码](../use-print-image.md#barcode)"
[//]: # "* **二维码示例**: [动态生成二维码](./use-print-image.md#qrcode)"

##### 字体相关

###### 字号, pt 对照表

| 序号 |    字号    | 点数(pt) | 尺寸(mm) |
| :--: | :--------: | :------: | :------: |
|  2   |   大特号   |    63    |  22.142  |
|  3   |    特号    |    54    |  18.979  |
|  4   |    初号    |    42    |  14.761  |
|  5   |   小初号   |    36    |  12.653  |
|  6   |   大一号   |   31.5   |  11.071  |
|  7   | 一（头）号 |    28    |  9.841   |
|  8   |    二号    |    21    |  7.381   |
|  9   |   小二号   |    18    |  6.326   |
|  10  |    三号    |    16    |  5.623   |
|  11  |    四号    |    14    |  4.920   |
|  12  |   小四号   |    12    |  4.218   |
|  13  |    五号    |   10.5   |  3.690   |
|  14  |   小五号   |    9     |  3.163   |
|  15  |    六号    |    8     |  2.812   |
|  16  |   小六号   |  6.875   |  2.416   |
|  17  |    七号    |   5.25   |  1.845   |
|  18  |    八号    |   4.5    |  1.581   |

其中，号数是指印刷中常用的字号，点数是指印刷中常用的点阵数，尺寸是指每个字号对应的字体大小。

### 打印机测试

#### 字体测试

打印 [字体测试页](pathname:///web-print-guide/fonts.html) 以测试打印机是否支持中文字体.

#### 偏移测试

通过打印 [测试图像](Philips_PM5544.svg){target="\_blank"} 以测试打印机是否存在偏移问题. 如果打印出的图像存在偏移, 可以通过调整打印机的偏移量来解决.

下方示例用于打印测试图像.

<script setup>
import PrinterTest from "./PrinterTest.vue";
import { onMounted, ref } from "vue"; 

const href = ref("");

onMounted(() => {
  href.value = window.location.href
});
</script>

<iframe data-why :data-href="href" style="height: 512px">
<PrinterTest />
</iframe>

## NPrint 组件说明

> 没用 NPrint 的不用看

NPrint 组件需要设置的四个参数 `size`, `orientation`, `margin`, `pagingMode` 已由[上一步](#开发前准备)准备好了.

### 处理参数

#### size

size 表示纸张打印的物理大小. 一般使用毫米, 厘米, 英寸等物理单位. 例如 A4 纸张的大小为 210mm x 297mm. 在浏览器的打印预览中可以通过选择不同的打印机来查看不同的纸张大小.
对于 `size` 属性, 你可以直接使用 `A4` 或者 `["210mm", "297mm"]` 来表示 A4 纸张的大小.

> 安装了虚拟打印机的情况下, 可以通过自定义纸张大小来测试. 详见 [虚拟打印机](#虚拟打印机).

#### orientation

orientation 表示纸张的打印方向. 有纵向(`landscape`)和横向(`portrait`)两种.

#### margin

margin 表示打印内容区域与纸张边缘的距离. 在打印场景下请使用毫米, 厘米, 英寸等物理单位以确保打印效果的准确性.
例如 A4 纸张的大小为 210mm x 297mm, 如果 margin 为 10mm, 那么打印内容区域的大小为 190mm x 277mm.

> 浏览器的打印预览窗口会根据 margin 的大小来决定是否显示 "打印页眉和页脚" 的选项. 所以当 margin 足够小时, 浏览器的打印预览窗口可能不会显示 "打印页眉和页脚" 的选项.

#### pagingMode

pagingMode 表示分页模式. 你需要在具体场景下选择合适的分页模式. 分页模式详情见 [pagingMode](../index.md#pagingmode).

在自动分页模式下, 当 table 内容超过一页时, 多余的部分会被渲染到下一页. 如果需要在每页重复表头/表尾, 你要将需要重复的内容放到 `thred`/`tfoot` 中.

[^1]: [打印照片用 300DPI 就已到人眼能分辨的极限？ - Eric Liu 的回答 - 知乎](https://www.zhihu.com/question/19750752/answer/12846038)
[^2]: [How to get Perfect Print Quality? What is DPI/PPI and why does it matter?](https://deep-image.ai/blog/how-to-get-higher-print-quality-what-is-dpi-ppi/)
[^3]: [Image DPI on the Web](https://apptools.com/examples/dpi.php)
[^4]: [What is the difference between PPI and DPI?](https://99designs.com/blog/tips/ppi-vs-dpi-whats-the-difference/)
[^5]: [Blurry web graphics on smartphone - double image size or resolution?](https://stackoverflow.com/questions/31701744/blurry-web-graphics-on-smart-phone-double-image-size-or-resolution)
