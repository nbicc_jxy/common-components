# 设置占位选项

通过设置 `placeholderOptions` 可以设置占位选项. 占位选项会被添加在选择器中, 但不会出现在选择器的下拉菜单.

占位选项可以解决选择器的 options 为空但 value 仅绑定了值时界面需要显示 label 的问题.

<script setup>
import { reactive } from "vue";

const modelData = reactive({
  name: "jack",
  nameList: ["jack", "lucy"]
});
</script>

<iframe data-why class="demo-container">
  <a-form :model="modelData">
    <a-form-item name="name" label="名称">
      <NSelect :placeholder-options="{label: 'Jack', value: 'jack'}" v-model:value="modelData.name" />
    </a-form-item>
    <a-form-item name="nameList" label="名称列表">
      <NSelect :placeholder-options="[{label: 'Jack', value: 'jack'}, {label: 'Lucy', value: 'lucy'}]" mode="multiple" v-model:value="modelData.nameList" />
    </a-form-item>
  </a-form>
</iframe>

::: code-group

```javascript [script setup]
import { reactive } from "vue";

const modelData = reactive({
  name: "jack",
  nameList: ["jack", "lucy"]
});
```

```vue [template]
<a-form :model="modelData">
  <a-form-item name="name" label="名称">
    <NSelect :placeholder-options="{label: 'Jack', value: 'jack'}" v-model:value="modelData.name" />
  </a-form-item>
  <a-form-item name="nameList" label="名称列表">
    <NSelect :placeholder-options="[{label: 'Jack', value: 'jack'}, {label: 'Lucy', value: 'lucy'}]" mode="multiple" v-model:value="modelData.nameList" />
  </a-form-item>
</a-form>
```

:::
