# 选择器组件

## NSelect

::: tip
NSelect 组件是对 ant-design-vue Select 组件的二次封装, 除了支持 ant-design-vue Select 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

###### Props

| 参数               | 说明                                 | 类型               | 可选值 | 默认值 | 必填 |
| :----------------- | :----------------------------------- | :----------------- | :----- | :----- | ---- |
| ...                | ant-design-vue Select 组件的所有参数 | ...                | ...    | ...    | ...  |
| placeholderOptions | 占位选项. [示例](custom-label.md)    | `object, []object` | -      | []     | 否   |

###### Events

| 事件名称       | 说明                                 | 回调参数 |
| :------------- | :----------------------------------- | :------- |
| ...            | ant-design-vue Select 组件的所有事件 | ...      |
| scrollToBottom | 滚动到底部时触发的事件               | function |

## NAutoloadSelect

NAutoloadSelect 组件支持下拉加载功能, 默认情况下传入一个用于获取选择项的函数即可.

###### Props

| 参数           | 说明                   | 类型                                                                       | 可选值 | 默认值 | 必填 |
| :------------- | :--------------------- | :------------------------------------------------------------------------- | :----- | :----- | ---- |
| ...            | NSelect 组件的所有参数 | ...                                                                        | ...    | ...    | ...  |
| loadRecord     | 用于获取选择项的函数   | [LoadSelectOptionsFunction](/types/modules.html#loadselectoptionsfunction) | -      | -      | 是   |
| pageStartIndex | 分页起始页码           | `number`                                                                   | -      | 1      | 否   |
| pageSize       | 每页的数据条数         | `number`                                                                   | -      | 100    | 否   |
