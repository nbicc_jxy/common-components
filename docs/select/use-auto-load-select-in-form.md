<script setup>
import { reactive, ref, watch } from "vue";
import { NForm } from "@/components/n-form";
import { FormItemControlType } from "@/components/form-helper";
import { requestPosts } from "../mock";

const modelData = ref({});
watch(modelData, () => console.log(modelData.value));

async function loadRecord(pagination, filter, sorter, extra) {
  const { current, pageSize } = pagination;
  const { abortSignal } = extra;

  return await requestPosts(current, pageSize, 1000);
}

const fields = ref([
  [
    { name: "autoLoadSelect", label: "自动加载下拉框", type: FormItemControlType.AUTO_LOAD_SELECT },
    { loadRecord, fieldNames: { label: "name", value: "id" } }
  ],
]);
</script>

<iframe data-why class="demo-container" style="height: 512px">
  <NForm v-model:data="modelData" :fields="fields" />
</iframe>

::: code-group

```javascript
import { reactive, ref, watch } from "vue";
import { NForm, FormItemControlType } from "@nbicc/common-components";
import { requestPosts } from "../mock";

const modelData = ref({});
watch(modelData, () => console.log(modelData.value));

async function loadRecord(pagination, filter, sorter, extra) {
  const { current, pageSize } = pagination;
  const { abortSignal } = extra;

  return await requestPosts(current, pageSize, 1000);
}

const fields = ref([
  [
    { name: "autoLoadSelect", label: "自动加载下拉框", type: FormItemControlType.AUTO_LOAD_SELECT },
    { loadRecord, fieldNames: { label: "name", value: "id" } }
  ]
]);
```

```vue
<NForm v-model:data="modelData" :fields="fields" />
```

:::
