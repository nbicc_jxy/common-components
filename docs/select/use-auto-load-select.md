# 使用 NAutoLoadSelect

::: tip
选项回显请参考 [设置占位选项](./custom-label.md)
:::

<script setup>
import { reactive } from "vue";
import { NAutoLoadSelect } from "@/components/n-select";
import {requestPosts} from "../mock";

const modelData = reactive({
  id: 0
});

async function loadRecord(pagination, filter, sorter, extra) {
  const { current, pageSize } = pagination;
  const { abortSignal } = extra;

  return await requestPosts(current, pageSize, 1000);
}
</script>

<iframe data-why class="demo-container" style="height: 512px">
  <a-form :model="modelData">
    <a-form-item name="name" label="名称">
      <NAutoLoadSelect v-model:value="modelData.id" :placeholder-options="[{name: 'Jack', id: 0}, {name: 'Lucy', id: 1}]" :page-size="10" :load-record="loadRecord" placeholder="请选择" :field-names="{ label: 'name', value: 'id' }"></NAutoLoadSelect>
    </a-form-item>
  </a-form>
</iframe>

::: code-group

```javascript
import { reactive } from "vue";
import { NAutoLoadSelect } from "@nbicc/common-components";
import { requestPosts } from "../mock";

const modelData = reactive({
  name: undefined
});

async function loadRecord(pagination, filter, sorter, { abortSignal }) {
  const { current, pageSize } = pagination;
  return await requestPosts(current, pageSize, 5000);
  // 测试空数据
  // return { data: [], total: 0 };
}
```

```vue
<a-form :model="modelData">
  <a-form-item name="name" label="名称">
    <NAutoLoadSelect v-model:value="modelData.name" :page-size="10" :load-record="loadRecord" placeholder="请选择" :field-names="{ label: 'name', value: 'id' }" />
  </a-form-item>
</a-form>
```

:::
