# 全局化的规范配置

提供配置项使组件库符合[前端开发规范](https://juejin.cn/post/7211348566396272700).

## NSpecProvider {#NSpecProvider}

`NSpecProvider` 使用 Vue 的 `provide/inject` 机制, 在应用外围包裹一层 `NSpecProvider` 组件即可生效.

```vue
<template>
  <n-spec-provider :form="formConfig">
    <app />
  </n-spec-provider>
</template>
<script setup>
const formConfig = { labelWidth: "100px" };
</script>
```

## 可选的配置项

### form {#form}

`form` 配置项用于配置 `NForm` 组件的默认配置.

#### form.labelWidth {#form-labelWidth}

表单项标签宽度, 默认为 `90px`.

- 类型: `string`, `number`

#### form.labelAlign {#form-labelAlign}

表单项标签对齐方式, 默认为 `right`.

- 类型: `left`, `right`

#### form.colon {#form-colon}

表单项标签后是否显示冒号, 默认为 `true`.

- 类型: `boolean`

#### form.allowClear {#form-allowClear}

表单项是否允许清除, 默认为 `true`.

- 类型: `boolean`

#### form.maxlength {#form-maxlength}

表单项最大长度. 只对支持的组件有效.

- 类型: `number`
- 默认值: `100`

#### form.placeholder {#form-placeholder}

表单项的占位符, 可以为字符串或字符串数组.

- 类型: `string`, `string[]`

#### form.modifiers {#form-modifiers}

[值的修饰符](https://cn.vuejs.org/guide/essentials/forms.html#modifiers)

- 类型: `{ trim: boolean, number: boolean, lazy: boolean }`

#### form.field {#form-field}

单独定义某个表单项类型的配置.

- 类型: { [FormItemControlType](/types/enums/FormItemControlType.html): [NSpecProviderFormItemConfig](/types/interfaces/NSpecProviderFormItemConfig.html#interface-nspecproviderformitemconfig) }
- 示例:
  ```javascript
  const formConfig = {
    field: {
      [FormItemControlType.INPUT]: {
        allowClear: false,
        placeholder: "请输入",
        maxlength: 100,
        modifiers: { trim: true }
      }
    }
  };
  ```

### easyForm {#easyForm}

`easyForm` 配置项与 [form](#form) 相同但用于 `NEasyForm` 组件的默认配置.

#### easyForm.defaultExpandRowCount {#easyForm-defaultExpandRowCount}

未展开时显示的行数, 可以通过对象的方式设置不同屏幕尺寸.

- 类型: `number | { xs: number, sm: number, md: number, lg: number, xl: number, xxl: number }`
- 默认值: `{ lg: 1, xl: 2 }`

#### easyForm.beforeSearch {#easyForm-beforeSearch}

表单项搜索前的钩子函数, 参考 [beforeSearch](/types/modules.html#neasyformprops-1).

- 类型: `(data: Record<string, any>) => boolean | Promise<boolean>>`
- 示例:
  ```javascript
  const config = {
    easyForm: {
      beforeSearch: (params) => {
        console.log(params);
        return true;
      }
    }
  };
  ```

### upload {#upload}

`upload` 配置项用于配置 `NUpload` 组件的默认配置.

#### upload.limit {#upload-limit}

上传文件的限制条件, 参考 [NUploadLimit](/types/interfaces/NUploadLimit.html).

- 类型: [NUploadLimit](/types/interfaces/NUploadLimit.html)

#### upload.accept {#upload-accept}

上传文件的类型, 参考 [accept](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#accept).

- 类型: `string`

#### upload.action {#upload-action}

上传文件的地址.

- 类型: `string`

#### upload.method {#upload-method}

上传文件的方法.

- 类型: `string`

#### upload.customRequest {#upload-customRequest}

自定义上传文件的方法, 参考 [customRequest](https://ant.design/components/upload-cn/#customRequest).

- 类型: `(options: UploadRequestOptions) => void`

### table {#table}

`table` 配置项用于配置 `NTable` / `NEasyTable` 组件的默认配置.

#### table.ellipsisTooltip {#table-ellipsis-tooltip}

当鼠标悬浮在开启了 `ellipsis` 的单元格上时，是否将完整内容用 Tooltip 组件显示. 默认为 false.

- 类型: `boolean` | [TooltipProps](https://3x.antdv.com/components/tooltip-cn#API)
  可通过传入 `TooltipProps` 对象来自定义 Tooltip 组件的属性.
- 默认值: `false`
