# 存储状态数据

组件库内置状态存储, 用于存储一些可以提升用户体验的的数据, 例如表格调整后的列宽信息, 自定义的显示列信息等等. 默认情况下组件不会使用状态存储.

状态存储的数据是以 `storageKey` 属性作为键名存储的, 因此如果组件没有设置 `storageKey` 属性, 则无法使用状态存储.

**当组件的 `storageKey` 属性不为空时会开启状态存储.**

::: danger 注意
**`storageKey` 属性的值必须是唯一的, 否则会导致数据冲突.**
:::

###### 禁用状态存储

对于某些本身就使用了 `storageKey` 属性的组件, 如果不希望启用状态存储, 可以通过设置 `disabled-storable` 属性来禁用状态存储.

```html
<Component disabled-storable />
```

## 保存/恢复状态数据

组件库提供 [StorageManager](/types/classes/SimpleStorageManager.html) 类来管理状态数据, 通过 [recovery](/types/classes/SimpleStorageManager.html#recovery)/[toString](/types/classes/SimpleStorageManager.html#toString) 可以恢复/保存状态数据.

###### 保存状态数据

状态数据可以在 [页面卸载 (beforeunload)](https://developer.mozilla.org/docs/Web/API/Window/beforeunload_event) 或用户登出前保存.

`toString` 接口返回的数据可以存储在本地(例如 `localStorage`), 也可以通过 http 接口发送到服务器存储.

```javascript
import { StorageManager } from "@nbicc/common-components";

// 保存状态存储的数据, 可以在页面卸载或用户登出前保存.
window.addEventListener("beforeunload", () => {
  const storagedData = StorageManager.toString();
  localStorage.setItem("__KEY__", storagedData);
});
```

###### 恢复状态数据

状态数据可以在 [页面加载 (load)](https://developer.mozilla.org/docs/Web/API/Window/load_event) 或用户登录后恢复.

```javascript
// 恢复状态存储的数据, 可以在页面加载或用户登录后恢复.
const storagedData = localStorage.getItem("__KEY__");
StorageManager.recovery(storagedData);
```

## 状态存储可用的组件

| 组件                         | 支持存储的数据                           | 说明                     |
| ---------------------------- | ---------------------------------------- | ------------------------ |
| [Table](/table/index.md)     | 列宽                                     | 调整列宽时会自动记录列宽 |
| [Toolbar](/toolbar/index.md) | 表格列设置窗口中的列宽/固定方向/是否隐藏 |                          |

## 示例

<script setup>
import Sample from "./storage-example.vue";
</script>

<iframe data-why class="demo-container" style="height: 768px">
<Sample></Sample>
</iframe>

::: code-group

<<< @/storage/storage-example.vue#template{13,32 html} [template]
<<< @/storage/storage-example.vue#script{javascript} [script setup]
