<script setup>
import { NEasyTable, useAntDesignVueTableHelper } from "@/components/n-table";
import {requestPosts} from "../mock";

// 初始化 table helper
const {
tableHelper,
columns,
onChange,
rowSelection,
searchForm,
} = useAntDesignVueTableHelper(requestDataSource, [
    {
        title: 'Checkbox',
        dataIndex: 'checkbox',
        fixed: 'left',
    },
    {
        title: 'Id',
        dataIndex: 'id',
        fixed: 'left',
    },
    {
        title: 'Name',
        dataIndex: 'name',
        sorter: true,
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Operator',
        dataIndex: 'operator',
    },
], 'id');
// 立即加载数据
tableHelper.loadRecord(true);

// 隐藏默认的选择框
rowSelection.columnWidth = '0';
rowSelection.columnTitle = ' ';
// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  return await requestPosts(current, pageSize, 1000);
}
</script>

# 自定义行选择框

<iframe data-why class="demo-container" style="height: 512px">
  <a-form :model="searchForm.data" layout="inline" style="margin-bottom: 16px">
    <a-form-item name="name" label="名称">
      <a-input v-model:value="searchForm.data.name" placeholder="请输入" />
    </a-form-item>
    <a-form-item name="description" label="描述">
      <a-input v-model:value="searchForm.data.description" placeholder="请输入" />
    </a-form-item>
    <a-form-item style="margin-right: 0">
      <a-space>
        <a-button @click="searchForm.reset">重置</a-button>
        <a-button type="primary" @click="searchForm.search">查询</a-button>
      </a-space>
    </a-form-item>
  </a-form>
  <NEasyTable style="flex: 1 1 auto;" :table-helper="tableHelper" :columns="columns" :row-selection="rowSelection" @change="onChange">
    <template #headerCell="{record, column}">
      <template v-if="column.dataIndex === 'checkbox'">
        <a-checkbox
          :checked="rowSelection.isAllSelected(record)"
          @change="rowSelection.selectAll()"></a-checkbox>
      </template>
    </template>
    <template #bodyCell="{record, column}">
      <template v-if="column.dataIndex === 'checkbox'">
        <a-checkbox
          :checked="rowSelection.isSelected(record)"
          @change="rowSelection.select(record)"></a-checkbox>
      </template>
    </template>
  </NEasyTable>
</iframe>

::: code-group

```javascript
import { NEasyTable, useAntDesignVueTableHelper } from "@nbicc/common-components";

import {requestPosts} from "../mock";

// 初始化 table helper
const {
  tableHelper,
  columns,
  onChange,
  rowSelection,
  searchForm,
} = useAntDesignVueTableHelper(requestDataSource, [
  {
    title: 'Checkbox',
    dataIndex: 'checkbox',
    fixed: 'left',
  },
  {
    title: 'Id',
    dataIndex: 'id',
    fixed: 'left',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    sorter: true,
  },
  {
    title: 'Description',
    dataIndex: 'description',
  },
  {
    title: 'Operator',
    dataIndex: 'operator',
  },
], 'id');
// 立即加载数据
tableHelper.loadRecord(true);

// 隐藏默认的选择框
rowSelection.columnWidth = '0';
rowSelection.columnTitle = ' ';
// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  return await requestPosts(current, pageSize, 1000);
}
```

```vue
<a-form :model="searchForm.data" layout="inline" style="margin-bottom: 16px">
  <a-form-item name="name" label="名称">
    <a-input v-model:value="searchForm.data.name" placeholder="请输入" />
  </a-form-item>
  <a-form-item name="description" label="描述">
    <a-input v-model:value="searchForm.data.description" placeholder="请输入" />
  </a-form-item>
  <a-form-item style="margin-right: 0">
    <a-space>
      <a-button @click="searchForm.reset">重置</a-button>
      <a-button type="primary" @click="searchForm.search">查询</a-button>
    </a-space>
  </a-form-item>
</a-form>
<NEasyTable style="flex: 1 1 auto;" :table-helper="tableHelper" :columns="columns" :row-selection="rowSelection" @change="onChange">
  <template #headerCell="{record, column}">
    <template v-if="column.dataIndex === 'checkbox'">
      <a-checkbox
        :checked="rowSelection.isAllSelected(record)"
        @change="rowSelection.selectAll()"></a-checkbox>
    </template>
  </template>
  <template #bodyCell="{record, column}">
    <template v-if="column.dataIndex === 'checkbox'">
      <a-checkbox
        :checked="rowSelection.isSelected(record)"
        @change="rowSelection.select(record)"></a-checkbox>
    </template>
  </template>
</NEasyTable>
```
