# 可编辑表格

<script setup>
import EditableRow from "./editable-row.vue";
import EditableRowByApi from "./editable-row-by-api.vue";
import EditableRowByRule from "./editable-row-by-rule.vue";
import EditableRowBySlot from "./editable-row-by-slot.vue";
</script>

## 编辑行

<iframe data-why class="demo-container" style="height: 512px">
<EditableRow />
</iframe>

::: code-group

<<< @/table/editable-table/editable-row.vue#script{javascript} [script setup]
<<< @/table/editable-table/editable-row.vue#template{html} [template]

:::

## 手动控制编辑行

可通过设置 `editByClickRow` 为 `false` 来禁用点击行编辑功能, 然后通过 `editableRowKey` 和 `editRow` 等方法来手动控制编辑行.

以下示例中, 通过 `editedRowKey` 设置默认编辑 `id` 为 `1` 的行. 另可通过操作列的按钮来控制编辑行.

<iframe data-why class="demo-container" style="height: 512px">
<EditableRowByApi />
</iframe>

::: code-group

<<< @/table/editable-table/editable-row-by-api.vue#script{javascript} [script setup]
<<< @/table/editable-table/editable-row-by-api.vue#template{html} [template]

:::

## 添加校验规则

校验规则可以通过 `fields` 属性来配置, 与 [NForm](../../form/use-form.md) 的 fields 属性一致.

<iframe data-why class="demo-container" style="height: 512px">
<EditableRowByRule />
</iframe>

::: code-group

<<< @/table/editable-table/editable-row-by-rule.vue#script{javascript} [script setup]
<<< @/table/editable-table/editable-row-by-rule.vue#template{html} [template]

:::

## 使用 slot 自定义控件

NTable 组件提供了 `bodyEditableCell` 和 `bodyEditableCellControl` 两个 slot 来自定义编辑状态下的单元格和单元格控件.

`bodyEditableCell` vs `bodyEditableCellControl`:

- `bodyEditableCell` 的优先级高于 `bodyEditableCellControl`.
- `bodyEditableCell` 用于自定义编辑状态下的单元格, 可以通过 slot 传出的 `validateInfos` 来获取校验信息.
- `bodyEditableCellControl` 用于自定义编辑状态下的单元格中的输入控件, 会保留表单校验功能.

##### slot 参数

| 参数               | 说明                                                                                                    | 类型        |
| ------------------ | ------------------------------------------------------------------------------------------------------- | ----------- |
| ...                | ...                                                                                                     | -           |
| field              | 编辑状态下该单元格对应的表单项配置                                                                      | -           |
| validateInfos      | 编辑状态下该单元格中表单项的校验信息                                                                    | -           |
| **popupContainer** | **_editByClickRow 为 ture 时_**, 如果自定义元素有弹出框(如: Select, TimePicker)请将弹出层容器设置为该值 | HTMLElement |

以下示例中, Name 列使用了 `bodyEditableCellControl` 来自定义输入控件, 并保留了校验功能. Level 列使用了 `bodyEditableCell` 来自定义单元格, 并通过 `validateInfos` 来获取校验信息.

<iframe data-why class="demo-container" style="height: 512px">
  <EditableRowBySlot />
</iframe>

::: code-group

<<< @/table/editable-table/editable-row-by-slot.vue#script{javascript} [script setup]
<<< @/table/editable-table/editable-row-by-slot.vue#template{html} [template]

:::
