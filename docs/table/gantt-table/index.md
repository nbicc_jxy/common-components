# 甘特图表格

## 介绍

下表为甘特图表格的相关属性和方法. 甘特图表格是在 [NTable](../index.md) 的基础上增加了编辑功能.

通过 `editable` 属性来控制是否启用编辑功能. 当启用编辑功能时, 可通过 `editableRowKey` 属性来控制当前编辑的行. 通过 `fields` 属性来配置可编辑的字段.

[//]: #
[//]: # "###### Props"
[//]: #
[//]: # "| 参数                    | 说明                                                                                                                            | 类型                                                                                                 | 默认值 |"
[//]: # "| :---------------------- | :------------------------------------------------------------------------------------------------------------------------------ | :--------------------------------------------------------------------------------------------------- | :----- |"
[//]: # "| editable                | 是否可以通过点击行来将改行变为编辑状态                                                                                          | boolean                                                                                              | false  |"
[//]: # "| editByClickRow          | 是否启用编辑功能                                                                                                                | boolean                                                                                              | true   |"
[//]: # "| editableRowKey(v-model) | 当前编辑的行的 key                                                                                                              | string, number                                                                                       | -      |"
[//]: # "| fields                  | 当可编辑时, 用于配置可编辑的字段. 与 [NForm](../../form/use-form.md) 的 fields 属性一致. [示例](./use-editable-table.md#编辑行) | Array<[FormModelFieldArgs](/types/modules.html#formmodelfieldargs)>                                  | -      |"
[//]: # '| bodyEditableCell        | 自定义编辑状态下的单元格. [示例](./use-editable-table.md#使用slot自定义控件)                                                    | v-slot:bodyEditableCell="{text, record, index, column, field, validateInfos, popupContainer}"        | -      |'
[//]: # '| bodyEditableCellControl | 自定义编辑状态下的单元格控件, 并可以保留校验状态.                                                                               | v-slot:bodyEditableCellControl="{text, record, index, column, field, validateInfos, popupContainer}" | -      |'
[//]: #
[//]: # "###### 方法"
[//]: #
[//]: # "| 方法名            | 说明                                                                                                                                                             | 参数                                                      |"
[//]: # "| :---------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------- |"
[//]: # "| saveEditedRow     | 将当前编辑行的数据保存到原始数据中. 可传入一个 boolean 控制保存前是否需要经过校验, 默认为 `true`. **注: 返回值是一个 Promise, 等待其完成后原始数据数据才会更改** | (needValidate: boolean) => Promise<void>                |"
[//]: # "| cancelEditRow     | 取消当前编辑行的编辑状态.                                                                                                                                        | () => void                                                |"
[//]: # "| editRow           | 编辑指定行. 如果存在正在编辑的行, 则会先保存正在编辑的行. `needValidate` 同 `saveEditedRow`                                                                      | (rowKey: string | number, needValidate: boolean) => void |"
[//]: # "| setEditedRowField | 设置当前编辑行数据的某个字段的值. 如果不在编辑状态, 则不会生效. `name` / `value` 分别为要修改的属性名和属性值                                                    | (name: string, value: any) => void                        |"
