# 可编辑表格

<script setup>
import BasicGanttTable from "./basic-gantt-table.vue";
</script>

## 基础用法

<iframe data-why class="demo-container" style="height: 512px">
<BasicGanttTable />
</iframe>

::: code-group

<<< @/table/gantt-table/basic-gantt-table.vue#script{javascript} [script setup]
<<< @/table/gantt-table/basic-gantt-table.vue#template{html} [template]

:::
