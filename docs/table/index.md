# 表格组件

## NTable

::: tip
NTable 组件是对 ant-design-vue Table 组件的二次封装, 除了支持 ant-design-vue Table 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

| 参数                   | 说明                                                                         | 类型                                                      | 默认值 | 必填 | 示例                                    |
| :--------------------- | :--------------------------------------------------------------------------- | :-------------------------------------------------------- | :----- | ---- | --------------------------------------- |
| ...                    | ant-design-vue Table 组件的所有参数                                          | ...                                                       | ...    | ...  |                                         |
| disabledRowKeys        | 禁用行的 key 数组, 如启用了 `rowSelection` 会在行前面显示一个禁用的 checkbox | Array                                                     | -      | ×    |                                         |
| columnConfig           | 列配置, 用于配置列的属性, 如是否可调整列宽                                   | [ColumnConfig](/types/interfaces/NTableColumnConfig.html) | -      | ×    | [可伸缩列宽](./resizable-column.md)     |
| disabledAdaptiveHeight | 禁用自适应高度, 禁用后, 表格的内容(tbody)高度将不会随着容器的高度变化而变化. | boolean                                                   | false  | ×    |                                         |
| striped                | 是否启用斑马纹                                                               | boolean                                                   | false  | ×    |                                         |
| editable               | 是否启用编辑模式                                                             | boolean                                                   | false  | ×    | [可编辑表格](./editable-table/index.md) |

## NEasyTable

NEasyTable 组件简化了 NTable 组件的使用, 只需要传入 `useAntDesignVueTableHelper` 的返回值即可.

###### Props

| 参数        | 说明                                  | 类型                      | 可选值 | 默认值 | 必填 |
| :---------- | :------------------------------------ | :------------------------ | :----- | :----- | ---- |
| ...         | ant-design-vue Table 组件的所有参数   | ...                       | ...    | ...    | ...  |
| tableHelper | `useAntDesignVueTableHelper` 的返回值 | `AntDesignVueTableHelper` | -      | -      | 是   |

[组件示例](use-easy-table.md)

### useAntDesignVueTableHelper

`useAntDesignVueTableHelper` 是一个用于初始化 NTable 组件的辅助函数, 用于简化 NTable 组件的使用.

[函数签名](/types/modules.html#useantdesignvuetablehelper)
