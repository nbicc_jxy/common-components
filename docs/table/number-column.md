# 序号列

`NTable` 提供 `showNumberColumn` 参数显示序号列.

- **类型:** `boolean | object`
  当 `showNumberColumn` 为对象时, 可以配置序号列的属性如下:

  - `title`: 序号列的标题
    - 默认值: `序号`
  - `startIndex`: 序号列的起始序号
    - 默认值: `1`
  - `width`: 序号列的宽度
    ::: tip
    当序号列的宽度不足以显示内容时, 会将超出的内容隐藏而不是显示省略号(`...`)
    :::
    - 默认值: `80`
  - ...还可以接受 Ant Design Table Column 中的所有属性, 详见 [Column](https://www.antdv.com/components/table#Column).

- **默认值:** `false`
  - 当值为 `false` 时不显示序号列.
  - 当值为 `true` 时与 `{ title: '序号', startIndex: 1, width: 80 }` 等价.

<script setup>
import { reactive, ref } from "vue";
import { requestPosts } from "../mock";

const dataSource = ref([]);
requestDataSource();

const total = ref(0);
const columns = [
    {
        title: "Id",
        dataIndex: "id",
    },
    {
        title: "Name",
        dataIndex: "name",
    },
    {
        title: "Description",
        dataIndex: "description",
    },
    {
        title: "Operator",
        dataIndex: "operator",
    },
];

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}
</script>

<iframe data-why class="demo-container" style="height: 512px">
  <NTable row-key="id" :data-source="dataSource" :total="total" :columns="columns" show-number-column />
</iframe>

::: code-group

```javascript [script setup]
import { reactive, ref } from "vue";
import { requestPosts } from "../mock";

const dataSource = ref([]);
requestDataSource();

const total = ref(0);
const columns = [
  {
    title: "Id",
    dataIndex: "id"
  },
  {
    title: "Name",
    dataIndex: "name"
  },
  {
    title: "Description",
    dataIndex: "description"
  },
  {
    title: "Operator",
    dataIndex: "operator"
  }
];

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}
```

```vue [template]
<NTable
  row-key="id"
  :data-source="dataSource"
  :total="total"
  :columns="columns"
  show-number-column
/>
```
