# 可伸缩列宽

`NTable` 提供 `columnsConfig` 参数用于配置列的属性, 如是否可调整列宽.

可通过设置 `columnsConfig` 的 `resizable` 属性来启用列宽可伸缩功能, 设置 `columnsConfig` 的 `columns` 属性单独指定某个列的可伸缩属性.

也可单独设置列的 `resizable` 属性来指定某个列的可伸缩属性.

<script setup>
import NTable from "@/components/n-table/NTable.vue";
import AButton from "ant-design-vue/lib/button";
import { reactive, ref } from "vue";
import { requestPosts } from "../mock";

const dataSource = ref([]);
requestDataSource();

const total = ref(0);
const columns = [
    {
        title: "Id",
        dataIndex: "id",
    },
    {
        title: "Name",
        dataIndex: "name",
    },
    {
        title: "Description",
        dataIndex: "description",
    },
    {
        title: "Operator",
        dataIndex: "operator",
    },
];

// 设置所有字段可伸缩, 除了 name 字段
const columnConfig = {
  resizable: true,
  columns: {
    name: {
      resizable: false
    }
  }
};

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}
</script>

<iframe data-why class="demo-container" style="height: 512px">
<NTable storage-key="resizable-column" row-key="id" :data-source="dataSource" :total="total" :columns="columns" :column-config="columnConfig">
  <template #bodyCell="{ record, column }">
    <span v-if="column.dataIndex === 'operator'">
      <AButton @click="handleClick(record)"> action </AButton>
    </span>
  </template>
</NTable>
</iframe>

::: code-group

```javascript {30-38} [script setup]
import { NTable } from "@nbicc/common-components";
import AButton from "ant-design-vue/lib/button";

import { reactive, ref } from "vue";
import { requestPosts } from "../mock";

const dataSource = ref([]);
requestDataSource();

const total = ref(0);
const columns = [
  {
    title: "Id",
    dataIndex: "id"
  },
  {
    title: "Name",
    dataIndex: "name"
  },
  {
    title: "Description",
    dataIndex: "description"
  },
  {
    title: "Operator",
    dataIndex: "operator"
  }
];

// 设置所有字段可伸缩, 除了 name 字段
const columnConfig = {
  resizable: true,
  columns: {
    name: {
      resizable: false
    }
  }
};

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}
```

```vue [template]
<NTable
  storage-key="resizable-column"
  row-key="id"
  :data-source="dataSource"
  :total="total"
  :columns="columns"
  :column-config="columnConfig"
>
  <template #bodyCell="{ record, column }">
      <span v-if="column.dataIndex === 'operator'">
        <AButton @click="handleClick(record)"> action </AButton>
      </span>
  </template>
</NTable>
```
