<script setup>
import NTable from "@/components/n-table/NTable.vue";
import { Button, Space } from "ant-design-vue";
import { EditOutlined } from "@ant-design/icons-vue";

import { reactive, ref } from 'vue';
import { requestPosts } from "../mock";

const dataSource = ref([]);
const total = ref(0);
requestDataSource();
const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
    },
    {
        title: 'Name',
        dataIndex: 'name',
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Operator',
        dataIndex: 'operator',
    },
];

const rowSelection = reactive({
  selectedRowKeys: [],
  onChange: (selectedRowKeys, selectedRows) => {
    rowSelection.selectedRowKeys = selectedRowKeys;
  },
});

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}

function handleClick(record) {
  console.log(record);
}
</script>

# 通过点击行进行选择

::: tip
默认情况下, 点击行中的可交互元素不会触发选择行为, 你可以通过设置 `@click.stop` 来阻止事件冒泡以阻止选择行为.

所有可交互元素: **`"A", "AUDIO", "BUTTON", "DETAIL", "EMBED", "IFRAME", "IMG", "INPUT", "KEYGEN", "LABEL", "MENU", "OBJECT", "SELECT", "TEXTAREA", "VIDEO"`**
:::

<iframe data-why class="demo-container" style="height: 512px">
<NTable :data-source="dataSource" :total="total" :columns="columns" :row-selection="rowSelection" row-key="id">
  <template #bodyCell="{ record, column }">
    <Space v-if="column.dataIndex === 'operator'">
      <Button @click="handleClick(record)"> action </Button>
      <NLoadingButton @loading-click="requestDataSource">
        <template #icon><EditOutlined /></template>
      </NLoadingButton>
    </Space>
  </template>
</NTable>
</iframe>

::: code-group

```javascript
import NTable from "@/components/n-table/NTable.vue";
import { Button, Space } from "ant-design-vue";
import { EditOutlined } from "@ant-design/icons-vue";

import { reactive, ref } from "vue";
import { requestPosts } from "../mock";

const dataSource = ref([]);
const total = ref(0);
requestDataSource();
const columns = [
  {
    title: "Id",
    dataIndex: "id"
  },
  {
    title: "Name",
    dataIndex: "name"
  },
  {
    title: "Description",
    dataIndex: "description"
  },
  {
    title: "Operator",
    dataIndex: "operator"
  }
];

const rowSelection = reactive({
  selectedRowKeys: [],
  onChange: (selectedRowKeys, selectedRows) => {
    rowSelection.selectedRowKeys = selectedRowKeys;
  }
});

async function requestDataSource() {
  const data = await requestPosts(1, 10);
  dataSource.value = data.data;
  total.value = data.total;
}

function handleClick(record) {
  console.log(record);
}
```

```vue
<NTable
  :data-source="dataSource"
  :total="total"
  :columns="columns"
  :row-selection="rowSelection"
  row-key="id"
>
<template #bodyCell="{ record, column }">
  <Space v-if="column.dataIndex === 'operator'">
    <Button @click="handleClick(record)"> action </Button>
    <NLoadingButton @loading-click="requestDataSource">
      <template #icon><EditOutlined /></template>
    </NLoadingButton>
  </Space>
</template>
</NTable>
```
