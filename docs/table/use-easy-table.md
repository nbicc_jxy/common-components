# 使用 NEasyTable

## NEasyTable

`NEasyTable` 组件需要与 [useAntDesignVueTableHelper](#useAntDesignVueTableHelper) hook 配合使用, 可以简化 NTable 组件的使用并提供统一的表格行为逻辑.

### `useAntDesignVueTableHelper(loadRecord, columns, KeyName, options)` {#useAntDesignVueTableHelper}

- **类型**:
  ```typescript
  function useAntDesignVueTableHelper(
    loadRecord: LoadTableRecordFunction,
    columns: NTableColumn[],
    recordKeyName: string,
    options?: { pageSize?: number; pageStartIndex?: number; showPagination?: boolean }
  ): {
    tableHelper: TableHelper;
    columns: Ref<NTableColumn[]>;
    onChange: (
      pagination: { current: number; pageSize: number },
      filters: Record<string, any>,
      sorter: Array<{ direction: "ascend" | "descend"; field: string }>
    ) => void;
    rowSelection: TableRowSelection;
    searchForm: TableSearchForm;
  };
  ```
  ::: details 查看子类型定义
  ```typescript
  type LoadTableRecordFunction = (
    pagination: { current: number; pageSize: number },
    filter: Record<string, any>,
    sorter: Array<{ direction: "ascend" | "descend"; field: string }>
  ) => Promise<{ data: any[]; total: number }>;

  type NTableColumn = {
    title: string;
    dataIndex: string;
    sorter: boolean;
    ellipsis: boolean;
    width: number | string;
    fixed: "left" | "right";
    visible: boolean;
  };

  type TableRowSelection = {
    selectedRowKeys: string[];
    selectedRows: any[];
    reset: () => void;
  };

  type TableSearchForm = {
    search: () => void;
    reset: () => void;
    data: Record<string, any>;
  };
  ```
  :::
  > 为了便于阅读, 对类型进行了简化
- **参数说明**:

  1. `loadRecord`: 用于加载数据, 该函数接收三个参数如下.

     - `pagination`: 分页参数, 有 `current` 和 `pageSize` 两个参数.
     - `filter`: 筛选参数, 一般会跟搜索表单的数据绑定, 是一个以表单项的 `name` 为 key, 值为 value 构成的对象.
     - `sorter`: 排序参数, 是由多个 `direction` 和 `field` 两个参数组成的对象构成的数组. **目前只支持单个排序参数, 即 sorter 数组中最多只会有一个数组项.**
     - **返回值**: 该函数需要返回一个类型为 `Promise<{ data: [], total: number }>` 的对象. 其中 `data` 为表格数据, `total` 为表格数据总数.

  2. `columns` 用于生成表格列, 表格列参数与 [antdv 的 table 组件的 Column](https://www.antdv.com/components/table#Column)
     参数一致. 但新增了以下参数.
     - `visible`: 用于控制列的显示和隐藏, 默认为 `true`.
  3. `rowKey` 用于指定表格行数据的 key.
  4. `options`
     - `pageSize`: 指定每页显示的条数, 默认为 `10`.
     - `pageStartIndex`: 指定页码的起始值, 默认为 `1`.
     - `showPagination`: 是否显示分页器, 默认为 `true`.

- **返回值说明**:
  - `tableHelper`: 传入 NEasyTable 中. 也可以直接使用, 见 [使用 TableHelper 章节](./use-table-helper.md)
  - `columns`: 直接用于 antdv 的 table 组件. 同时也可用于 [NToolbar](../toolbar/index.md) 组件的 `tableColumns` 属性用于筛选列功能.
  - `onChange`: 直接用于 antdv 的 table 组件的 `onChange` 属性.
  - `rowSelection`: 直接用于 antdv 的 table 组件. 提供跨页保留选择行的能力和 `reset` 用于重置已选择的行.
  - `searchForm`: 用于导出搜索表单数据对象, 并提供 `search`, `reset` 用于触发请求和重置表单.
- **示例**:
  <script setup>
  import UseEasyTable from './use-easy-table.vue';
  </script>

  <iframe data-why class="demo-container" style="height: 512px">
  <UseEasyTable />
  </iframe>

  ::: code-group

  <<< @/table/use-easy-table.vue#script{javascript} [script setup]
  <<< @/table/use-easy-table.vue#template{html} [template]

  :::
