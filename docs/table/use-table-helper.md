# 使用 TableHelper

TableHelper 提供了一些常用的 api 接口, 例如：分页、排序、筛选、搜索等.

## API

| 名称                                                                     | 说明               |
| :----------------------------------------------------------------------- | ------------------ |
| [firstPage](/types/classes/TableHelper.TableHelper.html#firstpage)       | 跳转到第一页       |
| [lastPage](/types/classes/TableHelper.TableHelper.html#lastpage)         | 跳转到最后一页     |
| [nextPage](/types/classes/TableHelper.TableHelper.html#nextpage)         | 跳转到下一页       |
| [previousPage](/types/classes/TableHelper.TableHelper.html#previouspage) | 跳转到上一页       |
| [skipToPage](/types/classes/TableHelper.TableHelper.html#skiptopage)     | 跳转到指定页码     |
| [reloadPage](/types/classes/TableHelper.TableHelper.html#reloadpage)     | 重新请求当前页数据 |

## 示例

<script setup>
import {ref} from "vue";
import { useAntDesignVueTableHelper } from "@nbicc/common-components";
import {requestPosts} from "../mock";

// 初始化 table helper
const {
  tableHelper,
  columns,
  onChange,
  rowSelection,
  searchForm,
} = useAntDesignVueTableHelper(requestDataSource, [
    {
        title: 'Id',
        dataIndex: 'id',
        fixed: 'left',
        ellipsis: true,
    },
    {
        title: 'Name',
        dataIndex: 'name',
        sorter: true,
        ellipsis: true,
    },
    {
        title: 'Description',
        dataIndex: 'description',
        ellipsis: true,
    },
    {
        title: 'Operator',
        dataIndex: 'operator',
        ellipsis: true,
    },
], 'id');

// 立即加载数据
tableHelper.loadRecord(true);

// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  const { data, total } = await requestPosts(current, pageSize, 1000);
  return { data, total }
}
</script>

<iframe data-why class="demo-container" style="height: 512px">
  <n-toolbar>
    <n-button @click="tableHelper.firstPage()">跳转到第一页</n-button>
    <n-button @click="tableHelper.lastPage()">跳转到最后一页</n-button>
    <n-button @click="tableHelper.nextPage()">跳转到下一页</n-button>
    <n-button @click="tableHelper.previousPage()">跳转到上一页</n-button>
    <n-button @click="tableHelper.reloadPage()">刷新</n-button>
  </n-toolbar>
  <n-easy-table :table-helper="tableHelper" :columns="columns" @change="onChange" :row-selection="rowSelection" />
</iframe>

::: code-group

```javascript [script setup]
import { ref } from "vue";
import { useAntDesignVueTableHelper } from "@nbicc/common-components";
import { requestPosts } from "../mock";

// 初始化 table helper
const { tableHelper, columns, onChange, rowSelection, searchForm } = useAntDesignVueTableHelper(
  requestDataSource,
  [
    {
      title: "Id",
      dataIndex: "id",
      fixed: "left",
      ellipsis: true
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: true,
      ellipsis: true
    },
    {
      title: "Description",
      dataIndex: "description",
      ellipsis: true
    },
    {
      title: "Operator",
      dataIndex: "operator",
      ellipsis: true
    }
  ],
  "id"
);

// 立即加载数据
tableHelper.loadRecord(true);

// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  const { data, total } = await requestPosts(current, pageSize, 1000);
  return { data, total };
}
```

```html [template]
<n-toolbar>
  <n-button @click="tableHelper.firstPage()">跳转到第一页</n-button>
  <n-button @click="tableHelper.lastPage()">跳转到最后一页</n-button>
  <n-button @click="tableHelper.nextPage()">跳转到下一页</n-button>
  <n-button @click="tableHelper.previousPage()">跳转到上一页</n-button>
  <n-button @click="tableHelper.reloadPage()">刷新</n-button>
</n-toolbar>
<n-easy-table
  :table-helper="tableHelper"
  :columns="columns"
  @change="onChange"
  :row-selection="rowSelection"
/>
```

:::
