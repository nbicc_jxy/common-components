# 工具栏组件

## NToolbar

##### Props

| 参数               | 说明                                                                                                             | 类型                                                                            | 可选值 | 默认值 | 必填 |
| :----------------- | :--------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------ | :----- | :----- | ---- |
| tableColumns       | 表格列数组, 用于控制显示隐藏. 通过 `update:tableColumns` 事件更新. 需要使用 `v-model:tableColumns` 双向绑定生效. | Array<[AntDesignVueTableHelperColumnType](#AntDesignVueTableHelperColumnType))> | -      | 否     |
| disabledColumnKeys | 指定哪些表格列是不可配置的.                                                                                      | `Array<string>`                                                                 | -      | 否     |
| exportAsFile       | 传入一个导出按钮被点击时的回调函数并显示导出按钮. 默认不显示导出按钮.                                            | `() => void`                                                                    | -      | 否     |

##### Slots

| 插槽名称  | 说明                                        | 类型                                                   |
| :-------- | :------------------------------------------ | :----------------------------------------------------- |
| default   | 默认插槽, 主要区域. 用于放置主要的工具按钮. | `v-slot:default`                                       |
| secondary | 次要区域, 用于放置次要的工具按钮.           | `v-slot:secondary`                                     |
| common    | 通用区域, 用于放置通用的工具按钮.           | `v-slot:common="{ toolsNode }"`, toolsNode: 内置的按钮 |

##### 类型说明

###### AntDesignVueTableHelperColumnType {#AntDesignVueTableHelperColumnType}

AntDesignVueTableHelperColumnType 在 ant-design-vue 的基础上增加了 `visible` 属性, 用于控制表格列的显示隐藏. 详细请参考 [AntDesignVueTableHelperColumnExtraOptions](/types/modules.html#antdesignvuetablehelpercolumnextraoptions)
