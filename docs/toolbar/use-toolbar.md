<script setup>
import {ref} from "vue";
import { FilterOutlined } from "@ant-design/icons-vue";
import { useAntDesignVueTableHelper } from "@nbicc/common-components";

import {requestPosts} from "../mock";

// 初始化 table helper
const {
  tableHelper,
  columns,
  onChange,
  rowSelection,
  searchForm,
} = useAntDesignVueTableHelper(requestDataSource, [
    {
        title: 'Id',
        dataIndex: 'id',
        fixed: 'left',
        ellipsis: { showTitle: true },
    },
    {
        title: 'Name',
        dataIndex: 'name',
        sorter: true,
        ellipsis: { showTitle: true },
    },
    {
        title: 'Description',
        dataIndex: 'description',
        ellipsis: { showTitle: true },
    },
    {
        title: 'Operator',
        dataIndex: 'operator',
        ellipsis: { showTitle: true },
    },
], 'id');

// 立即加载数据
tableHelper.loadRecord(true);

// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  return await requestPosts(current, pageSize, 1000);
  // 测试空数据
  // return { data: [], total: 0 };
}
</script>

# 使用工具栏

工具栏分为三个区域 `主要区域`, `次要区域` 以及 `通用区域`

<NToolbar :table-columns="[]">
  <div style="color: #2f54eb">主要区域</div>
  <template #secondary><div style="color: gray">次要区域</div></template>
  <template #common>通用区域</template>
</NToolbar>

`主要区域`, `次要区域` 用于放置自定义的业务按钮, `通用区域` 则用于放置通用的工具按钮一般不用修改, 例如筛选列按钮.

<iframe data-why class="demo-container" style="height: 768px">
  <NToolbar v-model:table-columns="columns" :disabled-column-keys="['id']" :export-as-file="() => requestPosts(1, 1, 1000)">
    <NButton type="primary">主要区域</NButton>
    <template #secondary>
      <NButton type="primary" @click="() => requestPosts(1, 1, 1000)">
        次要区域
        <template #icon><FilterOutlined /></template>
      </NButton>
      <NButton type="primary" danger>按钮</NButton>
      <NButton type="primary" ghost>按钮</NButton>
    </template>
  </NToolbar>
  <NEasyTable :table-helper="tableHelper" :columns="columns" @change="onChange" :row-selection="rowSelection" />
</iframe>

::: code-group

```javascript [script setup]
import { ref } from "vue";
import { FilterOutlined } from "@ant-design/icons-vue";
import { useAntDesignVueTableHelper } from "@nbicc/common-components";

import { requestPosts } from "../mock";

// 初始化 table helper
const { tableHelper, columns, onChange, rowSelection, searchForm } = useAntDesignVueTableHelper(
  requestDataSource,
  [
    {
      title: "Id",
      dataIndex: "id",
      fixed: "left",
      ellipsis: { showTitle: true }
    },
    {
      title: "Name",
      dataIndex: "name",
      sorter: true,
      ellipsis: { showTitle: true }
    },
    {
      title: "Description",
      dataIndex: "description",
      ellipsis: { showTitle: true }
    },
    {
      title: "Operator",
      dataIndex: "operator",
      ellipsis: { showTitle: true }
    }
  ],
  "id"
);

// 立即加载数据
tableHelper.loadRecord(true);

// 模拟请求
async function requestDataSource(pagination, filter, sorter) {
  const { current, pageSize } = pagination;
  return await requestPosts(current, pageSize, 1000);
  // 测试空数据
  // return { data: [], total: 0 };
}
```

```html [template]
<NToolbar
  v-model:table-columns="columns"
  :disabled-column-keys="['id']"
  :export-as-file="() => requestPosts(1, 1, 1000)"
>
  <NButton type="primary">按钮</NButton>
  <template #secondary>
    <NButton type="primary" @click="() => requestPosts(1, 1, 1000)">
      按钮
      <template #icon>
        <FilterOutlined />
      </template>
    </NButton>
    <NButton type="primary" danger>按钮</NButton>
    <NButton type="primary" ghost>按钮</NButton>
  </template>
</NToolbar>

<NEasyTable
  :table-helper="tableHelper"
  :columns="columns"
  @change="onChange"
  :row-selection="rowSelection"
/>
```

:::

## 自定义通用区域

<iframe data-why class="demo-container" style="height: 768px">
  <NToolbar v-model:table-columns="columns" :disabled-column-keys="['id']" :export-as-file="() => requestPosts(1, 1, 1000)">
    <NButton type="primary">主要区域</NButton>
    <template #secondary>
      <NButton type="primary" @click="() => requestPosts(1, 1, 1000)">
        次要区域
        <template #icon><FilterOutlined /></template>
      </NButton>
      <NButton type="primary" danger>按钮</NButton>
      <NButton type="primary" ghost>按钮</NButton>
    </template>
    <template #common="{ toolsNode: tools }">
      <component :is="tools" />
      <NButton type="primary" ghost>自定义的通用区域按钮</NButton>
    </template>
  </NToolbar>
  <NEasyTable :table-helper="tableHelper" :columns="columns" @change="onChange" :row-selection="rowSelection" />
</iframe>

::: code-group

```html [template] {14-17}
<NToolbar
  v-model:table-columns="columns"
  :disabled-column-keys="['id']"
  :export-as-file="() => requestPosts(1, 1, 1000)"
>
  <NButton type="primary">主要区域</NButton>
  <template #secondary>
    <NButton type="primary" @click="() => requestPosts(1, 1, 1000)">
      次要区域
      <template #icon>
        <FilterOutlined />
      </template>
    </NButton>
    <NButton type="primary" danger>按钮</NButton>
    <NButton type="primary" ghost>按钮</NButton>
  </template>
  <template #common="{ toolsNode: tools }">
    <component :is="tools" />
    <NButton type="primary" ghost>自定义的通用区域按钮</NButton>
  </template>
</NToolbar>
```

:::
