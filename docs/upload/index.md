# 上传组件

## NUpload

::: tip
NUpload 同 antdv Upload 组件的二次封装, 同 antdv Upload 组件的所有参数/事件/方法/插槽外, 还支持以下参数.
:::

###### Props

| 参数                               | 说明                                                            | 类型                                                | 可选值 | 默认值 | 必填 |
| :--------------------------------- | :-------------------------------------------------------------- | :-------------------------------------------------- | :----- | :----- | ---- |
| ... 同 antdv Upload 组件的所有参数 | ...                                                             | ...                                                 | ...    | ...    |
| limit                              | 上传文件的限制条件, 用于限制上传文件的大小和宽高. 详见下方说明. | [NUploadLimit](/types/interfaces/NUploadLimit.html) | -      | `{}`   | 否   |

## NUploadButton

NUploadButton 组件以按钮形式上传文件, 不会显示已上传的文件列表, 但会在按钮上显示上传进度. 适用于只上传一个文件的场景.

[组件示例](./upload-button.md)

## NUploadPictureCard

NUploadPictureCard 组件以卡片形式上传图片, 会显示已上传的图片列表, 并支持预览/删除/重新上传等操作. 适用于上传图片的场景.

[组件示例](./upload-picture-card.md)

## 设置全局的上传处理函数

[NSpecProvider 组件](../spec-provider/index.md)的 upload 属性可以设置全局的上传处理函数, 该函数会在 NUpload/NUploadButton/NUploadPictureCard 组件的 customRequest 属性未设置时生效.

upload 属性支持以下参数:

| 参数          | 说明                                                    | 类型                                                | 默认值 |
| :------------ | :------------------------------------------------------ | :-------------------------------------------------- | :----- |
| accept        | 上传文件的类型, 同 antdv Upload 组件的 accept 属性      | string                                              | -      |
| action        | 上传的地址, 同 antdv Upload 组件的 action 属性          | string                                              | -      |
| method        | 上传的请求方法, 同 antdv Upload 组件的 method 属性      | string                                              | -      |
| customRequest | 上传处理函数, 同 antdv Upload 组件的 customRequest 属性 | (options: UploadRequestOption) => void              | -      |
| limit         | 上传文件的限制条件, 用于限制上传文件的大小和宽高.       | [NUploadLimit](/types/interfaces/NUploadLimit.html) | `{}`   |

::: tip

- 以上参数都可以被 NUpload/NUploadButton/NUploadPictureCard 组件的同名参数覆盖.
- 如果需要在上传文件时显示进度条, 需要在 customRequest 函数中调用 options.onProgress 方法.
  :::

### 示例

<script setup>
import { UploadOutlined } from "@ant-design/icons-vue";

const uploadConfig = {
  accept: "image/*",
  action: "xxxx",
  method: "POST",
  customRequest: async (options) => {
    const { action, method, file } = options;

    // 上传文件
    try {
      // 上传中
      options.onProgress({ percent: 80 });

      await new Promise((resolve) => setTimeout(resolve, 1000));
      
      // 上传成功
      options.onSuccess({ url: '' });
    } catch (e) {
      // 上传失败
      options.onError(e);
    }
  },
}
</script>

<iframe data-why class="demo-container">
  <n-spec-provider :upload="uploadConfig">
    <n-upload-button>
      <UploadOutlined />
      点击上传
    </n-upload-button>
  </n-spec-provider>
</iframe>

::: code-group

```javascript [script setup]
const uploadConfig = {
  accept: "image/*",
  action: "xxxx",
  method: "POST",
  customRequest: (options) => {
    const { action, method, file } = options;

    // 上传文件
    try {
      // 上传中
      options.onProgress({ percent: 80 });
      // 上传成功
      options.onSuccess({ url: "" });
    } catch (e) {
      // 上传失败
      options.onError(e);
    }
  }
};
```

```html [template]
<n-spec-provider :upload="uploadConfig">
  <n-upload-button>
    <UploadOutlined />
    点击上传
  </n-upload-button>
</n-spec-provider>
```

:::
