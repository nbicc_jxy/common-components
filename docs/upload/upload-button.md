# NUploadButton

<script setup>
import axios from 'axios';
import { ref } from "vue";
import { UploadOutlined } from "@ant-design/icons-vue";

const handleCustomRequest = async (options) => {
  const file = options.file;
  const formData = new FormData();
  formData.append('file', file);

  try {
    await axios.post(options.action, formData, {
      onUploadProgress: (e) => {
        options.onProgress({ percent: e.loaded / e.total * 100 });
      }
    });
    options.onSuccess({ url: '' });
  } catch (e) {
    options.onError(e);
  }
};

const fileList = ref([]);
</script>

<iframe data-why class="demo-container">
  <n-upload-button action="https://run.mocky.io/v3/3cf541ae-6a76-479e-9ee1-e3036d3d842f" v-model:fileList="fileList" :custom-request="handleCustomRequest">
    <UploadOutlined />
    点击上传
  </n-upload-button>
</iframe>

::: code-group

```javascript [script setup]
import axios from "axios";
import { ref } from "vue";
import { UploadOutlined } from "@ant-design/icons-vue";

const handleCustomRequest = async (options) => {
  const file = options.file;
  const formData = new FormData();
  formData.append("file", file);

  try {
    await axios.post(options.action, formData, {
      onUploadProgress: (e) => {
        options.onProgress({ percent: (e.loaded / e.total) * 100 });
      }
    });
    options.onSuccess({ url: "" });
  } catch (e) {
    options.onError(e);
  }
};

const fileList = ref([]);
```

```html [template]
<n-upload-button
  action="https://run.mocky.io/v3/3cf541ae-6a76-479e-9ee1-e3036d3d842f"
  v-model:fileList="fileList"
  :custom-request="handleCustomRequest"
>
  <UploadOutlined />
  点击上传
</n-upload-button>
```

:::
