# NUploadPictureCard

<script setup>
import axios from 'axios'; import { ref } from "vue"; 

const handleCustomRequest = async (options) => {
  const file = options.file;
  const formData = new FormData();
  formData.append('file', file);

  axios.post(options.action, formData, {
    onUploadProgress: (e) => {
      options.onProgress({ percent: e.loaded / e.total * 100 });
    }
  }).then(res => {
    options.onSuccess({ url: '' });
  }).catch(err => {
    options.onError(err);
  })
}

const fileList = ref([
  { url: "https://gw.alipayobjects.com/zos/antfincdn/LlvErxo8H9/photo-1503185912284-5271ff81b9a8.webp"},
  { url: "https://gw.alipayobjects.com/zos/antfincdn/LlvErxo8H9/photo-1503185912284-5271ff81b9a8.webp" , status: "error" },
]);
</script>

<iframe data-why class="demo-container">
  <n-upload-picture-card action="https://run.mocky.io/v3/3cf541ae-6a76-479e-9ee1-e3036d3d842f" v-model:fileList="fileList" :custom-request="handleCustomRequest" />
</iframe>
