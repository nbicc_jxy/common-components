/**
 * 指定文件环境
 * @see https://eslint.org/docs/latest/use/configure/language-options#using-configuration-comments
 */
/* eslint-env node */

import { request } from "https";

const options = {
  hostname: "registry-direct.npmmirror.com",
  path: "/@nbicc/common-components/sync?publish=false&nodeps=false",
  method: "PUT"
};

const req = request(options, (res) => {
  let chunks = Buffer.from([]);
  res.on("data", (chunk) => (chunks = Buffer.concat([chunks, chunk])));

  res.on("end", () => {
    const result = JSON.parse(chunks.toString());

    console.log(`sync ${result.ok ? "success" : "failed"}`);
    process.exit(result.ok ? 0 : 1);
  });
});

req.on("error", (error) => {
  console.error(error);
  process.exit(1);
});

req.end();
