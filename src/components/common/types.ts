import type { ExtractPropTypes } from "vue";

export type ExtendProps<T1, T2> = Partial<ExtractPropTypes<T1>> & T2;

/**
 * 从 T 中挑选出值类型为 V 的属性
 */
export type PickByValueType<T, V> = {
  [P in keyof T as T[P] extends V | undefined ? P : never]: T[P];
};
