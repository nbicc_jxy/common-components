import type { FormItemControlArgs } from "./form-item-control";
import type { FormModelFieldArgs } from "./form-helper";
import type { FormItemOptions } from "./form-item";
import { isArray } from "lodash-es";

/**
 * @deprecated
 * 提取表单项的属性, 用于创建表单项, 以及表单项的控件.
 * @param field
 */
export function extractProps<TYPE>(field: FormModelFieldArgs<TYPE>): {
  formItemOptions: Pick<FormItemOptions, "name"> & Partial<FormItemOptions>;
  formItemControlArgs: FormItemControlArgs<TYPE>;
} {
  if (!isArray(field)) field = [field];

  // 第一个元素是表单项的属性, 后面的元素是表单项的控件的额外属性
  const [fieldOptions, ...rest] = field;
  const { name, label, rules, required, ...restProps } = fieldOptions;

  return {
    formItemOptions: {
      name,
      label,
      rules,
      required
    },
    formItemControlArgs: [restProps, ...rest]
  };
}
