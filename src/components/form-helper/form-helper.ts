import { cloneDeep, isArray, merge, isNil } from "lodash-es";
import type { FormItemControlArgs, FormItemControlOptions } from "./form-item-control";
import type { FormItemOptions } from "./form-item";
import { FormItem } from "./form-item";
import { FormItemControlType } from "./types";
import { FormValidator } from "./validator";

export class FormHelper {
  protected static readonly defaultFormHelperOptions: FormHelperOptions = {
    data: {},
    presets: [],
    validator: new FormValidator()
  };

  constructor(options: Partial<FormHelperOptions> = {}) {
    const _options = { ...FormHelper.defaultFormHelperOptions, ...options };

    this.preset = this.mergePresets(_options.presets);
    this.data = options.data ?? {};
    this.rawData = cloneDeep(this.data);
    this.validator = _options.validator;
  }

  // options: InnerFormHelperOptions;

  preset: Partial<Record<FormItemControlType, any>>;
  fields: Array<FormItem> = [];
  data: Record<string, any>;
  rawData: Record<string, any>;
  validator: FormValidator;

  /**
   * 表单项控件值变化后的回调函数.
   */
  onFormItemControlChange: (name: string, value: any) => void = () => {};
  onValidateError: (error: any) => void = () => {};

  addFields(...fields: Array<FormModelFieldArgs<any>>) {
    const _fields = this.transformFieldOption(fields);

    const fieldMap: Record<string, FormItem> = {};
    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];
      fieldMap[field.options.name] = field;
    }

    for (let i = _fields.length; i--; ) {
      const field = _fields[i];

      const originField = fieldMap[field.options.name];
      if (originField) {
        field.value = originField.value;
        this.fields.splice(i, 1, field);
      } else {
        this.fields.push(field);
      }
    }

    this.postInitial();
  }

  removeFields(...names: Array<string>) {
    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];
      if (names.includes(field.options.name)) {
        this.fields.splice(i, 1);
      }
    }
  }

  /**
   * 设置表单数据对象. 该方法会将表单数据对象中的值赋值到表单项控件中.
   * @param data
   */
  setRawData(data: Record<string, any>) {
    this.rawData = { ...data };

    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];

      field.value = this.rawData[field.options.name];
      delete this.rawData[field.options.name];

      field.assignToFormData(this.rawData);
    }
  }

  /**
   * 设置表单项集合.
   * @param fields 表单项的集合, 用于描述表单的结构.
   */
  setFields(fields: Array<FormModelFieldArgs<any>>) {
    this.validateOptions(fields);

    this.fields = this.transformFieldOption(fields);
    this.validator.updateLinkedFieldGroup(this);

    this.postInitial();

    // 继承原有的表单项值
    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];

      field.value = this.rawData[field.options.name];

      // 当值为空时, 重置表单项的值为 `initialValue`.
      if (isNil(field.value)) field.reset();
    }
  }

  /**
   * 根据 `initialValue` 重置表单项的值
   */
  reset() {
    this.resetFields();
  }

  /**
   * 设置表单项控件的值. 该方法会将表单项控件的值赋值到表单数据对象中, 并更新界面.
   * @param name
   * @param value
   */
  setField(name: string, value: any) {
    const field = this.getField(name);
    if (!field) return;

    field.value = value;
    field.onValueChange(value);
  }

  /**
   * 获取与 `name` 对应的表单项控件对象.
   * @param name
   */
  getField(name: string): FormItem | undefined {
    return this.fields.find((field) => field.options.name === name);
  }

  /**
   * 重置表单项控件值为初始值.
   */
  protected resetFields(): void {
    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];
      field.reset();
    }
  }

  /**
   * 校验传入的表单项集合是否合法.
   * @protected
   * @param fields
   */
  protected validateOptions(fields: Array<FormModelFieldArgs<any>>) {
    if (!isArray(fields)) throw new Error("表单项集合不能为空");

    for (let i = fields.length; i--; ) {
      const item = fields[i];
      const field = isArray(item) ? item[0] : item;
      if (!field.name) throw new Error(`第${i + 1}个表单项的 name 不能为空`);
    }
  }

  /**
   * 根据传入的表单项集合, 创建表单项对象.
   * @protected
   * @param fields
   */
  protected transformFieldOption(
    fields: Array<FormModelFieldArgs<any>>
  ): InnerFormHelperOptions["fields"] {
    const _fields: InnerFormHelperOptions["fields"] = [];
    const len = fields.length;
    for (let i = 0; i < len; i++) {
      const field = fields[i];
      const { formItemOptions, formItemControlArgs } = this.extractProps(field);
      _fields.push(this.buildFormItem(formItemOptions, formItemControlArgs));
    }

    return _fields;
  }

  /**
   * 合并预设值
   * @param presets
   * @protected
   */
  protected mergePresets(
    presets: FormHelperOptions["presets"]
  ): Partial<Record<FormItemControlType, any>> {
    const _presets: Partial<Record<FormItemControlType, any>> = {};
    for (let i = presets.length; i--; ) {
      const preset = presets[i];

      merge(_presets, preset);
    }

    return _presets;
  }

  /**
   * 初始化完成后执行的方法.
   * @protected
   */
  protected postInitial(): void {
    for (let i = this.fields.length; i--; ) {
      const field = this.fields[i];
      // 表单字段变化后将其同步到 data
      field.onValueChange = (value: any) => {
        field.assignToFormData(this.rawData);
        this.onFormItemControlChange(field.options.name, value);

        this.validator
          .validateLinkedField(field.options.name)
          .catch((error) => this.onValidateError(error));
      };
    }
  }

  /**
   * 创建表单项对象
   * @param options
   * @param control
   * @protected
   */
  protected buildFormItem<TYPE>(
    options: Pick<FormItemOptions, "name"> & Partial<Omit<FormItemOptions, "name">>,
    control: FormItemControlArgs<TYPE>
  ): FormItem {
    return new FormItem(options, control);
  }

  /**
   * 提取表单项的属性, 用于创建表单项, 以及表单项的控件.
   * @param field
   */
  protected extractProps<TYPE>(field: FormModelFieldArgs<TYPE>): {
    formItemOptions: Pick<FormItemOptions, "name"> & Partial<FormItemOptions>;
    formItemControlArgs: FormItemControlArgs<TYPE>;
  } {
    if (!isArray(field)) field = [field];

    // 第一个元素是表单项的属性, 后面的元素是表单项的控件的额外属性
    const [fieldOptions, controlProps, ...rest] = field;
    const { type, initialValue, modifiers, events, ...restProps } = fieldOptions;

    // 提取控件的预设值
    const controlPreset = this.preset[type ?? FormItemControlType.INPUT];
    const { events: presetEvents = {}, modifiers: presetModifiers = {} } = controlPreset ?? {};

    return {
      formItemOptions: cloneDeep(restProps),
      formItemControlArgs: [
        {
          type,
          initialValue,
          modifiers: { ...presetModifiers, ...modifiers },
          // 合并控件预设的 events 值和传入的 events 属性
          events: { ...presetEvents, ...events }
        },
        { ...controlPreset, ...controlProps },
        ...rest
      ]
    };
  }
}

export interface InnerFormHelperOptions {
  fields: Array<FormItem>;
  data: Record<string, any>;
}

export interface FormHelperOptions {
  /**
   * 表单数据对象
   */
  data: Record<string, any>;
  /**
   * 表单项的预设值, 用于设置控件的默认属性.
   */
  presets: Array<Partial<Record<FormItemControlType, any>>>;
  /**
   * 表单验证器
   */
  validator: FormValidator;
}

/**
 * 表单项的集合, 用于描述表单的结构. 接受两种类型的参数: FormModelField 和 [FormModelField, ...args].
 * @example FormModelField
 * {
 *  name: "username",
 *  label: "用户名",
 *  type: "input",
 *  initialValue: "admin"
 *  }
 * @example [FormModelField, ...args]
 * [
 *  {
 *    name: "username",
 *    label: "用户名",
 *    type: "input",
 *    initialValue: "admin"
 *  },
 *  {
 *    maxlength: 11
 *  }
 * ]
 */
export type FormModelFieldArgs<TYPE> = FormModelField<TYPE> | [FormModelField<TYPE>, ...Array<any>];
export type FormModelField<TYPE> = Pick<FormItemOptions, "name"> &
  Partial<Omit<FormItemOptions, "control" | "name"> & FormItemControlOptions<TYPE>>;
