import { cloneDeep, merge } from "lodash-es";
import { FormItemControlType } from "./types";

export class FormItemControl<TYPE> {
  protected static readonly defaultFormItemControlOptions: FormItemControlOptions = {
    type: FormItemControlType.INPUT,
    initialValue: null,
    events: {},
    slots: {},
    modifiers: {
      trim: false,
      number: false,
      lazy: false
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    const _options = cloneDeep(FormItemControl.defaultFormItemControlOptions);
    this.options = merge(_options, options);
  }

  options: FormItemControlOptions<TYPE>;

  private __value: TYPE | undefined;
  get value(): TYPE {
    return this.__value ?? this.options.initialValue;
  }
  set value(value: TYPE) {
    this.__value = value;
  }
  onValueChange: (value: TYPE) => void = () => {};

  /**
   * 重置控件值为初始值. 初始值来自 options.initialValue
   */
  reset() {
    this.value = this.options.initialValue;
    this.onValueChange(this.value);
  }
}

export type FormItemControlArgs<TYPE> = [Partial<FormItemControlOptions<TYPE>>, ...Array<any>];

export interface FormItemControlOptions<TYPE = any> {
  /**
   * 控件类型
   */
  type: FormItemControlType;
  /**
   * 控件初始值, 重置表单时会使用该值做为表单的初始值.
   */
  initialValue: TYPE;

  /**
   * [值的修饰符](https://cn.vuejs.org/guide/essentials/forms.html#modifiers)
   */
  modifiers: Partial<{
    trim: boolean;
    number: boolean;
    lazy: boolean;
  }>;

  events: Record<string, any>;
  slots: Record<string, any>;
}
