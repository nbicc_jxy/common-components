import { FormItemControl } from "./form-item-control";
import type { FormItemControlArgs, FormItemControlOptions } from "./form-item-control";
import { isFunction, isString, merge } from "lodash-es";
import type { Rule } from "ant-design-vue/lib/form";
import type { VNode } from "vue";
import type { BuildInValidator } from "./validator";

export class FormItem<TYPE = any> {
  protected static readonly defaultFormItemOptions: FormItemOptions = {
    name: "",
    label: "",
    required: false,
    rules: [],
    valueMapTo: "",
    events: {},
    slots: {}
  };

  constructor(
    options: Pick<FormItemOptions, "name"> & Partial<Omit<FormItemOptions, "name">>,
    control: FormItemControlArgs<TYPE>
  ) {
    this.options = merge(
      {
        // valueMapTo 默认值为 name 属性值
        valueMapTo: options.name
      },
      FormItem.defaultFormItemOptions,
      options
    );

    const [, ...args] = control;
    this.control = this.buildFormItemControl(control[0], ...args);
    this.control.onValueChange = (value: TYPE) => this.onValueChange(value);
  }

  options: FormItemOptions;

  /**
   * 表单项控件对象. 用于描述表单项的控件类型, 控件值, 控件属性, 事件等.
   */
  control: FormItemControl<TYPE>;

  get value(): TYPE {
    return this.control.value;
  }
  set value(value: TYPE) {
    this.control.value = value;
  }
  onValueChange: (value: TYPE) => void = () => {};

  /**
   * 将该表单项控件的值赋值到指定的表单数据对象中.
   * @param data
   */
  assignToFormData(data: Record<string, any>): void {
    const valueMapTo = this.options.valueMapTo;

    if (isString(valueMapTo) && valueMapTo.length > 0) {
      data[valueMapTo] = this.value;
    } else if (isFunction(valueMapTo)) {
      valueMapTo(this.value, data, this.options);
    } else {
      data[this.options.name] = this.value;
    }
  }

  /**
   * 重置表单项控件值为初始值.
   */
  reset() {
    this.control.reset();
  }

  /**
   * 创建表单项控件对象
   * @param options
   * @param args
   * @protected
   */
  protected buildFormItemControl(
    options: Partial<FormItemControlOptions<TYPE>>,
    ...args: Array<any>
  ): FormItemControl<TYPE> {
    return new FormItemControl<TYPE>(options, ...args);
  }
}

export interface FormItemOptions {
  /**
   * 表单项名称, 用于表单项的唯一标识.
   * **必填项**
   */
  name: string;
  /**
   * 表单项标签
   */
  label: string;
  /**
   * 表单项是否必填
   */
  rules: Array<
    Omit<Rule, "validator"> & {
      validator?: (...args: any) => any;
      buildInValidator?: BuildInValidator;
    }
  >;
  required: boolean;
  /**
   * @deprecated 无法解决映射到多个未定义属性时的双向绑定问题.
   * 表单项控件的值如何映射到表单绑定的 data 中. 默认为该表单项的 name 属性值.
   *
   * * 当值为字符串时, 表示映射到 data 中的字段名.
   * * 当值为函数时, 可自定义映射逻辑. 函数参数为表单项控件的值和表单项控件的原始数据.
   *   ```typescript
   *   // 将表单项控件的值映射到 data 中的 field1 字段
   *   valueMapTo: (value) => {
   *     data["field1"] = value;
   *   }
   *   ```
   */
  valueMapTo:
    | string
    | (<TYPE>(value: TYPE, rawData: Record<string, any>, options: FormItemOptions) => void);
  /**
   * 表单项标签后缀
   */
  labelSuffix?: (props: { field: FormItem }) => Array<VNode> | VNode;
  /**
   * 表单项标签前缀
   */
  labelPrefix?: (props: { field: FormItem }) => Array<VNode> | VNode;
  events: Record<string, any>;
  slots: Record<string, any>;
}
