export enum FormItemControlType {
  INPUT = "input",
  INPUT_NUMBER = "input-number",
  SELECT = "select",
  AUTO_LOAD_SELECT = "auto-load-select",
  DATE_PICKER = "date-picker",
  DATE_RANGE_PICKER = "date-range-picker",
  TIME_PICKER = "time-picker",
  TIME_RANGE_PICKER = "time-range-picker",
  SWITCH = "switch",
  RADIO_GROUP = "radio-group",
  CHECKBOX_GROUP = "checkbox-group",
  TEXT_AREA = "textarea",
  SLIDER = "slider",
  TREE_SELECT = "tree-select",
  UPLOAD = "upload",
  CASCADER = "cascader",
  CUSTOM = "custom"
}
