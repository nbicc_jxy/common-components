import type { FormHelper } from "../form-helper";
import { isFunction, isObject, isSet } from "lodash-es";
import type { Rule } from "ant-design-vue/lib/form";
import type { BuildInValidatorRule } from "./types";

/**
 * 表单校验器
 */
export class FormValidator {
  constructor() {
    this.linkedFieldGroup = new Map<string, Set<string>>();
  }

  /**
   * 存储字段间的联动关系, **用于在字段值变化时触发联动校验**.
   */
  linkedFieldGroup: Map<string, Set<string>>;

  /**
   * 当表单项配置变化时, 更新字段间的联动关系.
   * @param formHelper
   */
  updateLinkedFieldGroup(formHelper: FormHelper): void {
    const { linkedFieldGroup } = transformBuildInRules(formHelper, this);
    this.linkedFieldGroup = linkedFieldGroup;
  }

  /**
   * 校验与指定字段关联的字段. 从 {@link linkedFieldGroup} 中获取与指定字段关联的字段名数组, 并校验这些字段.
   * @param name
   */
  async validateLinkedField(name: string): Promise<any> {
    const linkedFieldSet = this.linkedFieldGroup.get(name);
    if (!isSet(linkedFieldSet) || linkedFieldSet.size <= 0) return;

    const fieldList = Array.from(linkedFieldSet);
    fieldList.push(name);
    return this.validate(fieldList);
  }

  /**
   * 校验指定的字段. 该方法应由子类实现.
   * @param nameList
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async validate(nameList: Array<string>): Promise<any> {
    return Promise.resolve();
  }
}

/**
 * 初始化内置校验器并将其转换为 ant-design-vue 的自定义校验器类型.
 * @param formHelper
 * @param formValidator
 * @return 返回值中的 linkedFieldGroup 为字段间的联动关系, **用于在字段值变化时触发联动校验**.
 */
function transformBuildInRules(formHelper: FormHelper, formValidator: FormValidator) {
  const { fields } = formHelper;
  const linkGroup = new Map<string, Set<string>>();

  for (let i = fields.length; i--; ) {
    const field = fields[i];
    const fieldName = field.options.name;

    for (let j = field.options.rules.length; j--; ) {
      const rule = field.options.rules[j] as BuildInValidatorRule;
      const buildInInitialFunc = rule.validator;

      /**
       * 判断是否为内置校验器. 然后执行内置校验器的初始化函数, 获取该校验器所关联的字段名数组和校验函数.
       */
      if (!isFunction(buildInInitialFunc) || buildInInitialFunc.isBuildIn !== true) continue;

      const validatorInfo = buildInInitialFunc(formHelper, formValidator, field);
      if (!isObject(validatorInfo)) continue;

      const { grouped, validator } = validatorInfo;
      for (let k = grouped.length; k--; ) {
        const name = grouped[k];

        if (fieldName === name) continue;

        if (!linkGroup.has(fieldName)) linkGroup.set(fieldName, new Set());
        linkGroup.get(fieldName)?.add(name);
        if (!linkGroup.has(name)) linkGroup.set(name, new Set());
        linkGroup.get(name)?.add(fieldName);
      }
      rule.buildInValidator = validator;
    }
  }

  /**
   * 将内置校验器转换为 ant-design-vue 的自定义校验器类型, 并将其赋值给校验规则的 validator .
   */
  for (let i = fields.length; i--; ) {
    const field = fields[i];

    for (let j = field.options.rules.length; j--; ) {
      const rule = field.options.rules[j] as BuildInValidatorRule;
      const { buildInValidator } = rule;

      if (!isFunction(buildInValidator)) continue;

      (rule.validator as Rule["validator"]) = (_rule, value, callback) =>
        buildInValidator(formHelper, formValidator, field, _rule, value, callback);
    }
  }

  return {
    linkedFieldGroup: linkGroup
  };
}
