import { expect, describe, test, vi, beforeEach } from "vitest";
import { allowCharsOrCustomize } from "../character-rules";
import { TestFormValidator } from "./test-form-validator";
import { FormHelper } from "../../../form-helper";
import { FormItemControlType } from "../../../types";

describe("allowCharsOrCustomize函数", () => {
  let formHelper: FormHelper;
  beforeEach(() => {
    const validator = new TestFormValidator();
    formHelper = new FormHelper({ validator });
    validator.formHelper = formHelper;
    formHelper.onValidateError = vi.fn();
  });

  test("当应用了allowCharsOrCustomize的表单项的值只包含指定字符集中的字符时，应该返回true", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT,
        name: "field",
        rules: [{ validator: allowCharsOrCustomize(["en", "number"]) }]
      }
    ]);
    formHelper.setField("field", "abc123");
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(0);
  });

  test("当应用了allowCharsOrCustomize的表单项的值包含自定义字符集中的字符时，应该返回true", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT,
        name: "field",
        rules: [{ validator: allowCharsOrCustomize(["en"], "$%^") }]
      }
    ]);
    formHelper.setField("field", "abc$%^");
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(0);
  });

  test("当应用了allowCharsOrCustomize的表单项的值包含自定义字符集中的字符时，应该返回true", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT,
        name: "field",
        rules: [{ validator: allowCharsOrCustomize([], "$%^") }]
      }
    ]);
    formHelper.setField("field", "abc$%^");
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(0);
  });
});
