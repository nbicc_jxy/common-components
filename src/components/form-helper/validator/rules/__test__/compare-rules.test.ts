import { expect, describe, test, vi, beforeEach } from "vitest";
import { FormHelper } from "../../../form-helper";
import { FormItemControlType } from "../../../types";
import { isLessThan } from "../compare-rules";
import { TestFormValidator } from "./test-form-validator";

describe("isLessThan函数", () => {
  let formHelper: FormHelper;
  beforeEach(() => {
    const validator = new TestFormValidator();
    formHelper = new FormHelper({ validator });
    validator.formHelper = formHelper;
    formHelper.onValidateError = vi.fn();
  });

  test("当应用了isLessThan的表单项的值小于指定表单项的值时，应该返回true", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field1",
        rules: [{ validator: isLessThan("field2") }]
      },
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field2"
      }
    ]);
    formHelper.setField("field1", 1);
    formHelper.setField("field2", 2);
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(0);
  });

  test("当应用了isLessThan的表单项的值等于指定表单项的值时，如果allowEqual为true，应该返回true", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field1",
        rules: [{ validator: isLessThan("field2", undefined, true) }]
      },
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field2"
      }
    ]);
    formHelper.setField("field1", 1);
    formHelper.setField("field2", 1);
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(0);
  });

  test("当应用了isLessThan的表单项的值等于指定表单项的值时，如果allowEqual为false，应该返回false", async () => {
    formHelper.setFields([
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field1",
        rules: [{ validator: isLessThan("field2") }]
      },
      {
        type: FormItemControlType.INPUT_NUMBER,
        name: "field2"
      }
    ]);
    await new Promise((resolve) => setTimeout(resolve, 0));

    formHelper.setField("field1", 1);
    formHelper.setField("field2", 1);
    await new Promise((resolve) => setTimeout(resolve, 0));
    expect(formHelper.onValidateError).toBeCalledTimes(2);
  });
});
