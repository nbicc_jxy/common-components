import { FormValidator } from "../../form-validator";
import type { FormHelper } from "../../../form-helper";

export class TestFormValidator extends FormValidator {
  public formHelper: FormHelper | undefined;

  override async validate(nameList: Array<string>): Promise<any> {
    for (const name of nameList) {
      const field = this.formHelper!.getField(name);
      if (!field) {
        continue;
      }
      const value = field.value;
      const rules = field.options.rules;
      if (!rules) {
        continue;
      }
      for (const rule of rules) {
        rule.validator && (await rule.validator(rule as any, value, undefined as any));
      }
    }
  }
}
