import type { Validator } from "../../../utils/validate-rules/types";
import { validate } from "../../../utils/validate-rules";
import { isString } from "lodash-es";
import {
  characterWith,
  enValidator,
  numberValidator,
  zhCNValidator
} from "../../../utils/validate-rules/string-rules";
import type { BuildInValidator, BuildInValidatorInitialFunc } from "../types";

/**
 * 应用了 {@link allowCharsOrCustomize} 的表单项的值只能包含指定的字符集或自定义字符集中的字符.
 * 内置字符集:
 * * zh: 中文字符
 * * en: 英文字符
 * * number: 数字
 *
 * @param buildIn 内置字符集组成的数组
 * @param custom 自定义字符集
 * @param message 校验失败时的提示信息
 */
export function allowCharsOrCustomize(
  buildIn: Array<"zh" | "en" | "number">,
  custom?: string,
  message: string = "存在不允许的字符"
): BuildInValidatorInitialFunc {
  const func: BuildInValidator = (formHelper, formValidator, field, rule, value) => {
    const validators: Array<Validator> = [];

    const buildInSet = new Set(buildIn);
    buildInSet.forEach((item) => {
      switch (item) {
        case "zh":
          validators.push(zhCNValidator);
          break;
        case "en":
          validators.push(enValidator);
          break;
        case "number":
          validators.push(numberValidator);
          break;
      }
    });
    if (isString(custom) && custom.length > 0) validators.push(characterWith(custom));

    const { allMatch } = validate(value, ...validators);

    if (!allMatch) throw new Error(message);
  };

  const initialFunc: BuildInValidatorInitialFunc = () => {
    return {
      grouped: [],
      validator: func
    };
  };
  initialFunc.isBuildIn = true;

  return initialFunc;
}

/**
 * 应用了 {@link allowCharsOrCustomize} 的表单项的值只能包含指定的字符集中的字符.
 * 内置字符集同 {@link allowCharsOrCustomize}.
 * @param buildIn
 * @param message
 */
export function allowChars(
  buildIn: Array<"zh" | "en" | "number">,
  message?: string
): BuildInValidatorInitialFunc {
  return allowCharsOrCustomize(buildIn, undefined, message);
}
