import { isArray, isNil } from "lodash-es";
import { dateRules, numberRules, validate } from "../../../utils/validate-rules";
import type { BuildInValidator, BuildInValidatorInitialFunc } from "../types";
import type { FormHelper } from "../../form-helper";
import { FormItemControlType } from "../../types";
import dayjs from "dayjs";

/**
 * 应用了 {@link isLessThan} 的表单项的值必须小于 {@link name} 表单项的值.
 * 支持的表单项类型:
 * * {@link FormItemControlType.INPUT_NUMBER}
 * * {@link FormItemControlType.DATE_PICKER}
 * * {@link FormItemControlType.DATE_RANGE_PICKER}
 * * {@link FormItemControlType.TIME_PICKER}
 * * {@link FormItemControlType.TIME_RANGE_PICKER}
 *
 * @param name
 * @param message
 * @param allowEqual
 */
export function isLessThan(
  name: string,
  message?: string | [string, string],
  allowEqual: boolean = false
): BuildInValidatorInitialFunc {
  const func: BuildInValidatorInitialFunc = (formHelper, formValidator, field) => {
    const startMessage = isArray(message) ? message[0] : message;
    const endMessage = isArray(message) ? message[1] : message;
    return initialFunc(formHelper, field.options.name, startMessage, name, endMessage, allowEqual);
  };
  func.isBuildIn = true;
  return func;
}

/**
 * 应用了 {@link isLessThan} 的表单项的值必须小于等于 {@link name} 表单项的值.
 * 支持的表单项类型同 {@link isLessThan}.
 * @param name
 * @param message
 */
export function isLessThanOrEqual(name: string, message?: string | [string, string]) {
  return isLessThan(name, message, true);
}

/**
 * 应用了 {@link isGreaterThan} 的表单项的值必须大于 {@link name} 表单项的值.
 * 支持的表单项类型同 {@link isLessThan}.
 * @param name
 * @param message
 * @param allowEqual
 */
export function isGreaterThan(
  name: string,
  message?: string | [string, string],
  allowEqual: boolean = false
): BuildInValidatorInitialFunc {
  const func: BuildInValidatorInitialFunc = (formHelper, formValidator, field) => {
    const startMessage = isArray(message) ? message[0] : message;
    const endMessage = isArray(message) ? message[1] : message;
    return initialFunc(formHelper, name, startMessage, field.options.name, endMessage, allowEqual);
  };
  func.isBuildIn = true;
  return func;
}

/**
 * 应用了 {@link isGreaterThan} 的表单项的值必须大于等于 {@link name} 表单项的值.
 * 支持的表单项类型同 {@link isLessThan}.
 * @param name
 * @param message
 */
export function isGreaterThanOrEqual(name: string, message?: string | [string, string]) {
  return isGreaterThan(name, message, true);
}

function initialFunc(
  formHelper: FormHelper,
  startName: string,
  startMessage: string = "范围不合法",
  endName: string,
  endMessage: string = "范围不合法",
  allowEqual: boolean = false
): ReturnType<BuildInValidatorInitialFunc> {
  const startField = formHelper.getField(startName);
  const endField = formHelper.getField(endName);

  if (isNil(startField) || isNil(endField)) return;

  endField.options.rules.push({
    buildInValidator: validateRange(
      startField.options.name,
      endField.options.name,
      endMessage,
      allowEqual
    )
  });

  return {
    grouped: [startName, endName],
    validator: validateRange(
      startField.options.name,
      endField.options.name,
      startMessage,
      allowEqual
    )
  };
}

function validateRange(
  startName: string,
  endName: string,
  message: string,
  allowEqual = false
): BuildInValidator {
  return async (formHelper) => {
    const startField = formHelper.getField(startName);
    const endField = formHelper.getField(endName);
    if (isNil(startField) || isNil(endField)) return;
    if (startField.control.options.type !== endField.control.options.type) return;

    const start = formHelper.getField(startName)?.value;
    const end = formHelper.getField(endName)?.value;
    // 如果两个值有一个为空, 不需要进行校验
    if (isNil(start) || isNil(end)) return;

    const controlType = startField.control.options.type;

    switch (controlType) {
      case FormItemControlType.INPUT_NUMBER:
        if (
          !validate(
            [start, end],
            numberRules.isLessThan,
            allowEqual ? numberRules.isEqual : undefined
          ).leastOneMatch
        )
          throw new Error(message);
        break;
      case FormItemControlType.DATE_PICKER:
      case FormItemControlType.DATE_RANGE_PICKER:
        if (
          !validate(
            [dayjs(start).startOf("day"), dayjs(end).startOf("day")],
            dateRules.isLessThan,
            allowEqual ? dateRules.isEqual : undefined
          ).leastOneMatch
        )
          throw new Error(message);
        break;
      case FormItemControlType.TIME_PICKER:
      case FormItemControlType.TIME_RANGE_PICKER:
        if (
          !validate([start, end], dateRules.isLessThan, allowEqual ? dateRules.isEqual : undefined)
            .leastOneMatch
        )
          throw new Error(message);
        break;
    }
  };
}
