import type { RuleObject } from "ant-design-vue/lib/form";
import type { StoreValue } from "ant-design-vue/lib/form/interface";
import type { FormHelper } from "../form-helper";
import type { FormItem } from "../form-item";
import type { FormValidator } from "./form-validator";

/**
 * 内置校验器规则
 */
export interface BuildInValidatorRule {
  /**
   * 内置校验器的初始化函数, 在校验前执行并只执行一次.
   */
  validator?: BuildInValidatorInitialFunc;
  buildInValidator?: BuildInValidator;
}

/**
 * 内置校验器的校验函数.
 */
export interface BuildInValidator {
  (
    formHelper: FormHelper,
    formValidator: FormValidator,
    field: FormItem,
    rule: RuleObject,
    value: StoreValue,
    callback: (error?: string) => void
  ): Promise<void> | void;
}

/**
 * 内置校验器的初始化函数.
 * @return { grouped: Array<string> } grouped: 该校验器所关联的字段名数组.
 */
export interface BuildInValidatorInitialFunc {
  (formHelper: FormHelper, formValidator: FormValidator, field: FormItem): {
    grouped: Array<string>;
    validator: BuildInValidator;
  } | void;
  /**
   * 内置校验器标记, 用于标记该校验器是否为内置校验器.
   */
  isBuildIn?: boolean;
}
