import { expect, test, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { NButton } from "../";

test("NButton 默认属性下正确渲染", () => {
  const wrapper = mount(NButton);
  expect(wrapper.html()).toMatchSnapshot();
});

test("NButton 自定义文本正确渲染", () => {
  const wrapper = mount(NButton, {
    props: {
      text: "点击我！"
    }
  });
  expect(wrapper.html()).toMatchSnapshot();
});

test("NButton 点击时触发 click 事件", async () => {
  const wrapper = mount(NButton);
  await wrapper.trigger("click");
  expect(wrapper.emitted("click")).toBeTruthy();
});

test("NButton disabled 属性为 true 时禁用", () => {
  const wrapper = mount(NButton, {
    props: {
      disabled: true
    }
  });
  expect(wrapper.attributes("disabled")).not.toBeUndefined();
});

test("NButton type 为 secondary 时正确渲染", () => {
  const wrapper = mount(NButton, {
    props: {
      type: "secondary"
    }
  });
  expect(wrapper.classes()).toContain("n-button-secondary");
});

test("NButton 点击时正确触发 debounce(带有 leading)", async () => {
  vi.useFakeTimers();

  const onClick = vi.fn();
  const wrapper = mount(NButton, {
    props: {
      delay: 500,
      onClick
    }
  });
  await wrapper.trigger("click");
  expect(onClick).toBeCalledTimes(1);

  await wrapper.trigger("click");
  await wrapper.trigger("click");
  await wrapper.trigger("click");
  await wrapper.trigger("click");
  await wrapper.trigger("click");
  vi.advanceTimersByTime(500);
  expect(onClick).toBeCalledTimes(2);

  vi.useRealTimers();
});

test("NButton 组件类型为 secondary 时, 样式正确", () => {
  const wrapper = mount(NButton, {
    props: {
      type: "secondary"
    }
  });
  expect(wrapper.classes()).toContain("n-button-secondary");
});

test("NButton 组件设置 warning 属性时, 样式正确", () => {
  const wrapper = mount(NButton, {
    props: {
      warning: true
    }
  });
  expect(wrapper.classes()).toContain("n-button-warning");
});
