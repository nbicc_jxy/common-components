import { expect, test, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { NLoadingButton } from "../";

test("NLoadingButton 默认属性下正确渲染", () => {
  const wrapper = mount(NLoadingButton);
  expect(wrapper.html()).toMatchSnapshot();
});

test("NLoadingButton 自定义文本正确渲染", () => {
  const wrapper = mount(NLoadingButton, {
    slots: {
      default: "点击我！"
    }
  });
  expect(wrapper.html()).toMatchSnapshot();
});

test("NLoadingButton 点击时触发 click 事件", async () => {
  const onClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      onClick
    }
  });
  await wrapper.trigger("click");
  expect(onClick).toBeCalled();
});

test("NLoadingButton loading 为 true 时禁用", async () => {
  const onClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: true,
      onClick
    }
  });
  await wrapper.trigger("click");
  expect(onClick).not.toBeCalled();
});

test("NLoadingButton onLoadingClick 返回 Promise 时正确渲染", async () => {
  const onLoadingClick = vi.fn(() => Promise.resolve());
  const wrapper = mount(NLoadingButton, {
    props: {
      onLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(wrapper.classes().includes("ant-btn-loading")).toBe(true);
});

test("NLoadingButton onLoadingClick 返回非 Promise 时正确渲染", async () => {
  const onLoadingClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      onLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(wrapper.classes().includes("ant-btn-loading")).toBe(false);
});

test("NLoadingButton loading 为 true 时不触发 onLoadingClick", async () => {
  const onLoadingClick = vi.fn(() => Promise.resolve());
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: true,
      onLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(onLoadingClick).not.toBeCalled();
});

test("NLoadingButton loading 为 null 时抛出异常", async () => {
  const onLoadingClick = vi.fn(() => Promise.resolve());
  expect(() => {
    // @ts-ignore
    mount(NLoadingButton, {
      props: {
        // @ts-ignore
        loading: null,
        onLoadingClick
      }
    });
  }).toThrowError();
});

test("NLoadingButton loading 为 false 时触发 onLoadingClick", async () => {
  const onLoadingClick = vi.fn(() => Promise.resolve());
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: false,
      onLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(onLoadingClick).toBeCalled();
});

test("NLoadingButton loading 为 null 时触发 onClick", async () => {
  const onClick = vi.fn();
  expect(() => {
    // @ts-ignore
    mount(NLoadingButton, {
      props: {
        // @ts-ignore
        loading: null,
        onClick
      }
    });
  }).toThrowError();
});

test("NLoadingButton loading 为 false 时触发 onClick", async () => {
  const onClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: false,
      onClick
    }
  });
  await wrapper.trigger("click");
  expect(onClick).toBeCalled();
});

test("NLoadingButton loading 为 true 时不触发 onClick", async () => {
  const onClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: true,
      onClick
    }
  });
  await wrapper.trigger("click");
  expect(onClick).not.toBeCalled();
});

test("NLoadingButton loading 为 false 时触发 handleLoadingClick", async () => {
  const handleLoadingClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: false,
      onLoadingClick: handleLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(handleLoadingClick).toBeCalled();
});

test("NLoadingButton loading 为 true 时不触发 handleLoadingClick", async () => {
  const handleLoadingClick = vi.fn();
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: true,
      onLoadingClick: handleLoadingClick
    }
  });
  await wrapper.trigger("click");
  expect(handleLoadingClick).not.toBeCalled();
});

test("NLoadingButton loading 为 true 时正确渲染 loading 状态", async () => {
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: true
    }
  });
  expect(wrapper.find(".ant-btn-loading-icon").exists()).toBe(true);
});

test("NLoadingButton loading 为 false 时正确渲染非 loading 状态", async () => {
  const wrapper = mount(NLoadingButton, {
    props: {
      loading: false
    }
  });
  expect(wrapper.find(".ant-btn-loading-icon").exists()).toBe(false);
});
