import type { ExtendProps } from "../common/types";
import type { ButtonProps } from "ant-design-vue";
import type { PropType, ExtractPropTypes } from "vue";

export const nButtonProps = () =>
  ({
    type: {
      type: String as PropType<ButtonProps["type"] | "secondary">
    },
    secondary: {
      type: Boolean,
      default: false
    },
    warning: {
      type: Boolean,
      default: false
    },
    /**
     * 点击事件延时时间, 用于防止重复点击.
     * 单位毫秒. 默认 250 毫秒.
     */
    delay: {
      type: Number,
      default: 250
    },
    onClick: {
      type: Function as PropType<NButtonEmits["click"]>
    }
  } as const);

export type NButtonProps = ExtractPropTypes<ReturnType<typeof nButtonProps>>;

export const nLoadingButtonProps = () =>
  ({
    /**
     * 该参数与 loadingClick 冲突，当 该参数存在时 loadingClick 失效.
     */
    loading: {
      type: [Boolean, Object] as PropType<ButtonProps["loading"]>
    },
    /**
     * 点击事件回调函数. 该函数执行前会将 loading 状态设置为 true.并且当返回值为 Promise 时, 会在 Promise 执行完毕后将 loading 状态设置为 false.
     */
    onLoadingClick: {
      type: Function as PropType<NButtonEmits["loadingClick"]>
    },
    onClick: {
      type: Function as PropType<NButtonEmits["click"]>
    }
  } as const);

export type NLoadingButtonProps = ExtendProps<ReturnType<typeof nLoadingButtonProps>, NButtonProps>;

export interface NButtonEmits {
  click: (event: MouseEvent) => void;
  loadingClick: (event: MouseEvent) => Promise<unknown> | undefined;
}
