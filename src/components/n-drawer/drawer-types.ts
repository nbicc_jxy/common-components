import type { ExtractPropTypes, PropType, VNode } from "vue";
import type { DrawerProps } from "ant-design-vue";

export const nDrawerWrapperProps = () => ({
  provide: {
    type: Boolean,
    default: true
  }
});
export type NDrawerWrapperProps = ExtractPropTypes<ReturnType<typeof nDrawerWrapperProps>>;

export const NDrawerWrapperContextKey = "DRAWER_WRAPPER_CONTEXT_KEY";

export interface NDrawerWrapperContext {
  drawerFunc: (props: NDrawerFuncProps) => { destroy: () => void };
  showDrawer: (
    drawerComponent: VNode,
    props?: Record<string, any>,
    drawerProps?: Omit<DrawerProps, "visible" | "onUpdate:visible"> & Partial<NDrawerProps>
  ) => { destroy: () => void };
}

export interface NDrawerFuncProps extends DrawerProps {
  content?: string | (() => VNode) | VNode;
}

export const nDrawerProps = () =>
  ({
    initialData: {
      type: Function as PropType<() => Promise<Record<string, any>> | Record<string, any>>,
      default: () => undefined
    },
    loadingText: {
      type: String,
      default: "加载中..."
    }
  } as const);

export type NDrawerProps = ExtractPropTypes<ReturnType<typeof nDrawerProps>>;

export const nDrawerContainerProps = () =>
  ({
    ...nDrawerProps(),
    beforeOpen: {
      type: Function as PropType<() => Promise<boolean | void> | boolean | void>,
      default: () => true
    },
    beforeClose: {
      type: Function as PropType<() => Promise<boolean | void> | boolean | void>,
      default: () => true
    }
  } as const);

export type NDrawerContainerProps = ExtractPropTypes<ReturnType<typeof nDrawerContainerProps>>;

/**
 * NDrawer 通过 {@link NDrawerParentTeleportKey} 为下级的 NDrawer 提供挂载点
 */
export const NDrawerParentTeleportKey = "DRAWER_PARENT_TELEPORT_KEY";

/**
 * NDrawerContainer 通过 {@link NDrawerContainerProvideKey} 为内部的 NDrawer 提供默认的属性
 */
export const NDrawerContainerProvideKey = "DRAWER_CONTAINER_PROVIDE_KEY";

/**
 * NDrawerContainer 通过 {@link NDrawerContextKey} 为下级组件提供操作 NDrawer 的方法. 如 `closeDrawer` 等
 */
export const NDrawerContextKey = "DRAWER_CONTEXT_KEY";

export interface NDrawerContext {
  closeDrawer: () => void;
  setDrawerProps: (props: DrawerProps & Partial<NDrawerProps>) => void;
}
