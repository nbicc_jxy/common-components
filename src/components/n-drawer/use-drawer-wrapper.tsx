import { reactive, shallowReactive } from "vue";
import { Drawer } from "ant-design-vue";
import type { NDrawerFuncProps, NDrawerWrapperContext } from "./drawer-types";
import { isFunction, isNil, isString } from "lodash-es";
import NDrawerContainer from "./NDrawerContainer.vue";

export function useDrawerWrapper() {
  const legacyDrawerMap = reactive(new Map<string, NDrawerFuncProps>());
  const drawerMap = shallowReactive(
    new Map<string, Parameters<NDrawerWrapperContext["showDrawer"]>>()
  );
  const drawerContext: NDrawerWrapperContext = {
    drawerFunc: (props) => {
      props = reactive(props);
      props["visible"] = true;
      props["onUpdate:visible"] = (visible) => (props.visible = visible);

      const id = Date.now().toString();
      legacyDrawerMap.set(id, props);

      // 关闭抽屉的动画结束后再删除.
      const afterVisibleChange = props["onAfterVisibleChange"];
      props["onAfterVisibleChange"] = (visible) => {
        if (visible) return;

        if (isFunction(afterVisibleChange)) afterVisibleChange(visible);
        legacyDrawerMap.delete(id);
      };

      return {
        destroy: () => (props["visible"] = false)
      };
    },
    showDrawer: (drawerComponent, props, drawerProps = {}) => {
      const id = Date.now().toString();
      drawerMap.set(id, [drawerComponent, props, drawerProps]);

      /**
       * `onAfterVisibleChange`: 切换抽屉时动画结束后的回调
       * 关闭抽屉的动画结束后再删除组件.
       */
      const afterVisibleChange = drawerProps?.["onAfterVisibleChange"];
      drawerProps["onAfterVisibleChange"] = (visible) => {
        if (visible) return;

        if (isFunction(afterVisibleChange)) afterVisibleChange(visible);
        destroyDrawer();
      };
      const destroyDrawer = () => drawerMap.delete(id);

      return {
        destroy: destroyDrawer
      };
    }
  };

  return {
    context: drawerContext,
    render: () => {
      const legacyDrawers = Array.from(legacyDrawerMap.values()).map((props) => {
        const { content, footer, extra, closeIcon, title, ...restProps } = props;

        const slots: Record<string, any> = {};
        if (!isNil(content))
          slots["default"] = isFunction(content)
            ? content
            : () => (isString(content) ? <>{content}</> : content);
        if (!isNil(footer))
          slots["footer"] = isFunction(footer)
            ? footer
            : () => (isString(footer) ? <>{footer}</> : footer);
        if (!isNil(extra))
          slots["extra"] = isFunction(extra)
            ? extra
            : () => (isString(extra) ? <>{extra}</> : extra);
        if (!isNil(closeIcon))
          slots["closeIcon"] = isFunction(closeIcon)
            ? closeIcon
            : () => (isString(closeIcon) ? <>{closeIcon}</> : closeIcon);
        if (!isNil(title))
          slots["title"] = isFunction(title)
            ? title
            : () => (isString(title) ? <>{title}</> : title);

        return <Drawer {...restProps}>{slots}</Drawer>;
      });

      const drawers = Array.from(drawerMap.values()).map(([component, props, drawerProps]) => {
        return (
          <NDrawerContainer {...drawerProps}>
            <component {...props} />
          </NDrawerContainer>
        );
      });

      return (
        <>
          {legacyDrawers}
          {drawers}
        </>
      );
    }
  };
}
