import {
  type NDrawerContext,
  NDrawerContextKey,
  type NDrawerWrapperContext,
  NDrawerWrapperContextKey
} from "./drawer-types";
import { inject } from "vue";
import { isNil } from "lodash-es";

/**
 * 导出用于操作当前 NDrawer 的方法.
 */
export function useDrawer(): NDrawerContext {
  const drawerMethods = inject<NDrawerContext>(NDrawerContextKey);
  if (isNil(drawerMethods)) throw new Error("inject key [NDrawerContextKey] is not found");

  return drawerMethods;
}

export function useDrawerManager(): NDrawerWrapperContext {
  const drawerMethods = inject<NDrawerWrapperContext>(NDrawerWrapperContextKey);
  if (isNil(drawerMethods)) throw new Error("inject key [NDrawerWrapperContextKey] is not found");

  return drawerMethods;
}
