import { expect, describe, test } from "vitest";
import { mount } from "@vue/test-utils";
import { NEasyForm } from "../";
import { FormItemControlType } from "../../form-helper";

const fields = [
  { name: "name", label: "姓名", initialValue: "张三", type: FormItemControlType.INPUT },
  { name: "age", label: "年龄", initialValue: 18, type: FormItemControlType.INPUT_NUMBER }
];

describe("NEasyForm initialValue 属性", () => {
  test("initialValue 属性正确绑定到 data 上", () => {
    const data = {};
    mount(NEasyForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields
      }
    });

    expect(data).toEqual({ name: "张三", age: 18 });
  });

  test("NEasyForm initialValue 属性不会覆盖 data 上的非空属性", () => {
    const data = { name: "张三", age: 18 };
    mount(NEasyForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields
      }
    });
    expect(data).toEqual({ name: "张三", age: 18 });
  });
});

describe("NEasyForm data 属性", () => {
  describe("表单项控件值变化时能够正确同步", () => {
    test("输入框输入值变化时 data 同步", async () => {
      const data = { name: "张三", age: 18 };
      const wrapper = mount(NEasyForm, {
        props: {
          data,
          "onUpdate:data": (newData: any) => Object.assign(data, newData),
          fields
        }
      });

      const nameInput = wrapper.find('input[data-n-form-item-name="name"]');
      const ageInput = wrapper.find('input[data-n-form-item-name="age"]');
      await nameInput.setValue("李四");
      await ageInput.setValue(20);

      await triggerSearch(wrapper);

      expect(data).toEqual({ name: "李四", age: 20 });
    });

    test("下拉框选项变化时 data 同步", async () => {
      const data = { gender: "male" };
      const wrapper = mount(NEasyForm, {
        props: {
          data,
          "onUpdate:data": (newData: any) => Object.assign(data, newData),
          fields: [
            [
              {
                name: "gender",
                label: "性别",
                initialValue: "male",
                type: FormItemControlType.SELECT
              },
              {
                options: [
                  { label: "男", value: "male" },
                  { label: "女", value: "female" }
                ],
                open: true
              }
            ]
          ]
        },
        attachTo: document.body
      });

      const selectOption = document.body.querySelector('[title="女"]');
      selectOption?.dispatchEvent(new MouseEvent("click"));

      await triggerSearch(wrapper);

      expect(data).toEqual({ gender: "female" });
    });

    test("多选框选项变化时 data 同步", async () => {
      const data = { hobbies: ["reading"] };
      const wrapper = mount(NEasyForm, {
        props: {
          data,
          "onUpdate:data": (newData: any) => Object.assign(data, newData),
          fields: [
            [
              {
                name: "hobbies",
                label: "爱好",
                initialValue: ["reading"],
                type: FormItemControlType.CHECKBOX_GROUP
              },
              {
                options: [
                  { label: "阅读", value: "reading" },
                  { label: "旅游", value: "travel" },
                  { label: "音乐", value: "music" }
                ]
              }
            ]
          ]
        }
      });

      const travelCheckbox = wrapper.find(
        '[data-n-form-item-name="hobbies"] input[value="travel"]'
      );
      await travelCheckbox.trigger("change");

      await triggerSearch(wrapper);

      expect(data).toEqual({ hobbies: ["reading", "travel"] });
    });

    test("单选框选项变化时 data 同步", async () => {
      const data = { gender: "male" };
      const wrapper = mount(NEasyForm, {
        props: {
          data,
          "onUpdate:data": (newData: any) => Object.assign(data, newData),
          fields: [
            [
              {
                name: "gender",
                label: "性别",
                initialValue: "male",
                type: FormItemControlType.RADIO_GROUP
              },
              {
                options: [
                  { label: "男", value: "male" },
                  { label: "女", value: "female" }
                ]
              }
            ]
          ]
        }
      });

      const femaleRadio = wrapper.find('[data-n-form-item-name="gender"] input[value="female"]');
      await femaleRadio.trigger("change");

      await triggerSearch(wrapper);

      expect(data).toEqual({ gender: "female" });
    });
  });

  /**
   * @see git revision: 787f1b3f
   */
  test("data 中存在未在 fields 中声明的属性时, 保留这些属性", async () => {
    const data = { name: "张三", age: 18, gender: "male" };
    const wrapper = mount(NEasyForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields
      }
    });

    const genderInput = wrapper.find('input[data-n-form-item-name="name"]');
    await genderInput.setValue("李四");

    await triggerSearch(wrapper);

    expect(data).toEqual({ name: "李四", age: 18, gender: "male" });
  });
});

describe("NEasyForm change 事件", () => {
  test("data 属性变化时能够正确触发 change 事件", async () => {
    const wrapper = mount(NEasyForm, {
      props: {
        data: { name: "John", age: 30 }
      }
    });

    // 初始化时会触发一次 change 事件, 以便父组件能够获取到初始值
    expect(wrapper.emitted("change")).toBeTruthy();
    expect(wrapper.emitted("change")?.length).toBe(1);
    expect(wrapper.emitted("change")?.[0]?.[0]).toEqual({ name: "John", age: 30 });

    // 修改 data 属性后, 会再次触发 change 事件
    await wrapper.setProps({ data: { name: "Jane", age: 25 } });

    expect(wrapper.emitted("change")?.length).toBe(2);
    expect(wrapper.emitted("change")?.[1]?.[0]).toEqual({ name: "Jane", age: 25 });
  });
});

async function triggerSearch(wrapper: any) {
  const searchButton = wrapper.find('[data-n-role="search-button"]');
  await searchButton.trigger("click");
  // 等待表单校验完成
  await new Promise((resolve) => setTimeout(resolve));
}
