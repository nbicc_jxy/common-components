import { expect, describe, test, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { NForm } from "../";
import type { AntDesignVueFormModelFieldArgs } from "../";
import { FormItemControlType } from "../../form-helper";
import zhCN from "ant-design-vue/lib/locale/zh_CN";
import dayjs, { Dayjs } from "dayjs";

const fields: Array<AntDesignVueFormModelFieldArgs> = [
  { name: "name", label: "姓名", initialValue: "张三", type: FormItemControlType.INPUT },
  { name: "age", label: "年龄", initialValue: 18, type: FormItemControlType.INPUT_NUMBER },
  [
    { name: "gender", label: "性别", initialValue: "male", type: FormItemControlType.SELECT },
    {
      options: [
        { label: "男", value: "male" },
        { label: "女", value: "female" }
      ]
    }
  ],
  {
    name: "birthday",
    label: "生日",
    initialValue: dayjs("1998-03-31"),
    type: FormItemControlType.DATE_PICKER
  },
  {
    type: FormItemControlType.DATE_RANGE_PICKER,
    name: "dateRange",
    label: "日期范围",
    initialValue: [dayjs("1998-03-31"), dayjs("1998-03-31")]
  },
  {
    type: FormItemControlType.TIME_PICKER,
    name: "time",
    label: "时间",
    initialValue: dayjs("1998-03-31")
  },
  {
    type: FormItemControlType.TIME_RANGE_PICKER,
    name: "timeRange",
    label: "时间范围",
    initialValue: [dayjs("1998-03-31"), dayjs("1998-03-31")]
  },
  { type: FormItemControlType.SWITCH, name: "switch", label: "开关", initialValue: true },
  [
    {
      type: FormItemControlType.RADIO_GROUP,
      name: "radioGroup",
      label: "单选框",
      initialValue: "1"
    },
    {
      options: [
        { label: "选项1", value: "1" },
        { label: "选项2", value: "2" }
      ]
    }
  ],
  [
    {
      type: FormItemControlType.CHECKBOX_GROUP,
      name: "checkboxGroup",
      label: "多选框",
      initialValue: ["1"]
    },
    {
      options: [
        { label: "选项1", value: "1" },
        { label: "选项2", value: "2" }
      ]
    }
  ],
  {
    type: FormItemControlType.TEXT_AREA,
    name: "textArea",
    label: "文本域",
    initialValue: "文本域"
  },
  { type: FormItemControlType.SLIDER, name: "slider", label: "滑块", initialValue: 50 }
];

describe("NForm rules 属性", () => {
  describe("表单项控件值变化时能够正确同步", () => {
    /**
     * @see git revision: 85c6213b
     */
    test("当 data 从外部修改且属性值为空时, 保持该属性的值", async () => {
      const data = { name: "张三", age: 18 };
      const wrapper = mount(NForm, {
        props: {
          data,
          "onUpdate:data": (newData: any) => Object.assign(data, newData),
          fields
        }
      });

      // 外部修改 data
      await wrapper.setProps({ data: { name: "李四", age: 20 } });
      // 输入框输入值变化时 data 同步
      const ageInput = wrapper.find('input[data-n-form-item-name="age"]');
      await ageInput.setValue(233);
      expect(data).toEqual({ name: "李四", age: 233 });

      // 再次修改 data, name 属性值为空
      await wrapper.setProps({ data: { name: undefined, age: 20 } });
      // 同步 data
      await ageInput.setValue(233);
      expect(data).toEqual({ name: undefined, age: 233 });
    });
  });

  /**
   * @see git revision: 8ff17dca
   */
  test("中文环境下, 能够正确显示默认校验信息中的 label 属性", async () => {
    const data = {};
    const wrapper = mount(NForm, {
      props: {
        validateMessages: zhCN.Form?.defaultValidateMessages,
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields: [
          {
            name: "name",
            label: "姓名",
            type: FormItemControlType.INPUT,
            rules: [{ required: true }]
          }
        ]
      }
    });

    try {
      // @ts-ignore
      await wrapper.vm.validate();
    } catch (e) {
      console.error(e);
      await new Promise((resolve) => setTimeout(resolve, 0));
      expect(wrapper.find(".ant-form-item-explain").text()).toBe("请输入姓名");
    }
  });
});

describe("NForm fields 属性", () => {
  /**
   * @see git revision: 54f59708
   */
  test("fields 属性修改时会将值为空且存在 initialValue 的表单项重置为 initialValue", async () => {
    const data = { name: "张三", age: 18 };
    const wrapper = mount(NForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields
      }
    });

    // 清空 data
    await wrapper.setProps({ data: {} });
    // 修改 fields
    await wrapper.setProps({
      fields: [
        { name: "name", label: "姓名", type: FormItemControlType.INPUT, initialValue: "李四" }
      ]
    });
    expect(data.name).toEqual("李四");
    expect(data.age).to.oneOf([undefined, null]);
  });

  /**
   * @see git revision: 0c149338
   */
  test("initialValue 属性只在 data 中对应的属性为 null 或 undefined 时生效", async () => {
    const data: Record<string, any> = { name: "李四", age: 20 };
    mount(NForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields
      }
    });

    expect(data.name).toEqual("李四");
    expect(data.age).toEqual(20);
    expect(data.birthday.toString()).toEqual(dayjs("1998-03-31").toString());
    expect(data.dateRange.map((item: Dayjs) => item.toString())).toEqual([
      dayjs("1998-03-31").toString(),
      dayjs("1998-03-31").toString()
    ]);
    expect(data.time.toString()).toEqual(dayjs("1998-03-31").toString());
    expect(data.timeRange.map((item: Dayjs) => item.toString())).toEqual([
      dayjs("1998-03-31").toString(),
      dayjs("1998-03-31").toString()
    ]);
    expect(data.switch).toEqual(true);
    expect(data.radioGroup).toEqual("1");
    expect(data.checkboxGroup).toEqual(["1"]);
    expect(data.textArea).toEqual("文本域");
    expect(data.slider).toEqual(50);
  });

  test("valueMapTo 会在初始化时使用 initialValue 执行一次", async () => {
    const data: Record<string, any> = {};
    const valueMapTo = vi.fn((value: any, rawData) => {
      rawData.name = value;
      rawData.name2 = value;
    });
    const wrapper = mount(NForm, {
      props: {
        data,
        "onUpdate:data": (newData: any) => Object.assign(data, newData),
        fields: [
          {
            name: "name",
            label: "姓名",
            initialValue: "张三",
            type: FormItemControlType.INPUT,
            valueMapTo
          }
        ]
      }
    });

    expect(data.name).toEqual("张三");
    expect(data.name2).toEqual("张三");

    const nameInput = wrapper.find('input[data-n-form-item-name="name"]');
    await nameInput.setValue("李四");
    expect(data.name).toEqual("李四");
    expect(data.name2).toEqual("李四");
  });
});
