import type { VNode, Slots } from "vue";
import type { ColProps, RowProps } from "ant-design-vue";
import { FormItemControlComponentMap } from "./types";
import { FormItemControlType } from "../form-helper";
import type { AntDesignVueFormItem, AntDesignVueFormItemControl } from "./ant-design-vue-adaptive";
import { cloneDeep, isFunction, isObject, merge } from "lodash-es";
import type { FieldStatus, NFormFieldLayout, NFormFieldStatus } from "./form-types";
import NFormItem from "./NFormItem.vue";

export function generateFormItemElement(
  field: AntDesignVueFormItem,
  slots: Slots,
  formData: Record<string, any>,
  status: FieldStatus
): Array<VNode> {
  const name = field.options.name;
  const slot = slots[name];
  const labelSlot = slots[`${name}Label`];
  const labelPrefixSlot = slots[`${name}LabelPrefix`];
  const labelSuffixSlot = slots[`${name}LabelSuffix`];
  const controlSlot = slots[`${name}Control`];

  if (isFunction(slot)) return slot({ field, formData, status });

  // 设置禁用状态, 优先级: fieldStatus > fields
  field.antDesignVueControl.componentProps["disabled"] = status.disabled;

  return [
    <NFormItem field={field} status={status} key={name}>
      {{
        default: controlSlot,
        label: labelSlot,
        labelPrefix: labelPrefixSlot,
        labelSuffix: labelSuffixSlot
      }}
    </NFormItem>
  ];
}

export function generateFormItemControlElement<TYPE>(
  name: string,
  control: AntDesignVueFormItemControl<TYPE, FormItemControlType>
): VNode {
  const { type } = control.options;

  let Component: any = null;
  switch (type) {
    case FormItemControlType.INPUT:
    case FormItemControlType.SELECT:
    case FormItemControlType.AUTO_LOAD_SELECT:
    case FormItemControlType.CHECKBOX_GROUP:
    case FormItemControlType.RADIO_GROUP:
    case FormItemControlType.DATE_PICKER:
    case FormItemControlType.DATE_RANGE_PICKER:
    case FormItemControlType.TIME_PICKER:
    case FormItemControlType.TIME_RANGE_PICKER:
    case FormItemControlType.SWITCH:
    case FormItemControlType.SLIDER:
    case FormItemControlType.TEXT_AREA:
    case FormItemControlType.CASCADER:
    case FormItemControlType.UPLOAD:
    case FormItemControlType.INPUT_NUMBER:
    case FormItemControlType.TREE_SELECT:
      Component = FormItemControlComponentMap[type];
      break;
    case FormItemControlType.CUSTOM:
      return <div />;
    default:
      throw new Error(`未知的表单控件类型: ${type}`);
  }

  // data-n-form-item-name 用于表单项的自动化测试
  return <Component {...control.componentProps} data-n-form-item-name={name} />;
}

export function generateFormItemControlRawElement<TYPE>(
  control: AntDesignVueFormItemControl<TYPE, FormItemControlType>
): VNode {
  const { type } = control.options;

  let Component: any = null;
  switch (type) {
    case FormItemControlType.INPUT:
    case FormItemControlType.SELECT:
    case FormItemControlType.AUTO_LOAD_SELECT:
    case FormItemControlType.CHECKBOX_GROUP:
    case FormItemControlType.RADIO_GROUP:
    case FormItemControlType.DATE_PICKER:
    case FormItemControlType.DATE_RANGE_PICKER:
    case FormItemControlType.TIME_PICKER:
    case FormItemControlType.TIME_RANGE_PICKER:
    case FormItemControlType.SWITCH:
    case FormItemControlType.SLIDER:
    case FormItemControlType.TEXT_AREA:
    case FormItemControlType.CASCADER:
    case FormItemControlType.UPLOAD:
    case FormItemControlType.INPUT_NUMBER:
    case FormItemControlType.TREE_SELECT:
      Component = <span class="ant-form-text">{control.displayValue}</span>;
      break;
    case FormItemControlType.CUSTOM:
      return <div />;
    default:
      throw new Error(`未知的表单控件类型: ${type}`);
  }

  return Component;
}

/**
 * 从 fields 和 layout 中提取属性并生成每个表单项的布局属性.
 * @param fields
 * @param layout
 */
export function generateFormItemLayoutProps(
  fields: Array<AntDesignVueFormItem>,
  layout: NFormFieldLayout
): Record<string, ColProps> {
  const _layout = { ...layout };
  const _designatedLayout = _layout.fields ?? {};
  delete _layout.fields;

  const result: Record<string, ColProps> = {};
  for (let i = fields.length; i--; ) {
    const field = fields[i];
    const name = field.options.name;

    const designatedFieldLayout = _designatedLayout[name];
    const generalFieldLayout = cloneDeep(_layout);

    result[name] = isObject(designatedFieldLayout)
      ? merge(generalFieldLayout, designatedFieldLayout)
      : generalFieldLayout;
  }

  return result;
}

/**
 * 从 layout 中提取属性并生成包裹表单项的 Row 组件的布局属性.
 * @param layout
 */
export function generateFormItemWrapperLayoutProps(layout: NFormFieldLayout): RowProps {
  return {
    align: layout.align,
    gutter: layout.gutter,
    justify: layout.justify,
    wrap: layout.wrap
  };
}

/**
 * 从 layout 中提取属性并生成包裹表单项的 Row 组件的布局属性.
 */
export function generateFormItemStatusProps(
  fields: Array<AntDesignVueFormItem>,
  status: NFormFieldStatus
): Record<string, FieldStatus> {
  const _designatedStatus = status.fields || {};
  const _status: Omit<NFormFieldStatus, "fields"> = {
    disabled: status.disabled,
    readOnly: status.readOnly
  };

  const result: Record<string, FieldStatus> = {};
  for (let i = fields.length; i--; ) {
    const field = fields[i];
    const name = field.options.name;

    const designatedFieldStatus = _designatedStatus[name];
    const generalFieldStatus = cloneDeep(_status);

    result[name] = isObject(designatedFieldStatus)
      ? merge(generalFieldStatus, designatedFieldStatus)
      : generalFieldStatus;
  }

  return result;
}
