import type {
  FormHelperOptions,
  FormItemControlArgs,
  FormItemControlOptions,
  FormItemOptions
} from "../../form-helper";
import { FormHelper, FormItem, FormItemControlType } from "../../form-helper";
import type { AntDesignVueFormItemOptions } from "./ant-design-vue-form-item";
import { AntDesignVueFormItem } from "./ant-design-vue-form-item";
import type { FormItemControlPropsMap } from "../types";

export class AntDesignVueFormHelper extends FormHelper {
  constructor(options: Partial<FormHelperOptions> = {}) {
    super(options);
  }

  protected buildFormItem<TYPE>(
    options: Pick<FormItemOptions, "name"> & Partial<Omit<FormItemOptions, "name">>,
    control: FormItemControlArgs<TYPE>
  ): FormItem<TYPE> {
    return new AntDesignVueFormItem(options, control);
  }
}

export type AntDesignVueFormModelField<TYPE> = Pick<AntDesignVueFormItemOptions, "name"> &
  Partial<Omit<AntDesignVueFormItemOptions, "control" | "name"> & FormItemControlOptions<TYPE>>;

/**
 * 适应 Ant Design Vue 的表单项的属性. 详见 {@link FormModelFieldArgs}
 */
export type AntDesignVueFormModelFieldArgs<
  TYPE = any,
  CONTROL extends FormItemControlType = FormItemControlType.INPUT
> =
  | AntDesignVueFormModelField<TYPE>
  | [AntDesignVueFormModelField<TYPE>, ...Array<any>]
  | [AntDesignVueFormModelField<TYPE>, FormItemControlPropsMap[CONTROL], ...Array<any>];
