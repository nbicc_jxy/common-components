import { reactive } from "vue";
import { toString } from "lodash-es";
import { FormItemControl, FormItemControlType } from "../../form-helper";
import type { FormItemControlOptions } from "../../form-helper";
import type { FormItemControlPropsMap } from "../types";
import dayjs from "dayjs";
import type { Dayjs } from "dayjs";

const { isDayjs } = dayjs;

export class AntDesignVueFormItemControl<
  TYPE,
  CONTROL extends FormItemControlType = FormItemControlType.INPUT
> extends FormItemControl<TYPE> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);

    this.props = args[0] || {};
    this.componentProps = this.generateComponentProps();
  }

  protected props: FormItemControlPropsMap[CONTROL];

  componentProps: Record<string, any>;

  get value(): TYPE {
    return this.componentProps.value;
  }

  set value(value: TYPE) {
    this.componentProps["value"] = value;
  }

  get displayValue(): string {
    return toString(this.value);
  }

  protected generateComponentProps(): Record<string, any> {
    const { trim, number, lazy } = this.options.modifiers;
    const props = reactive({
      ...this.props,
      ...this.options.events,
      value: this.options.initialValue,
      valueModifiers: { trim, number, lazy },
      "onUpdate:value": (value: TYPE) => {
        props.value = value;
        this.onValueChange(value);
      }
    }) as Record<string, any>;

    return props;
  }
}

class AntDesignVueFormItemControlInput<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.INPUT
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlInputNumber<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.INPUT_NUMBER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlSelect<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.SELECT
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlDatePicker<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.DATE_PICKER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  get displayValue(): string {
    const value = this.value;
    const isShowTime = this.props.showTime;

    const formatStr = isShowTime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD";

    return isDayjs(value) ? value.format(formatStr) : toString(value);
  }
}

class AntDesignVueFormItemControlDateRangePicker<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.DATE_RANGE_PICKER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  get displayValue(): string {
    const value = this.value as [Dayjs, Dayjs];
    // @ts-ignore
    const isShowTime = this.props.showTime;

    const formatStr = isShowTime ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD";

    return value.map((v) => (isDayjs(v) ? v.format(formatStr) : toString(v))).join(" ~ ");
  }
}

class AntDesignVueFormItemControlTimePicker<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.TIME_PICKER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  get displayValue(): string {
    const value = this.value;
    const formatStr = "HH:mm:ss";

    return isDayjs(value) ? value.format(formatStr) : toString(value);
  }
}

class AntDesignVueFormItemControlTimeRangePicker<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.TIME_RANGE_PICKER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  get displayValue(): string {
    const value = this.value as [Dayjs, Dayjs];
    const formatStr = "HH:mm:ss";

    return value.map((v) => (isDayjs(v) ? v.format(formatStr) : toString(v))).join(" ~ ");
  }
}

class AntDesignVueFormItemControlSwitch<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.SWITCH
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  get value(): TYPE {
    return this.componentProps.checked;
  }

  set value(value: TYPE) {
    this.componentProps["checked"] = value;
  }

  protected generateComponentProps(): Record<string, any> {
    const props = super.generateComponentProps();

    props.checked = props.value;
    props["checkedModifiers"] = props["valueModifiers"];
    props["onUpdate:checked"] = (value: TYPE) => {
      props.checked = value;
      this.onValueChange(value);
    };

    delete props.value;
    delete props["onUpdate:value"];
    delete props["valueModifiers"];

    return props;
  }
}

class AntDesignVueFormItemControlRadioGroup<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.RADIO_GROUP
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlCheckboxGroup<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.CHECKBOX_GROUP
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlTextArea<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.TEXT_AREA
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlSlider<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.SLIDER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlTreeSelect<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.TREE_SELECT
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlUpload<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.UPLOAD
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }

  protected generateComponentProps(): Record<string, any> {
    const props = super.generateComponentProps();

    props.fileList = props.value;
    props["fileListModifiers"] = props["valueModifiers"];
    props["onUpdate:fileList"] = (value: TYPE) => {
      props.fileList = value;
      this.onValueChange(value);
    };

    delete props.value;
    delete props["onUpdate:value"];
    delete props["valueModifiers"];

    return props;
  }
}

class AntDesignVueFormItemControlCascader<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.CASCADER
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

class AntDesignVueFormItemControlAuthLoadSelect<TYPE> extends AntDesignVueFormItemControl<
  TYPE,
  FormItemControlType.AUTO_LOAD_SELECT
> {
  constructor(options: Partial<FormItemControlOptions<TYPE>>, ...args: Array<any>) {
    super(options, ...args);
  }
}

export function buildFormItemControl<TYPE>(
  options: Partial<FormItemControlOptions<TYPE>>,
  ..._args: Array<any>
): FormItemControl<TYPE> {
  const type: FormItemControlType = options.type!;
  const [props, ...args] = _args;

  let control: AntDesignVueFormItemControl<TYPE>;
  switch (type) {
    case FormItemControlType.INPUT:
      control = new AntDesignVueFormItemControlInput<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.INPUT_NUMBER:
      control = new AntDesignVueFormItemControlInputNumber<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.SELECT:
      control = new AntDesignVueFormItemControlSelect<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.AUTO_LOAD_SELECT:
      control = new AntDesignVueFormItemControlAuthLoadSelect<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.DATE_PICKER:
      control = new AntDesignVueFormItemControlDatePicker<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.DATE_RANGE_PICKER:
      control = new AntDesignVueFormItemControlDateRangePicker<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.TIME_PICKER:
      control = new AntDesignVueFormItemControlTimePicker<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.TIME_RANGE_PICKER:
      control = new AntDesignVueFormItemControlTimeRangePicker<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.SWITCH:
      // @ts-ignore
      control = new AntDesignVueFormItemControlSwitch<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.RADIO_GROUP:
      control = new AntDesignVueFormItemControlRadioGroup<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.CHECKBOX_GROUP:
      control = new AntDesignVueFormItemControlCheckboxGroup<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.TEXT_AREA:
      control = new AntDesignVueFormItemControlTextArea<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.SLIDER:
      control = new AntDesignVueFormItemControlSlider<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.TREE_SELECT:
      control = new AntDesignVueFormItemControlTreeSelect<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.UPLOAD:
      // @ts-ignore
      control = new AntDesignVueFormItemControlUpload<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    case FormItemControlType.CASCADER:
      control = new AntDesignVueFormItemControlCascader<TYPE>(
        options,
        props,
        ...args
      ) as AntDesignVueFormItemControl<TYPE>;
      break;
    default:
      throw new Error(`未知的FormItemControlType: ${type}`);
  }

  return control;
}
