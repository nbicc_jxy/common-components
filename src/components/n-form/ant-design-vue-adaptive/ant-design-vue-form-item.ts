import type { FormItemProps } from "ant-design-vue/lib/form";
import { FormItem, FormItemControl } from "../../form-helper";
import type {
  FormItemOptions,
  FormItemControlOptions,
  FormItemControlArgs
} from "../../form-helper";
import {
  AntDesignVueFormItemControl,
  buildFormItemControl
} from "./ant-design-vue-form-item-control";

export class AntDesignVueFormItem<TYPE = any> extends FormItem<TYPE> {
  constructor(
    options: Pick<FormItemOptions, "name"> & Partial<Omit<FormItemOptions, "name">>,
    control: FormItemControlArgs<TYPE>
  ) {
    super(options, control);

    this.antDesignVueControl = this.control as AntDesignVueFormItemControl<TYPE>;
  }

  antDesignVueControl: AntDesignVueFormItemControl<TYPE>;

  protected buildFormItemControl(
    options: Partial<FormItemControlOptions<TYPE>>,
    ...args: Array<any>
  ): FormItemControl<TYPE> {
    return buildFormItemControl(options, ...args);
  }
}

export type AntDesignVueFormItemOptions = FormItemOptions & FormItemProps;
