import { FormValidator } from "../../form-helper";
import type { FormInstance } from "ant-design-vue";
import type { Ref } from "vue";

/**
 * 适配 ant-design-vue 的表单校验器.
 */
export class AntDesignVueFormValidator extends FormValidator {
  constructor(public formInstance: Ref<FormInstance>) {
    super();
  }

  override validate(nameList: Array<string>): Promise<any> {
    return this.formInstance.value?.validate(nameList);
  }
}
