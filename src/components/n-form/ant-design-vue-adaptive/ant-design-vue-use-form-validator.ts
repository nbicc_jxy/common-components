import { FormValidator } from "../../form-helper";
import type { Form } from "ant-design-vue";

/**
 * 适配 ant-design-vue useForm 的表单校验器.
 */
export class AntDesignVueUseFormValidator extends FormValidator {
  constructor(public useFormResult: ReturnType<typeof Form.useForm>) {
    super();
  }

  async validate(nameList: Array<string>): Promise<any> {
    return this.useFormResult.validate(nameList);
  }
}
