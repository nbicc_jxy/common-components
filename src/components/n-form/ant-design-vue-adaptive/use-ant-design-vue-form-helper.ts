import type { FormItemControlType, FormValidator } from "../../form-helper";
import { AntDesignVueFormHelper } from "./ant-design-vue-form-helper";

export function useAntDesignVueFormHelper(
  onChange: (name: string, value: any) => void,
  onValidateError: (error: any) => void,
  presets: Array<Partial<Record<FormItemControlType, any>>> = [],
  formValidator: FormValidator
) {
  const formHelper = new AntDesignVueFormHelper({
    data: {},
    presets,
    validator: formValidator
  });

  formHelper.onFormItemControlChange = onChange;
  formHelper.onValidateError = onValidateError;

  return {
    formHelper
  };
}
