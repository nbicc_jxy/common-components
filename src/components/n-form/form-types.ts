import type { ColProps, RowProps } from "ant-design-vue";
import type { FormExpose } from "ant-design-vue/lib/form/Form";
import type { PropType, VNode, ExtractPropTypes, ComponentPublicInstance } from "vue";
import type { AntDesignVueFormModelFieldArgs } from "./ant-design-vue-adaptive";
import { AntDesignVueFormItem } from "./ant-design-vue-adaptive";
import type { NSpecProviderFormConfig } from "../n-spec-provider";
import type { ValidateInfo } from "ant-design-vue/lib/form/useForm";

export const nFormProps = () =>
  ({
    /**
     * 表单项的集合, 用于描述表单的结构.
     */
    fields: {
      type: Array as PropType<Array<AntDesignVueFormModelFieldArgs>>,
      default: () => []
    },
    /**
     * 表单项的布局, 用于自定义表单项的布局. 集合了 Ant Design Vue 的 Row 和 Col 组件的 props.
     * @see https://www.antdv.com/components/grid/#Row
     * @see https://www.antdv.com/components/grid/#Col
     */
    fieldLayout: {
      type: Object as PropType<NFormFieldLayout>,
      default: () => ({ span: 24 })
    },
    /**
     * 表单项的状态, 用于描述表单项的状态. 默认值为 `{ disabled: false, readOnly: false }`.
     * * `disabled`: 表单项是否禁用. 设置为 true 时, 表单项控件不可编辑.
     * * `readOnly`: 表单项是否只读. 设置为 true 时, 将仅展示表单项的值不会渲染控件.
     */
    fieldStatus: {
      type: Object as PropType<Partial<NFormFieldStatus>>,
      default: () => ({})
    },
    /**
     * 表单数据对象, 可以用 v-model:data 双向绑定.
     */
    data: {
      type: Object as PropType<Record<string, any>>,
      default: () => ({})
    },
    "onUpdate:data": {
      type: Function as PropType<(data: Record<string, any>) => void>,
      default: () => {}
    },
    onValidateError: {
      type: Function as PropType<(error: any) => void>
    },
    contextSlots: {
      type: Object as PropType<{
        /**
         * 表单项的插槽, 用于自定义表单项的布局.
         * @param props
         */
        // formItems: (props: { formItemNodes: Array<VNode> }) => Array<VNode>;
      }>,
      default: () => ({})
    }
  } as const);

export interface NFormEmits {
  "update:data": (data: Record<string, any>) => void;
  validateError: (error: any) => void;
}

export type NFormProps = ExtractPropTypes<ReturnType<typeof nFormProps>>;

export type NFormExpose = NBaseFormExpose;

export type NFormInstance = ComponentPublicInstance<NFormProps, NFormExpose>;

export const nEasyFormProps = () =>
  ({
    /**
     * 未展开时显示的行数, 默认值为 2. 可以通过对象的方式设置不同屏幕尺寸下的行数.
     */
    defaultExpandRowCount: {
      type: Number as PropType<
        | number
        | Partial<{
            xs: number;
            sm: number;
            md: number;
            lg: number;
            xl: number;
            xxl: number;
            xxxl: number;
          }>
      >
    },
    /**
     * search 事件触发前的钩子, 返回 false 或 Promise.reject 时将阻止 search 事件的触发.
     */
    beforeSearch: {
      type: Function as PropType<(data: Record<string, any>) => boolean | Promise<boolean>>
    },
    /**
     * 表单数据对象, 可以用 v-model:data 双向绑定. data 仅在 search 事件触发时更新.
     */
    data: {
      type: Object as PropType<Record<string, any>>,
      default: () => ({})
    },
    "onUpdate:data": {
      type: Function as PropType<(data: Record<string, any>) => void>,
      default: () => {}
    },
    // /**
    //  * 是否展开
    //  */
    // expanded: {
    //   type: Boolean,
    //   default: false
    // },
    /**
     * 搜索按钮点击事件
     */
    onSearch: {
      type: Function as PropType<() => void>
    },
    /**
     * 重置按钮点击事件
     */
    onReset: {
      type: Function as PropType<() => void>
    },
    /**
     * 表单数据变化时触发的事件.
     */
    onChange: {
      type: Function as PropType<(data: Record<string, any>) => void>
    }
  } as const);

export interface NEasyFormEmits {
  "update:data": (data: Record<string, any>) => void;
  search: () => void;
  reset: () => void;
  change: (data: Record<string, any>) => void;
}

export type NEasyFormProps = ExtractPropTypes<ReturnType<typeof nEasyFormProps>>;

export interface NEasyFormExpose extends FormExpose {
  search: () => void;
  reset: () => void;
}

export type NEasyFormInstance = ComponentPublicInstance<NEasyFormProps, NEasyFormExpose>;

/**
 * 表单项的布局, 用于自定义表单项的布局. 与 ant-design-vue Col 组件的 props 一致.
 * @see https://www.antdv.com/components/grid-cn#Col
 */
type FieldLayout = ColProps & RowProps;

export interface NFormFieldLayout extends FieldLayout {
  /**
   * 可通过此属性单独指定某个表单项的布局属性.
   *
   * 以此属性的值为 key, 以 ant-design-vue Col 组件的 props 为 value.
   */
  fields?: Record<string, ColProps>;
}

export interface FieldStatus {
  /**
   * 表单项是否禁用. 设置为 true 时, 表单项控件不可编辑.
   */
  disabled: boolean;
  /**
   * 表单项是否只读. 设置为 true 时, 将仅展示表单项的值不会渲染控件.
   */
  readOnly: boolean;
}

/**
 * 表单项的状态, 用于描述表单项的状态. 可通过 `fields` 属性单独指定某个表单项的状态.
 */
export interface NFormFieldStatus extends FieldStatus {
  /**
   * 可通过此属性单独指定某个表单项的状态.
   */
  fields: Record<string, FieldStatus>;
}

export const nFormItemProps = () =>
  ({
    /**
     * 表单项配置属性. 见 {@link nFormProps.fields}.
     */
    field: {
      type: AntDesignVueFormItem,
      required: true
    },
    /**
     * 表单项状态配置, 见 {@link nFormProps.fieldStatus}.
     */
    status: {
      type: Object as PropType<Partial<FieldStatus>>
    },
    validateInfo: {
      type: Object as PropType<ValidateInfo>
    },
    contextSlots: {
      type: Object as PropType<{
        control: () => Array<VNode>;
      }>,
      default: () => ({})
    }
  } as const);

export type NFormItemProps = ExtractPropTypes<ReturnType<typeof nFormItemProps>>;

/**
 * 为 {@link NFormItem} 的插槽提供带有响应式的表单数据.
 */
export const NBaseFormExternalFormDataKey = "N_BASE_FORM_EXTERNAL_FORM_DATA_KEY";

export const nBaseFormProps = () =>
  ({
    ...nFormProps(),
    specConfig: {
      type: Object as PropType<NSpecProviderFormConfig>,
      required: true
    }
  } as const);

export type NBaseFormProps = ExtractPropTypes<ReturnType<typeof nBaseFormProps>>;

export interface NBaseFormExpose extends FormExpose {
  setField: (name: string, value: any) => void;
  getFormData: () => Record<string, any>;
}

export type NBaseFormInstance = ComponentPublicInstance<NBaseFormProps, NBaseFormExpose>;
