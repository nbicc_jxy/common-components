/* 翻译自 https://github.com/vueComponent/ant-design-vue/blob/main/components/form/utils/messages.ts */

const typeTemplate = "'${name}' 不是一个有效的 ${type}";

export const defaultValidateMessages = {
  default: "字段 '${name}' 验证失败",
  required: "'${name}' 是必填字段",
  enum: "'${name}' 必须是 [${enum}] 中的一个",
  whitespace: "'${name}' 不能为空",
  date: {
    format: "'${name}' 日期格式无效",
    parse: "'${name}' 无法被解析为日期",
    invalid: "'${name}' 日期无效"
  },
  types: {
    string: typeTemplate,
    method: typeTemplate,
    array: typeTemplate,
    object: typeTemplate,
    number: typeTemplate,
    date: typeTemplate,
    boolean: typeTemplate,
    integer: typeTemplate,
    float: typeTemplate,
    regexp: typeTemplate,
    email: typeTemplate,
    url: typeTemplate,
    hex: typeTemplate
  },
  string: {
    len: "'${name}' 必须是 ${len} 个字符",
    min: "'${name}' 不能少于 ${min} 个字符",
    max: "'${name}' 不能多于 ${max} 个字符",
    range: "'${name}' 必须是 ${min} 到 ${max} 个字符"
  },
  number: {
    len: "'${name}' 必须等于 ${len}",
    min: "'${name}' 不能小于 ${min}",
    max: "'${name}' 不能大于 ${max}",
    range: "'${name}' 必须是 ${min} 到 ${max} 之间的数字"
  },
  array: {
    len: "'${name}' 必须是 ${len} 个元素",
    min: "'${name}' 不能少于 ${min} 个元素",
    max: "'${name}' 不能多于 ${max} 个元素",
    range: "'${name}' 必须是 ${min} 到 ${max} 个元素"
  },
  pattern: {
    mismatch: "'${name}' 与模式 ${pattern} 不匹配"
  }
};
