import type {
  InputProps,
  SelectProps,
  RadioGroupProps,
  CheckboxGroupProps,
  SwitchProps,
  DatePickerProps,
  TimeRangePickerProps,
  TimePickerProps,
  TextAreaProps,
  SliderProps,
  TreeSelectProps,
  UploadProps,
  CascaderProps
} from "ant-design-vue";
import {
  Cascader,
  CheckboxGroup,
  DatePicker,
  RangePicker,
  Input,
  RadioGroup,
  Select,
  Slider,
  Switch,
  Textarea,
  TimePicker,
  TimeRangePicker,
  TreeSelect,
  Upload
} from "ant-design-vue";
import { NAutoLoadSelect } from "../n-select";
import type { NAutoLoadSelectProps } from "../n-select";
import { FormItemControlType } from "../form-helper";
import type { RangePickerProps } from "ant-design-vue/lib/date-picker";
import type { NInputNumberProps } from "../n-input-number";
import { NInputNumber } from "../n-input-number";

export interface FormItemControlPropsMap {
  [FormItemControlType.INPUT]: InputProps;
  [FormItemControlType.INPUT_NUMBER]: NInputNumberProps;
  [FormItemControlType.SELECT]: SelectProps;
  [FormItemControlType.AUTO_LOAD_SELECT]: NAutoLoadSelectProps;
  [FormItemControlType.DATE_PICKER]: DatePickerProps;
  [FormItemControlType.DATE_RANGE_PICKER]: RangePickerProps;
  [FormItemControlType.TIME_PICKER]: TimePickerProps;
  [FormItemControlType.TIME_RANGE_PICKER]: TimeRangePickerProps;
  [FormItemControlType.SWITCH]: SwitchProps;
  [FormItemControlType.RADIO_GROUP]: RadioGroupProps;
  [FormItemControlType.CHECKBOX_GROUP]: CheckboxGroupProps;
  [FormItemControlType.TEXT_AREA]: TextAreaProps;
  [FormItemControlType.SLIDER]: SliderProps;
  [FormItemControlType.TREE_SELECT]: TreeSelectProps;
  [FormItemControlType.UPLOAD]: UploadProps;
  [FormItemControlType.CASCADER]: CascaderProps;
  [FormItemControlType.CUSTOM]: Record<string, unknown>;
}

export const FormItemControlComponentMap = {
  [FormItemControlType.INPUT]: Input,
  [FormItemControlType.INPUT_NUMBER]: NInputNumber,
  [FormItemControlType.SELECT]: Select,
  [FormItemControlType.AUTO_LOAD_SELECT]: NAutoLoadSelect,
  [FormItemControlType.DATE_PICKER]: DatePicker,
  [FormItemControlType.DATE_RANGE_PICKER]: RangePicker,
  [FormItemControlType.TIME_PICKER]: TimePicker,
  [FormItemControlType.TIME_RANGE_PICKER]: TimeRangePicker,
  [FormItemControlType.SWITCH]: Switch,
  [FormItemControlType.RADIO_GROUP]: RadioGroup,
  [FormItemControlType.CHECKBOX_GROUP]: CheckboxGroup,
  [FormItemControlType.TEXT_AREA]: Textarea,
  [FormItemControlType.SLIDER]: Slider,
  [FormItemControlType.TREE_SELECT]: TreeSelect,
  [FormItemControlType.UPLOAD]: Upload,
  [FormItemControlType.CASCADER]: Cascader,
  [FormItemControlType.CUSTOM]: null
} as const;
