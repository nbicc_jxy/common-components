import type { Ref } from "vue";
import { elementRect } from "../utils";
import { computed, onMounted, reactive } from "vue";
import { isNil } from "lodash-es";

export function useExpandAdaptiveHeight(
  elementRef: Ref<Element>,
  expandedRef: Ref<boolean | undefined>,
  defaultExpandRowCount: Ref<number>
) {
  const colRectMap = reactive(new Map()) as Map<Element, Ref<DOMRectReadOnly>>;
  const rectObject = reactive({}) as { gridRow: DOMRectReadOnly };

  onMounted(() => {
    rectObject.gridRow = elementRect.watch(
      elementRef.value.querySelector(".n-form__grid_row")!
    ) as unknown as DOMRectReadOnly;
  });

  const rowMaxHeightList = computed(() => {
    if (isNil(rectObject.gridRow) || rectObject.gridRow.width <= 0) return [];

    const gridRowRect = rectObject.gridRow;
    const colRects = Array.from(colRectMap.values());

    /**
     * 注: 以下计算方式未考虑到 col 的内外边距. 当内外边距较大时会影响到是否换行的判断.
     */
    const groupedRects: Array<Array<Ref<DOMRectReadOnly>>> = [];
    let remainWidth = 0;
    for (let i = 0; i < colRects.length; i++) {
      const rect = colRects[i];

      // 剩余宽度不足以容纳当前矩形, 换行.
      if (remainWidth < rect.value.width) {
        remainWidth = gridRowRect.width;
        groupedRects.push([]);
      }
      remainWidth -= rect.value.width;

      groupedRects[groupedRects.length - 1].push(rect);
    }

    return groupedRects.map((rects) => Math.max(...rects.map((rect) => rect.value.height)));
  }) as Ref<Array<number>>;

  const expandHeight = computed(() => {
    if (expandedRef.value) {
      return rowMaxHeightList.value.reduce((prev, curr) => prev + curr, 0);
    } else {
      const rowHeightList = rowMaxHeightList.value.slice(0, defaultExpandRowCount.value);

      let totalHeight = 0;
      for (let i = 0; i < rowHeightList.length; i++) {
        totalHeight += rowHeightList[i];
      }

      return totalHeight;
    }
  });

  onMounted(() => {
    refreshColRect();
  });

  return {
    expandHeight,
    rowMaxHeightList
  };

  function refreshColRect() {
    const container = elementRef.value;
    const row = elementRef.value.querySelector(".n-form__grid_row");
    if (!container || !row) return;

    // 清理
    for (const [col] of colRectMap) elementRect.unwatch(col);
    colRectMap.clear();

    const cols = row.querySelectorAll(".n-form__grid_col");
    for (let i = 0; i < cols.length; i++) {
      const col = cols[i];
      colRectMap.set(col, elementRect.watch(col));
    }
  }
}
