import type { NSpecProviderFormConfig, NSpecProviderFormItemConfig } from "../n-spec-provider";
import { FormItemControlType } from "../form-helper";

export function generateFormHelperPreset(
  specConfig: NSpecProviderFormConfig
): Record<FormItemControlType, NSpecProviderFormItemConfig> {
  const { field, ...restConfig } = specConfig;
  const result = {} as Record<FormItemControlType, NSpecProviderFormItemConfig>;

  const controlTypes = Object.keys(FormItemControlType).map(
    (key) => FormItemControlType[key as keyof typeof FormItemControlType]
  ) as Array<FormItemControlType>;
  for (let i = controlTypes.length; i--; ) {
    const controlType = controlTypes[i];
    const designatedFieldConfig = field[controlType] ?? {};

    result[controlType] = {
      ...restConfig,
      ...designatedFieldConfig
    };
  }

  return result;
}

export function generateCustomFormHelperPreset(
  callback: (
    type: FormItemControlType,
    preset: Record<FormItemControlType, Record<string, any>>
  ) => Record<string, any>
): Record<FormItemControlType, Record<string, any>> {
  const result = {} as Record<FormItemControlType, Record<string, any>>;
  const controlTypes = Object.keys(FormItemControlType).map(
    (key) => FormItemControlType[key as keyof typeof FormItemControlType]
  ) as Array<FormItemControlType>;

  for (let i = controlTypes.length; i--; ) {
    const controlType = controlTypes[i];
    result[controlType] = callback(controlType, result);
  }

  return result;
}
