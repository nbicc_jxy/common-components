import type { NEasyFormProps } from "../form-types";
import { isNumber, isObject } from "lodash-es";
import { breakpoints } from "../../utils/hooks";
import type { Breakpoint } from "../../utils/hooks";

/**
 * 补全 {@link NEasyFormProps.defaultExpandRowCount} 的值
 * @param defaultExpandRowCount
 */
export function completionExpandRows(
  defaultExpandRowCount: NEasyFormProps["defaultExpandRowCount"]
) {
  const result = breakpoints.reduce(
    (acc, cur) => ((acc[cur] = 0), acc),
    {} as { [key in Breakpoint]: number }
  );

  if (isNumber(defaultExpandRowCount)) {
    (Object.keys(result) as Array<keyof typeof result>).forEach(
      (key) => (result[key] = defaultExpandRowCount)
    );
  } else if (isObject(defaultExpandRowCount)) {
    let minBreakPointValue = 1;
    for (let i = 0; i < breakpoints.length; i++) {
      const value = defaultExpandRowCount[breakpoints[i]];

      if (!isNumber(value)) continue;

      minBreakPointValue = value;
      break;
    }

    for (let i = 0; i < breakpoints.length; i++) {
      const value = defaultExpandRowCount[breakpoints[i]];

      if (isNumber(value)) {
        result[breakpoints[i]] = value;
        minBreakPointValue = value;
      } else {
        result[breakpoints[i]] = minBreakPointValue;
      }
    }
  }

  return result;
}
