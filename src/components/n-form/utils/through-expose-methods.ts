import type { Ref } from "vue";
import type { PickByValueType } from "../../common/types";

export function throughExposeMethods<
  T extends { [key: string]: any },
  K extends keyof PickByValueType<T, (...args: any) => any>
>(instance: Ref<T>, methods: Array<K>) {
  const result = {} as {
    [P in K]: (...args: Parameters<T[P]>) => ReturnType<T[P]>;
  };

  for (let i = 0; i < methods.length; i++) {
    const method = methods[i];
    result[method] = (...args: any) => instance.value[method]?.(...args);
  }

  return result;
}
