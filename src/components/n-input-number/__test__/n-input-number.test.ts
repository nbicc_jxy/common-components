import { expect, test } from "vitest";
import { mount } from "@vue/test-utils";
import { NInputNumber } from "../";
import type { ValueType } from "ant-design-vue/lib/input-number/src/utils/MiniDecimal";

test("NInputNumber 启用四舍五入时正确转换精度", async () => {
  // @ts-ignore
  const wrapper = mount(NInputNumber, {
    props: {
      value: 0,
      "onUpdate:value": (value: ValueType) => wrapper.setProps({ value }),
      round: true,
      precision: 2
    }
  });

  const input = wrapper.find("input");
  await input.setValue("1.235");
  await input.trigger("blur");
  expect(wrapper.props("value")).toBe(1.24);
});

test("NInputNumber 禁用四舍五入时正确转换精度", async () => {
  // @ts-ignore
  const wrapper = mount(NInputNumber, {
    props: {
      value: 0,
      "onUpdate:value": (value: ValueType) => wrapper.setProps({ value }),
      round: false,
      precision: 2
    }
  });

  const input = wrapper.find("input");
  await input.setValue("1.235");
  await input.trigger("blur");
  expect(wrapper.props("value")).toBe(1.23);
});
