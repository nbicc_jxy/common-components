import type { PropType } from "vue";
import type { InputNumberProps } from "ant-design-vue";
import { storableProps } from "../../storage";
import type { ExtractPropTypes } from "vue";

export const nInputNumberProps = () =>
  ({
    ...storableProps(),
    value: {
      type: Number as PropType<InputNumberProps["value"]>
    },
    "onUpdate:value": {
      type: Function as PropType<InputNumberProps["onUpdate:value"]>
    },
    formatter: {
      type: Function as PropType<InputNumberProps["formatter"]>
    },
    parser: {
      type: Function as PropType<InputNumberProps["parser"]>
    },
    precision: {
      type: Number as PropType<InputNumberProps["precision"]>
    },
    precisionRange: {
      type: Array as PropType<Array<number>>
    },
    round: {
      type: Boolean as PropType<Boolean>,
      default: true
    }
  } as const);

export type NInputNumberProps = ExtractPropTypes<ReturnType<typeof nInputNumberProps>>;
