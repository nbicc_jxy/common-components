import type { Ref } from "vue";
import type { NInputNumberProps } from "./input-number-types";
import { computed } from "vue";
import { isNumber } from "lodash-es";

export function usePrecisionRange(
  precision: Ref<NInputNumberProps["precision"]>,
  precisionRange: Ref<NInputNumberProps["precisionRange"]>
) {
  const minPrecision = computed(() => precisionRange.value?.[0] ?? 0);
  const maxPrecision = computed(() => precisionRange.value?.[1]);

  const getClampedPrecision = (value: string) => {
    // precision 优先级最高
    if (isNumber(precision.value)) return precision.value;

    const [, decimalPart] = value.split(".");
    const clampedPrecision = decimalPart?.length ?? 0;

    if (clampedPrecision < minPrecision.value) return minPrecision.value;
    else if (isNumber(maxPrecision.value) && clampedPrecision > maxPrecision.value)
      return maxPrecision.value;
    else return clampedPrecision;
  };

  return {
    getClampedPrecision
  };
}
