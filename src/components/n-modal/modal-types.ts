import type { ExtractPropTypes, PropType, VNode } from "vue";
import type { ModalProps } from "ant-design-vue";

export const nModalWrapperProps = () => ({
  provide: {
    type: Boolean,
    default: true
  }
});
export type NModalWrapperProps = ExtractPropTypes<ReturnType<typeof nModalWrapperProps>>;

export const NModalWrapperContextKey = "MODAL_WRAPPER_CONTEXT_KEY";

export interface NModalWrapperContext {
  modalFunc: (props: NModalFuncProps) => { destroy: () => void };
  showModal: (
    modalComponent: VNode,
    props?: Record<string, any>,
    modalProps?: Omit<ModalProps, "visible" | "onUpdate:visible"> & Partial<NModalProps>
  ) => { destroy: () => void };
}

export interface NModalFuncProps extends ModalProps {
  content?: string | (() => VNode) | VNode;
}

export const nModalProps = () =>
  ({
    initialData: {
      type: Function as PropType<() => Promise<Record<string, any>> | Record<string, any>>,
      default: () => undefined
    },
    loadingText: {
      type: String,
      default: "加载中..."
    }
  } as const);

export type NModalProps = ExtractPropTypes<ReturnType<typeof nModalProps>>;

export const nModalContainerProps = () =>
  ({
    ...nModalProps(),
    beforeOpen: {
      type: Function as PropType<() => Promise<boolean | void> | boolean | void>,
      default: () => true
    },
    beforeClose: {
      type: Function as PropType<() => Promise<boolean | void> | boolean | void>,
      default: () => true
    }
  } as const);

export type NModalContainerProps = ExtractPropTypes<ReturnType<typeof nModalContainerProps>>;

/**
 * NModal 通过 {@link NModalParentTeleportKey} 为下级的 NModal 提供挂载点
 */
export const NModalParentTeleportKey = "MODAL_PARENT_TELEPORT_KEY";

/**
 * NModalContainer 通过 {@link NModalContainerProvideKey} 为内部的 NModal 提供默认的属性
 */
export const NModalContainerProvideKey = "MODAL_CONTAINER_PROVIDE_KEY";

/**
 * NModalContainer 通过 {@link NModalContextKey} 为下级组件提供操作 NModal 的方法. 如 `closeModal` 等
 */
export const NModalContextKey = "MODAL_CONTEXT_KEY";

export interface NModalContext {
  closeModal: () => void;
  setModalProps: (props: ModalProps & Partial<NModalProps>) => void;
}
