import { reactive, shallowReactive } from "vue";
import { Modal } from "ant-design-vue";
import type { NModalFuncProps, NModalWrapperContext } from "./modal-types";
import { isFunction, isNil, isString } from "lodash-es";
import NModalContainer from "./NModalContainer.vue";

export function useModalWrapper() {
  const legacyModalMap = reactive(new Map<string, NModalFuncProps>());
  const modalMap = shallowReactive(
    new Map<string, Parameters<NModalWrapperContext["showModal"]>>()
  );
  const modalContext: NModalWrapperContext = {
    modalFunc: (props) => {
      props = reactive(props);
      props["visible"] = true;
      props["onUpdate:visible"] = (visible) => (props.visible = visible);
      const id = Date.now().toString();
      legacyModalMap.set(id, props);

      return {
        destroy: () => {
          props["visible"] = false;
          legacyModalMap.delete(id);
        }
      };
    },
    showModal: (modalComponent, props, modalProps = {}) => {
      const id = Date.now().toString();
      modalMap.set(id, [modalComponent, props, modalProps]);

      /**
       * `afterClose`: Modal 完全关闭后的回调
       * Modal 关闭后再删除组件.
       */
      const afterClose = modalProps?.["afterClose"];
      modalProps["afterClose"] = () => {
        if (isFunction(afterClose)) afterClose();
        destroyModal();
      };
      const destroyModal = () => modalMap.delete(id);

      return {
        destroy: destroyModal
      };
    }
  };

  return {
    context: modalContext,
    render: () => {
      const legacyModals = Array.from(legacyModalMap.values()).map((props) => {
        const { content, footer, ...restProps } = props;

        const slots: Record<string, any> = {};
        if (!isNil(content))
          slots["default"] = isFunction(content)
            ? content
            : () => (isString(content) ? <>{content}</> : content);
        if (!isNil(footer))
          slots["footer"] = isFunction(footer)
            ? footer
            : () => (isString(footer) ? <>{footer}</> : footer);

        return <Modal {...restProps}>{slots}</Modal>;
      });

      const modals = Array.from(modalMap.values()).map(([component, props, modalProps]) => {
        return (
          <NModalContainer {...modalProps}>
            <component {...props} />
          </NModalContainer>
        );
      });

      return (
        <>
          {legacyModals}
          {modals}
        </>
      );
    }
  };
}
