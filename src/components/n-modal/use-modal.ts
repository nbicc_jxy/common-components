import {
  type NModalContext,
  NModalContextKey,
  type NModalWrapperContext,
  NModalWrapperContextKey
} from "./modal-types";
import { inject } from "vue";
import { isNil } from "lodash-es";

/**
 * 导出用于操作当前 NModal 的方法.
 */
export function useModal(): NModalContext {
  const modalMethods = inject<NModalContext>(NModalContextKey);
  if (isNil(modalMethods)) throw new Error("inject key [NModalContextKey] is not found");

  return modalMethods;
}

export function useModalManager(): NModalWrapperContext {
  const modalMethods = inject<NModalWrapperContext>(NModalWrapperContextKey);
  if (isNil(modalMethods)) throw new Error("inject key [NModalWrapperContextKey] is not found");

  return modalMethods;
}
