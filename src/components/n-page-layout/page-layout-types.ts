import type { VNode, PropType } from "vue";

export const nPageLayoutProps = () =>
  ({
    contextSlots: {
      type: Object as PropType<ContextSlots>,
      default: () => ({})
    }
  } as const);

export type NPageLayoutProps = ReturnType<typeof nPageLayoutProps>;

export interface NPageLayoutEmits {}

type ContextSlots = {
  default: () => Array<VNode>;
  header: () => Array<VNode>;
  content: () => Array<VNode>;
};
