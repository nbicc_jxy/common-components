import type { ComponentPublicInstance, PropType } from "vue";
import type { NPrintBaseExpose, NPrintPageProps, NPrintProps } from "../print-types";
import { nPrintProps } from "../print-types";

export type NPrintLodopInstance = ComponentPublicInstance<NPrintProps, NPrintBaseExpose>;

export const nPrintLodopProps = () =>
  ({
    ...nPrintProps(),
    pagingMode: {
      type: String as PropType<NPrintProps["pagingMode"] | NPrintLodopPagingMode>
    },
    /**
     * 自定义 LODOP 打印逻辑. 目前支持对打印流程的完整自定义, 以及对打开弹框流程的自定义
     *
     * * 完整自定义需要传入一个函数.
     * * 对打开弹框流程的自定义需要传入一个对象, 对象的 `print` 属性为一个函数.
     *
     * 函数签名为: `(props: Readonly<NPrintPageProps>, iframe: HTMLIFrameElement, lodop: any) => void | Promise<void>`.
     */
    customLodop: {
      type: [Function, Object] as PropType<
        NPrintLodopCustomPrintFunc | { print: NPrintLodopCustomPrintFunc }
      >
    },
    /**
     * C-LODOP 打印服务的配置.
     */
    serviceConfig: {
      type: Object as PropType<NPrintCLodopServiceConfig>,
      default: () => ({
        hostname: "localhost",
        port: 8000,
        standbyPort: 18000
      })
    },
    /**
     * 打印窗口关闭时的回调函数.
     *
     * @param count 窗口关闭前打印的次数. 为 0 时表示关闭窗口未打印.
     */
    onClose: {
      type: Function as PropType<(count: number) => void>,
      default: () => {}
    }
  } as const);

/**
 * 自定义 LODOP 打印逻辑时的参数.
 *
 * @param props 页面属性, 包括页面大小, 内外边距等.
 * @param htmlStr 打印内容
 * @param htmlStr.style 打印内容的样式, 为一个字符串.
 * @param htmlStr.pages 打印内容, 为一个数组, 每个元素为一个页面的内容.
 * @param lodop LODOP 对象.
 */
export type NPrintLodopCustomPrintFunc = (
  props: Readonly<NPrintPageProps>,
  iframe: HTMLIFrameElement,
  lodop: any
) => void | Promise<void>;

/**
 * C-LODOP 打印服务的配置.
 */
export type NPrintCLodopServiceConfig = {
  /**
   * 服务地址. 默认值为 `localhost`.
   */
  hostname: string;
  /**
   * 服务端口. 默认值为 `8000`.
   */
  port: number;
  /**
   * 备用端口. 默认值为 `18000`.
   */
  standbyPort: number;
};

export enum NPrintLodopPagingMode {
  /**
   * 打印表格. 支持表头/表尾在每页重复打印.
   */
  TABLE = "table"
}
