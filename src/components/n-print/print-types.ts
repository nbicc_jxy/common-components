import type { ComponentPublicInstance, ExtractPropTypes, PropType, VNode } from "vue";
import type { BreakAfterProperty, OverflowProperty } from "csstype";

export const nPrintProps = () =>
  ({
    /**
     * 打印时的文档标题
     */
    title: {
      type: String,
      default: ""
    },
    size: {
      type: [String, Array] as PropType<NPrintPageSize>,
      default: "A4"
    },
    orientation: {
      type: String as PropType<NPrintPageOrientation>,
      default: "portrait"
    },
    /**
     * 外边距
     */
    margin: {
      type: [String, Object] as PropType<NPrintMargin>,
      default: "0"
    },
    /**
     * 内边距
     */
    padding: {
      type: [String, Object] as PropType<NPrintMargin>,
      default: "0"
    },
    pagingMode: {
      type: String as PropType<NPrintPagingMode>,
      default: NPrintPagingMode.AUTO
    },
    onClose: {
      type: Function as PropType<() => void>,
      default: () => {}
    },
    onError: {
      type: Function as PropType<(error: Error) => void>,
      default: (error: Error) => {
        throw error;
      }
    },
    contextSlots: {
      type: Object as PropType<{
        default: () => Array<VNode>;
        header: () => Array<VNode>;
        toolbar: (props: { print: () => void }) => Array<VNode>;
      }>,
      default: () => ({})
    }
  } as const);

export type NPrintProps = ExtractPropTypes<ReturnType<typeof nPrintProps>>;

export type NPrintPresetPageSize =
  | "A5"
  | "A4"
  | "A3"
  | "B5"
  | "B4"
  | "JIS-B5"
  | "JIS-B4"
  | "letter"
  | "legal"
  | "ledger";
export type NPrintPageSize = NPrintPresetPageSize | string | [string, string];
/**
 * 纸张方向, 根据定义:
 * - portrait: 矩形平面以短边为底边的方向, 即高度大于等于宽度
 * - landscape: 矩形平面以长边为底边的方向, 即宽度大于高度
 *
 * @see https://zhuanlan.zhihu.com/p/19974005
 * @see https://developer.mozilla.org/en-US/docs/Web/CSS/@media/orientation
 */
export type NPrintPageOrientation = "portrait" | "landscape";
export type NPrintMarginObject = { top: string; bottom: string; left: string; right: string };
export type NPrintMargin = string | NPrintMarginObject;
export type NPrintPadding = string | NPrintMarginObject;

/**
 * 分页模式
 */
export enum NPrintPagingMode {
  /**
   * 自动分页, 当内容超出一页时自动分页.
   */
  AUTO = "auto",
  /**
   * 裁剪分页, 当内容超出一页时不分页, 超出部分被裁剪.
   */
  CLIP = "clip",
  /**
   * 紧凑分页, 类似自动分页, 但当一页纸能够完整渲染多个 {@link NPrintPage} 时, 会尽量将多个内容渲染在同一页纸上.
   */
  COMPACT = "compact"
}

export const nPrintPageProps = () =>
  ({
    width: {
      type: String,
      default: "0"
    },
    height: {
      type: String,
      default: "0"
    },
    margin: {
      type: Object as PropType<NPrintMarginObject>,
      default: () => ({})
    },
    padding: {
      type: Object as PropType<NPrintMarginObject>,
      default: () => ({})
    },
    breakAfter: {
      type: String as PropType<BreakAfterProperty>,
      default: ""
    },
    overflow: {
      type: String as PropType<OverflowProperty>,
      default: ""
    }
  } as const);

export type NPrintPageProps = ExtractPropTypes<ReturnType<typeof nPrintPageProps>>;

export const NPrintPagePropsKey = "PRINT_PAGE_PROPS_KEY";

export const nPrintBaseProps = () =>
  ({
    ...nPrintProps(),
    /**
     * 自定义打印函数, 用于自定义打印逻辑.
     */
    customPrint: {
      type: Function as PropType<NPrintBaseCustomPrintFunc>
    }
  } as const);

export type NPrintBaseCustomPrintFunc = (
  props: Readonly<NPrintProps>,
  element: HTMLElement
) => void | Promise<void>;

export type NPrintBaseProps = ExtractPropTypes<ReturnType<typeof nPrintBaseProps>>;

export type NPrintBaseExpose = {
  print: () => void;
};

export type NPrintBaseInstance = ComponentPublicInstance<NPrintBaseProps, NPrintBaseExpose>;

export type NPrintInstance = ComponentPublicInstance<NPrintProps, NPrintBaseExpose>;
