import { NPrintPagingMode } from "./print-types";
import type { NPrintPageProps, NPrintProps } from "./print-types";
import { generateMarginOrPaddingObject, parsePageSize } from "./utils";
import type { Ref } from "vue";
import { onUnmounted, ref, watchEffect } from "vue";
import type { BreakAfterProperty, OverflowProperty } from "csstype";

export function useComposePageProps(
  pagingMode: Ref<NPrintProps["pagingMode"]>,
  size: Ref<NPrintProps["size"]>,
  orientation: Ref<NPrintProps["orientation"]>,
  margin: Ref<NPrintProps["margin"]>,
  padding: Ref<NPrintProps["padding"]>
) {
  const styleObject = ref({}) as Ref<NPrintPageProps>;

  const stop = watchEffect(() => {
    styleObject.value = composePageProps(
      pagingMode.value,
      size.value,
      orientation.value,
      margin.value,
      padding.value
    );
  });
  onUnmounted(() => stop());

  return {
    pageProps: styleObject
  };
}

export function composePageProps(
  pagingMode: NPrintProps["pagingMode"],
  size: NPrintProps["size"],
  orientation: NPrintProps["orientation"],
  margin: NPrintProps["margin"],
  padding: NPrintProps["padding"]
) {
  const styleObject: Record<string, any> = {};

  const { width, height } = parsePageSize(size, orientation);

  /**
   * 计算 NPrintPage 的高度, 以实现不同的分页模式.
   *
   * * {@link NPrintPagingMode.AUTO} 和 {@link NPrintPagingMode.COMPACT} 模式下 NPrintPage 需要根据内容自适应的调节高度.
   * * {@link NPrintPagingMode.CLIP} 模式下 NPrintPage 需要固定高度, 以实现内容的裁剪.
   *   减去 1Q 的高度可以避免出现空白页.(可能浏览器内部在计算页面大小时会保留一些长度)
   */
  let heightPropertyValue: string;
  switch (pagingMode) {
    case NPrintPagingMode.AUTO:
    case NPrintPagingMode.COMPACT:
      heightPropertyValue = "auto";
      break;
    case NPrintPagingMode.CLIP:
    default:
      heightPropertyValue = `calc(${height} - 1Q)`;
      break;
  }

  /**
   * 计算 NPrintPage 的分页方式, 以实现不同的分页模式.
   *
   * * {@link NPrintPagingMode.AUTO} 和 {@link NPrintPagingMode.CLIP} 模式下 NPrintPage 会在页尾强制进行分页.
   * * {@link NPrintPagingMode.COMPACT} 模式下多个 NPrintPage 会尽量打印在同一页, 仅在页面剩余空间不足时进行分页.
   */
  let breakAfterPropertyValue: BreakAfterProperty;
  switch (pagingMode) {
    case NPrintPagingMode.AUTO:
    case NPrintPagingMode.CLIP:
      breakAfterPropertyValue = "page";
      break;
    case NPrintPagingMode.COMPACT:
    default:
      breakAfterPropertyValue = "auto";
  }

  let overflowPropertyValue: OverflowProperty = "visible";
  switch (pagingMode) {
    case NPrintPagingMode.CLIP:
      overflowPropertyValue = "hidden";
      break;
  }

  styleObject.width = `${width}`;
  styleObject.height = heightPropertyValue;
  styleObject.margin = generateMarginOrPaddingObject(margin);
  styleObject.padding = generateMarginOrPaddingObject(padding);
  styleObject.breakAfter = breakAfterPropertyValue;
  styleObject.overflow = overflowPropertyValue;

  return styleObject as NPrintPageProps;
}
