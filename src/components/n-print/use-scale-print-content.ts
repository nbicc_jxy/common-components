import {
  computed,
  onMounted,
  type Ref,
  type ComponentPublicInstance,
  onUnmounted,
  watch
} from "vue";
import { elementRect } from "../utils";
import { debounce, isNil } from "lodash-es";
import { toPx } from "../utils/physical-unit-conversion";
import { elementMutation } from "../utils/reactive-dom-mutation";

export function useScalePrintContent(
  containerRef: Ref<ComponentPublicInstance | undefined>,
  contentElementRef: Ref<HTMLElement | undefined>,
  scaleElementRef: Ref<HTMLElement | undefined>,
  pageSizeRef: Ref<{ width: string; height: string }>
) {
  const containerElementRef = computed(() => containerRef.value?.$el as HTMLElement);

  onMounted(() => {
    if (isNil(containerElementRef.value) || isNil(scaleElementRef.value))
      throw new Error("element is undefined");

    /**
     * 以下情况需要重新计算缩放比例:
     * 1. {@link containerRef} 代表的 dom 元素大小变化
     * 2. {@link scaleElementRef} 代表的 dom 元素子元素变化.
     *    注: 出于性能考虑, 监听直接子元素变化, 而不是所有子元素.
     */
    watch(
      [
        elementRect.watch(containerElementRef.value),
        elementMutation.watch(scaleElementRef.value, { childList: true })
      ],
      () => refreshStyle(),
      { immediate: true }
    );
  });
  onUnmounted(() => {
    containerElementRef.value && elementRect.unwatch(containerElementRef.value);
    scaleElementRef.value && elementMutation.unwatch(scaleElementRef.value);
  });

  const getScale = () => {
    const container = containerElementRef.value;

    if (isNil(container)) return 1;

    const wrapWidth = container.clientWidth - 32; // 32 是 containerRef 的 padding
    const innerWidth = toPx(pageSizeRef.value.width);

    return Math.min(wrapWidth / innerWidth, 1);
  };

  /**
   * 根据外部设置的 pageSize, 计算缩放比例. 为 contentEl 设置缩放后的宽高(transform 仅改变视觉效果, 不改变元素的宽高), 为 scaleEl 设置缩放比例.
   * @see https://stackoverflow.com/questions/32835144/css-transform-scale-does-not-change-dom-size
   * @param contentEl
   * @param scaleEl
   */
  const applyScaleStyle = (contentEl: HTMLElement, scaleEl: HTMLElement) => {
    const scaleValue = getScale();
    const width = contentEl.dataset.width
      ? Number.parseInt(contentEl.dataset.width)
      : contentEl.clientWidth;
    const height = contentEl.dataset.height
      ? Number.parseInt(contentEl.dataset.height)
      : contentEl.clientWidth;

    const contentElStyles = contentEl.style;
    contentElStyles.width = `${width * scaleValue}px`;
    contentElStyles.height = `${height * scaleValue}px`;

    const scaleElStyles = scaleEl.style;
    scaleElStyles.transform = `scale(${scaleValue})`;
    scaleElStyles.transformOrigin = "left top";
  };

  /**
   * 恢复由 {@link applyScaleStyle} 设置的缩放样式.
   *
   * 直接将外部设置的 pageSize 应用到 contentEl 上. 并移除 scaleEl 上的缩放样式.
   * @param contentEl
   * @param scaleEl
   */
  const retrieveScaleStyle = (contentEl: HTMLElement, scaleEl: HTMLElement) => {
    const contentElStyles = contentEl.style;
    contentElStyles.width = `${toPx(pageSizeRef.value.width)}px`;
    contentElStyles.height = "";

    // 保存原始的宽高. 用于恢复缩放样式.
    contentEl.dataset.width = contentEl.clientWidth.toString();
    contentEl.dataset.height = contentEl.clientHeight.toString();

    const scaleElStyles = scaleEl.style;
    scaleElStyles.transform = "";
    scaleElStyles.transformOrigin = "";
  };

  const refreshStyle = debounce(async () => {
    if (isNil(scaleElementRef.value) || isNil(contentElementRef.value)) return;

    await waitAllImageLoaded(scaleElementRef.value);

    retrieveScaleStyle(contentElementRef.value, scaleElementRef.value);
    applyScaleStyle(contentElementRef.value, scaleElementRef.value);
  }, 250);

  return {
    refreshStyle,
    retrieveScaleStyle
  };
}

/**
 * 等待所有图片加载完成
 * @param contentEl
 */
export async function waitAllImageLoaded(contentEl: HTMLElement) {
  const originImageElements = contentEl.querySelectorAll("img");

  let imageElements = Array.from(originImageElements);
  const checkImageLoaded = (): boolean => {
    imageElements = imageElements.filter((imageEl) => !imageEl.complete);
    return imageElements.length === 0;
  };

  let errorHandler: (ev: ErrorEvent) => void;
  await new Promise<void>((resolve, reject) => {
    errorHandler = (ev: ErrorEvent) => reject(ev.error);

    originImageElements.forEach((image) => image.addEventListener("error", errorHandler));

    // 如果图片已经加载完成，直接返回
    if (checkImageLoaded()) {
      resolve();
      return;
    }

    const cancelKey = setInterval(() => {
      if (!checkImageLoaded()) return;

      clearInterval(cancelKey);
      resolve();
    }, 500);
  });

  originImageElements.forEach((image) => image.removeEventListener("error", errorHandler));
}
