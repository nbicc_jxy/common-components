import type {
  NPrintMargin,
  NPrintMarginObject,
  NPrintPadding,
  NPrintPageOrientation,
  NPrintPageSize
} from "./print-types";
import { isArray, isString } from "lodash-es";
import { toPx } from "../utils/physical-unit-conversion";

export function composeAtPageStyle(size: NPrintPageSize, orientation: NPrintPageOrientation) {
  return `
  html,body {
    width: auto !important;
    min-width: auto !important;
    height: auto !important;
    min-height: auto !important;
    overflow: visible !important;

    margin: 0 !important;
    padding: 0 !important;
  }
  @page {
    size: ${isArray(size) ? size.join(" ") : size} ${orientation};
    margin: 0;
  }

  .n-print-page__wrap {
    box-shadow: none !important;
  }
`;
}

const pageSizeMap: Record<string, [string, string]> = {
  A5: ["148mm", "210mm"],
  A4: ["210mm", "297mm"],
  A3: ["297mm", "420mm"],
  B5: ["176mm", "250mm"],
  B4: ["250mm", "353mm"],
  "JIS-B5": ["182mm", "257mm"],
  "JIS-B4": ["257mm", "364mm"],
  letter: ["216mm", "279mm"],
  legal: ["216mm", "356mm"],
  ledger: ["279mm", "432mm"]
};

export function parsePageSize(
  size: NPrintPageSize,
  orientation: NPrintPageOrientation
): { width: string; height: string } {
  const width = isString(size)
    ? isArray(pageSizeMap[size])
      ? pageSizeMap[size][0]
      : size
    : size[0];
  const height = isString(size)
    ? isArray(pageSizeMap[size])
      ? pageSizeMap[size][1]
      : size
    : size[1];
  // 根据宽高计算方向
  const presetOrientation: NPrintPageOrientation =
    toPx(height) >= toPx(width) ? "portrait" : "landscape";

  return {
    width: presetOrientation === orientation ? width : height,
    height: presetOrientation === orientation ? height : width
  };
}

export function generatePaddingStyle(mOrp: NPrintMargin | NPrintPadding): string {
  if (isString(mOrp)) {
    return `padding: ${mOrp};`;
  }

  const { top = "0", bottom = "0", left = "0", right = "0" } = mOrp;
  return `padding-top: ${top}; padding-right: ${right}; padding-bottom: ${bottom}; padding-left: ${left};`;
}

export function generateMarginOrPaddingObject(
  mOrp: NPrintMargin | NPrintPadding
): NPrintMarginObject {
  const result: NPrintMarginObject = { top: "0", bottom: "0", left: "0", right: "0" };

  if (isString(mOrp))
    result.top = result.bottom = result.left = result.right = validateLength(mOrp);
  else {
    const { top, bottom, left, right } = mOrp;
    result.top = validateLength(top);
    result.bottom = validateLength(bottom);
    result.left = validateLength(left);
    result.right = validateLength(right);
  }

  return result;
}

function validateLength(length: string): string {
  if (length.length > 0 && 48 <= length.charCodeAt(0) && length.charCodeAt(0) <= 57) {
    return length;
  } else {
    return "0";
  }
}
