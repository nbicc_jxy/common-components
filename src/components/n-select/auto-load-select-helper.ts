import { reactive } from "vue";
import type { SelectProps } from "ant-design-vue/lib/select";
import { TableHelper } from "../table-helper";
import { isNil } from "lodash-es";

export class AutoLoadSelectHelper extends TableHelper.TableHelper<
  TableHelper.RecordKey,
  TableHelper.RecordType
> {
  constructor(
    loadSelectOptions: LoadSelectOptionsFunction<TableHelper.RecordType>,
    recordKeyName: TableHelper.RecordKey,
    options?: Partial<TableHelper.TableHelperOptions>
  ) {
    const loadRecord: LoadSelectOptionsFunction<TableHelper.RecordType> = async (
      pagination,
      filter,
      sort,
      extra
    ) => {
      const { abortSignal } = extra;

      const abortPromise = new Promise<
        TableHelper.LoadTableRecordFunctionResult<TableHelper.RecordType>
      >((_, reject) => abortSignal.addEventListener("abort", () => reject(abortSignal.reason)));
      const loadPromise = loadSelectOptions(pagination, filter, sort, extra);

      return Promise.race([abortPromise, loadPromise]);
    };

    super(loadRecord, recordKeyName, options);

    /**
     * 在新的记录加载开始前, 中断之前的加载. 并创建新的中断器.
     */
    this.on("recordLoadStart", () => {
      if (this._abortController && !this._abortController.signal.aborted) {
        this._abortController.abort();
        this._abortController = undefined;
      }

      this._abortController = new AbortController();
    });
    this.on("recordLoadEnd", () => {
      if (isNil(this.latestLoadRecord)) return;

      /**
       * 当前页码等于起始页码时, 是选择器下拉框打开时的第一次加载.
       * 此时应该将 {@link optionSource} 重置为最新加载的记录而不是在 {@link optionSource} 的基础上追加数据.
       */
      if (this.paginationParams.current <= this.options.pageStartIndex) {
        this.optionSource = this.latestLoadRecord;
      } else {
        this.optionSource = this.optionSource.concat(this.latestLoadRecord);
      }
    });
  }

  optionSource: Array<TableHelper.RecordType> = [];

  private _abortController: AbortController | undefined = undefined;

  protected _joinLoadRecordArguments(): Parameters<
    LoadSelectOptionsFunction<TableHelper.RecordType>
  > {
    const [pagination, filter, sorter] = super._joinLoadRecordArguments();
    return [pagination, filter, sorter, { abortSignal: this._abortController!.signal }];
  }

  protected _onBeforeLoadRecord(): Error | undefined {
    return undefined;
  }

  protected _onAfterLoadRecord(
    changes: Partial<TableHelper.TableChangedParams>,
    error?: Error
  ): Error | undefined {
    const returnedError = super._onAfterLoadRecord(changes, error);

    if (!returnedError) return;

    switch (returnedError.name) {
      case "AbortError":
        return;
      default:
        return returnedError;
    }
  }
}

export function useAutoLoadSelectHelper(
  loadRecord: LoadSelectOptionsFunction<TableHelper.RecordType>,
  recordKeyName: TableHelper.RecordKey,
  options?: Partial<TableHelper.TableHelperOptions>
) {
  const selectHelper = reactive(new AutoLoadSelectHelper(loadRecord, recordKeyName, options));
  // why: 为了触发 selectHelper 的响应式更新, 使得 optionSource 的更新能够被监听到.
  // todo
  selectHelper.on(
    "recordLoadEnd",
    () => (selectHelper.optionSource = selectHelper.optionSource.slice())
  );

  const onSearchValueChange: SelectProps["onSearch"] = (value) => {
    selectHelper.updatePaginationParams({ current: -1 }).updateFilterParams({ searchValue: value });
  };

  return {
    selectHelper,
    onSearchValueChange
  };
}

/**
 * 加载记录的函数, 用于 {@link AutoLoadSelectHelper} 的构造函数.
 * @param pagination 分页参数
 * @param filter 过滤参数
 * @param sort 排序参数
 * @param extra 额外参数
 * @param extra.abortSignal 中断信号, 用于中断请求
 *
 * @returns 返回值为 {@link LoadTableRecordFunctionResult}
 */
export type LoadSelectOptionsFunction<RECORD_TYPE> = (
  pagination: TableHelper.TablePaginationParams,
  filter: TableHelper.TableFilterParams,
  sort: Array<TableHelper.TableSortParams>,
  extra: { abortSignal: AbortSignal }
) => Promise<TableHelper.LoadTableRecordFunctionResult<RECORD_TYPE>>;
