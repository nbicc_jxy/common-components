import type { PropType } from "vue";
import type { SelectProps } from "ant-design-vue";
import type { DefaultOptionType, FieldNames } from "ant-design-vue/lib/vc-select/Select";
import type { LoadSelectOptionsFunction } from "./auto-load-select-helper";
import type { TableHelper } from "../table-helper";
import type { ExtendProps } from "../common/types";

export const nSelectProps = () =>
  ({
    label: {
      type: [String, Array] as PropType<string | Array<string>>
    },
    value: {
      type: [String, Number, Array, Object] as PropType<SelectProps["value"]>
    },
    "onUpdate:value": {
      type: Function as PropType<SelectProps["onUpdate:value"]>
    },
    options: {
      type: Array as PropType<Required<SelectProps>["options"]>,
      default: () => []
    },
    placeholderOptions: {
      type: [Array, Object] as PropType<Required<SelectProps>["options"] | DefaultOptionType>,
      default: () => []
    },
    fieldNames: {
      type: Object as PropType<SelectProps["fieldNames"]>
    },
    onPopupScroll: {
      type: Function as PropType<SelectProps["onPopupScroll"]>
    },
    onSearch: {
      type: Function as PropType<SelectProps["onSearch"]>
    },
    showSearch: {
      type: Boolean,
      default: true
    }
  } as const);

export type NSelectProps = ExtendProps<ReturnType<typeof nSelectProps>, SelectProps>;

export interface NSelectEmits {
  "update:value": (value: SelectProps["value"]) => void;
  scrollToBottom: () => void;
  search: (value: string) => void;
}

export const nAutoLoadSelectProps = () =>
  ({
    pageStartIndex: {
      type: Number,
      default: 1
    },
    pageSize: {
      type: Number,
      default: 100
    },
    fieldNames: {
      type: Object as PropType<FieldNames>
    },
    loadRecord: {
      type: Function as PropType<LoadSelectOptionsFunction<TableHelper.RecordType>>,
      required: true
    }
  } as const);

export type NAutoLoadSelectProps = ExtendProps<
  ReturnType<typeof nAutoLoadSelectProps>,
  NSelectProps
>;
