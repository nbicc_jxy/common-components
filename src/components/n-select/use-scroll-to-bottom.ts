import { throttle } from "lodash-es";
import type { SelectProps } from "ant-design-vue";
import { onUnmounted } from "vue";
import { preventScrollPropagation } from "../utils";

export function useScrollToBottom(
  callback: () => void,
  passedCallback: SelectProps["onPopupScroll"]
) {
  let target: HTMLElement;
  let unPreventScrollPropagation: () => void;
  const throttledOnScrollToBottom = throttle((ev: UIEvent) => {
    const _target = ev.target;
    if (!target && _target !== null) {
      target = _target as HTMLElement;
      unPreventScrollPropagation = preventScrollPropagation.watch(target);
    }

    if (!target) return;

    const { scrollHeight, scrollTop, clientHeight } = target;

    if (Math.abs(scrollHeight - scrollTop - clientHeight) >= 1) return;

    callback();
    passedCallback && passedCallback(ev);
  }, 200);

  return {
    throttledOnScrollToBottom,
    unuse: () => {
      onUnmounted(() => {
        unPreventScrollPropagation && unPreventScrollPropagation();
      });
    }
  };
}
