export function useSearch(
  callback: (searchValue: string) => void,
  passedCallback?: (searchValue: string) => void
) {
  const search = (searchValue: string) => {
    callback(searchValue);
    passedCallback && passedCallback(searchValue);
  };

  return {
    search
  };
}
