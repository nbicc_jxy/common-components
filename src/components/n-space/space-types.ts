import type { SpaceProps } from "ant-design-vue";
import type { PropType, ExtractPropTypes } from "vue";

export const nSpaceProps = () =>
  ({
    size: {
      type: [String, Number] as PropType<SpaceProps["size"]>,
      default: 6
    }
  } as const);

export type NSpaceProps = ExtractPropTypes<ReturnType<typeof nSpaceProps>>;

export interface NSpaceEmits {}
