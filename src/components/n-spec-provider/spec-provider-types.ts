import type { ExtractPropTypes, PropType } from "vue";
import type { FormItemControlType, FormItemControlOptions } from "../form-helper";
import type { NUploadLimit } from "../n-upload";
import type { TooltipProps, UploadProps } from "ant-design-vue";
import type { NEasyFormProps, NFormProps } from "../n-form";

export const nSpecProviderProps = () => ({
  wrapModal: {
    type: Boolean as PropType<Boolean>,
    default: true
  },
  wrapDrawer: {
    type: Boolean as PropType<Boolean>,
    default: true
  },
  form: {
    type: Object as PropType<Partial<NSpecProviderFormConfig>>
  },
  easyForm: {
    type: Object as PropType<Partial<NSpecProviderEasyFormConfig>>
  },
  upload: {
    type: Object as PropType<{
      accept: UploadProps["accept"];
      action: UploadProps["action"];
      method: UploadProps["method"];
      limit: NUploadLimit;
      customRequest: UploadProps["customRequest"];
    }>
  },
  table: {
    type: Object as PropType<{
      /**
       * 当鼠标悬浮在开启了 ellipsis 的单元格上时，是否将完整内容用 Tooltip 组件显示. 默认为 false
       */
      ellipsisWithTooltip: TooltipProps | boolean;
    }>
  }
});
export type NSpecProviderProps = Partial<ExtractPropTypes<ReturnType<typeof nSpecProviderProps>>>;

export const NSpecProvideKey = "SPEC_PROVIDER_KEY";

export interface NSpecProviderFormItemConfig {
  /**
   * 是否显示清除按钮
   */
  allowClear: boolean;
  /**
   * 输入框最大长度, 仅对特定类型的输入框有效
   */
  maxlength: number;
  /**
   * 占位符
   */
  placeholder: string | string[];
  /**
   * 事件
   */
  events: Partial<NSpecProviderFormItemEvents>;
  /**
   * 修饰符
   */
  modifiers: FormItemControlOptions["modifiers"];
}

export interface NSpecProviderFormConfig extends NSpecProviderFormItemConfig {
  labelWidth: string | number;
  labelAlign: "left" | "right";
  colon: boolean;
  field: Record<FormItemControlType, Partial<NSpecProviderFormItemConfig>>;
  fieldLayout: Required<NFormProps>["fieldLayout"];
}

type NSpecProviderFormItemEvents = {
  /**
   * @see https://www.typescriptlang.org/docs/handbook/2/mapped-types.html#key-remapping-via-as
   */
  [P in keyof HTMLElementEventMap as `on${Capitalize<P>}`]: (e: HTMLElementEventMap[P]) => void;
};

export interface NSpecProviderEasyFormConfig extends NSpecProviderFormConfig {
  defaultExpandRowCount: Required<NEasyFormProps>["defaultExpandRowCount"];
  beforeSearch: Required<NEasyFormProps>["beforeSearch"];
}
