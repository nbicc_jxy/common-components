<script lang="tsx">
import { Table, TypographyText } from "ant-design-vue";
import type { TableProps } from "ant-design-vue";
import type { DefineComponent, Ref, SetupContext, Slot, VNode } from "vue";
import { computed, defineComponent, ref, toRef, toRefs, watch } from "vue";
import { isArray, isFunction, isNil, isString, merge, omit, pick } from "lodash-es";
import type { Key } from "ant-design-vue/lib/table/interface";
import { useAdaptiveHeight } from "./use-adaptive-height";
import { useRowClickable } from "./use-row-clickable";
import type { NTableColumnConfig, NTableEmits } from "./table-types";
import { nTableProps } from "./table-types";
import { useColumnResize } from "./use-column-resize";
import { applyColumnConfig } from "./apply-column-config";
import type { ColumnsType } from "ant-design-vue/lib/table";
import { cacheStorage, IGNORE, isStorable } from "../../storage";
import { useShowNumber } from "./use-show-number";
import { useStripe } from "./use-stripe";
import { useEditable } from "./use-editable";
import type { ExtendProps } from "../common/types";
import type { TableHelper } from "../table-helper";
import { composeCallback, useComposeCallback } from "../utils/hooks/use-compose-callback";
import { useFallbackBodyCell } from "./use-fallback-body-cell";
import type { BodyCellSlot } from "./external-types";
import { useSpecInject } from "../utils/hooks";

type RECORD_TYPE = TableHelper.RecordType;

export default defineComponent({
  name: "NTable",
  props: nTableProps(),
  setup(props, { slots, attrs, emit, expose }: SetupContext<NTableEmits>) {
    const { table: TableSpecConfig } = useSpecInject("table");

    const {
      storageKey,
      disabledRowKeys: _disabledRowKeys,
      columns,
      columnConfig,
      rowSelection,
      rowKey: _rowKey,
      dataSource,
      pagination,
      customRow,
      childrenColumnName,
      disabledAdaptiveHeight,
      onResizeColumn,
      showNumberColumn,
      rowClassName,
      striped,
      editable,
      editByClickRow,
      fields,
      size,
      editedRowKey
    } = toRefs(props);
    const rowKey = computed(() => (record: RECORD_TYPE) => {
      const rowKeyValue = _rowKey.value;
      if (isFunction(rowKeyValue)) return rowKeyValue(record);
      else return record[rowKeyValue ?? "key"];
    });
    const disabledRowKeys = computed(() => _disabledRowKeys?.value || []);
    const _columnConfig = computed<NTableColumnConfig>(() =>
      merge({ resizable: false }, columnConfig.value ?? {})
    );
    // 使 columns 支持响应式
    const _columns = ref() as Ref<ColumnsType>;
    watch(
      () => ({
        columns,
        _columnConfig,
        // 列顺序变化时也需要重新应用列配置
        order: (columns.value ?? []).map((column) => column.order)
      }),
      () =>
        /**
         * 注: applyColumnConfig 和 useColumnResize 会修改 _columns 的值, 导致外部的 columns 也被修改.
         */
        (_columns.value = applyColumnConfig(columns.value || [], _columnConfig.value)),
      { immediate: true }
    );

    // 本地存储对象
    const storage = cacheStorage.derive(
      isStorable(props) ? `${storageKey.value}__n-table` : IGNORE
    );
    // 合并 customRow
    const { callbackMap: customRowCallbackMap, callback: _customRow } = useComposeCallback([
      "props",
      customRow
    ]);
    // 合并 rowSelection
    const { callback: _rowSelection } = useComposeCallback([
      "props",
      () => ({
        preserveSelectedRowKeys: true,
        ...rowSelection.value,
        getCheckboxProps: (record: RECORD_TYPE) => ({
          disabled: disabledRowKeys.value.includes(rowKey.value(record)),
          ...rowSelection.value?.getCheckboxProps?.(record)
        })
      })
    ]);
    const composedRowSelection = computed(() =>
      isNil(rowSelection.value) ? undefined : _rowSelection.value()
    );
    // 导出的方法
    let exposedMethods: Parameters<SetupContext["expose"]>[0] = {};

    /**
     * 支持通过行点击事件选中行.
     */
    const { customRow: useRowClickableCustomRow } = useRowClickable(
      rowSelection,
      disabledRowKeys,
      rowKey
    );
    customRowCallbackMap.set("useRowClickable", useRowClickableCustomRow);

    /**
     * 自适应高度
     */
    const tableWrapperRef = ref();
    const { scrollOptions } = disabledAdaptiveHeight.value
      ? { scrollOptions: toRef(attrs, "scroll") as Ref<TableProps["scroll"]> }
      : useAdaptiveHeight(tableWrapperRef, toRef(attrs, "scroll") as Ref<TableProps["scroll"]>);

    const { onResizeColumn: _onResizeColumn } = useColumnResize(
      _columns,
      storage.derive("columnWidth"),
      onResizeColumn.value
    );

    const { numberColumn } = useShowNumber(
      pagination,
      showNumberColumn,
      _columns,
      rowKey,
      childrenColumnName
    );

    const { rowClassName: _rowClassName } = useStripe(striped, rowClassName);

    /************************* 编辑相关 *************************/
    const findRecordByKey = (key: Key | undefined) => {
      if (isNil(key)) return undefined;

      for (let i = (dataSource.value ?? []).length; i--; ) {
        const record = dataSource.value![i];
        if (rowKey.value(record) === key) return record;
      }
      return undefined;
    };
    const tablePopupContainerRef = ref() as Ref<HTMLDivElement | undefined>;
    const {
      customRow: useEditableCustomRow,
      customCell: useEditableCustomCell,
      bodyCell: useEditableBodyCell,
      deactivateRow,
      saveActiveRow,
      editRow,
      setActiveRowField
    } = useEditable(
      tableWrapperRef,
      tablePopupContainerRef,
      computed(() => findRecordByKey(editedRowKey.value)),
      editByClickRow,
      fields,
      rowKey,
      size,
      slots,
      (record) => emit("update:editedRowKey", isNil(record) ? undefined : rowKey.value(record))
    );
    const bodyCellSlotRef = computed<BodyCellSlot>(() =>
      editable.value ? useEditableBodyCell.value : (slots.bodyCell as BodyCellSlot)
    );
    watch(
      editable,
      (value) =>
        value
          ? customRowCallbackMap.set("useEditable", useEditableCustomRow)
          : customRowCallbackMap.delete("useEditable"),
      { immediate: true }
    );
    exposedMethods = {
      ...exposedMethods,
      saveEditedRow: saveActiveRow,
      cancelEditedRow: deactivateRow,
      editRow: (key: Key, needValidate = true) => {
        const record = findRecordByKey(key);
        if (isNil(record)) return;

        /**
         * editRow 在当前有编辑行时, 会自动保存当前正在编辑的行.
         * 但在使用 api 控制编辑时, 不需要自动保存, 因此先调用 deactivateRow 取消当前编辑行.
         */
        deactivateRow();
        editRow(record, needValidate);
      },
      setEditedRowField: setActiveRowField
    };

    /**
     * 自定义单元格的默认渲染方式.
     */
    const { fallbackBodyCell } = useFallbackBodyCell(
      bodyCellSlotRef,
      computed(() => TableSpecConfig.value.ellipsisWithTooltip),
      tablePopupContainerRef
    );

    const composedColumns = computed(() => {
      const __columns = (
        showNumberColumn.value ? [numberColumn.value].concat(_columns.value) : _columns.value
      )
        // 创建新的列对象数组, 避免修改外部传入的原始列对象
        .map((column) => ({ ...column }));

      for (let i = __columns.length; i--; ) {
        const column = __columns[i];
        column.customCell = composeCallback(column.customCell, useEditableCustomCell.value);
      }
      return __columns;
    });

    expose(exposedMethods);

    return () => {
      return (
        <div
          {...pick(attrs as Record<string, string>, "class", "style")}
          class={{
            "n-table": true,
            "n-table--empty": (dataSource.value ?? []).length <= 0,
            "n-table--adaptive-height": !disabledAdaptiveHeight.value
          }}
          ref={tableWrapperRef}
        >
          <div ref={tablePopupContainerRef} class="n-table__popup-container">
            <Table
              {...omit(attrs, "class", "style")}
              {...{
                rowSelection: composedRowSelection.value,
                customRow: _customRow.value,
                scroll: scrollOptions.value,
                rowKey: rowKey.value,
                dataSource: dataSource.value,
                pagination: pagination.value,
                columns: composedColumns.value,
                rowClassName: _rowClassName.value,
                size: size.value,
                onResizeColumn: _onResizeColumn,
                childrenColumnName: childrenColumnName.value
              }}
            >
              {{ ...slots, bodyCell: fallbackBodyCell }}
            </Table>
          </div>
        </div>
      );
    };
  }
}) as DefineComponent<ExtendProps<ReturnType<typeof nTableProps>, TableProps>>;

/**
 * 默认的省略处理, 用于处理表格中的单元格内容过长时, 进行省略处理.
 * @deprecated 性能问题
 * @param bodyCellSlot
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
function defaultEllipsisBodyCell(bodyCellSlot?: Slot): Slot {
  return (...args: Parameters<Slot>): Array<VNode> => {
    const result = bodyCellSlot?.(...args);

    // 如果存在有效子节点, 则不进行省略处理
    if (isArray(result) && result.filter((child) => !isEmptyVN(child)).length > 0) {
      return result;
    } else {
      const { record, column } = args[0];
      return [
        <TypographyText
          ellipsis={{ tooltip: true }}
          content={record[column.dataIndex]?.toString() || "-"}
        />
      ];
    }
  };
}

/**
 * 判断 VNode 是否为空, 用于判断是否需要进行省略处理.
 * @param vn
 */
function isEmptyVN(vn: VNode): boolean {
  if (isArray(vn.children)) return vn.children.length <= 0;
  else if (isString(vn.children)) return !!vn.children;
  else if (isNil(vn.children)) return true;
  else return isNil(vn.component);
}
</script>

<style lang="scss">
.n-table {
  &.n-table--adaptive-height {
    position: relative;

    width: 100%;
    height: 100%;

    overflow: hidden !important;
  }

  .ant-pagination.ant-table-pagination {
    flex-wrap: nowrap;

    margin-bottom: 0;
  }

  .n-table-row--odd td {
    background-color: #fafafa;
  }

  .ant-form-item {
    margin: -1em 0;
  }

  .ant-popover {
    .ant-form-item {
      margin: 0 0 10px;
    }
  }

  .ant-table-small {
    .ant-form-item {
      margin: -1px 0;
    }
  }

  .ant-form-item-control-input {
    min-height: auto;
  }
}

.n-table--empty {
  .ant-table-wrapper,
  .ant-spin-nested-loading,
  .ant-spin-container,
  .ant-table,
  .ant-table-container,
  .ant-table-body,
  .ant-table-body > table {
    height: 100%;
  }
}
</style>
