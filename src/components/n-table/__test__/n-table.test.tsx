import { expect, describe, test, beforeEach, vi } from "vitest";
import { DOMWrapper, mount } from "@vue/test-utils";
import { NTable } from "../";
import type { NTableRowSelection } from "../";
import { reactive } from "vue";

const columns = [
  { title: "Name", dataIndex: "name" },
  { title: "Age", dataIndex: "age" }
];
const dataSource = [
  { key: "1", name: "John", age: 30 },
  { key: "2", name: "Jane", age: 25 },
  { key: "3", name: "Bob", age: 40 }
];
const pagination = { current: 1, pageSize: 10, total: 3 };
const disabledRowKeys = ["2"];
const rowSelection: Partial<NTableRowSelection> = reactive({
  selectedRowKeys: [],
  selectedRows: [],
  onChange: (selectedRowKeys, selectedRows) => {
    rowSelection.selectedRowKeys = selectedRowKeys;
    rowSelection.selectedRows = selectedRows;
  }
});

describe("NTable 基本用法", () => {
  test("能够正确渲染", () => {
    const wrapper = mount(NTable, {
      props: { columns, dataSource, pagination }
    });
    expect(wrapper.find("table").exists()).toBe(true);
    expect(wrapper.findAll("thead th")).toHaveLength(2);

    expect(wrapper.findAll("thead th").at(0)?.text()).toBe("Name");
    expect(wrapper.findAll("thead th").at(1)?.text()).toBe("Age");

    expect(wrapper.findAll("tbody tr").at(1)?.text()).toBe("John30");
    expect(wrapper.findAll("tbody tr").at(2)?.text()).toBe("Jane25");
    expect(wrapper.findAll("tbody tr").at(3)?.text()).toBe("Bob40");
  });

  describe("disabledRowKeys 属性", () => {
    test("设置 disabledRowKeys 来禁用行选择", () => {
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, disabledRowKeys, rowSelection }
      });

      expect(
        wrapper.findAll("tbody tr").at(2)?.find("input").attributes("disabled")
      ).not.toBeUndefined();
    });

    test("设置 disabledRowKeys 来禁用行选择, 但会被自定义的 rowSelection 覆盖", () => {
      const rowSelection: Partial<NTableRowSelection> = reactive({
        selectedRowKeys: [],
        selectedRows: [],
        onChange: (selectedRowKeys, selectedRows) => {
          rowSelection.selectedRowKeys = selectedRowKeys;
          rowSelection.selectedRows = selectedRows;
        },
        getCheckboxProps: (record) => ({
          disabled: !disabledRowKeys.includes(record["key"])
        })
      });
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, disabledRowKeys, rowSelection }
      });

      const checkboxes = wrapper
        .findAll("tbody tr")
        .splice(1)
        .map((row) => row.find("input"));

      expect(checkboxes[0]?.attributes("disabled")).not.toBeUndefined();
      expect(checkboxes[1]?.attributes("disabled")).toBeUndefined();
      expect(checkboxes[2]?.attributes("disabled")).not.toBeUndefined();
    });
  });

  test("设置 showNumberColumn 来显示序号列", () => {
    const wrapper = mount(NTable, {
      props: { columns, dataSource, pagination, showNumberColumn: true }
    });

    expect(wrapper.findAll("tbody tr td:first-child").at(1)?.text()).toBe("1");
    expect(wrapper.findAll("tbody tr td:first-child").at(2)?.text()).toBe("2");
    expect(wrapper.findAll("tbody tr td:first-child").at(3)?.text()).toBe("3");
  });

  test("设置 customRow 来自定义行, 属性和回调事件能够正常触发", async () => {
    const onClick = vi.fn();
    const wrapper = mount(NTable, {
      props: {
        columns,
        dataSource,
        pagination,
        customRow: (record, index) => ({
          class: `custom-row-${index}`,
          onClick
        })
      }
    });

    expect(wrapper.findAll("tbody tr.custom-row-0")).toHaveLength(1);
    expect(wrapper.findAll("tbody tr.custom-row-1")).toHaveLength(1);
    expect(wrapper.findAll("tbody tr.custom-row-2")).toHaveLength(1);

    const rows = wrapper.findAll("tbody tr").splice(1);
    for (let i = 0; i < rows.length; i++) {
      await rows[i].trigger("click");
    }
    expect(onClick).toHaveBeenCalledTimes(3);
  });
});

describe("bug 修复", () => {
  /**
   * @see git revision dba2f81d
   */
  test.skip("设置表格元素高度默认值 (单元测试无法覆盖)", async () => {
    const node = (
      <div style="width: 1024px; height: 512px">
        <NTable columns={columns} dataSource={dataSource} pagination={pagination} />
      </div>
    );
    const wrapper = mount({ render: () => node });

    console.log(document.body.getBoundingClientRect());
    await new Promise((resolve) => setTimeout(resolve, 1000));
    console.log(wrapper.element.clientWidth);
    expect(wrapper.find("table").attributes("style")).toContain("height: 100%;");
  });

  /**
   * @see git revision 41d2c5e3
   */
  describe("选择行时能够同时更新 selectedRowKeys 和 selectedRows", async () => {
    // 重置 rowSelection
    beforeEach(() => {
      rowSelection.selectedRowKeys = [];
      rowSelection.selectedRows = [];
    });

    const clickRow = async (rows: Array<DOMWrapper<Element>>, index: number) =>
      await rows?.[index].trigger("click");
    const clickCheckbox = async (checkboxes: Array<DOMWrapper<Element>>, index: number) =>
      await checkboxes?.[index].trigger("change");

    test("点击行", async () => {
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, rowSelection }
      });

      const rows = wrapper.findAll("tbody tr").splice(1);

      const sequence = [0, 1, 2, 1, 0];
      for (let i = 0; i < sequence.length; i++) await clickRow(rows, sequence[i]);

      expect(rowSelection.selectedRowKeys).toEqual([dataSource[2].key]);
      expect(rowSelection.selectedRows).toEqual([dataSource[2]]);
    });

    test("点击复选框", async () => {
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, rowSelection }
      });

      const rows = wrapper.findAll("tbody tr").splice(1);
      const checkboxes = rows.map((row) => row.find("input[type=checkbox]"));

      const sequence = [0, 1, 2, 1, 0];
      for (let i = 0; i < sequence.length; i++) await clickCheckbox(checkboxes, sequence[i]);

      expect(rowSelection.selectedRowKeys).toEqual([dataSource[2].key]);
      expect(rowSelection.selectedRows).toEqual([dataSource[2]]);
    });

    test("点击行和复选框交替", async () => {
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, rowSelection }
      });

      const rows = wrapper.findAll("tbody tr").splice(1);
      const checkboxes = rows.map((row) => row.find("input[type=checkbox]"));

      const sequence = [0, 1, 2];
      for (let i = 0; i < sequence.length; i++)
        i % 2 === 0
          ? await clickRow(rows, sequence[i])
          : await clickCheckbox(checkboxes, sequence[i]);
      expect(rowSelection.selectedRowKeys).toEqual([
        dataSource[0].key,
        dataSource[1].key,
        dataSource[2].key
      ]);
      expect(rowSelection.selectedRows).toEqual([dataSource[0], dataSource[1], dataSource[2]]);

      await clickRow(rows, 0);
      expect(rowSelection.selectedRowKeys).toEqual([dataSource[1].key, dataSource[2].key]);
      expect(rowSelection.selectedRows).toEqual([dataSource[1], dataSource[2]]);

      await clickCheckbox(checkboxes, 1);
      expect(rowSelection.selectedRowKeys).toEqual([dataSource[2].key]);
      expect(rowSelection.selectedRows).toEqual([dataSource[2]]);

      await clickRow(rows, 2);
      expect(rowSelection.selectedRowKeys).toEqual([]);
      expect(rowSelection.selectedRows).toEqual([]);
    });

    test("全选时能够同时更新 selectedRowKeys 和 selectedRows", async () => {
      const wrapper = mount(NTable, {
        props: { columns, dataSource, pagination, rowSelection }
      });

      const headerCheckbox = wrapper.find("thead input[type=checkbox]");
      const rows = wrapper.findAll("tbody tr").splice(1);
      const checkboxes = rows.map((row) => row.find("input[type=checkbox]"));

      await clickCheckbox([headerCheckbox], 0);
      expect(rowSelection.selectedRowKeys).toEqual([
        dataSource[0].key,
        dataSource[1].key,
        dataSource[2].key
      ]);
      expect(rowSelection.selectedRows).toEqual([dataSource[0], dataSource[1], dataSource[2]]);

      await clickRow(rows, 0);
      expect(rowSelection.selectedRowKeys).toEqual([dataSource[1].key, dataSource[2].key]);
      expect(rowSelection.selectedRows).toEqual([dataSource[1], dataSource[2]]);

      await clickCheckbox(checkboxes, 0);
      // 选择的顺序会影响 selectedRowKeys 和 selectedRows 的顺序.
      // 顺序为 1, 2, 0. 顺序为触发选择时的顺序.
      expect(rowSelection.selectedRowKeys).toEqual([
        dataSource[1].key,
        dataSource[2].key,
        dataSource[0].key
      ]);
      expect(rowSelection.selectedRows).toEqual([dataSource[1], dataSource[2], dataSource[0]]);
    });
  });
});
