import { TableHelper } from "../table-helper";
import { isBoolean } from "lodash-es";
import type { TablePaginationConfig } from "ant-design-vue/lib/table/interface";

/**
 * 适配 Ant Design Vue 的 TableHelper.
 */
export class AntDesignVueTableHelper<
  RECORD_KEY extends TableHelper.RecordKey,
  RECORD_TYPE extends TableHelper.RecordType
> extends TableHelper.TableHelper<RECORD_KEY, RECORD_TYPE> {
  constructor(
    loadRecord: TableHelper.LoadTableRecordFunction<RECORD_TYPE>,
    recordKeyName: RECORD_KEY,
    options?: Partial<AntDesignVueTableHelperOptions>
  ) {
    super(loadRecord, recordKeyName, options);

    this.antdvOptions = new AntDesignVueTableHelperOptions(options);

    this._antdvPaginationParams = {
      total: this.total,
      current: this.options.pageStartIndex,
      pageSize: this.options.pageSize,
      pageSizeOptions: ["10", "20", "30"],
      showSizeChanger: true,
      showTotal: (total) => `共 ${total} 条记录`,
      showLessItems: false,
      size: "default"
    };
  }

  private _antdvPaginationParams: AntDesignVueTablePaginationParams;

  antdvOptions: AntDesignVueTableHelperOptions;

  /**
   * 用于 {@link TableProps.rowKey} 属性.
   */
  get rowKey(): RECORD_KEY {
    return this.recordKeyName;
  }

  /**
   * 用于 {@link TableProps.dataSource} 属性.
   */
  get dataSource(): Array<RECORD_TYPE> {
    return this.latestLoadRecord || [];
  }

  /**
   * 用于 {@link TableProps.pagination} 属性.
   *
   * 因为 {@link TableHelper.TablePaginationParams} 无法满足 Ant Design Vue 的分页参数, 所以需要额外的分页参数.
   */
  get antdvPaginationParams(): AntDesignVueTablePaginationParams | false {
    return this.antdvOptions.showPagination
      ? {
          ...this._antdvPaginationParams,
          ...this.paginationParams,
          total: this.total
        }
      : false;
  }

  /**
   * 清除最新加载的记录
   */
  clearDataSource(): void {
    this.latestLoadRecord = [];
  }
}

/**
 * 特定于 Ant Design Vue 的 TableHelper 的分页参数
 */
export interface AntDesignVueTablePaginationParams extends TablePaginationConfig {
  total: number;
  /**
   * pageSize 要使用 string 类型, 否则会导致 Ant Design Vue 的分页组件无法正常工作.
   */
  pageSizeOptions: Array<string>;
  showSizeChanger: boolean;
  showTotal: (total: number, range: [number, number]) => string;

  showLessItems: boolean;
}

/**
 * 特定于 Ant Design Vue 的 TableHelper 的配置
 */
export class AntDesignVueTableHelperOptions extends TableHelper.TableHelperOptions {
  /**
   * 是否显示分页组件, 默认为 true.
   */
  showPagination: boolean;

  constructor(params: Partial<AntDesignVueTableHelperOptions> = {}) {
    super(params);

    this.showPagination = isBoolean(params.showPagination) ? params.showPagination : true;
  }
}
