import type { ColumnsType } from "ant-design-vue/lib/table";
import type { ColumnConfig, NTableColumnConfig } from "./table-types";
import { isArray, isNil } from "lodash-es";
import type { DataIndex } from "ant-design-vue/lib/vc-table/interface";
import type { NTableColumn } from "./table-types";

export function applyColumnConfig(
  columns: Array<Partial<NTableColumn>>,
  config: NTableColumnConfig
) {
  // 过滤设置为隐藏掉不可见的列.
  columns = columns.filter((column) => column.visible !== false);

  const columnsConfig = config.columns;

  for (let i = columns.length; i--; ) {
    const col = columns[i];

    // @ts-ignore
    const children: ColumnsType = col.children;
    // @ts-ignore
    const dataIndex: DataIndex = col.dataIndex;

    if (isArray(children)) {
      applyColumnConfig(children, config);
    } else if (!isNil(dataIndex)) {
      const designatedConfig = resolveConfigByDataIndex(columnsConfig, dataIndex);

      col.resizable = col.resizable ?? designatedConfig?.resizable ?? config.resizable;
    } else {
      console.warn("applyColumnConfig: dataIndex is nil", col);
    }

    // 当列可调整宽度时，必须设置宽度.
    if (col.resizable === true) {
      /**
       * 最后一列禁用拖拽, 以自适应宽度
       *
       * why: 当表格宽度大于内部内容宽度时(例如: 只有两列不足以填满表格宽度)，列宽会自动扩展以满足表格宽度. 导致实际渲染宽度与配置宽度不匹配.
       * 在拖动调整列宽时，会导致列宽变化剧烈(antdv 的 bug?).
       *
       * 因此默认不设置最后一列的宽度并禁用拖拽让其自动扩展以确保其他列宽度不会变化.
       */
      if (i === columns.length - 1) {
        col.resizable = false;
      } else {
        col.width = col.width || 200;
      }
    }

    // 给列设置用于排序的优先级.
    col.order = col.order ?? i;
  }

  // 按照优先级排序.
  (columns as Array<NTableColumn>).sort((a, b) => a.order - b.order);

  return columns;
}

function resolveConfigByDataIndex(
  columnsConfig: NTableColumnConfig["columns"],
  dataIndex: DataIndex
): ColumnConfig | null {
  if (isNil(columnsConfig)) return null;

  const _dataIndex: Array<string | number> = isArray(dataIndex) ? dataIndex : [dataIndex];

  const len = _dataIndex.length;
  let config: any = columnsConfig;
  for (let i = 0; i < len; i++) {
    const key = _dataIndex[i];

    config = columnsConfig[key];
    if (isNil(config)) return null;
  }

  return config;
}
