import type { TableHelper } from "../table-helper";
import type { NTableColumn } from "./table-types";
import type { VNode } from "vue";

export type BodyCellSlot = (params: {
  text: string;
  record: TableHelper.RecordType;
  index: number;
  column?: NTableColumn;
}) => VNode | Array<VNode> | string | undefined;

export type HeaderCellSlot = (params: {
  title: string;
  column: NTableColumn;
}) => VNode | Array<VNode> | string | undefined;
