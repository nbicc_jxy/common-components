import type { PropType, Ref, VNode } from "vue";
import type { TableHelper } from "../../table-helper";
import type { Dayjs } from "dayjs";
import type { ExtractPropTypes } from "vue";
import type { TableProps } from "ant-design-vue";
import type { NTableColumn, NTableRowSelection } from "../table-types";
import type { BodyCellSlot } from "../external-types";
import type { useElementSize, useScroll } from "@vueuse/core";

/**
 * 甘特图滑动条数据对象.
 */
export interface TableBar {
  /**
   * 滑动条开始时间. 支持 {@link Dayjs} 或者 {@link string}.
   */
  start: Dayjs | string;
  /**
   * 滑动条结束时间. 支持 {@link Dayjs} 或者 {@link string}.
   */
  end: Dayjs | string;
  /**
   * 滑动条唯一标识. 当存在多个滑动条时, 可通过此值标识.
   */
  id?: string;
}

export type TableBarInternal = Omit<TableBar, "start" | "end"> & {
  start: Dayjs;
  end: Dayjs;
};

export type TableBarInit = Omit<TableBar, "start" | "end"> & {
  /**
   * 从数据中获取滑动条开始时间的键名.
   */
  startKey: string;
  /**
   * 从数据中获取滑动条结束时间的键名.
   */
  endKey: string;
};

export type getGanttTableBars = (record: TableHelper.RecordType, index: number) => Array<TableBar>;

export const nGanttTableProps = () =>
  ({
    /**
     * 组件通过 {@link bars} 获取甘特图滑动条数据. 支持之间传入一个 {@link TableBar} 对象 或由 {@link TableBar} 对象组成的数组.
     * 也可以传入一个函数, 函数接收当前行数据和索引, 返回一个由 {@link TableBar} 对象组成的数组.
     */
    bars: {
      type: [Object, Function] as PropType<TableBarInit | Array<TableBarInit> | getGanttTableBars>,
      required: true
    },
    /**
     * 甘特图时间轴的精度. 默认为 `day`. 可选值有 `month`, `week`, `day`.
     */
    precision: {
      type: String as PropType<"month" | "week" | "day">,
      default: "month"
    },
    /**
     * 甘特图节点的宽度. 默认为 `50`.
     */ cellWidth: {
      type: Number,
      default: 50
    },
    /**
     * 甘特图左侧部分是否可调整宽度. 默认为 `false`.
     */ resize: {
      type: Boolean,
      default: false
    },
    /**
     * 日期格式化字符串. 仅当 {@link start} 和 {@link end} 为 {@link string} 时有效.
     */ dateFormat: {
      type: String
    },
    dataSource: {
      type: Array as PropType<TableProps["dataSource"]>,
      default: () => []
    },
    columns: {
      type: Array as PropType<Array<Partial<NTableColumn>>>,
      default: () => []
    },
    rowSelection: {
      type: Object as PropType<Partial<NTableRowSelection>>
    }
  } as const);

export type NGanttTableProps = ExtractPropTypes<ReturnType<typeof nGanttTableProps>>;

export type NGanttTableSlots = {
  ganttBar: (
    params: Omit<Parameters<BodyCellSlot>[0], "text" | "column"> & { bar: TableBar }
  ) => VNode | Array<VNode>;
};

export const nGanttBarProps = () =>
  ({
    start: {
      type: Object as PropType<Dayjs>,
      required: true
    },
    end: {
      type: Object as PropType<Dayjs>,
      required: true
    },
    id: {
      type: String as PropType<TableBar["id"]>
    },
    /**
     * 滑动条是否可通过拖动两侧控件调整宽度.
     */
    resizable: {
      type: Boolean,
      default: true
    },
    /**
     * 滑动条是否可通过拖动来移动位置.
     */
    movable: {
      type: Boolean,
      default: true
    },
    color: {
      type: String,
      default: "gray"
    },
    height: {
      type: Number
    },
    index: {
      type: Number,
      required: true
    }
  } as const);

export type NGanttBarSlots = {
  default: () => VNode;
};

export type NGanttBarProps = ExtractPropTypes<ReturnType<typeof nGanttBarProps>>;

export interface NGanttTableContext {
  rowHeight: Ref<number>;
  precision: Ref<"month" | "week" | "day">;
  timeline: Ref<readonly [Dayjs, Dayjs]>;
  cellWidth: Ref<number>;
  chartTableScroll: ReturnType<typeof useScroll>;
  chartTableSize: ReturnType<typeof useElementSize>;
}

export const NGanttTableContextKey = "GANTT_TABLE_CONTEXT_KEY";
