import { cloneVNode, computed, type Ref, type VNode } from "vue";
import dayjs, { Dayjs } from "dayjs";
import type {
  TableBar,
  TableBarInternal,
  getGanttTableBars,
  NGanttTableProps,
  NGanttTableSlots
} from "./gantt-table-types";
import { isArray, isFunction, isNil } from "lodash-es";
import type { TableColumnGroupType, TableColumnType } from "ant-design-vue";
import type { TableHelper } from "../../table-helper";
import type { ColumnGroupType } from "ant-design-vue/lib/table/interface";
import type { BodyCellSlot, HeaderCellSlot } from "../external-types";

export function useGanttChart(
  bars: Ref<NGanttTableProps["bars"]>,
  precision: Ref<NGanttTableProps["precision"]>,
  cellWidth: Ref<NGanttTableProps["cellWidth"]>,
  dataSource: Ref<NGanttTableProps["dataSource"]>,
  rowHeight: Ref<number>,
  ganttBarSlot: NGanttTableSlots["ganttBar"]
) {
  const getRowBarsFunc = computed<getGanttTableBars>(() => {
    if (isNil(bars.value)) return () => [];
    else if (isFunction(bars.value)) return bars.value;

    const barInitList = isArray(bars.value) ? bars.value : [bars.value];

    return (record) => {
      const result: Array<TableBar> = [];
      for (let i = 0; i < barInitList.length; i++) {
        const barInit = barInitList[i];
        if (isNil(barInit)) continue;

        const { startKey, endKey } = barInit;

        result.push({ start: record[startKey], end: record[endKey], ...barInit });
      }

      return result;
    };
  });

  const barDataSource = computed<
    Array<
      | {
          minStart: Dayjs;
          maxEnd: Dayjs;
          bars: Array<TableBar>;
        }
      | undefined
    >
  >(() => {
    const _dataSource = dataSource.value ?? [];
    const _getRowBarsFunc = getRowBarsFunc.value;
    const result: Array<
      | {
          minStart: Dayjs;
          maxEnd: Dayjs;
          bars: Array<TableBarInternal>;
        }
      | undefined
    > = [];

    for (let i = 0; i < _dataSource.length; i++) {
      const record = _dataSource[i];
      const bars = _getRowBarsFunc(record, i);

      if (bars.length <= 0) {
        result.push(undefined);
        continue;
      }

      let minStart = dayjs(bars[0].start),
        maxEnd = dayjs(bars[0].end);
      const internalBars: Array<TableBarInternal> = [];
      for (let j = 0; j < bars.length; j++) {
        const start = dayjs(bars[j].start);
        const end = dayjs(bars[j].end);

        if (minStart > start) minStart = start;
        if (maxEnd < end) maxEnd = end;

        internalBars.push({ ...bars[j], start, end });
      }

      result.push({ minStart, maxEnd, bars: internalBars });
    }

    return result;
  });

  const timelineRange = computed(() => calcTimelineRange(barDataSource.value));
  const timeline = computed<Array<TableColumnGroupType<TableHelper.RecordType>>>(() => {
    const [start, end] = timelineRange.value;

    // const years = start.diff(end, "year");
    const months = end.diff(start, "month");
    // const days = start.diff(end, "day");
    // const hours = start.diff(end, "hour");

    const columns: Array<ColumnGroupType<TableHelper.RecordType>> = [];
    for (let i = 0; i < months; i++) {
      const date = start.add(i, "month");
      const monthDays = date.daysInMonth();

      const column: ColumnGroupType<TableHelper.RecordType> = {
        title: date
          .toDate()
          .toLocaleDateString(dayjs.locale(), { year: "numeric", month: "numeric" }),
        children: [],
        width: monthDays * cellWidth.value
      };

      for (let j = 1; j <= monthDays; j++) {
        column.children.push({
          title: j.toString(10),
          width: cellWidth.value
        });
      }

      columns.push(column);
    }

    return columns;
  });

  const timelineColumn = computed<TableColumnType<TableHelper.RecordType>>(() => {
    const _timeline = timeline.value;
    return {
      title: "时间轴",
      dataIndex: "timeline",
      // 加上 16px 的 padding
      width: _timeline.reduce((pre, cur) => pre + (cur.width as number), 0) + 16,
      customCell: () => ({ style: { padding: "0", position: "static" } }),
      customHeaderCell: () => ({
        class: "timeline-header-cell",
        style: { padding: "0", position: "static" }
      })
    };
  });

  /**
   * 甘特图内容插槽. 渲染背景和拖动滑块
   */
  const timelineBodyCell: BodyCellSlot = ({ column, index, record }) => {
    if (!column || column.dataIndex !== "timeline") return;

    const result: Array<VNode> = [<div class="timeline-row__placeholder">X</div>];

    // 第一行中增加甘特图背景
    if (index === 0) {
      // const columnNodes: Array<VNode> = [];
      // for (let i = 0; i < _timeline.length; i++) {
      //   const { children, width } = _timeline[i];
      //
      //   const cells: Array<VNode> = [];
      //   for (let j = 0; j < children.length; j++) {
      //     cells.push(<div class="row__cell" />);
      //   }
      //
      //   columnNodes.push(
      //     <div class="row__column" style={`width: ${width}px`}>
      //       {cells}
      //     </div>
      //   );
      // }
      //
      // result.push(<div class="timeline-row__background">{columnNodes}</div>);
    }

    const bars = barDataSource.value[index]?.bars ?? [];
    for (let i = 0; i < bars.length; i++) {
      const bar = bars[i];
      let barVNodes = ganttBarSlot({ bar, index, record });
      barVNodes = isArray(barVNodes) ? barVNodes : [barVNodes];

      for (let j = 0; j < barVNodes.length; j++) {
        result.push(
          cloneVNode(barVNodes[j], {
            ...bar,
            timeline: timelineRange.value,
            precision: precision.value,
            cellWidth: cellWidth.value,
            index
          })
        );
      }
    }

    return result;
  };
  /**
   * 甘特图时间轴(表头)插槽.
   */
  const timelineHeaderCell: HeaderCellSlot = ({ column }) => {
    if (column.dataIndex !== "timeline") return;

    const titleElements: Array<VNode> = [];
    const cellElements: Array<VNode> = [];
    const _timeline = timeline.value;
    for (let i = 0; i < _timeline.length; i++) {
      const { title, children, width } = _timeline[i];

      for (let j = 0; j < children.length; j++) {
        const child = children[j];
        cellElements.push(
          <div class="header__cell" style={{ width: child.width + "px" }}>
            {child.title}
          </div>
        );
      }

      titleElements.push(
        <div class="header__title-cell" style={`width: ${width}px`}>
          <span>{title}</span>
        </div>
      );
    }

    return (
      <div class="timeline-header">
        <div class="header__title-cells">{titleElements}</div>
        <div class="header__cells">{cellElements}</div>
      </div>
    );
  };

  return {
    column: timelineColumn,
    bodyCell: timelineBodyCell,
    headerCell: timelineHeaderCell,
    timelineRange
  };
}

const calcTimelineRange = (
  barDataSource: Array<
    | {
        minStart: Dayjs;
        maxEnd: Dayjs;
        bars: Array<TableBar>;
      }
    | undefined
  >
) => {
  const result: [Dayjs, Dayjs] = [dayjs(), dayjs().add(1, "year")];

  for (let i = 0; i < barDataSource.length; i++) {
    const bar = barDataSource[i];
    if (isNil(bar)) continue;

    const { minStart, maxEnd } = bar;

    if (result[0] > minStart) result[0] = minStart;
    if (result[1] < maxEnd) result[1] = maxEnd;
  }

  const [minStart, maxEnd] = result;
  return [
    minStart.startOf("month"),
    maxEnd.diff(minStart, "month") < 10 ? maxEnd.add(10, "month") : maxEnd
  ] as const;
};
