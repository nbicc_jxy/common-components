import { ref, type Ref, unref, watch } from "vue";
import { useDraggable } from "@vueuse/core";

export function useResizeBar(
  barElementRef: Ref<HTMLDivElement | undefined>,
  containerElementRef: Ref<HTMLDivElement | undefined>,
  minWidth: Ref<number>,
  enableResize: Ref<boolean>
) {
  const resizeBarPosition = useDraggable(barElementRef, {
    preventDefault: true,
    stopPropagation: true,
    capture: true,
    axis: "x",
    initialValue: { x: unref(minWidth), y: 0 },
    containerElement: containerElementRef,
    onStart: () => (enableResize.value ? undefined : false),
    onMove: (position) => (resizeBarPosition.x.value = Math.max(position.x, unref(minWidth))),
    onEnd: (position) => (contentTableWidth.value = position.x)
  });
  const contentTableWidth = ref();
  watch(minWidth, (width) => (contentTableWidth.value = width), { immediate: true });

  return {
    width: contentTableWidth,
    barStyle: resizeBarPosition.style,
    isDragging: resizeBarPosition.isDragging
  };
}
