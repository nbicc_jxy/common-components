import type { Ref } from "vue";
import { watch } from "vue";
import { useScroll } from "@vueuse/core";

export function useSyncScroll(a: Ref<HTMLElement | undefined>, b: Ref<HTMLElement | undefined>) {
  const aScroll = useScroll(a);
  const bScroll = useScroll(b);
  watch(aScroll.y, (contentY) => {
    if (bScroll.y.value === contentY) return;
    bScroll.y.value = contentY;
  });
  watch(bScroll.y, (chartY) => {
    if (aScroll.y.value === chartY) return;
    aScroll.y.value = chartY;
  });
}
