import type { PropType, ExtractPropTypes, ComponentPublicInstance } from "vue";
import type {
  ColumnGroupType,
  Key,
  TableRowSelection,
  ColumnType
} from "ant-design-vue/lib/table/interface";
import type { TableProps, TableColumnType } from "ant-design-vue";
import { AntDesignVueTableHelper } from "./ant-design-vue-table-helper";
import { storableProps } from "../../storage";
import type { TableHelper } from "../table-helper";
import type { AntDesignVueFormModelFieldArgs } from "../n-form";
import type { FormItemControlType } from "../form-helper";

export const nTableProps = () =>
  ({
    ...storableProps(),
    disabledRowKeys: {
      type: Array as PropType<Array<Key>>,
      default: () => []
    },
    showNumberColumn: {
      type: [Boolean, Object] as PropType<true | (TableColumnType & { startIndex: number })>,
      default: false
    },
    columns: {
      type: Array as PropType<Array<Partial<NTableColumn>>>,
      default: () => []
    },
    rowSelection: {
      type: Object as PropType<Partial<NTableRowSelection>>
    },
    rowKey: {
      type: [String, Function] as PropType<TableProps["rowKey"]>,
      default: "key"
    },
    dataSource: {
      type: Array as PropType<TableProps["dataSource"]>,
      default: () => []
    },
    pagination: {
      type: [Object, Boolean] as PropType<TableProps["pagination"]>,
      default: true
    },
    customRow: {
      type: Function as PropType<TableProps<TableHelper.RecordType>["customRow"]>
    },
    childrenColumnName: {
      type: String as PropType<Required<TableProps>["childrenColumnName"]>,
      default: "children"
    },
    columnConfig: {
      type: Object as PropType<Partial<NTableColumnConfig>>,
      default: () => ({})
    },
    /**
     * 禁用自适应高度.
     * 禁用后, 表格的内容(tbody)高度将不会随着容器的高度变化而变化.
     */
    disabledAdaptiveHeight: {
      type: Boolean,
      default: false
    },
    rowClassName: {
      type: [String, Function] as PropType<TableProps["rowClassName"]>
    },
    /**
     * 是否显示斑马纹
     */
    striped: {
      type: Boolean,
      default: false
    },
    /**
     * 是否启用编辑功能
     */
    editable: {
      type: Boolean,
      default: false
    },
    /**
     * 是否可以通过点击行来将改行变为编辑状态
     */
    editByClickRow: {
      type: Boolean,
      default: true
    },
    /**
     * 当可编辑时, 用于配置可编辑的字段. 与 {@link NForm} 的 fields 属性一致.
     */
    fields: {
      type: Array as PropType<Array<AntDesignVueFormModelFieldArgs<any, FormItemControlType>>>,
      default: () => []
    },
    size: {
      type: String as PropType<TableProps["size"]>,
      default: "small"
    },
    onResizeColumn: {
      type: Function as PropType<(w: number, col: ColumnType) => void>
    },
    /**
     * 当前编辑的行的 key
     */
    editedRowKey: {
      type: [String, Number] as PropType<Key | undefined>
    },
    ["onUpdate:editedRowKey"]: {
      type: Function as PropType<(data: Array<Key | undefined>) => void>,
      default: () => {}
    },
    contextSlots: {
      type: Object as PropType<{
        bodyEditableCell: ({
          text,
          record,
          index,
          column
        }: {
          text: string;
          record: TableHelper.RecordType;
          index: number;
          column: NTableColumn;
        }) => any;
        bodyEditableCellControl: ({
          text,
          record,
          index,
          column
        }: {
          text: string;
          record: TableHelper.RecordType;
          index: number;
          column: NTableColumn;
        }) => any;
      }>,
      default: () => ({})
    }
  } as const);

export type NTableProps = ExtractPropTypes<ReturnType<typeof nTableProps>>;

export interface NTableEmits {
  "update:editedRowKey": (data: Array<Key | undefined>) => void;
}

export interface NTableExpose {
  saveEditedRow: (needValidate: boolean) => void;
  cancelEditedRow: () => void;
  editRow: (rowKey: Key, needValidate: boolean) => void;
}

export type NTableInstance = ComponentPublicInstance<NTableProps, NTableExpose>;

export const nEasyTableProps = () =>
  ({
    tableHelper: {
      type: AntDesignVueTableHelper,
      required: true
    },
    columns: {
      type: Array as PropType<Array<Partial<NTableColumn>>>,
      default: () => []
    }
  } as const);

export type NEasyTableProps = ExtractPropTypes<ReturnType<typeof nEasyTableProps>>;

export type NEasyTableInstance = ComponentPublicInstance<NEasyTableProps, NTableExpose>;

export interface ColumnConfig {
  /**
   * 是否可拖拽调整列宽
   */
  resizable: boolean;
}

type NestColumnConfig = Record<string | number, ColumnConfig>;

/**
 * 表格列的配置项
 */
export interface NTableColumnConfig extends ColumnConfig {
  /**
   * 可通过此属性单独指定某个列的配置属性.
   */
  columns?: Record<string | number, ColumnConfig | NestColumnConfig>;
}

export type NTableColumn<RECORD_TYPE = TableHelper.RecordType> = ColumnGroupType<RECORD_TYPE> &
  ColumnType<RECORD_TYPE> &
  NTableColumnExtraOptions;

/**
 * 额外的列选项
 */
export type NTableColumnExtraOptions = {
  /**
   * 该列是否可见
   */
  visible: boolean;
  /**
   * 列排序时的的优先级
   */
  order: number;
};

export interface NTableRowSelection extends TableRowSelection {
  /**
   * 已选择行的 key
   */
  selectedRowKeys: Array<TableHelper.RecordType[TableHelper.RecordKey]>;
  /**
   * 已选择行的数据
   */
  selectedRows: Array<TableHelper.RecordType>;
  /**
   * 禁用的行的 key
   */
  disabledRowKeys: Array<TableHelper.RecordType[TableHelper.RecordKey]>;
  /**
   * 选择行变化时的回调
   */
  onChange: Required<TableRowSelection>["onChange"];
  /**
   * 重置已选择行
   */
  reset: () => void;
  /**
   * 选择或取消选择行
   * @param data 行数据
   * @param selected 是否选中
   * @param force 是否包括被禁用的行
   */
  select: (data: TableHelper.RecordType, selected?: boolean, force?: boolean) => void;
  /**
   * 判断行是否被选中
   * @param data 行数据
   */
  isSelected: (data: TableHelper.RecordType) => boolean;
  /**
   * 选择或取消选择所有行
   * @param selected 是否选中
   * @param force 是否包括被禁用的行
   */
  selectAll: (selected?: boolean, force?: boolean) => void;
  /**
   * 判断是否所有行都被选中. 如果 excludeDisabledRow 为 true, 被禁用的行不会被计算在内.
   * @param excludeDisabledRow 是否排除被禁用的行. 默认为 false.
   */
  isAllSelected: (excludeDisabledRow?: boolean) => boolean;
  /**
   * 被选中的行数是否为 0
   */
  isNoneSelected: () => boolean;
}
