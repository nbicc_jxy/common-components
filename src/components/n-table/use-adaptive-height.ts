import type { TableProps } from "ant-design-vue/lib/table";
import type { Ref } from "vue";
import { computed, onMounted, onUnmounted, reactive, ref, toRefs, watch } from "vue";
import { isNil } from "lodash-es";
import { elementRect } from "../utils";
import { elementMutation } from "../utils/reactive-dom-mutation";
import { watchDebounced } from "@vueuse/core";

/**
 * 自适应表格高度的 hook.
 * @param tableWrapperRef
 * @param scroll
 */
export function useAdaptiveHeight(
  tableWrapperRef: Ref<HTMLDivElement>,
  scroll: Ref<TableProps["scroll"] | undefined>
) {
  const adaptiveHeight = ref(0);

  const tableScrollOptions = computed(() => ({
    scrollToFirstRowOnChange: true,
    y: adaptiveHeight.value,
    ...scroll.value
  }));

  onMounted(async () => {
    // 保存 dom 元素
    const tableWrapperElement = tableWrapperRef.value;
    const tableElement: HTMLElement | null = tableWrapperElement.querySelector(
      ".ant-spin-container"
    ) as HTMLElement | null;
    const headerElement: HTMLElement | null =
      tableWrapperElement.querySelector(".ant-table-header") ??
      tableWrapperElement.querySelector(".ant-table-thead");
    // 只关心设置了 fixed 的总结栏. 未设置 fixed 的总结栏认为属于表格内容不影响高度计算.
    const summaryElement: HTMLElement | null = tableWrapperElement.querySelector(
      ".ant-table-container > .ant-table-summary"
    );
    // why let: 分页器将被延迟加载, 可能无法获取 dom 元素
    let paginationElement: HTMLElement | null =
      tableWrapperElement.querySelector(".ant-pagination");

    if (isNil(tableWrapperElement)) throw new Error("table wrapper is undefined");
    if (isNil(tableElement)) throw new Error("table is undefined");
    if (isNil(headerElement)) throw new Error("header is undefined");

    // 初始化高度
    const elementRects = reactive({
      tableWrapper: elementRect.watch(tableWrapperElement),
      tableHeader: elementRect.watch(headerElement),
      tableSummary: summaryElement ? elementRect.watch(summaryElement) : undefined,
      tablePagination: paginationElement ? elementRect.watch(paginationElement) : undefined
    });
    watchDebounced(
      () => toRefs(elementRects),
      () => {
        const tableWrapperH = elementRects.tableWrapper.height;
        const headerH = elementRects.tableHeader.height;
        const paginationH = elementRects.tablePagination?.height ?? 0;
        const footerH = elementRects.tableSummary?.height ?? 0;
        // 获取分页器 margin
        const { marginTop, marginBottom, paddingTop, paddingBottom } = isNil(paginationElement)
          ? { marginTop: "0", marginBottom: "0", paddingTop: "0", paddingBottom: "0" }
          : window.getComputedStyle(paginationElement);
        const paginationMarginH = Number.parseInt(marginTop) + Number.parseInt(marginBottom);
        const paginationPaddingH = Number.parseInt(paddingTop) + Number.parseInt(paddingBottom);
        // 计算表格高度
        adaptiveHeight.value =
          tableWrapperH -
          headerH -
          (paginationH + paginationMarginH + paginationPaddingH) -
          footerH;
      },
      { debounce: 128 }
    );

    // 分页器 dom 元素还未加载时, 监听 table 元素的子元素变化, 获取分页器元素.
    if (isNil(paginationElement)) {
      const dom = tableWrapperElement.querySelector(".ant-spin-container")!;
      const unwatch = watch(elementMutation.watch(dom, { childList: true }), () => {
        paginationElement = tableWrapperElement.querySelector(".ant-pagination");
        if (isNil(paginationElement)) return;

        elementRects.tablePagination = elementRect.watch(
          paginationElement
        ) as unknown as DOMRectReadOnly;
        unwatch();
      });
      onUnmounted(unwatch);
    }

    onUnmounted(() => {
      elementRect.unwatch(tableWrapperElement);
      elementRect.unwatch(headerElement);
      elementRect.unwatch(paginationElement);
      elementRect.unwatch(summaryElement);
    });
  });

  return {
    scrollOptions: tableScrollOptions
  };
}
