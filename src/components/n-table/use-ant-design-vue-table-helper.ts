import {
  AntDesignVueTableHelper,
  AntDesignVueTableHelperOptions
} from "./ant-design-vue-table-helper";
import type { TableHelper } from "../table-helper";
import { computed, reactive, ref, watchEffect } from "vue";
import type { Ref } from "vue";
import type { TableProps } from "ant-design-vue/lib/table";
import { isArray, isNil, values } from "lodash-es";
import { appendRows } from "./use-row-clickable";
import type { NTableColumn, NTableRowSelection } from "./table-types";

/**
 * 易于使用的 Ant Design Vue TableHelper hook.
 * @param loadRecord 加载数据的函数
 * @param columns 表格列
 * @param recordKeyName 表格行的 key
 * @param options 配置项
 */
export function useAntDesignVueTableHelper<
  RECORD_KEY extends TableHelper.RecordKey,
  RECORD_TYPE extends TableHelper.RecordType
>(
  loadRecord: TableHelper.LoadTableRecordFunction<RECORD_TYPE>,
  columns: Array<Partial<NTableColumn>>,
  recordKeyName: RECORD_KEY,
  options?: Partial<AntDesignVueTableHelperOptions>
) {
  const tableHelper = reactive(
    new AntDesignVueTableHelper(loadRecord, recordKeyName, options)
  ) as unknown as AntDesignVueTableHelper<RECORD_KEY, RECORD_TYPE>;

  /**
   * 监听 Ant Design Vue Table 的 onChange 事件
   * @param pagination
   * @param filters
   * @param sorter
   * @param extra
   */
  const onChange: TableProps["onChange"] = async (pagination, filters, sorter, extra) => {
    if (!extra?.action) return;

    switch (extra.action) {
      case "sort":
        tableHelper.removeSortParams();

        if (!isArray(sorter)) sorter = [sorter];

        for (let i = 0; i < sorter.length; i++) {
          const { field: _field, order } = sorter[i];
          const field = isArray(_field) ? (_field[0] as string) : (_field as string);

          switch (order) {
            case "ascend":
            case "descend":
              tableHelper.addSortParams({ field, direction: order });
              break;
            default:
              tableHelper.removeSortParams(field);
          }
        }

        // 排序参数变化后, 重置页数
        tableHelper.updatePaginationParams({ current: -1 });
        tableHelper.loadRecord(true);
        break;
      case "paginate":
        tableHelper
          .updatePaginationParams({
            // 每页条数数大小变化时, 重置页数
            current:
              tableHelper.paginationParams.pageSize !== pagination.pageSize
                ? -1
                : pagination.current,
            pageSize: pagination.pageSize
          })
          .loadRecord(true);
        break;
    }
  };

  const selectedRowKeyMap = computed(() => {
    const map = {} as Record<TableHelper.RecordType[TableHelper.RecordKey], boolean>;
    for (let i = rowSelection.selectedRowKeys.length; i--; )
      map[rowSelection.selectedRowKeys[i]] = true;

    return map;
  });
  const rowSelection = reactive({
    selectedRowKeys: [],
    selectedRows: [],
    disabledRowKeys: [],
    onChange: (_selectedRowKeys, _selectedRows) => {
      rowSelection.selectedRowKeys = _selectedRowKeys as Array<
        TableHelper.RecordType[TableHelper.RecordKey]
      >;
      rowSelection.selectedRows = _selectedRows;
    },
    reset: () => ((rowSelection.selectedRowKeys = []), (rowSelection.selectedRows = [])),
    select: (data: TableHelper.RecordType, selected?: boolean, force: boolean = false) => {
      selected = selected ?? !rowSelection.isSelected(data);

      const { rows, rowKeys } = appendRows(
        rowSelection.selectedRowKeys,
        rowSelection.selectedRows,
        force ? [] : rowSelection.disabledRowKeys,
        (record) => record[recordKeyName],
        [data],
        selected
      );

      rowSelection.selectedRowKeys = rowKeys;
      rowSelection.selectedRows = rows;
    },
    isSelected: (data: TableHelper.RecordType): boolean => {
      return selectedRowKeyMap.value[data[recordKeyName]] ?? false;
    },
    selectAll: (selected?: boolean, force: boolean = false) => {
      selected = selected ?? !rowSelection.isAllSelected(true);

      const { rows, rowKeys } = appendRows(
        rowSelection.selectedRowKeys,
        rowSelection.selectedRows,
        force ? [] : rowSelection.disabledRowKeys,
        (record) => record[recordKeyName],
        tableHelper.dataSource,
        selected
      );

      rowSelection.selectedRowKeys = rowKeys;
      rowSelection.selectedRows = rows;
    },
    isAllSelected: (excludeDisabledRow: boolean = false): boolean => {
      const selectedCount = rowSelection.selectedRowKeys.length;

      if (!excludeDisabledRow) {
        return selectedCount >= tableHelper.dataSource.length;
      } else {
        const disabledRowKeySet = new Set(rowSelection.disabledRowKeys);
        return (
          selectedCount >=
          tableHelper.dataSource.filter((item) => !disabledRowKeySet.has(item[recordKeyName]))
            .length
        );
      }
    },
    isNoneSelected: (): boolean => {
      return rowSelection.selectedRowKeys.length <= 0;
    }
  }) as NTableRowSelection;
  /**
   * 以下情况将会重置已选择行:
   *  1. 存在除页数变化外的其他参数变化
   */
  tableHelper.on("recordLoadStart", ({ changes }) => {
    const changedCount = values(changes).filter((value) => value).length;

    // 无变化, 忽略
    if (changedCount <= 0) return;
    // 仅页数变化时, 忽略
    if (changes.current && changedCount === 1) return;

    // 重置已选择行
    rowSelection.reset();
  });

  /**
   * 绑定搜索表单的行为
   */
  // 为了跟 Ant Design Vue Form 的表单联动, 需要由内部管理搜索表单的数据.
  const searchForm = reactive({
    data: {},
    // 根据 searchFormData 的内容, 更新 tableHelper 的过滤参数并重新加载数据.
    search: async () => {
      try {
        await tableHelper
          // 过滤参数变化时重置页数
          .updatePaginationParams({ current: -1 })
          .updateFilterParams(searchForm.data)
          .loadRecord(true);
      } finally {
        // 过滤参数变化时重置已选择行
        rowSelection.reset();
      }
    },
    // 重置搜索表单
    reset: () => {
      Object.keys(searchForm.data).forEach((key) => (searchForm.data[key] = undefined));
      tableHelper
        .updatePaginationParams({ pageSize: tableHelper.options.pageSize, current: -1 })
        .updateFilterParams(searchForm.data)
        .removeSortParams();
      rowSelection.reset();
    }
  }) as {
    /**
     * 搜索表单的数据
     */
    data: Record<string, unknown>;
    /**
     * 确认搜索, 更新过滤参数并重新加载数据
     */
    search: () => void;
    /**
     * 重置搜索表单, 更新过滤参数并重新加载数据
     */
    reset: () => void;
  };

  /**
   * 为了跟 Ant Design Vue Table 的表头排序 UI 联动, 需要由内部管理 columns 属性.
   *
   * 目前只会对第一个排序参数进行处理.
   */
  const computedColumns = ref(
    columns.map((column) => ({ ...column, visible: isNil(column.visible) ? true : column.visible }))
  ) as Ref<Array<NTableColumn>>;
  // const computedColumns = reactive(
  //   columns.map((column) => ({ ...column, visible: isNil(column.visible) ? true : column.visible }))
  // ) as Array<AntDesignVueTableHelperColumnType>;
  watchEffect(() => {
    const { field, direction } = tableHelper.sortParams[0] || {};

    computedColumns.value.forEach(
      (column) => (column.sortOrder = field === column.dataIndex ? direction : undefined)
    );
  });

  return {
    /**
     * {@link AntDesignVueTableHelper} 实例对象
     */
    tableHelper,

    /**
     * 列属性
     */
    columns: computedColumns,
    /**
     * 监听 Ant Design Vue Table row-selection 属性的 onChange 事件
     */
    onChange,

    /**
     * 搜索表单相关的属性
     */
    searchForm,

    /**
     * 行选择相关的属性
     */
    rowSelection
  };
}
