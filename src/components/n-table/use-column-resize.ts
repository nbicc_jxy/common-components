import type { ColumnType } from "ant-design-vue/lib/table/interface";
import type { ColumnsType } from "ant-design-vue/es/table";
import type { Ref } from "vue";
import { isNil } from "lodash-es";
import type { DataIndex } from "ant-design-vue/lib/vc-table/interface";
import { joinDataIndex } from "../utils";
import type { SimpleStorage } from "../../storage";
import { computed } from "vue";

export function useColumnResize(
  columnsRef: Ref<ColumnsType>,
  columnWidthStorage: SimpleStorage,
  passedCallback?: (w: number, col: ColumnType) => void
) {
  // 初始化时从本地缓存恢复之前设置的列宽.
  for (let i = columnsRef.value.length; i--; ) {
    const col = columnsRef.value[i];
    // @ts-ignore
    const dataIndex: DataIndex = col.dataIndex;
    if (isNil(dataIndex)) continue;

    col.width = columnWidthStorage.get(joinDataIndex(dataIndex)) ?? col.width;
  }

  const columnMap = computed(() => {
    const flattedColumns: Array<ColumnType> = columnsRef.value.flatMap(
      // @ts-ignore
      (column) => column.children ?? column
    );
    return new Map<string, ColumnType>(
      flattedColumns.map((col) => [joinDataIndex(col.dataIndex!), col])
    );
  });

  const callback = (w: number, col: ColumnType) => {
    if (isNil(col.dataIndex)) return;
    const dataIndex = joinDataIndex(col.dataIndex);

    const column = columnMap.value.get(dataIndex);
    if (isNil(column)) return;

    column.width = w;
    columnWidthStorage.set(joinDataIndex(col.dataIndex), w);

    passedCallback && passedCallback(w, col);
  };

  return {
    onResizeColumn: callback
  };
}

export function initialColumnWidthStorageData(
  columns: ColumnsType
): Record<string, string | number> {
  const result: Record<string, string | number> = {};

  for (let i = columns.length; i--; ) {
    const col = columns[i];
    // @ts-ignore
    const dataIndex: DataIndex = col.dataIndex;
    if (isNil(dataIndex) || isNil(col.width)) continue;

    result[joinDataIndex(dataIndex)] = col.width;
  }

  return result;
}
