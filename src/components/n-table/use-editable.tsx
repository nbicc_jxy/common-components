import type { NTableProps } from "./table-types";
import type { Ref, Slots, VNode } from "vue";
import { computed, nextTick, onMounted, onUnmounted, ref, toRaw, watch } from "vue";
import { Form, Popover, Tooltip, TypographyText } from "ant-design-vue";
import type { GetComponentProps, GetRowKey } from "ant-design-vue/lib/vc-table/interface";
import type { TableHelper } from "../table-helper";
import { cloneDeep, debounce, isArray, isEmpty, isFunction, isNil, isString } from "lodash-es";
import { ignoreClickEvent } from "./use-row-clickable";
import { AntDesignVueFormItem, NFormItem, useAntDesignVueFormHelper } from "../n-form";
import type { Rule } from "ant-design-vue/lib/form";
import type { FormItem } from "../form-helper";
import { FormItemControlType } from "../form-helper";
import { useSpecInject } from "../utils/hooks";
import { isValidElement } from "../utils/is-valid-element";
import { generateCustomFormHelperPreset, generateFormHelperPreset } from "../n-form/utils";
import { isEqualRecord } from "./utils";
import type { ValidateInfo } from "ant-design-vue/lib/form/useForm";
import { elementVisibilityWithRoot } from "../utils/reactive-dom-visibility";
import type { ColumnType } from "ant-design-vue/lib/table/interface";
import { joinDataIndex } from "../utils";
import { AntDesignVueUseFormValidator } from "../n-form/ant-design-vue-adaptive/ant-design-vue-use-form-validator";
import type { BodyCellSlot } from "./external-types";
import { composeCallback } from "../utils/hooks/use-compose-callback";

const { useForm } = Form;

export function useEditable(
  tableWrapperRef: Ref<HTMLDivElement | undefined>,
  tablePopupContainerRef: Ref<HTMLDivElement | undefined>,
  editedRow: Ref<TableHelper.RecordType | undefined>,
  editByClickRow: Ref<NTableProps["editByClickRow"]>,
  fields: Ref<NTableProps["fields"]>,
  rowKey: Ref<GetRowKey<TableHelper.RecordType>>,
  size: Ref<NTableProps["size"]>,
  slots: Slots,
  onRowEdited: (record: TableHelper.RecordType | undefined) => void
) {
  let originRowRecord: TableHelper.RecordType | undefined;
  const activeRowRecord = ref() as Ref<TableHelper.RecordType | undefined>;
  const activeRowKey = computed(() =>
    activeRowRecord.value ? rowKey.value(activeRowRecord.value) : undefined
  );
  /**
   * 将 record 设置为编辑状态
   * @param record
   */
  const activateRow = (record: TableHelper.RecordType) => {
    if (activeRowKey.value === rowKey.value(record)) return;

    originRowRecord = record;
    activeRowRecord.value = cloneDeep(originRowRecord);
    onRowEdited(originRowRecord);

    nextTick(() => watchRowCellVisibility()).then();
  };
  /**
   * 取消当前编辑行的编辑状态
   */
  const deactivateRow = () => {
    originRowRecord = activeRowRecord.value = undefined;
    clearValidate();

    onRowEdited(originRowRecord);

    nextTick(() => unwatchRowCellVisibility()).then();
  };
  /**
   * 将当前编辑行的数据保存到原始数据中
   * @param needValidate 是否需要校验
   */
  const saveActiveRow = async (needValidate: boolean = true) => {
    const record = activeRowRecord.value;
    if (isNil(record) || isNil(originRowRecord)) return;

    if (needValidate) await validate();

    const keys = Object.keys(record);
    for (let i = keys.length; i--; ) {
      const key = keys[i];
      originRowRecord[key] = record[key];
    }

    deactivateRow();
  };
  /**
   * 保存当前编辑行后将 record 设置为编辑状态
   * @param record
   * @param needValidate 是否需要校验
   */
  const editRow = async (record: TableHelper.RecordType, needValidate: boolean = true) => {
    // 保存当前编辑的行
    await saveActiveRow(needValidate);

    // 激活传入的行, 设置为编辑状态
    activateRow(record);
  };
  watch(editedRow, (value) => value && activateRow(value), { immediate: true });
  /**
   * 设置当前编辑行数据的某个字段的值. 如果不在编辑状态, 则不会生效.
   * @param name 字段名
   * @param value 字段值
   */
  const setActiveRowField = (name: string, value: any) => {
    if (isNil(activeRowRecord.value)) return;

    if (isNil(formHelper.getField(name))) {
      activeRowRecord.value[name] = value;
    } else {
      formHelper.setField(name, value);
    }
  };

  const _fields = ref() as Ref<Array<FormItem>>;
  /************************* useForm *************************/
  const rulesRef = computed(() =>
    (_fields.value ?? []).reduce((rules, field) => {
      rules[field.options.name] = field.options.rules as Array<Rule>;
      return rules;
    }, {} as Record<string, Array<Rule>>)
  );
  const useFormResult = useForm(activeRowRecord, rulesRef);
  const { validate, clearValidate, validateInfos } = useFormResult;

  /************************* formHelper *************************/
  const { form: FormSpecConfig } = useSpecInject("form");
  const { formHelper } = useAntDesignVueFormHelper(
    (name, value) => {
      const record = activeRowRecord.value;
      if (isNil(record)) return;

      record[name] = value;
    },
    () => {},
    [
      generateFormHelperPreset(FormSpecConfig.value),
      generateCustomFormHelperPreset(() => ({
        size: size.value,
        getPopupContainer: () => tablePopupContainerRef.value
      }))
    ],
    new AntDesignVueUseFormValidator(useFormResult)
  );
  watch(activeRowRecord, (value) => !isNil(value) && formHelper.setRawData({ ...value }), {
    immediate: true
  });
  watch(
    fields,
    (value) => {
      const clonedFields = cloneDeep(toRaw(value) ?? []);
      for (let i = clonedFields.length; i--; ) {
        const clonedField = clonedFields[i];
        const _clonedField = isArray(clonedField) ? clonedField[0] : clonedField;

        _clonedField.events = {
          ..._clonedField.events,
          onBlur: composeCallback(_clonedField?.events?.["onBlur"], () =>
            validate(_clonedField.name, { trigger: "blur" })
          )
        };
      }

      formHelper.setFields(clonedFields);
      _fields.value = formHelper.fields;
    },
    { immediate: true, deep: true }
  );
  const _fieldMap = computed(
    () => new Map(_fields.value.map((field) => [field.options.name, field]))
  );

  const customRow = computed<GetComponentProps<TableHelper.RecordType>>(() => (data) => ({
    onClick: async (event: MouseEvent) => {
      if (!editByClickRow.value) return;
      if (ignoreClickEvent(event)) return;

      if (isEqualRecord(data, activeRowRecord.value, rowKey.value)) return;

      await editRow(data);
    }
  }));
  onMounted(() => {
    /**
     * 在捕获阶段阻止表格行上的 mousedown 事件冒泡, 避免影响到在 document 上添加的 mousedown 事件.
     */
    const stopMousedownEvent = (event: MouseEvent) => {
      event.stopPropagation();
    };
    const save = () => saveActiveRow();

    /**
     * 点击页面上除表格行之外的任何地方, 会保存当前编辑的行(通过校验的话).
     */
    watch(
      editByClickRow,
      (value, oldValue, onCleanup) => {
        let cleanupFunc: () => void;

        if (value) {
          document.addEventListener("mousedown", save);
          tablePopupContainerRef.value?.addEventListener("mousedown", stopMousedownEvent);
          cleanupFunc = () => {
            document.removeEventListener("mousedown", save);
            tablePopupContainerRef.value?.removeEventListener("mousedown", stopMousedownEvent);
          };
        } else {
          cleanupFunc = () => {};
        }

        onCleanup(cleanupFunc);
      },
      { immediate: true }
    );
  });

  /**
   * 根据控件类型渲染表单项
   * @param field 表单项对象
   * @param controlNode 控件节点
   * @param originBodyCellOrText 原始的表格单元格内容
   * * 当内容为字符串时, 表示用户未自定义单元格内容.
   * * 当内容为 VNode 时, 表示用户自定义了单元格内容.
   * @param validateInfo
   */
  const renderFormItem = (
    field: FormItem,
    controlNode: Array<VNode> | null,
    originBodyCellOrText: Array<VNode> | string,
    validateInfo: ValidateInfo
  ) => {
    const name = field.options.name;

    switch (field.control.options.type) {
      // 当控件类型为文本域时, 需要将控件放到 Popover 中, 以便在内容过长时显示完整内容.
      case FormItemControlType.TEXT_AREA: {
        return (
          <Popover
            visible={!isScrolling.value && cellVisibleMap.value[name]}
            placement="bottom"
            trigger="click"
            getPopupContainer={(triggerNode) => tablePopupContainerRef.value ?? triggerNode}
          >
            {{
              content: () => (
                <>
                  <NFormItem
                    {...{
                      label: undefined,
                      autoLink: false,
                      validateStatus: validateInfo.validateStatus
                    }}
                    field={field as AntDesignVueFormItem}
                  >
                    {{ default: () => controlNode }}
                  </NFormItem>
                </>
              ),
              default: () =>
                isString(originBodyCellOrText) ? (
                  <TypographyText
                    {...{
                      style: {
                        width: "100%"
                      }
                    }}
                    ellipsis
                    content={originBodyCellOrText}
                  />
                ) : (
                  originBodyCellOrText
                )
            }}
          </Popover>
        );
      }
      default:
        return (
          <NFormItem
            {...{ label: undefined, autoLink: false, validateStatus: validateInfo.validateStatus }}
            field={field as AntDesignVueFormItem}
          >
            {{ default: () => controlNode }}
          </NFormItem>
        );
    }
  };
  const customBodyCell = computed<BodyCellSlot>(() => ({ text, record, index, column }) => {
    const originBodyCell = slots["bodyCell"]?.({ text, record, index, column });
    if (isNil(activeRowKey.value)) return originBodyCell;

    const recordKey = rowKey.value(record);
    if (activeRowKey.value !== recordKey) return originBodyCell;

    // 获取对应的 field
    if (isNil(column)) return originBodyCell;
    const field = _fieldMap.value.get(joinDataIndex(column.dataIndex!));
    if (isNil(field)) return originBodyCell;

    // 自定义编辑单元格, 优先级最高.
    if (isFunction(slots["bodyEditableCell"])) {
      const slot = slots["bodyEditableCell"];
      const node = slot({
        text,
        record: activeRowRecord.value,
        index,
        column,
        field,
        validateInfos,
        popupContainer: tablePopupContainerRef.value
      });

      if (!isEmpty(node) && isValidElement(node)) return node;
    }

    // 自定义编辑单元格控件, 优先级次之.
    let controlNode: Array<VNode> | null = null;
    if (isFunction(slots["bodyEditableCellControl"])) {
      const slot = slots["bodyEditableCellControl"];
      controlNode = slot({
        text,
        record: activeRowRecord.value,
        index,
        column,
        field,
        validateInfos,
        popupContainer: tablePopupContainerRef.value
      });

      if (isEmpty(controlNode) || !isValidElement(controlNode)) controlNode = null;
    }

    const validateInfo = validateInfos[field.options.name] ?? {};

    const fieldName = field.options.name;
    if (!isEmpty(cellVisibleMap.value) && isNil(cellVisibleMap.value[fieldName]))
      console.warn(`不兼容的可编辑单元格, 请检查 fields 属性中字段 ${fieldName} 的配置.`);

    return (
      <Tooltip
        color="#FF4D4F"
        title={validateInfo.help}
        visible={
          validateInfo.validateStatus === "error" &&
          !isScrolling.value &&
          cellVisibleMap.value[fieldName]
        }
        destroyTooltipOnHide
        getPopupContainer={(triggerNode) => tablePopupContainerRef.value ?? triggerNode}
      >
        {renderFormItem(
          field,
          controlNode,
          // 优先使用用户自定义的单元格内容
          !isNil(originBodyCell) && isValidElement(originBodyCell) ? originBodyCell : text,
          validateInfo
        )}
      </Tooltip>
    );
  });

  const isScrolling = ref(false);
  onMounted(() => {
    const tableWrapperElement = tableWrapperRef.value;
    if (isNil(tableWrapperElement)) return;
    const tableBodyElement: HTMLElement | null =
      tableWrapperElement.querySelector(".ant-table-body") ??
      tableWrapperElement.querySelector(".ant-table-content");
    if (isNil(tableBodyElement)) return;

    const onScroll = debounce(
      () => {
        /**
         * @see https://lodash.com/docs/4.17.15#debounce
         * Note: If leading and trailing options are true, func is invoked on the trailing edge of the timeout only if the debounced function is invoked more than once during the wait timeout.
         */
        if (isScrolling.value === false) onScroll();

        isScrolling.value = !isScrolling.value;
      },
      100,
      {
        leading: true
      }
    );

    tableBodyElement.addEventListener("scroll", onScroll);

    return () => {
      tableBodyElement.removeEventListener("scroll", onScroll);
    };
  });

  /************************* 检测可见性并控制弹出框隐藏 *************************/
  let elementVisibility: ReturnType<typeof elementVisibilityWithRoot>;
  const cellVisibleMap = ref<Record<string, boolean>>({});

  /**
   * 初始化可见性检测对象, 并计算左右固定列的宽度以限制检测范围(IntersectionObserver 无法检测到被固定列遮挡的元素).
   */
  function initialElementVisibility() {
    if (elementVisibility) return;

    const tablePopupContainer = tablePopupContainerRef.value;
    if (isNil(tablePopupContainer)) return;

    const rowElement = tablePopupContainer.querySelector(".ant-table-row");
    if (isNil(rowElement)) throw new Error("表格行数据为空, 无法初始化可见性检测.");
    const leftMargin = Array.from(rowElement.querySelectorAll(".ant-table-cell-fix-left"))
      .map((item) => item.getBoundingClientRect().width)
      .reduce((prev, curr) => prev + curr, 0);
    const rightMargin = Array.from(rowElement.querySelectorAll(".ant-table-cell-fix-right"))
      .map((item) => item.getBoundingClientRect().width)
      .reduce((prev, curr) => prev + curr, 0);

    elementVisibility = elementVisibilityWithRoot(tablePopupContainer, {
      rootMargin: `0px -${rightMargin}px 0px -${leftMargin}px`
    });
  }
  const watchRowCellVisibility = () => {
    initialElementVisibility();

    unwatchRowCellVisibility();

    const cells = document.querySelectorAll("[data-n-table-body-cell-editable]");
    for (let i = cells.length; i--; ) {
      const cell = cells[i] as HTMLElement;
      cellVisibleMap.value[cell.dataset.nTableBodyCellEditable!] = elementVisibility.watch(
        cell,
        0.8
      ) as unknown as boolean;
    }
  };
  const unwatchRowCellVisibility = () => {
    elementVisibility?.unwatchAll();
    cellVisibleMap.value = {};
  };
  /**
   * 为当前编辑行的单元格添加 `data-n-table-body-cell-editable` attribute 且值为对应的 field name.  用于 dom 查询获取单元格对应的表单项.
   */
  const customEditableCell = computed(
    () =>
      ((data: TableHelper.RecordType, index: unknown, column: ColumnType) => {
        if (activeRowKey.value !== rowKey.value(data)) return;

        const field = _fieldMap.value.get(joinDataIndex(column.dataIndex!));
        if (isNil(field)) return;

        return { "data-n-table-body-cell-editable": field?.options.name };
      }) as GetComponentProps<TableHelper.RecordType>
  );
  onUnmounted(() => {
    elementVisibility?.destroy();
  });

  return {
    customRow: customRow,
    customCell: customEditableCell,
    bodyCell: customBodyCell,
    activateRow,
    deactivateRow,
    saveActiveRow,
    editRow,
    setActiveRowField
  };
}
