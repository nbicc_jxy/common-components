import type { Ref } from "vue";
import { Tooltip, type TooltipProps } from "ant-design-vue";
import { isBoolean, isEmpty, isFunction, isString } from "lodash-es";
import { isEmptyElement } from "../utils/is-valid-element";
import type { BodyCellSlot } from "./external-types";
import { computed } from "vue";

export function useFallbackBodyCell(
  bodyCellSlotRef: Ref<BodyCellSlot>,
  tooltipProps: Ref<TooltipProps | boolean>,
  tablePopupContainerRef: Ref<HTMLDivElement | undefined>
) {
  const _tooltipProps = computed(() => {
    if (isBoolean(tooltipProps.value)) return tooltipProps.value ? {} : null;
    else return tooltipProps.value;
  });

  const fallbackBodyCell: BodyCellSlot = (params) => {
    const bodyCellSlot = bodyCellSlotRef.value;

    /**
     * 当 bodyCell 非空且内容也不为空时, 才使用 bodyCell. 否则用 Tooltip 包裹文本.
     */
    if (isFunction(bodyCellSlot)) {
      const vnodes = bodyCellSlot(params);
      if (!isEmpty(vnodes) && !isEmptyElement(vnodes)) return vnodes;
    }

    const renderedText = params.text;

    // 启用了 ellipsisTooltip 时, 使用 Tooltip 包裹文本.
    if (params.column?.ellipsis && _tooltipProps.value) {
      /**
       * 提前返回空值, 避免被 Tooltip 包裹导致无法享受到 Table 组件的回退机制.
       * 注: 返回非空值将无法享受到 Table 组件的回退机制.
       */
      if (!isString(renderedText) || renderedText.length === 0) return renderedText;

      return (
        <Tooltip
          {..._tooltipProps.value}
          title={params.text}
          getPopupContainer={(triggerNode) => tablePopupContainerRef.value ?? triggerNode}
        >
          {{
            default: () => params.text
          }}
        </Tooltip>
      );
    }

    return renderedText;
  };

  return {
    fallbackBodyCell
  };
}
