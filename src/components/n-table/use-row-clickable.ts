import type { Ref } from "vue";
import type { GetComponentProps, GetRowKey } from "ant-design-vue/lib/vc-table/interface";
import { computed, ref, toRaw, watch } from "vue";
import { isNil, isString } from "lodash-es";
import type { TableHelper } from "../table-helper";
import type { NTableProps } from "./table-types";

type RECORD_KEY = TableHelper.RecordType[TableHelper.RecordKey];
type RECORD_TYPE = TableHelper.RecordType;

/**
 * 可通过点击整行选择
 * 注: 行中的按钮等需要进行其他点击操作的元素需要中止事件冒泡
 */
export function useRowClickable(
  rowSelection: Ref<NTableProps["rowSelection"]>,
  disabledRowKeys: Ref<Array<RECORD_KEY>>,
  rowKey: Ref<GetRowKey<RECORD_TYPE>>
) {
  if (isNil(rowSelection)) return {};

  /**
   * 如设置了 {@link rowSelection} 双向绑定 {@link props.selectedRowKeys} 属性
   */
  const selectedRowKeys = computed(() => rowSelection.value?.selectedRowKeys ?? []);
  const selectedRows = ref<Array<RECORD_TYPE>>([]);
  watch(
    () => rowSelection.value?.selectedRows,
    (value) => (selectedRows.value = value ?? [])
  );
  watch(
    () => rowSelection.value?.selectedRowKeys,
    (keys) => {
      const keySet = new Set(keys);
      selectedRows.value = selectedRows.value.filter((row) => keySet.has(rowKey.value(row)));
    }
  );

  /**
   * 增加行的点击事件
   */
  const customRow: GetComponentProps<RECORD_TYPE> = (data) => ({
    onClick: (event: Event) => handleRowClick(rowKey.value(data), data, event)
  });

  const handleRowClick = (key: RECORD_KEY, data: RECORD_TYPE, event: Event) => {
    if (ignoreClickEvent(event)) return;

    const selected = !selectedRowKeys.value?.includes(key);

    const { rowKeys, rows } = appendRows(
      selectedRowKeys.value,
      selectedRows.value,
      disabledRowKeys.value,
      rowKey.value,
      [data],
      selected
    );

    selectedRows.value = rows;
    rowSelection.value?.onSelect?.(data, selected, rows, event);

    handleRowSelected(rowKeys, rows);
  };

  /**
   * 更新被勾选行的数据和 {@link props.selectedRowKeys}
   */
  const handleRowSelected = (
    rowKeys: Array<RECORD_KEY>,
    rows: Array<TableHelper.RecordType>
  ): void => {
    rowSelection?.value?.onChange?.(toRaw(rowKeys), toRaw(rows));
  };

  return {
    customRow: customRow
  };
}

export function appendRows(
  _originSelectedRowKeys: Array<RECORD_KEY>,
  _originSelectedRows: Array<RECORD_TYPE>,
  _disabledRowKeys: Array<RECORD_KEY>,
  getRowKey: GetRowKey<RECORD_TYPE>,
  rows: Array<RECORD_TYPE>,
  selected: boolean
): { rowKeys: Array<RECORD_KEY>; rows: Array<RECORD_TYPE> } {
  const selectedKeys: Array<RECORD_KEY> = [..._originSelectedRowKeys];
  const selectedRows: Array<RECORD_TYPE> = [..._originSelectedRows];

  const selectedKeyIndexSet = new Set(selectedKeys);

  for (let i = 0; i < rows.length; i++) {
    const data = rows[i];
    const key = getRowKey(data);

    if (_disabledRowKeys.includes(key)) continue;

    /**
     * 如果该行已处于 {@link selected} 状态, 则跳过
     */
    if (selectedKeyIndexSet.has(key) === selected) continue;

    if (selected) {
      selectedKeys.push(key);
      selectedRows.push(data);
    } else {
      selectedKeys.splice(selectedKeys.indexOf(key), 1);
      selectedRows.splice(
        /**
         * 直接判断对象相等不可靠, 需要通过 {@link getRowKey} 获取对应的值进行比较.
         */
        selectedRows.findIndex((record) => getRowKey(record) === key),
        1
      );
    }
  }

  return {
    rowKeys: selectedKeys,
    rows: selectedRows
  };
}

/**
 * 判断是否需要忽略点击事件.
 */
// [可交互元素列表](https://www.w3.org/TR/2011/WD-html5-20110525/content-models.html#interactive-content)
const INTERACTION_ELEMENTS = [
  "A",
  "AUDIO",
  "BUTTON",
  "DETAIL",
  "EMBED",
  "IFRAME",
  "IMG",
  "INPUT",
  "KEYGEN",
  "LABEL",
  "MENU",
  "OBJECT",
  "SELECT",
  "TEXTAREA",
  "VIDEO"
];
// 忽略的标签名
const IGNORE_TAG_NAMES = [...INTERACTION_ELEMENTS];
// 搜索到则立即返回的元素标签名
const STOP_TAG_NAMES = ["TD", "TR", "TBODY", "TABLE"];
export function ignoreClickEvent(event: Event): boolean {
  const path = event.composedPath();

  for (let i = 0; i < path.length; i++) {
    const parent = path[i] as HTMLElement;
    const tagName = parent.tagName;

    /**
     * 1. 如果标签名为空, 则忽略
     * 2. 如果标签名在忽略列表中, 则忽略
     * 3. 如果标签名在停止列表中, 则停止
     */
    if (!isString(tagName) || tagName.length <= 0) return true;
    else if (IGNORE_TAG_NAMES.includes(tagName)) return true;
    else if (STOP_TAG_NAMES.includes(tagName)) return false;
  }

  return false;
}
