import type { Ref } from "vue";
import type { TableProps } from "ant-design-vue";
import { computed } from "vue";
import type { NTableProps } from "./table-types";
import { isBoolean, merge } from "lodash-es";
import type { TableHelper } from "../table-helper";
import type { ColumnType, Key } from "ant-design-vue/lib/table/interface";
import type { GetRowKey } from "ant-design-vue/lib/vc-table/interface";

export function useShowNumber(
  pagination: Ref<TableProps["pagination"]>,
  showNO: Ref<NTableProps["showNumberColumn"]>,
  columns: Ref<Required<NTableProps>["columns"]>,
  rowKey: Ref<GetRowKey<TableHelper.RecordType>>,
  childrenColumnName: Ref<NTableProps["childrenColumnName"]>
) {
  const currentPagination = computed(() => {
    const _pagination = pagination.value;
    if (isBoolean(_pagination)) return { current: 1, size: 0 };

    return { current: _pagination?.current ?? 1, size: _pagination?.pageSize ?? 0 };
  });

  const defaultShowNO = { title: "序号", startIndex: 1, width: 80, ellipsis: true };

  const childrenIdSet = new Set<Key>();
  // 序号列
  const numberColumn = computed(() => {
    const { current, size } = currentPagination.value;
    const { title, startIndex, ...restColumnProps } =
      showNO.value === true ? defaultShowNO : merge({ ...defaultShowNO }, showNO.value);

    return {
      fixed: columns.value.length > 0 ? columns.value[0].fixed : false,
      ...restColumnProps,
      title,
      dataIndex: "N_TABLE_NUMBER_COLUMN",
      customCell: () => ({ style: "text-overflow: clip" }),
      customRender: ({
        record,
        renderIndex
      }: {
        renderIndex: number;
        record: TableHelper.RecordType;
      }) => {
        const children = (record[childrenColumnName.value] as Array<TableHelper.RecordType>) ?? [];

        /**
         * 因为总是先渲染父节点. 当子节点渲染时 childrenIdSet 中已存在子节点的id.
         */
        // 子节点不显示序号
        if (childrenIdSet.has(rowKey.value(record))) return "";

        // 将该节点中所有子节点的id加入到 childrenIdSet 中
        for (let i = children.length; i--; )
          childrenIdSet.add(children[i][rowKey.value(children[i])]);

        return ((current - 1) * size + renderIndex + startIndex).toString(10);
      }
    } as ColumnType<TableHelper.RecordType>;
  });

  return {
    numberColumn
  };
}
