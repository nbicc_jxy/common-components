import type { TableProps } from "ant-design-vue";
import type { Ref } from "vue";
import { computed } from "vue";
import { isFunction, isString } from "lodash-es";
import type { TableHelper } from "../table-helper";
import type { NTableProps } from "./table-types";

export function useStripe(
  striped: Ref<NTableProps["striped"]>,
  rowClassName: Ref<TableProps["rowClassName"]>
) {
  const _rowClassName = computed<TableProps["rowClassName"]>(() => {
    const _rowClassName = rowClassName.value;
    const _striped = striped.value;

    return (record: TableHelper.RecordType, index: number, indent: number) => {
      let className = "";

      if (isString(_rowClassName)) className += _rowClassName;
      else if (isFunction(_rowClassName)) className += _rowClassName(record, index, indent);

      if (_striped && index % 2 === 1) className += " n-table-row--odd";

      return className;
    };
  });

  return {
    rowClassName: _rowClassName
  };
}
