import type { TableHelper } from "../table-helper";
import { isNil } from "lodash-es";
import type { GetRowKey } from "ant-design-vue/lib/vc-table/interface";

/**
 * 比较两条记录是否相等, 通过 rowKey 指定比较的字段.
 * @param record1
 * @param record2
 * @param rowKey
 */
export function isEqualRecord(
  record1?: TableHelper.RecordType,
  record2?: TableHelper.RecordType,
  rowKey?: GetRowKey<TableHelper.RecordType>
): boolean {
  const record1Key = record1 ? rowKey?.(record1) : undefined;
  const record2Key = record2 ? rowKey?.(record2) : undefined;

  if (isNil(record1Key) || isNil(record2Key)) return false;
  else return record1Key === record2Key;
}
