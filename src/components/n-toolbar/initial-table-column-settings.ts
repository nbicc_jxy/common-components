import type { NTableColumn } from "../n-table";
import type { DataIndex, FixedType } from "ant-design-vue/lib/vc-table/interface";
import { isBoolean, isNil, isNumber, isString } from "lodash-es";
import { joinDataIndex } from "../utils";
import type { SimpleStorage } from "../../storage";

/**
 * 初始化表格列设置, 从 {@link storage} 中读取存储在本地的列设置, 并将其应用到 {@link tableColumns} 中.
 * @param storage
 * @param tableColumns
 */
export function initialTableColumnSettings(
  storage: SimpleStorage,
  tableColumns: Array<NTableColumn>
) {
  const widthStorage = storage.derive("columnsWidth");
  const fixedStorage = storage.derive("columnsFixed");
  const visibleStorage = storage.derive("columnsVisible");
  const orderStorage = storage.derive("columnsOrder");

  for (let i = tableColumns.length; i--; ) {
    const col = tableColumns[i];

    // @ts-ignore
    const children: Array<NTableColumn> = col.children;
    // @ts-ignore
    const dataIndex: DataIndex = col.dataIndex;

    if (children) {
      initialTableColumnSettings(storage, children);
    } else if (dataIndex) {
      const dataIndexStr = joinDataIndex(dataIndex);
      const width = widthStorage.get(dataIndexStr) as number | undefined;
      const fixed = fixedStorage.get(dataIndexStr) as FixedType | undefined;
      const visible = visibleStorage.get(dataIndexStr) as boolean | undefined;
      const order = orderStorage.get(dataIndexStr) as number | undefined;

      // 如果本地存储中没有该列的设置, 则将当前列的设置存储到本地
      if (isNil(width) && isNumber(col.width)) widthStorage.set(dataIndexStr, col.width);
      if (isNil(fixed) && isString(col.fixed)) fixedStorage.set(dataIndexStr, col.fixed);
      if (isNil(visible) && isBoolean(col.visible)) visibleStorage.set(dataIndexStr, col.visible);
      if (isNil(order) && isNumber(col.order)) orderStorage.set(dataIndexStr, col.order);

      col.width = width ?? col.width;
      col.fixed = fixed ?? col.fixed;
      col.visible = visible ?? col.visible;
      col.order = order ?? col.order;
    }
  }
}
