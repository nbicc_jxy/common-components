import type { ExtractPropTypes, PropType, VNode } from "vue";
import type { NTableColumn } from "../n-table";
import { storableProps } from "../../storage";

export const nToolbarProps = () =>
  ({
    ...storableProps(),
    /**
     * 表格列数组, 用于控制显示隐藏. 通过 `update:tableColumns` 事件更新.
     * 需要使用 v-model:tableColumns 双向绑定生效.
     */
    tableColumns: Array as PropType<Array<NTableColumn>>,
    /**
     * 不可编辑的表格列 key 的数组.
     */
    disabledColumnKeys: {
      type: Array as PropType<Array<string>>,
      default: () => []
    },
    /**
     * 传入一个导出按钮被点击时的回调函数并显示导出按钮.  默认不显示导出按钮.
     */
    exportAsFile: Function as PropType<() => void>,
    contextSlots: {
      type: Object as PropType<ContextSlots>,
      default: () => ({})
    }
  } as const);
export type ContextSlots = {
  /**
   * 默认插槽, 主要区域. 用于放置主要的工具按钮.
   */
  default: () => VNode;
  /**
   * 次要区域, 用于放置次要的工具按钮.
   */
  secondary: () => VNode;
  /**
   * 通用区域, 用于放置通用的工具按钮.
   * @param toolsNode
   */
  common: (props: { toolsNode: Array<VNode> }) => VNode;
};

export type NToolbarProps = ExtractPropTypes<ReturnType<typeof nToolbarProps>>;

export interface NToolbarEmits {
  "update:tableColumns": (value: Array<NTableColumn>) => void;
}
