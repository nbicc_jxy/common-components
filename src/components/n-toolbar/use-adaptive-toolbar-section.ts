import type { Ref } from "vue";
import { onMounted, reactive, ref } from "vue";
import { elementContentOverflow } from "../utils/element-content-overflow";

export function useAdaptiveToolbarSection(containerRef: Ref<HTMLElement | undefined>) {
  const collapseSection = reactive({
    primary: false,
    secondary: false
  });

  const primarySectionElementRef = ref() as Ref<HTMLElement | null>;
  const secondarySectionElementRef = ref() as Ref<HTMLElement | null>;

  onMounted(() => {
    if (!containerRef.value) {
      console.warn("containerRef is undefined");
      return;
    }

    primarySectionElementRef.value = containerRef.value.querySelector(
      ".n-toolbar__primary-section"
    );
    secondarySectionElementRef.value = containerRef.value.querySelector(
      ".n-toolbar__secondary-section"
    );

    if (!primarySectionElementRef.value || !secondarySectionElementRef.value) {
      return;
    }

    elementContentOverflow.watch(
      primarySectionElementRef.value,
      (isOverflow) => (collapseSection.primary = isOverflow)
    );
    elementContentOverflow.watch(
      secondarySectionElementRef.value,
      (isOverflow) => (collapseSection.secondary = isOverflow)
    );

    // 只能计算一次, 否则会导致无限循环.
    elementContentOverflow.unwatch(primarySectionElementRef.value);
    elementContentOverflow.unwatch(secondarySectionElementRef.value);
  });

  return {
    collapseSection
  };
}
