import { expect, test, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { NUpload } from "../";
import { appendFileToInputElement } from "./append-file-to-input-element";
import { readFileSync } from "fs";
import { join } from "path";

test("NUpload customRequest 为函数时正确触发", async () => {
  const customRequest = vi.fn();
  const wrapper = mount(NUpload, {
    props: {
      action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
      customRequest
    }
  });

  const input = wrapper.find("input[type=file]");

  appendFileToInputElement(new File(["foo"], "foo.txt"), input.element as HTMLInputElement);

  await input.trigger("change");
  await new Promise((resolve) => setTimeout(resolve, 0));

  expect(customRequest).toBeCalled();
});

test("NUpload customRequest 为 undefined 时正确触发", async () => {
  const wrapper = mount(NUpload, {
    props: {
      action: "https://www.mocky.io/v2/5cc8019d300000980a055e76"
    }
  });

  const input = wrapper.find("input[type=file]");

  appendFileToInputElement(new File(["foo"], "foo.txt"), input.element as HTMLInputElement);

  await input.trigger("change");
  await new Promise((resolve) => setTimeout(resolve, 0));

  expect(wrapper.emitted("change")).toBeTruthy();
});

/**
 * 由于 jsdom 的限制，不支持 URL.createObjectURL 方法, 所以无法测试.
 */
test.skip("NUpload limit 为 { width: 100, height: 100 } 时正确触发 beforeUpload", async () => {
  const wrapper = mount(NUpload, {
    props: {
      action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
      limit: {
        width: 100,
        height: 100
      }
    }
  });

  const input = wrapper.find("input[type=file]");

  appendFileToInputElement(
    new File([readFileSync(join(__dirname, "./images/1920x1080.jpg"))], "1920x1080.jpg", {
      type: "image/jpeg"
    }),
    input.element as HTMLInputElement
  );

  await input.trigger("change");
  await new Promise((resolve) => setTimeout(resolve, 0));

  expect(wrapper.emitted("error")).toBeTruthy();
  expect(wrapper.emitted<Array<Error>>("error")![0][0]?.message).toBe(
    "图片宽度不能超过 100px，图片高度不能超过 100px"
  );
});

test("NUpload limit 为 { size: 1024 } 时正确触发 beforeUpload", async () => {
  const wrapper = mount(NUpload, {
    props: {
      action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
      limit: {
        size: 1024
      }
    }
  });

  const input = wrapper.find("input[type=file]");

  appendFileToInputElement(
    new File(["foo".repeat(1024)], "foo.txt"),
    input.element as HTMLInputElement
  );

  await input.trigger("change");
  await new Promise((resolve) => setTimeout(resolve, 0));

  expect(wrapper.emitted("error")).toBeTruthy();
  expect(wrapper.emitted<Array<Error>>("error")![0][0]?.message).toBe("图片大小超出限制");
});
