import { JSDOM } from "jsdom";

const { FileList } = new JSDOM().window;

export function appendFileToInputElement(file: File, inputElement: HTMLInputElement): void {
  const fileList = [...(inputElement.files ?? []), file];

  // @ts-ignore
  fileList.__proto__ = Object.create(FileList.prototype);

  Object.defineProperty(inputElement, "files", {
    value: fileList,
    writable: false
  });
}
