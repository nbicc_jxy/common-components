import type { PropType } from "vue";
import type { UploadProps } from "ant-design-vue";
import { storableProps } from "../../storage";
import type { ExtendProps } from "../common/types";

export const nUploadProps = () =>
  ({
    ...storableProps(),
    limit: {
      type: Object as PropType<NUploadLimit>,
      default: () => ({})
    },
    customRequest: {
      type: Function as PropType<UploadProps["customRequest"]>
    }
  } as const);

export type NUploadProps = ExtendProps<ReturnType<typeof nUploadProps>, UploadProps>;

/**
 * 上传文件限制.
 */
export interface NUploadLimit {
  /**
   * 当上传的文件类型是图片时, 限制图片的宽度.
   */
  width?: number;
  /**
   * 当上传的文件类型是图片时, 限制图片的高度.
   */
  height?: number;
  /**
   * 限制上传文件的大小. 单位: 字节.
   *
   * @example 1024 * 1024 * 10 表示 10MB.
   */
  size?: number;
}

export const nUploadPictureCardProps = () => ({
  fileList: {
    type: Array as PropType<UploadProps["fileList"]>
  }
});

export type NUploadPictureCardProps = ExtendProps<
  ReturnType<typeof nUploadPictureCardProps>,
  NUploadProps
>;
