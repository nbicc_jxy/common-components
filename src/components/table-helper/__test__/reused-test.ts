import { beforeEach, describe, expect, test, vi } from "vitest";
import type { TableHelper } from "../table-helper";

export function commonTest(
  createTableHelper: (
    ...args: ConstructorParameters<typeof TableHelper.TableHelper>
  ) => TableHelper.TableHelper
) {
  const mockedLoadRecord = vi.fn();
  const rowKeyName = "id";

  let tableHelper: TableHelper.TableHelper;
  beforeEach(() => {
    mockedLoadRecord.mockRestore();
    tableHelper = createTableHelper(mockedLoadRecord, rowKeyName);
  });

  describe("加载最后一页的异常情况", () => {
    test("当前页被删除时, 再次加载并请求最后一页", async () => {
      // 初始化
      mockedLoadRecord.mockResolvedValueOnce({ data: [], total: 100 });
      await tableHelper.loadRecord().wait();
      expect(mockedLoadRecord).toHaveBeenCalledTimes(1);

      // 跳到最后一页
      mockedLoadRecord.mockResolvedValueOnce({ data: [], total: 100 });
      await tableHelper.lastPage().wait();
      expect(mockedLoadRecord).toHaveBeenCalledTimes(2);
      expect(tableHelper.paginationParams.current).toBe(10);

      // 模拟最后一页被删除
      mockedLoadRecord
        // 发现最后一页被删除
        .mockResolvedValueOnce({ data: [], total: 90 })
        // 重新请求最后一页
        .mockResolvedValueOnce({ data: [], total: 90 });
      await tableHelper.reloadPage().wait();
      expect(mockedLoadRecord).toHaveBeenCalledTimes(4);
      expect(tableHelper.paginationParams.current).toBe(9);
    });

    test("total 为 0 时不会再次请求最后一页", async () => {
      // 初始化
      mockedLoadRecord.mockResolvedValueOnce({ data: [], total: 0 });
      await tableHelper.loadRecord().wait();
      expect(mockedLoadRecord).toHaveBeenCalledTimes(1);
    });
  });

  describe("加载开始/结束的事件正常触发", () => {
    const mockedRecordLoadStartCallback = vi.fn();
    const mockedRecordLoadEndCallback = vi.fn();

    beforeEach(() => {
      mockedRecordLoadStartCallback.mockRestore();
      mockedRecordLoadEndCallback.mockRestore();
    });

    test("正常触发事件", async () => {
      // 初始化
      mockedLoadRecord.mockResolvedValueOnce({ data: [], total: 100 });
      tableHelper.loadRecord();
      tableHelper.on("recordLoadStart", mockedRecordLoadStartCallback);
      tableHelper.on("recordLoadEnd", mockedRecordLoadEndCallback);
      await tableHelper.wait();
      expect(mockedLoadRecord).toHaveBeenCalledTimes(1);
      expect(mockedRecordLoadStartCallback).toHaveBeenCalledTimes(1);
      expect(mockedRecordLoadEndCallback).toHaveBeenCalledTimes(1);
    });

    test("加载失败时抛出异常并重置状态", async () => {
      const mockedRecordLoadErrorCallback = vi.fn();

      // 初始化
      mockedLoadRecord.mockResolvedValueOnce({ data: null, total: null });
      tableHelper.on("recordLoadStart", mockedRecordLoadStartCallback);
      tableHelper.on("recordLoadEnd", mockedRecordLoadEndCallback);
      tableHelper.on("recordLoadError", mockedRecordLoadErrorCallback);

      tableHelper.loadRecord(true);
      expect(tableHelper.loading).toBe(true);
      expect(tableHelper.processing).toBe(true);

      await expect(() => tableHelper.wait()).rejects.toThrowError();

      expect(mockedLoadRecord).toHaveBeenCalledTimes(1);
      expect(mockedRecordLoadStartCallback).toHaveBeenCalledTimes(1);
      expect(mockedRecordLoadEndCallback).toHaveBeenCalledTimes(0);
      expect(mockedRecordLoadErrorCallback).toHaveBeenCalledTimes(1);

      expect(tableHelper.loading).toBe(false);
      expect(tableHelper.processing).toBe(false);
    });
  });
}

export function presetTableHelper(
  createTableHelper: (
    ...args: ConstructorParameters<typeof TableHelper.TableHelper>
  ) => TableHelper.TableHelper
) {
  const result = {
    mockedLoadRecord: vi.fn(),
    rowKeyName: "id",
    tableHelper: undefined as any as TableHelper.TableHelper
  };

  beforeEach(() => {
    result.mockedLoadRecord.mockRestore();
    result.tableHelper = createTableHelper(result.mockedLoadRecord, result.rowKeyName);
  });

  return result;
}
