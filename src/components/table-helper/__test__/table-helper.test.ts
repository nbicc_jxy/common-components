import { TableHelper } from "../table-helper";
import { reactive, toRef, watch } from "vue";
import { commonTest, presetTableHelper } from "./reused-test";
import { describe, expect, test, vi } from "vitest";

describe("通用测试用例", () => {
  commonTest((...args) => new TableHelper.TableHelper(...args));
});

describe("使用 reactive 包裹时", () => {
  describe("通用测试用例", () => {
    commonTest(
      (...args) => reactive(new TableHelper.TableHelper(...args)) as TableHelper.TableHelper
    );
  });

  describe("测试 Proxy 对象的响应式", () => {
    const preset = presetTableHelper(
      (...args) => reactive(new TableHelper.TableHelper(...args)) as TableHelper.TableHelper
    );

    /**
     * @see git revision: f2dfdc73
     */
    describe("调用 loadRecord 不会丢失 this 指向", async () => {
      test("响应式属性正常更新", async () => {
        const { mockedLoadRecord, tableHelper } = preset;

        const loadingCallback = vi.fn();
        watch(toRef(tableHelper, "loading"), loadingCallback);

        const processingCallback = vi.fn();
        watch(toRef(tableHelper, "processing"), processingCallback);

        mockedLoadRecord.mockResolvedValueOnce({ data: [], total: 100 });
        await tableHelper.loadRecord().wait();
        await Promise.resolve();

        // 断言 loading
        expect(loadingCallback).toHaveBeenCalledTimes(2);
        expect(loadingCallback).toHaveBeenNthCalledWith(1, true, false, expect.any(Function));
        expect(loadingCallback).toHaveBeenNthCalledWith(2, false, true, expect.any(Function));

        // 断言 processing
        expect(processingCallback).toHaveBeenCalledTimes(2);
        expect(processingCallback).toHaveBeenNthCalledWith(1, true, false, expect.any(Function));
        expect(processingCallback).toHaveBeenNthCalledWith(2, false, true, expect.any(Function));
      });
    });
  });
});
