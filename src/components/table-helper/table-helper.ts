import { cloneDeep, debounce, has, isArray, isError, isNumber, isString } from "lodash-es";
import eventEmitter3 from "event-emitter3";
import type { EventEmitter as EventEmitterType } from "event-emitter3";

const { EventEmitter } = eventEmitter3;

export namespace TableHelper {
  export type RecordKey = string;
  export type RecordType = { [key in RecordKey]: any };

  /**
   * 加载表格数据的函数
   */
  export class TableHelper<
    RECORD_KEY extends RecordKey = RecordKey,
    RECORD_TYPE extends RecordType = RecordType
  > {
    /**
     * @param loadRecord 加载数据的函数
     * @param recordKeyName 指定数据 key 的属性名称
     * @param options 可选的额外参数
     * @param options.pageStartIndex 分页时起始页的页数. 默认第一页的页数为 `1`
     * @param options.pageSize 分页时每页数据的条数, 默认为 `10` 页
     */
    constructor(
      loadRecord: LoadTableRecordFunction<RECORD_TYPE>,
      recordKeyName: RECORD_KEY,
      options: Partial<TableHelperOptions> = {}
    ) {
      this.recordKeyName = recordKeyName;
      this.loading = false;
      this.processing = false;
      this.options = new TableHelperOptions(options);
      this._recordMap = new Map<RECORD_TYPE[RECORD_KEY], RECORD_TYPE>();
      this._recordLoadFunction = loadRecord;
      this.eventEmitter = new EventEmitter();

      this.paginationParams = {
        current: this.options.pageStartIndex,
        pageSize: this.options.pageSize
      };
      this.filterParams = {};
      this.sortParams = [];

      this.total = 0;
    }

    /**
     * 保存最近一次加载数据时, 变化的查询参数.
     *
     * 数据加载完成后将会清空.
     * @private
     */
    private changes: Partial<TableChangedParams> = {};

    /**
     * 记录的 key 的属性名称
     * @protected
     */
    protected readonly recordKeyName: RECORD_KEY;
    protected readonly _recordLoadFunction: LoadTableRecordFunction<RECORD_TYPE>;
    protected readonly _recordMap: Map<RECORD_TYPE[RECORD_KEY], RECORD_TYPE>;
    /**
     * 最近一次加载的数据
     * @protected
     */
    latestLoadRecord?: Array<RECORD_TYPE>;
    protected eventEmitter: EventEmitterType;

    /**
     * 相对于 {@link options.pageStartIndex} 的当前页数, 从 1 开始计数.
     * @protected
     */
    protected get currentPageNumber() {
      const { current } = this.paginationParams;
      const { pageStartIndex } = this.options;
      return current - pageStartIndex + 1;
    }

    readonly options: TableHelperOptions;

    paginationParams: TablePaginationParams;
    filterParams: TableFilterParams;
    sortParams: Array<TableSortParams>;

    /**
     * 指示当前是否正在加载数据的流程中
     */
    loading: boolean;
    /**
     * 指示当前是否正在任何流程中, 包括加载中以及加载前后.
     *
     * @see {@link _loadRecord}
     */
    processing: boolean;
    /**
     * 数据总条数
     */
    total: number;

    /**
     * 获取最新加载的数据
     */
    get records(): Array<RECORD_TYPE> {
      return this.latestLoadRecord ?? [];
    }

    /**
     * 根据 {@link paginationParams.pageSize} 和 {@link total} 计算出的总页数, 以便于分页时使用.
     *
     * **该值为绝对页数, 不会受到 {@link options.pageStartIndex} 的影响.**
     */
    get pageCount() {
      return Math.max(Math.ceil(this.total / this.paginationParams.pageSize), 0);
    }

    /**
     * 加载下一页的数据, 已到达最后一页时被忽略.
     */
    nextPage(): this {
      if (this.currentPageNumber >= this.pageCount) return this;

      this.paginationParams.current++;
      this.loadRecord();

      return this;
    }

    /**
     * 加载最后一页的数据, 如果当前已经是最后一页则被忽略.
     */
    lastPage(): this {
      if (this.currentPageNumber >= this.pageCount) return this;

      this.paginationParams.current = this._relativeToAbsolutePageNumber(this.pageCount);
      this.loadRecord();

      return this;
    }

    /**
     * 加载上一页的数据, 已到达第一页时被忽略.
     */
    previousPage(): this {
      if (this.paginationParams.current <= this.options.pageStartIndex) return this;

      this.paginationParams.current--;
      this.loadRecord();

      return this;
    }

    /**
     * 加载第一页的数据, 如果当前已经是第一页则被忽略.
     */
    firstPage(): this {
      if (this.paginationParams.current <= this.options.pageStartIndex) return this;

      this.paginationParams.current = this._relativeToAbsolutePageNumber(1);
      this.loadRecord();

      return this;
    }

    /**
     * 重新请求当前页的数据.
     */
    reloadPage(): this {
      this.loadRecord();
      return this;
    }

    /**
     * 跳转到指定页码. 页码默认是相对与 {@link pageStartIndex} 的, 可以设置 `relative` 为 `false` 使用绝对页码.
     * @param pageNumber
     * @param relative
     */
    skipToPage(pageNumber: number, relative = true): this {
      return this.updatePaginationParams({
        current: relative ? this._relativeToAbsolutePageNumber(pageNumber) : pageNumber
      });
    }

    /**
     * 等待记录加载完成.
     *
     * **注意:** 如果当前没有在加载数据, 会等待直到下一次加载数据完成.
     */
    wait(): Promise<void> {
      return this.processing
        ? new Promise<void>((resolve, reject) => {
            this.on("recordLoadEnd", () => resolve());
            this.on("recordLoadError", ({ error }) => reject(error));
          })
        : Promise.resolve();
    }

    /**
     * 设置过滤参数, 并重新加载数据.
     * @param params
     */
    updateFilterParams(params: TableFilterParams): this {
      this.filterParams = {
        ...this.filterParams,
        ...params
      };

      this.changes.filter = true;

      this.loadRecord();
      return this;
    }

    /**
     * 设置排序参数, 并重新加载数据.
     * @param paramsList 支持同时设置多个排序参数
     */
    addSortParams(...paramsList: Array<TableSortParams>): this {
      for (let i = paramsList.length; i--; ) {
        const sortParam = paramsList[i];
        if (this.sortParams.findIndex((item) => item.field === sortParam.field) !== -1) continue;

        this.sortParams.push(sortParam);
      }

      this.changes.sort = true;

      this.loadRecord();
      return this;
    }

    /**
     * 移除排序参数, 并重新加载数据.
     * @param args 传入 {@link TableSortParams} 或 {@link TableSortParams.field} 以删除对应的排序参数. 如传空则删除所有排序参数
     */
    removeSortParams(...args: Array<TableSortParams | string>): this {
      if (args.length <= 0) {
        this.sortParams = [];
      } else {
        for (let i = args.length; i--; ) {
          const item = args[i];
          const field = isString(item) ? item : item.field;
          const index = this.sortParams.findIndex((item) => item.field === field);

          if (index <= -1) continue;
          this.sortParams.splice(index, 1);
        }
      }

      this.changes.sort = true;

      this.loadRecord();
      return this;
    }

    /**
     * 设置分页参数, 并重新加载数据.
     * @param params.current 当前页数
     * @param params.pageSize 每页的数据条数
     */
    updatePaginationParams(params: Partial<TablePaginationParams>): this {
      if (isNumber(params.current) && this.paginationParams.current !== params.current)
        this.changes.current = true;

      if (isNumber(params.pageSize) && this.paginationParams.pageSize !== params.pageSize)
        this.changes.pageSize = true;

      this.paginationParams = {
        ...this.paginationParams,
        ...params
      };

      const { current } = this.paginationParams;
      const { pageStartIndex } = this.options;

      // 修正页码
      if (current < pageStartIndex) {
        this.paginationParams.current = pageStartIndex;
      } else if (this.currentPageNumber > this.pageCount) {
        this.paginationParams.current = this._relativeToAbsolutePageNumber(this.pageCount);
      }

      this.loadRecord();
      return this;
    }

    /**
     * 使用防抖函数包装的 {@link _loadRecord} 方法， 以便于在短时间内多次调用时只会执行一次. 默认的防抖时间为 `480ms`.
     * @protected
     *
     * **请不要用箭头函数来定义该方法, 否则会导致 `this` 指向错误.**
     *
     * 其中一个用例是, 当外部用 [reactive](https://vuejs.org/api/reactivity-core.html#reactive) 包裹时, 会导致 `this` 指向 {@link TableHepler} 本身, **而不是 Proxy 对象**.
     */
    protected debounceLoadRecord = debounce(this._loadRecord, 480);

    /**
     * 重新加载数据, 默认会有 480 ms 的防抖. 如需要立即加载, 将 `immediate` 设置为 `true`, 则会忽略防抖并立即加载数据.
     * @param immediate 是否立即加载数据
     */
    loadRecord(immediate = false): this {
      this._loadRecordProcessStart();

      if (immediate) {
        /**
         * why: 为什么不使用 debounce.flush 来立即执行函数?
         * https://github.com/lodash/lodash/issues/4185#issuecomment-464005668
         */
        this.debounceLoadRecord.cancel();
        this._loadRecord().catch((e) => console.error(e));
      } else {
        this.debounceLoadRecord()?.then();
      }

      return this;
    }

    destroy(): void {
      this._recordMap.clear();
      this.debounceLoadRecord.cancel();
    }

    on<K extends keyof TableHelperEventMap>(
      type: K,
      listener: (event: TableHelperEventMap[K]) => any
    ): void {
      this.eventEmitter.on(type, listener);
    }

    off<K extends keyof TableHelperEventMap>(
      type: K,
      listener: (event: TableHelperEventMap[K]) => any
    ): void {
      this.eventEmitter.off(type, listener);
    }

    once<K extends keyof TableHelperEventMap>(
      type: K,
      listener: (event: TableHelperEventMap[K]) => any
    ): void {
      this.eventEmitter.once(type, listener);
    }

    protected emit<K extends keyof TableHelperEventMap>(
      type: K,
      event: TableHelperEventMap[K]
    ): void {
      this.eventEmitter.emit(type, event);
    }

    protected async _loadRecord(): Promise<void> {
      const changes = this.changes;
      this.changes = {};

      try {
        // 前置检查
        const returnedError = this._onBeforeLoadRecord(cloneDeep(changes));
        if (isError(returnedError)) {
          console.warn(`[TableHelper] 前置检查失败: ${returnedError}`);
          return;
        }

        // 触发开始加载事件
        this.emit("recordLoadStart", { changes: cloneDeep(changes) });

        this.loading = true;

        // 开始加载记录
        await this._internalLoadRecord();

        this.loading = false;

        // 触发结束加载事件
        this.emit("recordLoadEnd", { changes: cloneDeep(changes) });

        this._onAfterLoadRecord(cloneDeep(changes));
      } catch (e) {
        const error = e as Error;

        // 重置状态, 并触发加载失败事件
        this.loading = false;
        this.emit("recordLoadError", { changes, error });

        // 由返回值决定是否抛出异常
        const returnedError = this._onAfterLoadRecord(changes, error);
        if (isError(returnedError)) throw returnedError;
      } finally {
        this._loadRecordProcessFinally();
      }
    }

    protected async _internalLoadRecord(): Promise<void> {
      const { data, total } = await this._invokeLoadRecordFunction();
      const keyName = this.recordKeyName;

      for (let i = data.length; i--; ) {
        const record = data[i];

        this._recordMap.set(record[keyName], record);
      }

      this.latestLoadRecord = data;
      this.total = total;
    }

    protected async _invokeLoadRecordFunction(): Promise<
      LoadTableRecordFunctionResult<RECORD_TYPE>
    > {
      const recordResult = await this._recordLoadFunction(...this._joinLoadRecordArguments());
      let data = has(recordResult, "data") ? recordResult.data : [];
      if (isArray(data) === false) throw new Error(`invalid data type. ${typeof data}`);
      let total = has(recordResult, "total") ? recordResult.total : 0;
      if (isNumber(total) === false) throw new Error(`invalid total type. ${typeof total}`);

      this.total = total;

      // 正常情况下, 直接返回结果.
      if (this.currentPageNumber <= this.pageCount) {
        return { data, total };
      }
      // 如果记录条数为 0, 则重置当前页数为设置的起始页数.
      else if (this.total <= 0) {
        this.paginationParams.current = this.options.pageStartIndex;
        return { data, total };
      }

      /**
       * 以下情况需要重新请求记录:
       * 1. 当前页数为最后一页但请求前后的最大页数不一致(可能是记录被删除了). 需要重新请求最后一页.
       * 2. 当前页数不是最后一页但记录条数为 0 (可能是请求页数的记录都被删除了). 需要请求最新的最后一页.
       */
      this.paginationParams.current = this._relativeToAbsolutePageNumber(this.pageCount);
      const result = await this._recordLoadFunction(...this._joinLoadRecordArguments());

      data = result.data;
      total = result.total;

      return { data, total };
    }

    /**
     * 判断当前是否已处于最后一页.
     *
     * 特殊情况:
     * * 只有成功加载过记录后, 才会判断是否为最后一页.
     * @protected
     */
    protected _isLastPage(): boolean {
      // 未加载过记录, 不会是最后一页.
      if (this._recordMap.size <= 0) return false;

      const deltaPageSize = this.currentPageNumber;
      const totalPageSize = this.pageCount;

      return deltaPageSize >= totalPageSize;
    }

    protected _joinLoadRecordArguments(): [
      TablePaginationParams,
      TableFilterParams,
      TableSortParams[],
      ...Array<any>
    ] {
      return [this.paginationParams, this.filterParams, this.sortParams];
    }

    /**
     * 将相对页码(即从 {@link options.pageStartIndex} 开始的页码)转换为绝对页码.
     * @param page
     * @protected
     */
    protected _relativeToAbsolutePageNumber(page: number): number {
      // 相对页码不会小于 1.
      page = page < 1 ? 1 : page;

      return page + this.options.pageStartIndex - 1;
    }

    /**
     * 加载记录前的回调. 是否继续进行流程取决于该函数是否返回了一个 error.
     *
     * * 如果返回 error, 则将 {@link loading} 状态设置为 false 并中止流程.
     * * 如果返回值为空继续加载流程.
     * @param changes 发生变化的参数
     * @protected
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    protected _onBeforeLoadRecord(changes: Partial<TableChangedParams>): Error | undefined {
      // 如果已经是 loading 状态, 不允许再次加载.
      return this.loading ? new Error("abort load record.") : undefined;
    }

    /**
     * 加载记录后的回调. 加载完成后的 {@link loading} 状态依赖于该函数是否返回了一个 error.
     *
     * * 如果返回 error, 则将 {@link loading} 状态设置为 false.
     * * 如果返回值为空, 则认为 `error` 是预期的, {@link loading} 继续保持当前状态.
     * @param changes 发生变化的参数
     * @param error 加载失败时的错误信息
     * @protected
     */
    protected _onAfterLoadRecord(
      changes: Partial<TableChangedParams>,
      error?: Error
    ): Error | undefined {
      // 默认情况下没有 error 是预期行为. 任何 error 都会被作为异常抛出.
      return error;
    }

    /**
     * 加载流程开始前的回调.
     */
    protected _loadRecordProcessStart(): void {
      this.processing = true;
    }

    /**
     * 加载流程结束后的回调. 无论加载成功或失败都会执行该函数.
     * 应在记录加载完后且触发回调(recordLoadEnd, recordLoadError)前调用.
     */
    protected _loadRecordProcessFinally(): void {
      this.processing = false;
    }
  }

  export class TableHelperOptions {
    /**
     * 分页起始页码, 默认为 1
     */
    pageStartIndex: number;
    /**
     * 每页的数据条数, 默认为 10
     */
    pageSize: number;

    constructor(params: Partial<TableHelperOptions> = {}) {
      this.pageStartIndex = isNumber(params.pageStartIndex) ? params.pageStartIndex : 1;
      this.pageSize = isNumber(params.pageSize) ? params.pageSize : 10;
    }
  }

  /**
   * 分页查询参数, 用于记录分页信息.
   */
  export interface TablePaginationParams {
    /**
     * 当前页数
     */
    current: number;
    /**
     * 每页的数据条数
     */
    pageSize: number;
  }

  /**
   * 过滤查询参数, 用于记录搜索条件.
   */
  export type TableFilterParams = Record<string, unknown>;

  /**
   * 排序参数, 用于记录排序条件.
   */
  export interface TableSortParams {
    /**
     * 排序方式, `ascend` 为升序, `descend` 为降序, `undefined` 为不排序
     */
    direction: "ascend" | "descend" | undefined | null;
    /**
     * 排序字段
     */
    field: string;
  }

  /**
   * 加载记录的函数返回值.
   */
  export interface LoadTableRecordFunctionResult<TYPE> {
    /**
     * 加载到的记录
     */
    data: Array<TYPE>;
    /**
     * 记录总数
     */
    total: number;
  }

  /**
   * 加载记录的函数, 用于 {@link TableHelper} 的构造函数.
   * @param pagination 分页参数
   * @param filter 过滤参数
   * @param sort 排序参数
   *
   * @returns 返回值为 {@link LoadTableRecordFunctionResult}
   */
  export type LoadTableRecordFunction<RECORD_TYPE> = (
    pagination: TablePaginationParams,
    filter: TableFilterParams,
    sort: Array<TableSortParams>,
    ...args: Array<any>
  ) =>
    | LoadTableRecordFunctionResult<RECORD_TYPE>
    | Promise<LoadTableRecordFunctionResult<RECORD_TYPE>>;

  /**
   * 表格变更参数, 用于标记表格的哪些参数发生了变化, 用于 {@link TableHelperEventMap.recordLoadStart} 和 {@link TableHelperEventMap.recordLoadEnd} 的回调函数.
   */
  export interface TableChangedParams {
    current: boolean;
    pageSize: boolean;
    filter: boolean;
    sort: boolean;
  }

  /**
   * {@link TableHelper} 发出的事件.
   */
  export interface TableHelperEventMap {
    /**
     * 记录加载开始时触发.
     */
    recordLoadStart: { changes: Partial<TableChangedParams> };
    /**
     * 记录加载完成时触发.
     */
    recordLoadEnd: { changes: Partial<TableChangedParams> };
    recordLoadError: { changes: Partial<TableChangedParams>; error: Error };
  }
}
