export async function downloadFile(blobOrUrl: Blob | string, filename?: string): Promise<void> {
  const a = document.createElement("a");

  const url = typeof blobOrUrl === "string" ? blobOrUrl : URL.createObjectURL(blobOrUrl);

  a.href = url;
  a.download = filename || "";

  a.click();

  /**
   * https://stackoverflow.com/questions/37240551/how-can-i-revoke-an-object-url-only-after-its-downloaded
   */
  setTimeout(() => {
    URL.revokeObjectURL(url);
  }, 200);
}
