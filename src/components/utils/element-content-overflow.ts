import { ref, watch } from "vue";
import type { Ref, WatchStopHandle } from "vue";
import { elementRect } from "./reactive-dom-rect";

export const elementContentOverflow = {
  watch: watchElementContentOverflow,
  unwatch: unwatchElementContentOverflow
};

function watchElementContentOverflow(
  element: Element,
  callback?: (isOverflow: boolean) => void
): Ref<boolean> {
  if (resultMap.has(element)) return resultMap.get(element)!;

  const isOverflow = ref(false);
  const elementRectRef = elementRect.watch(element);

  const stopHandle = watch(
    elementRectRef,
    () => {
      isOverflow.value = element.clientWidth < element.scrollWidth;
      callback?.(isOverflow.value);
    },
    { immediate: true }
  );

  unwatchMap.set(element, stopHandle);
  resultMap.set(element, isOverflow);

  return isOverflow;
}

function unwatchElementContentOverflow(element: Element) {
  // 停止元素大小变化的监听
  elementRect.unwatch(element);

  // 停止 watch 函数并删除
  const unwatch = unwatchMap.get(element);
  if (unwatch) unwatch();
  unwatchMap.delete(element);

  // 删除存储的结果
  resultMap.delete(element);
}

const unwatchMap = new WeakMap<Element, WatchStopHandle>();
const resultMap = new WeakMap<Element, Ref<boolean>>();
