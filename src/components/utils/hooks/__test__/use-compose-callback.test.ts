import { expect, describe, test, vi } from "vitest";
import { composeCallback } from "../use-compose-callback";

describe("composeCallback", () => {
  test("应该返回一个函数, 该函数会依次调用传入的所有回调函数, 并将它们的返回值合并后作为自己的返回值.", () => {
    const callback1 = vi.fn().mockReturnValue({ foo: "bar" });
    const callback2 = vi.fn().mockReturnValue({ baz: "qux" });
    const callback3 = vi.fn().mockReturnValue({ quux: "corge" });

    const composedCallback = composeCallback(callback1, callback2, callback3);

    const result = composedCallback();

    expect(callback1).toHaveBeenCalled();
    expect(callback2).toHaveBeenCalled();
    expect(callback3).toHaveBeenCalled();
    expect(result).toEqual({ foo: "bar", baz: "qux", quux: "corge" });
  });

  test("如果回调函数的返回值中包含函数, 则应该将这些函数合并到一个函数中.", () => {
    const callback1 = vi.fn().mockReturnValue({ foo: () => {} });
    const callback2 = vi.fn().mockReturnValue({ foo: () => {} });

    const composedCallback = composeCallback(callback1, callback2);

    const result = composedCallback();

    expect(result.foo).toBeInstanceOf(Function);
  });

  test("如果提供的回调函数为 undefined, 则应该跳过它们.", () => {
    const callback1 = vi.fn().mockReturnValue({ foo: "bar" });
    const callback2 = undefined;
    const callback3 = vi.fn().mockReturnValue({ baz: "qux" });

    const composedCallback = composeCallback(callback1, callback2, callback3);

    const result = composedCallback();

    expect(callback1).toHaveBeenCalled();
    expect(callback2).toBeUndefined();
    expect(callback3).toHaveBeenCalled();
    expect(result).toEqual({ foo: "bar", baz: "qux" });
  });
});

describe("composeCallback 异常情况", () => {
  test("如果没有提供回调函数，则 composeCallback 函数的返回值应该为 undefined。", () => {
    const composedCallback = composeCallback();

    const result = composedCallback();

    expect(result).toBeUndefined();
  });

  test("如果所有提供的回调函数都为 undefined，则 composeCallback 函数的返回值应该为 undefined。", () => {
    const composedCallback = composeCallback(undefined, undefined, undefined);

    const result = composedCallback();

    expect(result).toBeUndefined();
  });

  test("当回调函数返回值为空时，composeCallback 函数应该跳过该回调函数，并将其他回调函数的返回值合并后作为自己的返回值。", () => {
    const callback1 = vi.fn(() => undefined);
    const callback2 = vi.fn(() => ({ foo: "bar" }));
    const callback3 = vi.fn(() => ({ baz: "qux" }));

    const composedCallback = composeCallback<() => Record<string, any> | undefined>(
      callback1,
      callback2,
      callback3
    );

    const result = composedCallback();

    expect(result).toEqual({ foo: "bar", baz: "qux" });
    expect(callback1).toHaveBeenCalled();
    expect(callback2).toHaveBeenCalled();
    expect(callback3).toHaveBeenCalled();
  });
});
