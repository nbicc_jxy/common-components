import { assignWith, isFunction, isNil } from "lodash-es";
import { computed, reactive, unref } from "vue";
import type { Ref } from "vue";

type CallBackFunction = (...args: any[]) => Record<string, any> | undefined | void;
type Callback<T extends CallBackFunction> = T | Ref<T | undefined> | undefined;

export function useComposeCallback<T extends CallBackFunction>(
  ...callbacks: Array<[string, Callback<T>]>
) {
  const callbackMap: Map<string, Callback<T>> = reactive(new Map(callbacks));
  const composedCallback = computed(() =>
    composeCallback(...Array.from(callbackMap.values()).map((item) => unref(item)))
  );

  return {
    callbackMap,
    callback: composedCallback
  };
}

/**
 * 传入多个回调函数, 返回一个新的回调函数, 该回调函数会依次调用传入的回调函数并将回调函数的返回值合并后作为自己的返回值.
 */
export function composeCallback<T extends CallBackFunction>(...callbacks: Array<T | undefined>) {
  return (...args: Parameters<T>): ReturnType<T> => {
    const callbacksResult: Array<ReturnType<CallBackFunction>> = [];

    for (let i = 0; i < callbacks.length; i++) {
      const callback = callbacks[i];
      if (isNil(callback)) continue;

      const callbackResult = callback(...args);
      if (isNil(callbackResult)) continue;
      callbacksResult.push(callbackResult);
    }

    return callbacksResult.length > 0
      ? assignWith({}, ...callbacksResult, customizer)
      : (undefined as any);
  };
}
const customizer = (objValue: any, srcValue: any) => {
  if (isFunction(objValue) && isFunction(srcValue)) {
    return (...args: any[]) => {
      const objResult = objValue(...args);
      const srcResult = srcValue(...args);

      return assignWith({}, objResult, srcResult, customizer);
    };
  } else {
    return srcValue;
  }
};
