import { onMounted, ref } from "vue";

/**
 * 获取滚动条的宽度和高度
 */
export function useMeasureScrollbar() {
  const width = ref(0);
  const height = ref(0);

  onMounted(() => measureScrollbar());

  const measureScrollbar = () => {
    const scrollDiv = document.createElement("div");
    scrollDiv.style.position = "absolute";
    scrollDiv.style.top = "-9999px";
    scrollDiv.style.width = "50px";
    scrollDiv.style.height = "50px";
    scrollDiv.style.overflow = "scroll";

    document.body.appendChild(scrollDiv);

    width.value = scrollDiv.offsetWidth - scrollDiv.clientWidth;
    height.value = scrollDiv.offsetHeight - scrollDiv.clientHeight;
    document.body.removeChild(scrollDiv);
  };

  return { width, height };
}
