export const breakpoints = ["xs", "sm", "md", "lg", "xl", "xxl", "xxxl"] as const;
export type Breakpoint = (typeof breakpoints)[number];
