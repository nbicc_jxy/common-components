import { onMounted, ref } from "vue";
import type { Ref } from "vue";
import type { ScreenMap } from "./responsive-observe";
import ResponsiveObserve from "./responsive-observe";
import type { Breakpoint } from "./break-point";
import { breakpoints } from "./break-point";

export * from "./break-point";
/**
 * 获取当前的响应式断点. 该值会随着窗口大小的变化而变化.
 *
 * 使用基于 [BootStrap 4](https://getbootstrap.com/docs/4.0/layout/overview/#responsive-breakpoints) 定制的断点规则.
 * 详见 {@link ./responsive-observe.ts}
 */
export function useResponsive() {
  const currentBreakPoint = ref("xs") as Ref<Breakpoint>;

  onMounted(() => {
    const token = ResponsiveObserve.subscribe(
      (screen) => (currentBreakPoint.value = getMaximumBreakPoint(screen))
    );

    return () => ResponsiveObserve.unsubscribe(token);
  });

  return {
    breakPoint: currentBreakPoint
  };
}

export function getMaximumBreakPoint(screen: ScreenMap): Breakpoint {
  for (let i = breakpoints.length; i--; ) {
    const breakpoint = breakpoints[i];
    if (screen[breakpoint]) return breakpoint;
  }

  return "xs";
}
