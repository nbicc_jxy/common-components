import { computed, inject } from "vue";
import type {
  NSpecProviderEasyFormConfig,
  NSpecProviderFormConfig,
  NSpecProviderProps
} from "../../../n-spec-provider";
import { NSpecProvideKey } from "../../../n-spec-provider";
import { FormItemControlType } from "../../../form-helper";
import { merge } from "lodash-es";

const defaultFormConfig: NSpecProviderFormConfig = {
  labelWidth: "90px",
  labelAlign: "right",
  colon: true,
  allowClear: true,
  maxlength: 30,
  events: {},
  modifiers: {},
  placeholder: "",
  field: {
    [FormItemControlType.INPUT]: { placeholder: "请输入" },
    [FormItemControlType.INPUT_NUMBER]: { placeholder: "请输入" },
    [FormItemControlType.UPLOAD]: { placeholder: "点击上传" },
    [FormItemControlType.SELECT]: { placeholder: "请选择" },
    [FormItemControlType.AUTO_LOAD_SELECT]: { placeholder: "请选择" },
    [FormItemControlType.DATE_PICKER]: { placeholder: "请选择" },
    [FormItemControlType.DATE_RANGE_PICKER]: { placeholder: ["请选择", "请选择"] },
    [FormItemControlType.TIME_PICKER]: { placeholder: "请选择" },
    [FormItemControlType.TIME_RANGE_PICKER]: { placeholder: ["请选择", "请选择"] },
    [FormItemControlType.CASCADER]: { placeholder: "请选择" },
    [FormItemControlType.RADIO_GROUP]: { placeholder: "请选择" },
    [FormItemControlType.CHECKBOX_GROUP]: { placeholder: "请选择" },
    [FormItemControlType.SWITCH]: { placeholder: "请选择" },
    [FormItemControlType.SLIDER]: { placeholder: "请选择" },
    [FormItemControlType.TREE_SELECT]: { placeholder: "请选择" },
    [FormItemControlType.TEXT_AREA]: { placeholder: "请输入" },
    [FormItemControlType.CUSTOM]: { placeholder: "" }
  },
  fieldLayout: {}
};

export const defaultSpecProvider = {
  wrapModal: true,
  wrapDrawer: true,
  form: defaultFormConfig,
  easyForm: {
    ...defaultFormConfig,
    beforeSearch: () => true,
    defaultExpandRowCount: { lg: 1, xl: 2 },
    fieldLayout: {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 8,
      xl: 6,
      gutter: [16, 0]
    }
  } as NSpecProviderEasyFormConfig,
  upload: {
    accept: "",
    action: "",
    method: "POST",
    limit: {},
    customRequest: () => {}
  } as Required<Required<NSpecProviderProps>["upload"]>,
  table: {
    ellipsisWithTooltip: true
  }
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function useSpecInject<T>(name: string) {
  const specProvider = inject<NSpecProviderProps>(NSpecProvideKey, {});

  const form = computed(() => merge({}, defaultSpecProvider.form, specProvider.form));
  const easyForm = computed(() => merge({}, defaultSpecProvider.easyForm, specProvider.easyForm));
  const upload = computed(() => ({
    accept: specProvider.upload?.accept ?? defaultSpecProvider.upload.accept,
    action: specProvider.upload?.action ?? defaultSpecProvider.upload.action,
    method: specProvider.upload?.method ?? defaultSpecProvider.upload.method,
    limit: specProvider.upload?.limit ?? defaultSpecProvider.upload.limit,
    customRequest: specProvider.upload?.customRequest ?? defaultSpecProvider.upload.customRequest
  }));
  const table = computed(() => merge({}, defaultSpecProvider.table, specProvider.table));

  return {
    form,
    easyForm,
    upload,
    table
  };
}
