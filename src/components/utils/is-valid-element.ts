import { Fragment, Comment, Text } from "vue";

/**
 * @see https://github.com/vueComponent/ant-design-vue/blob/eae2b5d41268badbaf5ed257f65437b3cf459526/components/_util/props-util/index.js#L398
 * @param element
 */
export function isValidElement(element: any): boolean {
  if (Array.isArray(element) && element.length === 1) {
    element = element[0];
  }
  return element && element.__v_isVNode && typeof element.type !== "symbol"; // remove text node
}

/**
 * https://github.com/vueComponent/ant-design-vue/blob/eae2b5d41268badbaf5ed257f65437b3cf459526/components/_util/props-util/index.js#L342
 * @param element
 */
export function isEmptyElement(element: any): boolean {
  return (
    element &&
    (element.type === Comment ||
      (element.type === Fragment && element.children.length === 0) ||
      (element.type === Text && element.children.trim() === ""))
  );
}
