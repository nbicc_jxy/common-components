import { isArray } from "lodash-es";
import type { DataIndex } from "ant-design-vue/lib/vc-table/interface";

export function joinDataIndex(dataIndex: DataIndex): string {
  const _dataIndex = isArray(dataIndex) ? dataIndex : [dataIndex];

  return _dataIndex.join(".");
}
