import { type Ref, ref } from "vue";
import { isNil } from "lodash-es";

const factor = ref() as Ref<ReturnType<typeof getConversionFactor>>;

/**
 * 将 px 转换为物理单位
 * @param value
 * @param unit
 */
function xp2(value: number, unit: "inch" | "cm" | "mm" | "pt"): number {
  initial();

  const { x } = factor.value[unit];
  return value / x;
}

/**
 * 将 px 转换为 inch
 * @param value
 */
export function px2inch(value: number) {
  return xp2(value, "inch");
}

/**
 * 将 px 转换为 cm
 */
export function px2cm(value: number) {
  return xp2(value, "cm");
}

/**
 * 将 px 转换为 mm
 */
export function px2mm(value: number) {
  return xp2(value, "mm");
}

/**
 * 将 px 转换为 pt
 */
export function px2pt(value: number) {
  return xp2(value, "pt");
}

/**
 * 将物理单位转换为 px
 */
function x2px(value: number, unit: "inch" | "cm" | "mm" | "pt"): number {
  initial();

  const { x } = factor.value[unit];
  return value * x;
}

export function toPx(str: string) {
  let number = 0;
  let unit = "mm";
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57) continue;

    number = Number.parseFloat(str.slice(0, i));
    unit = str.slice(i);
    break;
  }

  return x2px(number, unit as any);
}

/**
 * 将 inch 转换为 px
 */
export function inch2px(value: number) {
  return x2px(value, "inch");
}

/**
 * 将 cm 转换为 px
 */
export function cm2px(value: number) {
  return x2px(value, "cm");
}

/**
 * 将 mm 转换为 px
 */
export function mm2px(value: number) {
  return x2px(value, "mm");
}

/**
 * 将 pt 转换为 px
 */
export function pt2px(value: number) {
  return x2px(value, "pt");
}

function initial() {
  if (!isNil(factor.value)) return;

  factor.value = getConversionFactor();
}

/**
 * 计算当前设备的物理单位转换因子
 */
function getConversionFactor() {
  const div = document.createElement("div");
  div.style.width = "1in";
  div.style.height = "1in";
  div.style.position = "absolute";
  div.style.left = "-100%";
  div.style.top = "-100%";
  document.body.appendChild(div);

  const x = div.offsetWidth;
  const y = div.offsetHeight;

  const pixelPerInch = { x, y };
  const pixelPerMillimeter = { x: x / 25.4, y: y / 25.4 };
  const pixelPerCentimeter = { x: x / 2.54, y: y / 2.54 };
  const pixelPerPoint = { x: x / 72, y: y / 72 };

  return {
    inch: pixelPerInch,
    cm: pixelPerCentimeter,
    mm: pixelPerMillimeter,
    pt: pixelPerPoint
  };
}
