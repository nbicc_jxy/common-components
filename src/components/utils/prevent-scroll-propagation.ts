export const preventScrollPropagation = {
  watch,
  unwatch
};

/**
 * 阻止滚轮事件向父级元素传播. 以防止滚动到顶部或底部时, 父级元素也滚动.
 * ant-design-vue 的 Select 组件下拉框默认插入在 body 下, 会导致滚动到顶部或底部时, body 也滚动(如果可以滚动的话).
 * 但一般情况下 body 元素不会滚动, 滚动传播的问题不易发现.
 * @see https://stackoverflow.com/a/16324762
 * @param element 要阻止滚轮事件向父级元素传播的元素
 * @returns 释放事件监听器
 */
function watch(element: HTMLElement) {
  element.addEventListener("wheel", handleWheel);

  return () => element.removeEventListener("wheel", handleWheel);
}

/**
 * 取消阻止滚轮事件向父级元素传播, 释放事件监听器.
 * @param element
 */
function unwatch(element: HTMLElement) {
  element.removeEventListener("wheel", handleWheel);
}

const handleWheel = (ev: WheelEvent) => {
  const element = ev.currentTarget as HTMLElement;
  const { scrollHeight, scrollTop, clientHeight } = element;

  if (ev.deltaY > 0 && scrollHeight - scrollTop - clientHeight <= 1) {
    element.scrollTop = scrollHeight;
    return prevent(ev);
  } else if (ev.deltaY < 0 && scrollTop <= 1) {
    element.scrollTop = 0;
    return prevent(ev);
  }
};

const prevent = (ev: WheelEvent) => {
  ev.stopPropagation();
  ev.preventDefault();
  ev.returnValue = false;
  return false;
};
