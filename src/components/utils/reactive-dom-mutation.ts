import { ref } from "vue";
import type { Ref } from "vue";
import { isNil } from "lodash-es";

export const elementMutation = elementMutationWithRoot();

export function elementMutationWithRoot() {
  // 创建一个映射以存储元素引用及其可见性状态
  const domList = new Map<Node, Ref<MutationRecord | undefined>>();
  /**
   * @see https://github.com/whatwg/dom/issues/126#issuecomment-1049814948
   */
  const observerTargets = new Map<Node, MutationObserverInit>();

  // 定义函数以初始化观察者
  let observer: MutationObserver;
  function initialObserver() {
    if (observer) return;

    observer = new MutationObserver((entries) => {
      for (let i = entries.length; i--; ) {
        const entry = entries[i];
        const { target } = entry;

        // 获取元素的可见性状态
        const ref = domList.get(target);
        if (isNil(ref)) continue;

        ref.value = entry;
      }
    });
  }

  /**
   * 监听元素的可变化
   * @param node 要监听的元素
   * @param options 选项
   */
  function watchElementMutation(
    node: Node,
    options: MutationObserverInit
  ): Ref<MutationRecord | undefined> {
    initialObserver();

    if (domList.has(node)) unwatchElementMutation(node);

    // 将元素及其可见性状态添加到映射中
    domList.set(node, ref(undefined));
    observerTargets.set(node, options);

    // 观察元素
    observer.observe(node, options);

    return domList.get(node)!;
  }

  /**
   * 停止监听元素的可变化
   * @param node 要停止监听的元素
   */
  function unwatchElementMutation(node: Node) {
    initialObserver();

    // 从映射中删除元素
    domList.delete(node);
    observerTargets.delete(node);

    // 停止观察元素
    observer.disconnect();
    observerTargets.forEach((options, node) => observer.observe(node, options));
  }

  /**
   * 停止监听所有元素的可变化
   */
  function unwatchAll() {
    domList.clear();
    observerTargets.clear();
    observer.disconnect();
  }

  /**
   * 销毁观察者
   */
  function destroy() {
    unwatchAll();
    observer = undefined as any;
  }

  return {
    watch: watchElementMutation,
    unwatch: unwatchElementMutation,
    unwatchAll,
    destroy
  };
}
