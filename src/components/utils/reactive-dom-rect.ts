import { ref } from "vue";
import type { Ref } from "vue";
import { isNil } from "lodash-es";

export const elementRect = {
  watch: watchElementReact,
  unwatch: unwatchElementReact
};

/**
 * 监听元素的尺寸变化
 * @param element 要监听的元素
 */
function watchElementReact(element: Element): Ref<DOMRectReadOnly> {
  initialObserver();

  domList.set(element, ref(element.getBoundingClientRect()));
  observer.observe(element);

  return domList.get(element)!;
}

/**
 * 取消监听元素的尺寸变化
 * @param element
 */
function unwatchElementReact(element: Element | undefined | null) {
  if (isNil(element)) return;

  initialObserver();

  observer.unobserve(element);
  domList.delete(element);
}

const domList = new Map<Element, Ref<DOMRectReadOnly>>();

// 兼容 ssr 环境
let observer: ResizeObserver;
function initialObserver() {
  if (observer) return;

  observer = new ResizeObserver((entries) => {
    for (let i = entries.length; i--; ) {
      const entry = entries[i];
      const { target, contentRect } = entry;

      const rect = domList.get(target);
      if (!rect) continue;

      rect.value = contentRect;
    }
  });
}
