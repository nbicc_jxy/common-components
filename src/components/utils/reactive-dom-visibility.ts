import { ref } from "vue";
import type { Ref } from "vue";
import { isNil, isNumber } from "lodash-es";

// 定义默认的阈值
const defaultThreshold = [0, 0.3, 0.5, 0.8, 1] as const;

/**
 * 观察 document.body 下子元素的可见性.
 */
export const elementVisibility = elementVisibilityWithRoot(document.body);

/**
 * 指定根元素并观察根元素下子元素的可见性
 * @param root 根元素
 * @param options 选项
 */
export function elementVisibilityWithRoot(root: Element, options?: IntersectionObserverInit) {
  // 创建一个映射以存储元素引用及其可见性状态
  const domList = new Map<Element, Ref<boolean>>();
  // 创建一个映射以存储元素引用及其阈值
  const thresholdMap = new Map<Element, number>();

  // 定义函数以初始化观察者
  let observer: IntersectionObserver;
  function initialObserver() {
    if (observer) return;

    observer = new IntersectionObserver(
      (entries) => {
        for (let i = entries.length; i--; ) {
          const entry = entries[i];
          const { target, intersectionRatio } = entry;

          // 获取元素的可见性状态
          const visible = domList.get(target);
          if (isNil(visible)) continue;

          // 获取元素的阈值
          const threshold = thresholdMap.get(target)!;
          if (!isNumber(threshold)) continue;

          // 根据阈值更新元素的可见性状态
          visible.value = intersectionRatio >= threshold;
        }
      },
      {
        root,
        threshold: [...defaultThreshold],
        ...options
      }
    );
  }

  /**
   * 监听元素的可见性变化
   * @param element 要监听的元素
   * @param threshold 元素可见百分比的阈值
   * @returns 元素的可见性状态引用
   */
  function watchElementVisibility(
    element: Element,
    threshold: (typeof defaultThreshold)[number]
  ): Ref<boolean> {
    initialObserver();

    // 将元素及其可见性状态添加到映射中
    domList.set(element, ref(false));
    // 将元素及其阈值添加到映射中
    thresholdMap.set(element, threshold);

    // 观察元素
    observer.observe(element);

    return domList.get(element)!;
  }

  /**
   * 停止监听元素的可见性变化
   * @param element 要停止监听的元素
   */
  function unwatchElementVisibility(element: Element) {
    initialObserver();

    // 从映射中删除元素
    domList.delete(element);
    thresholdMap.delete(element);

    // 停止观察元素
    observer.unobserve(element);
  }

  /**
   * 停止监听所有元素的可见性变化
   */
  function unwatchAll() {
    const list = Array.from(domList.keys());
    for (let i = list.length; i--; ) {
      const element = list[i];
      unwatchElementVisibility(element);
    }
  }

  /**
   * 销毁观察者
   */
  function destroy() {
    observer.disconnect();
    observer = undefined as any;
  }

  return {
    watch: watchElementVisibility,
    unwatch: unwatchElementVisibility,
    unwatchAll,
    destroy
  };
}
