import { expect, describe, test } from "vitest";
import { isLessThan, isEqual } from "../date-rules";
import dayjs from "dayjs";

describe("isLessThan函数", () => {
  test("当第一个日期小于第二个日期时，应该返回true", () => {
    const date1 = dayjs("2021-01-01");
    const date2 = dayjs("2021-01-02");
    const result = isLessThan(date1, date2);
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当第一个日期大于或等于第二个日期时，应该返回false", () => {
    const date1 = dayjs("2021-01-02");
    const date2 = dayjs("2021-01-01");
    const result = isLessThan(date1, date2);
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});

describe("isEqual函数", () => {
  test("当第一个日期等于第二个日期时，应该返回true", () => {
    const date1 = dayjs("2021-01-01");
    const date2 = dayjs("2021-01-01");
    const result = isEqual(date1, date2);
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当第一个日期不等于第二个日期时，应该返回false", () => {
    const date1 = dayjs("2021-01-01");
    const date2 = dayjs("2021-01-02");
    const result = isEqual(date1, date2);
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});
