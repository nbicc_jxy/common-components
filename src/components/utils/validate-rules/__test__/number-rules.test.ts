import { expect, describe, test } from "vitest";
import { isLessThan, isEqual } from "../number-rules";

describe("isLessThan函数", () => {
  test("当第一个数字小于第二个数字时，应该返回true", () => {
    const number1 = 1;
    const number2 = 2;
    const result = isLessThan(number1, number2);
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当第一个数字大于或等于第二个数字时，应该返回false", () => {
    const number1 = 2;
    const number2 = 1;
    const result = isLessThan(number1, number2);
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});

describe("isEqual函数", () => {
  test("当第一个数字等于第二个数字时，应该返回true", () => {
    const number1 = 1;
    const number2 = 1;
    const result = isEqual(number1, number2);
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当第一个数字不等于第二个数字时，应该返回false", () => {
    const number1 = 1;
    const number2 = 2;
    const result = isEqual(number1, number2);
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});
