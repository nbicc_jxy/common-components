import { expect, describe, test } from "vitest";
import { startWith, endWith, byteSize, length, enValidator, characterWith } from "../string-rules";

describe("startWith函数", () => {
  test("当字符串以指定字符开头时，应该返回true", () => {
    const validator = startWith(enValidator);
    const result = validator("abc");
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当字符串不以指定字符开头时，应该返回false", () => {
    const validator = startWith(characterWith("a"));
    const result = validator("bcd");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});

describe("endWith函数", () => {
  test("当字符串以指定字符结尾时，应该返回true", () => {
    const validator = endWith(enValidator);
    const result = validator("abc");
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当字符串不以指定字符结尾时，应该返回false", () => {
    const validator = endWith(characterWith("b"));
    const result = validator("abd");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});

describe("byteSize函数", () => {
  test("当字符串大小在指定范围内时，应该返回true", () => {
    const validator = byteSize(1, 3);
    const result = validator("abc");
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当字符串大小小于指定范围时，应该返回false", () => {
    const validator = byteSize(2, 3);
    const result = validator("a");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });

  test("当字符串大小大于指定范围时，应该返回false", () => {
    const validator = byteSize(1, 2);
    const result = validator("abc");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});

describe("length函数", () => {
  test("当字符串长度在指定范围内时，应该返回true", () => {
    const validator = length(1, 3);
    const result = validator("abc");
    expect(result.allMatch).toBe(true);
    expect(result.leastOneMatch).toBe(true);
  });

  test("当字符串长度小于指定范围时，应该返回false", () => {
    const validator = length(2, 3);
    const result = validator("a");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });

  test("当字符串长度大于指定范围时，应该返回false", () => {
    const validator = length(1, 2);
    const result = validator("abc");
    expect(result.allMatch).toBe(false);
    expect(result.leastOneMatch).toBe(false);
  });
});
