import dayjs, { isDayjs, type ConfigType, Dayjs } from "dayjs";
import type { ValidateResult, Validator } from "./types";
import { ValidatorType } from "./types";

/**
 * 判断第一个日期是否小于第二个日期
 * @param date
 * @param targetDate
 */
export const isLessThan: Validator = (
  date: string | Dayjs,
  targetDate: string | Dayjs
): ValidateResult => {
  date = toDayjs(date);
  targetDate = toDayjs(targetDate);

  const isBefore = date.isBefore(targetDate);
  return {
    allMatch: isBefore,
    leastOneMatch: isBefore
  };
};
isLessThan.type = ValidatorType.NORMAL;

/**
 * 判断第一个日期是否等于第二个日期
 * @param date
 * @param targetDate
 */
export const isEqual: Validator = (
  date: string | Dayjs,
  targetDate: string | Dayjs
): ValidateResult => {
  date = toDayjs(date);
  targetDate = toDayjs(targetDate);

  const isEqual = date.isSame(targetDate);
  return {
    allMatch: isEqual,
    leastOneMatch: isEqual
  };
};
isEqual.type = ValidatorType.NORMAL;

function toDayjs(date: ConfigType) {
  return isDayjs(date) ? date : dayjs(date);
}
