import { isNumber } from "lodash-es";
import type { ValidateResult, Validator } from "./types";
import { ValidatorType } from "./types";

export const isLessThan: Validator = (number: number, anotherNumber: number): ValidateResult => {
  number = toNumber(number);
  anotherNumber = toNumber(anotherNumber);

  const lessThan = number < anotherNumber;
  return {
    allMatch: lessThan,
    leastOneMatch: lessThan
  };
};
isLessThan.type = ValidatorType.NORMAL;

export const isEqual: Validator = (number: number, anotherNumber: number): ValidateResult => {
  number = toNumber(number);
  anotherNumber = toNumber(anotherNumber);

  const equal = number === anotherNumber;
  return {
    allMatch: equal,
    leastOneMatch: equal
  };
};
isEqual.type = ValidatorType.NORMAL;

function toNumber(number: number): number {
  if (isNumber(number)) return number;

  return parseFloat(number);
}
