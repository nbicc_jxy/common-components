import { isString } from "lodash-es";
import { type ValidateResult, type Validator, ValidatorType } from "./types";
import { validate } from "./utils";

/**
 * 创建一个校验器, 该校验器会对字符串中的第一个字符应用传入的所有校验器.
 */
export function startWith(...validators: Array<Validator>): Validator {
  const func: Validator = (value: string) =>
    isString(value) && value.length > 0
      ? validate(value[0], ...validators)
      : { allMatch: false, leastOneMatch: false };
  func.type = ValidatorType.NORMAL;

  return func;
}

/**
 * 创建一个校验器, 该校验器会对字符串中的最后一个字符应用传入的所有校验器.
 */
export function endWith(...validators: Array<Validator>): Validator {
  const func: Validator = (value: string) =>
    isString(value) && value.length > 0
      ? validate(value[value.length - 1], ...validators)
      : { allMatch: false, leastOneMatch: false };
  func.type = ValidatorType.NORMAL;

  return func;
}

/**
 * 创建一个校验器, 该校验器将根据传入的 {@link min} 和 {@link max} 对字符串的大小进行校验.
 * 当 `min === max` 时, 该校验器会校验字符串的长度是否等于 `min`, 否则会校验字符串的长度是否在 [min, max] 之间.
 *
 * **注: 该校验器会将字符串转换为 Blob 对象, 并根据 Blob 对象的 size 属性进行校验. 校验字符串长度请使用 {@link length}.**
 * @param min 最小长度
 * @param max 最大长度 (默认为 {@link min})
 */
export function byteSize(min = 0, max = min): Validator {
  const func: Validator = (value: string) => {
    const size = isString(value) ? new Blob([value]).size : 0;
    const match = size >= min && size <= max;

    return {
      allMatch: match,
      leastOneMatch: match
    };
  };
  func.type = ValidatorType.NORMAL;

  return func;
}

/**
 * 创建一个校验器, 该校验器将根据传入的 {@link min} 和 {@link max} 对字符串的长度进行校验.
 * 当 `min === max` 时, 该校验器会校验字符串的长度是否等于 `min`, 否则会校验字符串的长度是否在 [min, max] 之间.
 * @param min 最小长度
 * @param max 最大长度 (默认为 {@link min})
 */
export function length(min = 0, max = min): Validator {
  const func: Validator = (value: string) => {
    const len = isString(value) ? value.length : 0;
    const match = len >= min && len <= max;

    return {
      allMatch: match,
      leastOneMatch: match
    };
  };
  func.type = ValidatorType.NORMAL;

  return func;
}

const zhCNCodePointRange: Array<[number, number]> = [[0x4e00, 0x9fa5]];
/**
 * 中文字符校验器
 */
export const zhCNValidator: Validator = function (value: string): ValidateResult {
  return validateCodePointRange(value, zhCNCodePointRange);
};
zhCNValidator.type = ValidatorType.SINGLE;

const enCodePointRange: Array<[number, number]> = [
  [0x41, 0x5a],
  [0x61, 0x7a]
];
/**
 * 英文字符校验器
 */
export const enValidator: Validator = function (value: string): ValidateResult {
  return validateCodePointRange(value, enCodePointRange);
};
enValidator.type = ValidatorType.SINGLE;

const numberCodePointRange: Array<[number, number]> = [[0x30, 0x39]];
/**
 * 数字字符校验器
 */
export const numberValidator: Validator = function (value: string): ValidateResult {
  return validateCodePointRange(value, numberCodePointRange);
};
numberValidator.type = ValidatorType.SINGLE;

/**
 * 创建一个校验器, 该校验器会校验字符串是否在给定的字符列表中.
 * @param characterList 字符列表
 */
export function characterWith(characterList: string): Validator {
  const ranges: Array<[number, number]> = characterList.split("").map((char) => {
    const code = char.codePointAt(0) as number;
    return [code, code];
  });

  const func = (value: string) => validateCodePointRange(value, ranges);
  func.type = ValidatorType.SINGLE;
  return func;
}

/**
 * 根据字符码点范围校验字符串
 */
function validateCodePointRange(value: string, range: Array<[number, number]>): ValidateResult {
  let isExact = true;
  let match = false;
  for (let i = value.length; i--; ) {
    const chat = value.codePointAt(i);

    if (chat === undefined) continue;

    for (let j = range.length; j--; ) {
      const [start, end] = range[j];
      if (chat < start || chat > end) {
        isExact = false;
      } else {
        match = true;
      }
    }
  }

  return {
    allMatch: isExact,
    leastOneMatch: match
  };
}
