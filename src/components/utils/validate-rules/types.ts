/**
 * 校验结果
 */
export interface ValidateResult {
  /**
   * 校验器全部匹配时为 true 否则为 false
   */
  allMatch: boolean;
  /**
   * 存在至少一个匹配时为 true 否则为 false
   */
  leastOneMatch: boolean;
}

/**
 * 校验器类型
 */
export enum ValidatorType {
  /**
   * 校验器的默认类型. 该类型的校验器会对整个字符串进行校验
   */
  NORMAL,
  /**
   * 该类型的校验器会对字符串中的每个字符进行校验
   */
  SINGLE
}

/**
 * 校验器
 */
export interface Validator {
  (...args: Array<any>): ValidateResult;
  type: ValidatorType;
}
