import { isArray, isFunction, isString } from "lodash-es";
import type { ValidateResult, Validator } from "./types";
import { ValidatorType } from "./types";

/**
 * 使用传入的校验器校验字符串. 返回值说明见 {@link ValidateResult}.
 * @param value 字符串
 * @param validators 校验器
 * @returns 校验结果
 */
export function validate(value: any, ...validators: Array<Validator | undefined>): ValidateResult {
  const _value = isArray(value) ? value : [value];

  const validatorGroup: Record<ValidatorType, Array<Validator>> = {
    [ValidatorType.NORMAL]: [],
    [ValidatorType.SINGLE]: []
  };

  for (let i = validators.length; i--; ) {
    const validator = validators[i];

    if (!isFunction(validator)) continue;

    switch (validator.type) {
      case ValidatorType.SINGLE:
        validatorGroup[ValidatorType.SINGLE].push(validator);
        break;
      case ValidatorType.NORMAL:
      default:
        validatorGroup[ValidatorType.NORMAL].push(validator);
        break;
    }
  }

  let isExact = true;
  let match = false;

  for (let i = validatorGroup[ValidatorType.NORMAL].length; i--; ) {
    const hasMatch = validatorGroup[ValidatorType.NORMAL][i](..._value).leastOneMatch;

    match = match || hasMatch;
    isExact = isExact && hasMatch;
  }

  if (isString(_value[0])) {
    const stringValue = _value[0] as string;
    for (let i = stringValue.length; i--; ) {
      let hasMatch = false;

      for (let j = validatorGroup[ValidatorType.SINGLE].length; j--; ) {
        if (validatorGroup[ValidatorType.SINGLE][j](stringValue[i]).leastOneMatch) {
          hasMatch = true;
          break;
        }
      }

      match = match || hasMatch;
      isExact = isExact && hasMatch;
    }
  }

  return {
    allMatch: isExact,
    leastOneMatch: match
  };
}
