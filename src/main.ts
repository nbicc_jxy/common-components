// 组件
export * from "./components/table-helper";
export * from "./components/n-table";
export * from "./components/n-select";
export * from "./components/n-button";
export * from "./components/form-helper";
export * from "./components/n-form";
export * from "./components/n-toolbar";
export * from "./components/n-page-layout";
export * from "./components/n-spec-provider";
export * from "./components/n-modal";
export * from "./components/n-drawer";
export * from "./components/n-upload";
export * from "./components/n-input-number";
export * from "./components/n-print";
export * from "./components/n-space";

// 存储
export * from "./storage";

// hooks
export * from "./components/utils/hooks";

import { install } from "./components";

// 样式
import "./styles.scss";

export default {
  install
};
