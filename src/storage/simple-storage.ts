import { isBoolean, isNil, isNumber, isObject, isPlainObject, isString } from "lodash-es";

type StorageDataValueType = string | number | boolean;
type StorageDataType = Record<string, StorageDataValueType>;
export class SimpleStorage {
  constructor(
    public name: string,
    protected cache: Record<string, StorageDataType | StorageDataValueType>,
    protected parent?: SimpleStorage
  ) {}

  get displayName(): string {
    return this.parent ? `${this.parent.displayName}:${this.name}` : this.name;
  }

  /**
   * 派生一个新的 {@link SimpleStorage} 实例. 如果派生的名称为 {@link IGNORE}, 则返回一个空的且不会被 {@link SimpleStorageManager} 序列化的 {@link SimpleStorage} 实例.
   *
   * **当派生的名称已经存在, 但不是一个对象时, 会将其重置为一个空对象.**
   * @param name
   */
  derive(name: string) {
    if (name === IGNORE) return new SimpleStorage(name, {} as StorageDataType, this);

    if (isNil(this.cache[name])) {
      this.cache[name] = {};
    } else if (!isPlainObject(this.cache[name])) {
      this.cache[name] = {};
      console.warn(
        `SimpleStorage.derive: ${this.name}:${name} 不是一个对象, 已经被重置为一个空对象.`
      );
    }

    return new SimpleStorage(name, this.cache[name] as StorageDataType, this);
  }

  delete(key: string): void {
    delete this.cache[key];
  }

  get<T extends StorageDataValueType>(key: string): T | undefined {
    const value = this.cache[key];

    if (isObject(value)) return undefined;

    return value as T;
  }

  set<T extends StorageDataValueType>(key: string, value: T): void {
    if (!isString(value) && !isNumber(value) && !isBoolean(value))
      throw new Error("Storage.set: value must be a string or number.");

    this.cache[key] = value;
  }

  toString(): string {
    return JSON.stringify(this.cache);
  }
}

/**
 * 管理 {@link SimpleStorage} 的类. 用于将 {@link SimpleStorage} 的数据进行序列化和反序列化.
 */
export class SimpleStorageManager {
  constructor(protected rootStorage: SimpleStorage) {}

  /**
   * 恢复从 {@link toString} 拿到的状态数据.
   * @param data
   */
  recovery(data: string) {
    let storageData: Record<string, StorageDataType | StorageDataValueType> = {};
    let version = "";

    try {
      const temp = JSON.parse(data);

      if (isPlainObject(temp.storageData)) storageData = temp.storageData;
      if (isString(temp.version)) version = temp.version;
    } catch (e) {
      console.warn("SimpleStorageManager.recovery: ", e);
    }

    // 如果版本不一致, 则清空缓存.
    if (version !== VERSION) {
      console.warn("SimpleStorageManager.recovery: 版本不一致, 清空缓存.");
      storageData = {};
    }

    if (!isPlainObject(storageData)) {
      console.warn("SimpleStorageManager.recovery: data 不是一个对象.");
      storageData = {};
    }

    this._recoveryDeeper(storageData, this.rootStorage);
  }

  /**
   * 将当前的状态数据序列化为字符串. 之后可以通过 {@link recovery} 方法恢复状态.
   */
  toString(): string {
    return JSON.stringify({
      storageData: JSON.parse(this.rootStorage.toString()),
      version: VERSION
    });
  }

  protected _recoveryDeeper(
    recoveryData: Record<string, StorageDataType | StorageDataValueType>,
    storage: SimpleStorage
  ) {
    const keys = Object.keys(recoveryData);

    for (let i = keys.length; i--; ) {
      const key = keys[i];
      const value = recoveryData[key];

      if (isPlainObject(value)) {
        this._recoveryDeeper(value as StorageDataType, storage.derive(key));
      } else {
        storage.set(key, value as StorageDataValueType);
      }
    }
  }
}

/**
 * 用于 {@link SimpleStorage.derive} 方法的参数, 用于表示派生出的子缓存对象不需要存储在本地.
 * 即随着页面关闭/刷新存储的数据会消失.
 */
export const IGNORE = "IGNORE";
const VERSION = "0.0.1";
/**
 * 根部的缓存对象, 用于存储所有的缓存数据. 可以派生出子缓存对象.
 */
export const cacheStorage = new SimpleStorage("", {});
export const StorageManager = new SimpleStorageManager(cacheStorage);
