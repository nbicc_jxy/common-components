import { isString } from "lodash-es";
import type { ExtractPropTypes } from "vue";

/**
 * 提供给支持本地存储的组件使用的 props.
 */
export const storableProps = () => ({
  /**
   * 表格的唯一标识, 作为本地存储的 key.
   */
  storageKey: {
    type: String
  },
  /**
   * 是否禁用本地存储
   */
  disabledStorable: {
    type: Boolean
  }
});
export type StorableProps = Partial<ExtractPropTypes<ReturnType<typeof storableProps>>>;

/**
 * 用于判断是否开启本地存储
 * @param props
 */
export function isStorable(props: StorableProps): boolean {
  const { storageKey, disabledStorable } = props;

  return !disabledStorable && isString(storageKey) && storageKey.trim().length > 0;
}
