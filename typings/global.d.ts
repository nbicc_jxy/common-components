declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    NButton: typeof import("@nbicc/common-components")["NButton"];

    NLoadingButton: typeof import("@nbicc/common-components")["NLoadingButton"];

    NForm: typeof import("@nbicc/common-components")["NForm"];

    NEasyForm: typeof import("@nbicc/common-components")["NEasyForm"];

    NFormItem: typeof import("@nbicc/common-components")["NFormItem"];

    NPageLayout: typeof import("@nbicc/common-components")["NPageLayout"];

    NSelect: typeof import("@nbicc/common-components")["NSelect"];

    NAutoLoadSelect: typeof import("@nbicc/common-components")["NAutoLoadSelect"];

    NSpecProvider: typeof import("@nbicc/common-components")["NSpecProvider"];

    NModalWrapper: typeof import("@nbicc/common-components")["NModalWrapper"];

    NDrawerWrapper: typeof import("@nbicc/common-components")["NDrawerWrapper"];

    NTable: typeof import("@nbicc/common-components")["NTable"];

    NEasyTable: typeof import("@nbicc/common-components")["NEasyTable"];

    NGanttTable: typeof import("@nbicc/common-components")["NGanttTable"];

    NGanttTableBar: typeof import("@nbicc/common-components")["NGanttTableBar"];

    NToolBar: typeof import("@nbicc/common-components")["NToolbar"];
    // 意外的兼容性问题. NToolbar 错误导出成了 NToolBar.
    NToolbar: typeof import("@nbicc/common-components")["NToolbar"];

    NInputNumber: typeof import("@nbicc/common-components")["NInputNumber"];

    NUpload: typeof import("@nbicc/common-components")["NUpload"];

    NUploadButton: typeof import("@nbicc/common-components")["NUploadButton"];

    NUploadPictureCard: typeof import("@nbicc/common-components")["NUploadPictureCard"];

    NPrint: typeof import("@nbicc/common-components")["NPrint"];

    NPrintLodop: typeof import("@nbicc/common-components")["NPrintLodop"];

    NPrintBase: typeof import("@nbicc/common-components")["NPrintBase"];

    NPrintPage: typeof import("@nbicc/common-components")["NPrintPage"];
  }
}

export {};
