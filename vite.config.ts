// import { fileURLToPath, URL } from "node:url";
import { resolve } from "path";

import { defineConfig, type PluginOption } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import { visualizer } from "rollup-plugin-visualizer";
import terser from "@rollup/plugin-terser";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx(), terser() as PluginOption, visualizer() as PluginOption],
  resolve: {
    alias: {
      // "@": fileURLToPath(new URL("./src", import.meta.url))
    }
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  },
  build: {
    target: "es2015",
    lib: {
      entry: resolve(__dirname, "src/main.ts"),
      fileName: "index",
      formats: ["es", "cjs"]
    },
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: [
        "vue",
        "ant-design-vue",
        "@ant-design/icons-vue",
        "ant-design-vue/lib/table",
        "ant-design-vue/lib/typography",
        "lodash-es",
        "dayjs"
      ]
    }
  }
});
